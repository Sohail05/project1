# Antelope

A framework used to bootstrap and create functional applications.
It provides a single page application with the UI components and re-usable utilities.

Focused on producing the highest level of quality with rich embedded features such as:
- Gesture support
- Responsive UI
- Mobile, tablet and desktop support.
- i18n
- Accessibility

## Environment Setup

### Requirements

- nodejs / npm
- Visual Studio Code

### CLI Tools

- Angular cli

Installing cli tools globally
```
npm run install:cli
```

Installing dependencies

```
npm install
```

### Visual Studio Code Extensions

Use ctrl + p to dropdown the command runner
- `tslint`: ext install tslint
- `ChromeDebug`: ext install debugger-for-chrome
- `TypeScript Importer`: ext install tsimporter

### NPM Commands 

```
    npm run build
    npm run lint
    npm run test
    npm run e2e
    npm run install:cli
    npm run clean:cli
    npm run clean:modules
    npm run clean
```

### File Structure

`src/app`: App source code for demo display and E2E testing. Not exported in package.
`src/lib`: Location where components and features reside. Where all the components are exported.
`main.ts`: TypeScript entry point 
`app.component.ts`: Entry point for demo display app.

> note: `index.html` and npm dependencies are not refreshed during ng serve.  
Retarting the process is required.