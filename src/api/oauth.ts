import { OAuthApi } from '../lib/core/antelope-interfaces';

export const OAuth: OAuthApi = {
    authorize: '/oauth/authorize?client_id={client_id}&response_type={response_type}&redirect_uri={redirect_uri}',
    /** Post */
    token: '/oauth/token?refresh_token={refresh_token}&grant_type={grant_type}&client_id={client_id}',
    /** Post */
    logout: '/logout?token={access_token}',
    /** GET & Post */
    lastUserAccess: '/api/public/usrLastAccess?token={access_token}'
}
