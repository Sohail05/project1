import { AntelopeApi, WorkflowApi, SystemApi } from '../lib/core/antelope-interfaces';
const workflow: WorkflowApi = {
    accessField: '/private/task/acs/fld/{wflSecRole}',
    accessRecord: '/private/task/acs/rec/{wflSecRole}',
    access: '/private/task/acs/{wflSecRole}',
    cancel: '/private/task/canWfl',
    pickup: '/private/task/pickup/{pickedTaskId}',
    rework: '/private/task/rework/{rwkToTaskId}',
    returnTask: '/private/task/rtuPool',
    reworkTasks: '/private/task/rwkTasks',
    view: '/private/task/vw',
    actionList: '/private/task/wflActions',
    proceed: '/private/task/proceed',
}

export const system: SystemApi = {
    appProperties: '/private/sys/appProps',
    log: '/private/sys/log/'
}

export const API: AntelopeApi = {
    workflow,
    langs: '/public/sys/langs/',
    locales: '/private/sys/locales/?aplId={aplId}&lang={lang}',
    localeCode: '/private/sys/locales/code?aplId={aplId}&lang={lang}&code={code}',
    codeType: '/private/code/?codeT={code}',
    accessMenu: '/private/sec/acs/mn',
    accessField: '/private/sec/acs/fld',
    accessRecord: '/private/sec/acs/rec',
    userInfo: '/private/sec/usrs/{id}',
    system
}
