import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-test-page-second-tab',
  templateUrl: '../form-tab.component.html',
  styleUrls: ['../form-tab.component.scss']
})
export class SecondTabComponent implements OnInit {

  form;
  val = ['EML'];
  textContent = '';

  // disable forms and buttons
  disabled = true;
  submitted = false;
  fnOpen = (x) => x.dsc;
  fnClose = (x) => x.code;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      test: '',
      inputId: this.fb.group({ idType: 'TIN', idValue: '12121223232' }),
      'text-test': new FormControl('', Validators.required),
      // id component field ID Type
      // id component field ID No.
      'number-integer-test': new FormControl(''),
      'date-picker-test': [{ value: '', disabled: true }],
      'multi-currency-test': new FormGroup({
        cur: new FormControl(''),
        amount: new FormControl(''),
        date: new FormControl(''),
        rate: new FormControl('')
      }),
      'number-percent-test': new FormControl(''),
      'autocomplete-test': new FormControl(''),
      'multi-complete-test': new FormControl(''),
      'dropdown-test': new FormControl(''),
      'time-picker-test': new FormControl(''),
      'password-test': new FormControl(''),
      'checkbox-test': new FormControl(''),
      'radio-button-test': new FormControl(''),
      'phone-number-test': new FormGroup({
        ctyCode: new FormControl('+60'),
        no: new FormControl('213213'),
      }),
      'email-test': new FormControl(''),
      'address-test': new FormGroup({
        addr1: new FormControl(''),
        addr2: new FormControl(''),
        addr3: new FormControl(''),
        cty: new FormControl(''),
        ste: new FormControl(''),
        postcode: new FormControl(''),
        city: new FormControl(''),
      }),
      'textarea-test': new FormControl(''),
    });
    this.form.disable();
  }

  validation(mandatory) { }
  submit() { }

}


