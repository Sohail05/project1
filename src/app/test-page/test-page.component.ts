import { Component, OnInit, HostListener } from '@angular/core';
import { DirtyCheckService } from '../../lib/core/dirty-check.service';
import { DirtyCheck } from '../../lib/dirty-check/index';

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.scss']
})
export class TestPageComponent extends DirtyCheck implements OnInit {
  indicator; // used to set the form indicator

  constructor(private dirtyCheckService: DirtyCheckService) {
    super(dirtyCheckService);
  }
  ngOnInit() {
  }

}
