import { NgModule } from '@angular/core';

import { TestPageComponent } from './test-page.component';
import { FirstTabComponent } from './first-tab/first-tab.component';
import { SecondTabComponent } from './second-tab/second-tab.component';
import { ThirdTabComponent } from './third-tab/third-tab.component';
import { MaterialModule } from '@angular/material';
import { AntelopeModule } from '../../lib/index';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AntelopeModule,
        FlexLayoutModule
        ],
    exports: [TestPageComponent, FirstTabComponent, SecondTabComponent, ThirdTabComponent],
    declarations: [TestPageComponent, FirstTabComponent, SecondTabComponent, ThirdTabComponent],
    providers: [],
})
export class TestPageModule { }
