import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-test-page-third-tab',
  templateUrl: './third-tab.component.html',
  styleUrls: ['../form-tab.component.scss']
})
export class ThirdTabComponent implements OnInit {

  accessControl = {
    edit: true,
    delete: true
  }
  searchGroup: FormGroup;

  get stringData() {
    return JSON.stringify(this.data, null, 2);
  }

  set stringData(v) {
    try {
      this._data = JSON.parse(v);
    } catch (e) {
      console.log('error occored while you were typing the JSON');
    };
  }
  _data;
  data = [
    {
      col1: 'cmo.ttl.srh.rsl',
      col2: 'cmo.ttl.sp.editor',
      col3: 'com.lbl.rsn.code',
      col4: 'cmo.ttl.quick.info',
      col5: 'cmo.ttl.qui.lnk',
      col6: 'cmo.ttl.old.val'
    }
  ];

  setting = [
    { key: 'col1', label: '123', styles: { width: '100px' }, classes: { 'text-ellipsis': true } },
    { key: 'col2', label: '123', styles: { width: '100px' }, classes: { 'text-ellipsis': true },
      type: 'multiline', fn: (data) => ['data', 'Sample data']
    },
    { key: 'col3', label: '123' },
    { key: 'col4', label: '123' },
    { key: 'col5', label: '123' },
    { key: 'col6', label: '123' },
  ]

  totalData = ['', 'total', '123'];
  // setting = Object.keys(this.data[0]).map((x) => {
  //   return { key: x, label: this.data[0][x] };
  // });
  form;
    constructor(private fb: FormBuilder) {
      this.searchGroup = this.fb.group({ test: 'test' });
    }

  onSubmit(val) {
      console.log('Submit filter form', val);
    }
  filterByCheckbox(val) {
      console.log('Filter by checkboxes', val);
    }
  onFilterToggleBtnClick(val: boolean) {
      console.log('filter toggle btn pressed', val);
    }

  onSearchFormReset() { }

  ngOnInit() {
      this.form = this.fb.group({
        test: ''
      });

      this.stringData = JSON.stringify(this.data);
    }
  refreshTable() {
      this.data = this._data;
      this.setting = Object.keys(this.data[0]).map((x) => {
        return { key: x, label: x };
      });
    }

}
