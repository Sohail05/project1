import { Component, OnInit, ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { ValidationService } from '../../../lib/core/validation.service';
import { MdSnackBar } from '@angular/material';
import { FormService } from '../../../lib/core/form.service';
import { DirtyCheckService } from '../../../lib/core/dirty-check.service';
import { SidenavService } from '../../../lib/sidenav-bar/sidenav/sidenav.service';

@Component({
  selector: 'app-test-page-first-tab',
  templateUrl: '../form-tab.component.html',
  styleUrls: ['../form-tab.component.scss'],
  providers: [FormService]
})
export class FirstTabComponent implements OnInit {
  form: FormGroup;
  val = ['EML'];
  textContent = '';
  // disable forms and buttons
  disabled = false;
  submitted = false;
  constructor(
    private fb: FormBuilder,
    private validationService: ValidationService,
    private snackBar: MdSnackBar,
    private formService: FormService,
    private dirtyCheckService: DirtyCheckService,
    private sidenavService: SidenavService
  ) { }

  ngOnInit() {

    // this.validationService.createKeyValidation('email-test', { email: true })

    this.form = this.fb.group({
      'text-test': new FormControl('', Validators.required),
      'inputId': new FormGroup({
        idType: new FormControl(''),
        idValue: new FormControl('')
      }),
      'number-integer-test': new FormControl('11111'),
      'date-picker-test': new FormControl(''),
      'multi-currency-test': new FormGroup({
        cur: new FormControl(''),
        amount: new FormControl(''),
        date: new FormControl(''),
        rate: new FormControl('')
      }),
      'number-percent-test': new FormControl('1234567'),
      'autocomplete-test': new FormControl(''),
      'multi-complete-test': new FormControl([]),
      'dropdown-test': new FormControl(''),
      'time-picker-test': new FormControl(''),
      'password-test': new FormControl(''),
      'checkbox-test': [['EML', 'FAX']],
      'radio-button-test': new FormControl('N'),
      'phone-number-test': new FormGroup({
        ctyCode: new FormControl('+60'),
        no: new FormControl('12121212121'),
      }),
      'email-test': new FormControl(''),
      'address-test': new FormGroup({
        addr1: new FormControl(''),
        addr2: new FormControl(''),
        addr3: new FormControl(''),
        cty: new FormControl(''),
        ste: new FormControl(''),
        postcode: new FormControl(''),
        city: new FormControl(''),
      }),
      'textarea-test': new FormControl(''),
    });
  }
  haha() {
    this.sidenavService.toggleAppMenu('home');
  }
  submit() {
    this.snackBar.open('Success', 'Submittion');
    this.dirtyCheckService.clearAll();
  }

  validation(mandatory) {
    if (mandatory) {
      this.formService.triggerValidation(true);
      this.submitted = true;
    }

    if (this.form.valid) {
      this.snackBar.open('Validation Successful', 'Submission');
    } else {
      this.snackBar.open('Invalid', 'Submission');
    }

  }

}

