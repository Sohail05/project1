import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-input-example',
  templateUrl: './date-input-example.component.html'
})
export class DateInputExampleComponent implements OnInit {
  dtdataModel = '2017-04-19T09:01:02';
  ddataModel = '2017-04-18';
  tdataModel = '09:03:04';
  constructor() { }

  ngOnInit() {
  }

}
