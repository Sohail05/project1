# How To Build a Form (manually)

This is a guide on how to build a static form by manually inserting custom input components into the form.

## Usage
### TS

```typescript
// parent-form.component.ts

form: FormGroup;
constructor(private fb: FormBuilder) { }

ngOnInit() {
    this.form = this.fb.group({
      modelName: 'Hello World'
    });
  }
```
### HTML
```html
  // parent-form.component.html

  <form [formGroup]="form">
    <nt-input 
        [parent]="form" 
        key="modelName"
        [validation]="{ required: true, email: true }" 
        [placeholder]="Hello World!">
    </nt-input>
    .
    .
    .
    <nt-button-submit (submit)="onSubmit()" [parent]="form"></nt-button-submit>
  </form>
```

The `<form [formGroup]="form"></form>` acts as a _container component_ - a parent, a container - for all of its child components, which are the so called _presentational components_. 

Data flows _uni-directionally_ via `@Input` from the container component to its presentional child components, which would `@Output` events and data to be handled in the container component's methods.

Validation is done locally in the components.

The parent's `FormGroup` is passed down to the `<nt-input>` as a binding to its `parent` input in order to register and communicate the input with the parent form:

```typescript
// nt-input.component.ts

@Input() parent: FormGroup;
@Input() key: string;
```
```html
// nt-input.component.html

<div [formGroup]="parent">
  ...
  <input [formControlName]="key">
  ...
</div>
```
