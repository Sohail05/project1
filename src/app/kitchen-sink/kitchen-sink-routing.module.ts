import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MultiLinguaExampleComponent } from './multi-lingua-example/multi-lingua-example.component';
import { KitchenSinkComponent } from './kitchen-sink.component';
import { entries } from './routes';
import { HomeComponent } from './home/home.component';

export const routes = [
  {
    path: '', component: KitchenSinkComponent, children: entries.concat([
      { path: '', component: HomeComponent }
    ])
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KitchenSinkRoutingModule { }
