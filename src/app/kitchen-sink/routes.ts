import { AddressExampleComponent } from './address-example/address-example.component';
import { BackExampleComponent } from './back-example/back-example.component';
import { CustomFormExampleComponent } from './custom-form-example/custom-form-example.component';
import { BreadcrumbExampleComponent } from './breadcrumb-example/breadcrumb-example.component';
import { AreaChartExampleComponent } from './area-chart-example/area-chart-example.component';
import { OrgChartExampleComponent } from './org-chart-example/org-chart-example.component';
import { CodeTypeExampleComponent } from './code-type-example/code-type-example.component';
import { CollapseExampleComponent } from './collapse-example/collapse-example.component';
import { AutoCompleteExampleComponent } from './auto-complete-example/auto-complete-example.component';
import { DateInputExampleComponent } from './date-input-example/date-input-example.component';
import { CusInputExampleComponent } from './cus-input-example/cus-input-example.component';
import { SelectExampleComponent } from './select-example/select-example.component';
import { CusSnackbarExampleComponent } from './cus-snackbar-example/cus-snackbar-example.component';
import { DatepickerExampleComponent } from './datepicker-example/datepicker-example.component';
import { IconsExampleComponent } from './icons-example/icons-example.component';
import { IdExampleComponent } from './id-example/id-example.component';
import { KitchenSinkComponent } from './kitchen-sink.component';
import { MultiAutoExampleComponent } from './multi-auto-example/multi-auto-example.component';
import { ProgressBarExampleComponent } from './progress-bar-example/progress-bar-example.component';
import { SearchBoxExampleComponent } from './search-box-example/search-box-example.component';
import { SidenavFormExampleComponent } from './sidenav-form-example/sidenav-form-example.component';
import { TableExampleComponent } from './table-example/table-example.component';
import { VersionExampleComponent } from './version-example/version-example.component';
import { MultiLinguaExampleComponent } from './multi-lingua-example/multi-lingua-example.component';
import { MultiCurrencyExampleComponent } from './multi-currency-example/multi-currency-example.component';
import { PhoneNoExampleComponent } from './phone-no-example/phone-no-example.component';
import { AccessControlExampleComponent } from './access-control-example/access-control-example.component';
import { CheckboxExampleComponent } from './checkbox-example/checkbox-example.component';
import { CardExampleComponent } from './card-example/card-example.component';
import { WorkflowBarExampleComponent } from './workflow-bar-example/workflow-bar-example.component';
import { RadioButtonExampleComponent } from './radio-button-example/radio-button-example.component';
import { TextareaExampleComponent } from './textarea-example/textarea-example.component';

export const getComponents = (c) => c.component;
export const sortFn = (a, b) => { return (a.path < b.path) ? -1 : (a.path > b.path) ? 1 : 0; }

export const entries = [
  {
    path: 'access-control', component: AccessControlExampleComponent,
  },
  {
    path: 'address', component: AddressExampleComponent,
  },
  {
    path: 'multi-lingua', component: MultiLinguaExampleComponent
  },
  {
    path: 'multi-currency', component: MultiCurrencyExampleComponent
  },
  {
    path: 'custom-form', component: CustomFormExampleComponent,
  },
  {
    path: 'date-picker', component: DatepickerExampleComponent,
  },
  {
    path: 'table', component: TableExampleComponent,
  },
  {
    path: 'area-chart', component: AreaChartExampleComponent,
  },
  {
    path: 'org-chart', component: OrgChartExampleComponent,
  },
  {
    path: 'cus-snackbar', component: CusSnackbarExampleComponent,
  },
  {
    path: 'icons', component: IconsExampleComponent,
  },
  {
    path: 'breadcrumb', component: BreadcrumbExampleComponent,
  },
  {
    path: 'multi-auto', component: MultiAutoExampleComponent,
  },
  {
    path: 'back-button', component: BackExampleComponent,
  },
  // component wip
  {
    path: 'id', component: IdExampleComponent,
  },
  {
    path: 'collapse', component: CollapseExampleComponent,
  },
  {
    path: 'custom-input', component: CusInputExampleComponent,
  },
  {
    path: 'progress-bar', component: ProgressBarExampleComponent,
  },
  {
    path: 'select', component: SelectExampleComponent,
  },
  {
    path: 'date-input', component: DateInputExampleComponent,
  },
  {
    path: 'search-box', component: SearchBoxExampleComponent,
  },
  {
    path: 'version', component: VersionExampleComponent,
  },
  {
    path: 'auto-complete', component: AutoCompleteExampleComponent,
  },
  {
    path: 'sidenav-form', component: SidenavFormExampleComponent,
  },
  {
    path: 'code-type', component: CodeTypeExampleComponent,
  },
  {
    path: 'phone-no', component: PhoneNoExampleComponent,
  },
  {
    path: 'checkbox', component: CheckboxExampleComponent,
  },
  {
    path: 'workflow-bar', component: WorkflowBarExampleComponent
  },
  {
    path: 'card', component: CardExampleComponent
  },
  {
    path: 'radio', component: RadioButtonExampleComponent
  },
  {
    path: 'textarea', component: TextareaExampleComponent
  }
].sort(sortFn);
