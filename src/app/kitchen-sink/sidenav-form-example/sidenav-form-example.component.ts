import { Component, OnInit } from '@angular/core';
import { SidenavFormService } from './../../../lib/sidenav-form/sidenav-form.service';

@Component({
  selector: 'nt-sidenav-form-example',
  templateUrl: './sidenav-form-example.component.html',
  styleUrls: ['./sidenav-form-example.component.scss'],
})
export class SidenavFormExampleComponent implements OnInit {
  options = [
    { code: '123', dsc: '234' },
    { code: '456', dsc: '567' },
    // '1','2'
  ];
  questions = [
    {
      component: 'InputComponent',
      data: {
        value: 'You are awesome',
        key: 'grpNm',
        placeholder: 'Group Name',
        validation: {
          required: true,
        },
      },
    },
    {
      component: 'InputComponent',
      data: {
        key: 'grpId',
        placeholder: 'Group ID',
        // masking: 'currency',
        validation: {
          required: true,
          email: true,
        },
      },
    },
    {
      component: 'SelectComponent',
      data: {
        key: 'ss1',
        placeholder: 'abc',
        options: this.options,
        required: true,
        disabled: false,
        displayFn: (v) => v ? v.dsc : '',
        storeFn: (v) => v ? v.code : '',
      },
    },
    {
      component: 'SelectComponent',
      data: {
        value: ['1'],
        key: 'ms2',
        placeholder: 'multi select',
        options: this.options,
        required: true,
        disabled: false,
        multiple: true,
        displayFn: (v) => v ? v.dsc : '',
        storeFn: (v) => v ? v.code : '',
      },
    },
    {
      component: 'AutocompleteComponent',
      data: {
        key: 'ac1',
        value: { code: '123', dsc: '234' },
        placeholder: 'single autocomplete',
        options: this.options,
        required: true,
        disabled: false,
        multiple: false,
        displayFn: (v) => v ? v.dsc : '',
        storeFn: (v) => v,
      },
    },
    {
      component: 'AutocompleteComponent',
      data: {
        key: 'ac2',
        value: [
          { key: '123', dsc: '234' },
          { key: '456', dsc: '567' },
        ],
        placeholder: 'Multi autocomplete',
        options: this.options,
        required: true,
        disabled: false,
        multiple: true,
        displayFn: (v) => v ? v.dsc : '',
        storeFn: (v) => v,
      },
    },
  ];
  questions2 = [
    {
      component: 'InputComponent',
      data: {
        value: 'You are awesome',
        key: 'grpNm',
        placeholder: 'Group Name',
        validation: {
          required: true,
        },
      },
    },
  ];

  constructor(private sidenavFormService: SidenavFormService) { }
  generateQuestions() {
    this.sidenavFormService.setQuestions(this.questions);
  }
  generateQuestions2() {
    this.sidenavFormService.setQuestions(this.questions2);
  }
  ngOnInit() {
    this.questions['type'] = 'Update';
    this.questions['title'] = 'Custom Title';
  }

}
