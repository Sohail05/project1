import { Component, OnInit, ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CodeTypeService } from '../../../lib/code-type/code-type.service';
import 'rxjs/add/operator/toPromise';
@Component({
  selector: 'app-code-type-example',
  templateUrl: './code-type-example.component.html',
})
export class CodeTypeExampleComponent implements OnInit {

  codeTypeSelection = 'CODE.CODE_T';
  codeListing = [];
  _codeTypeService: CodeTypeService;
  form: FormGroup;
  constructor(private _fb: FormBuilder, private codeTypeService: CodeTypeService) {
    this._codeTypeService = codeTypeService;
  }

  ngOnInit() {
    this.form = this._fb.group({
      codeType: ['CODE.CODE_T']
    });

    this._codeTypeService.getCodeTypes('CODE.CODE_T')
    .toPromise()
    .then( (codes) => {
      this.codeListing = codes;
    });
  }

}
