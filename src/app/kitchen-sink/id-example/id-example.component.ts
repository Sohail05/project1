import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-id-example',
  templateUrl: './id-example.component.html',
  styleUrls: ['./id-example.component.scss'],
})
export class IdExampleComponent implements OnInit {
  id = {};
  form: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      id: {}
    })
  }
}
