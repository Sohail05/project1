import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-org-chart-example',
  templateUrl: './org-chart-example.component.html',
  styleUrls: ['./org-chart-example.component.scss'],
})
export class OrgChartExampleComponent implements OnInit {
  /* -- Org Chart -- */
  nodeModel = {
    relToPntCode: { x: 100, y: 15, rectBg: true, rectBgFill: '#EEE', anchor: 'middle', textTransform: 'uppercase', fontSize: 8 },
    fullNm: { x: 100, y: 35, anchor: 'middle', textTransform: 'uppercase', fontSize: 13, fontWeight: 'bold', linesBreak: 2 },
    cifNo: { x: 100, y: 70, anchor: 'middle', textTransform: 'uppercase', fontSize: 10 },
    crmUsrId: { x: 100, y: 100, anchor: 'middle', textTransform: 'uppercase', fontSize: 10, delimiter: ['crmUsrNm'] },
    crmUsrNm: { x: 120, y: 137, anchor: 'middle', textTransform: 'uppercase', fontSize: 10 },
    shnPct: { x: 105, y: -5, anchor: 'start', fontSize: 9 },
    line: { startPoint: 'M 25 80', drawTill: 'L 175 80' },
    delimited: ['crmUsrNm'],
  }

 // todo: refactor POC code

  orgChartData = [
    {
      'id': 2,
      'fullNm': 'ABC INC.',
      'id1': {
        'idT': 'TIN',
        'idNbr': '12-345XXXX'
      },
      'aumAmt': 300000,
      'aumCcy': 'USD',
      'relSncDt': '01/20/2011',
      'ctlEnt': '',
      'type': 'counterparty',
      'grp': '',
      'crmUsrId': 'RM100',
      'crmUsrNm': 'Jon Snow',
      'updDt': '04/03/2016',
      'cifNo': 'CIF: 123124',
      'shnPct': '',
      'relToPntCode': 'Controlling Entity',
      'relType': 'CTLENT'
    },
    {
      'id': 3,
      'fullNm': 'ABC SYSTEMS',
      'id1': {
        'idT': 'TIN',
        'idNbr': '12-345XXXX'
      },
      'aumAmt': 200000,
      'aumCcy': 'USD',
      'relSncDt': '02/19/2011',
      'ctlEnt': '',
      'type': 'counterparty',
      'grp': 'ABC INC.',
      'crmUsrId': 'RM101',
      'crmUsrNm': 'Alice Johannes',
      'updDt': '09/22/2013',
      'cifNo': 'CIF: 324121',
      'shnPct': '51%',
      'relToPntCode': 'Held By',
      'relType': 'HLDBY'
    },
    {
      'id': 4,
      'fullNm': 'ABC CONSULTING',
      'id1': {
        'idT': 'TIN',
        'idNbr': '12-345XXXX'
      },
      'aumAmt': 50000,
      'aumCcy': 'USD',
      'relSncDt': '11/15/2012',
      'ctlEnt': '',
      'type': 'counterparty',
      'grp': 'ABC INC.',
      'crmUsrId': 'RM104',
      'crmUsrNm': 'James McCarthy',
      'updDt': '11/25/2013',
      'cifNo': 'CIF: 425122',
      'shnPct': '31.23%',
      'relToPntCode': 'Subsidiary Company Of',
      'relType': 'SBRCPY'
    },
    {
      'id': 5,
      'fullNm': 'ABC TECHNOLOGIES',
      'id1': {
        'idT': 'TIN',
        'idNbr': '12-345XXXX'
      },
      'aumAmt': 150000,
      'aumCcy': 'USD',
      'relSncDt': '01/31/2013',
      'ctlEnt': '',
      'type': 'counterparty',
      'grp': 'ABC INC.',
      'crmUsrId': 'RM104',
      'crmUsrNm': 'James McCarthy',
      'updDt': '10/10/2016',
      'cifNo': 'CIF: 319122',
      'shnPct': '44.1%',
      'relToPntCode': 'Subsidiary Company Of',
      'relType': 'SBRCPY'
    },
    {
      'id': 6,
      'fullNm': 'OUTSOURCE INTERNATIONAL',
      'id1': {
        'idT': 'TIN',
        'idNbr': '12-345XXXX'
      },
      'aumAmt': 120000,
      'aumCcy': 'USD',
      'relSncDt': '06/01/2015',
      'ctlEnt': '',
      'type': 'counterparty',
      'grp': 'ABC TECHNOLOGIES',
      'crmUsrId': 'RM105',
      'crmUsrNm': 'Alison Thomas',
      'updDt': '04/02/2016',
      'cifNo': 'CIF: 512909',
      'shnPct': '13%',
      'relToPntCode': 'Subsidiary Company Of',
      'relType': 'SBRCPY'
    },
    {
      'id': 7,
      'fullNm': 'WEBUILDIT SOLUTIONS',
      'id1': {
        'idT': 'TIN',
        'idNbr': '12-345XXXX'
      },
      'aumAmt': 80520,
      'aumCcy': 'USD',
      'relSncDt': '02/24/2016',
      'ctlEnt': '',
      'type': 'counterparty',
      'grp': 'OUTSOURCE INTERNATIONAL',
      'crmUsrId': 'RM106',
      'crmUsrNm': 'Robert Lee',
      'updDt': '04/12/2016',
      'cifNo': 'CIF: 514122',
      'shnPct': '9.5%',
      'relToPntCode': 'Subsidiary Company Of',
      'relType': 'SBRCPY'
    }
  ];

  constructor() { }

  ngOnInit() {}

}
