import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-multi-currency-example',
  templateUrl: './multi-currency-example.component.html',
  styleUrls: ['./multi-currency-example.component.scss']
})
export class MultiCurrencyExampleComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      mulCur1: {
        atlCode: 'GBP',
        atlAmt: 10000,
        fxRtDt: new Date(),
        fxRt: 10,
        lccAmt: 100000,
      },
      mulCur2: {

      }
    });

  }
}
