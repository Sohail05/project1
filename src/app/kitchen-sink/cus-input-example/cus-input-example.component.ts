import { Component, DoCheck, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-cus-input-example',
  templateUrl: './cus-input-example.component.html',
  styleUrls: ['./cus-input-example.component.scss'],
})
export class CusInputExampleComponent implements OnInit, DoCheck {
  form: FormGroup;
  dataModel = '';
  selectType = 'text';
  input;
  snippet: string;
  constructor(private fb: FormBuilder) { }
  ngDoCheck() {
    this.snippet =
      `
      <nt-input
        key="modelName">
        [parent]="form"
        [placeholder]="selectType"
        [validation]="{ minlength: 10, required: true, email: true }"
        [masking]="${this.selectType}"
      </nt-input>
    `;
  }
  ngOnInit() {
    this.form = this.fb.group({
      modelName: this.dataModel,
    });
  }
  onSubmit() {
    console.log('form data', this.form.value, this.form.valid);
  }

}
