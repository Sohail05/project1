import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdSnackBarRef } from '@angular/material';


@Component({
  selector: 'app-success-or-error',
  templateUrl: './success-or-error.component.html',
  styleUrls: ['./success-or-error.component.scss'],
})

export class SuccessOrErrorComponent {
  message: string;
}

@Component({
  selector: 'app-cus-snackbar-example',
  templateUrl: './cus-snackbar-example.component.html',
  styleUrls: ['./cus-snackbar-example.component.scss'],
})
export class CusSnackbarExampleComponent implements OnInit {

  msg;
  time;
  constructor(public snackBar: MdSnackBar) { }

  ngOnInit() {
  }

  openSnackBar() {
    const duration = this.time;
    const sb = this.snackBar.openFromComponent(SuccessOrErrorComponent, {
      duration,
    });
    sb.instance.message = this.msg;
    console.log(this.msg);
  }

}
