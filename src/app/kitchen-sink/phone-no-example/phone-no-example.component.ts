import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-phone-no-example',
  templateUrl: './phone-no-example.component.html',
  styleUrls: ['./phone-no-example.component.scss']
})
export class PhoneNoExampleComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder) { }
  phone = {};
  ngOnInit() {
    this.form = this.fb.group({
      phoneNo: {},
    });
    // this.form.disable();
  }

}
