import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-multi-auto-example',
  templateUrl: './multi-auto-example.component.html',
  styleUrls: ['./multi-auto-example.component.scss'],
})
export class MultiAutoExampleComponent implements OnInit {
  val = [
    '123',
    '456',
  ];
  options = [
    { code: '123', dsc: '234' },
    { code: '456', dsc: '567' },
  ];
  form: FormGroup;
  constructor(private fb: FormBuilder) { }

  fn = (n) => n ? n.dsc : '';
  ngOnInit() {
    this.form = this.fb.group({
      multi: this.fb.control(this.val)
    });
  }

}
