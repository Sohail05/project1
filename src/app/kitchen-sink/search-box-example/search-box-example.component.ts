import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-box-example',
  templateUrl: './search-box-example.component.html',
  styleUrls: ['./search-box-example.component.scss'],
})
export class SearchBoxExampleComponent implements OnInit {
  search;
  constructor() { }

  ngOnInit() {
  }

}
