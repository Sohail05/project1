import { Component, OnInit } from '@angular/core';
import { MultiLinguaService } from '../../../lib/multi-lingua/multi-lingua.service';
import { NgModel } from '@angular/forms';
import { Locale } from '../../../lib/core/model/locale';
import { environment } from 'environments/environment';
import { SessionService } from '../../../lib/auth/session.service';
import { ApiService } from '../../../lib/core/api/api.service';

@Component({
  selector: 'app-multi-lingua-example',
  templateUrl: './multi-lingua-example.component.html',
  styleUrls: ['./multi-lingua-example.component.scss'],
  providers: []
})
export class MultiLinguaExampleComponent implements OnInit {

  msgCode: string;
  langCode: string;
  desc: string;

  listing: Locale[];
  fallbackText = 'Not Found';



  // template variable
  auto;

  constructor(private api: ApiService) {

  }
  ngOnInit() {
    this.api.get(
      environment.api.locales,
      {
        aplId: environment.applicationId,
        lang: 'en'
      }
    )
      .toPromise()
      .then((data) => { this.listing = data })
  }

}
