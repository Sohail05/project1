import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-example',
  templateUrl: './card-example.component.html',
  styleUrls: ['./card-example.component.scss']
})
export class CardExampleComponent implements OnInit {
  data = [
    {
      aaa: 'weriou',
      bbb: 'weriou',
      ccc: 'weriou',
      ddd: 'weriou',
      eee: 'weriou',
    },
    {
      aaa: '10000',
      bbb: '10000',
      ccc: '10000',
      ddd: '10000',
      eee: '10000',
    }


  ];
  setting = [
    { label: 'Task Id', key: 'aaa', classes: 'label-txt' },
    { label: 'c', key: 'bbb', classes: 'sub-title' },
    { label: 'Task Id', key: 'ccc', classes: 'main-title' },
    { label: 'Id', key: 'ddd', classes: 'small-title' },
    { label: 'Task', key: 'eee' },
  ]

  sortOptions = [
    { key: 'aaa', label: 'AAAA', order: 'asc' },
    { key: 'aaa', label: 'AAAD', order: 'dsc' },
  ];

  headerSetting = {
    sts: 'aaa',
    stsIcon: 'icn_remove',
    stsColor: { others: 'red' },
    title: 'bbb',
    icons: ['icn_remove']
  }

  actions = {
    align: 'end',
    icons: ['icn_remove']
  }
  constructor() { }

  ngOnInit() {
  }

}
