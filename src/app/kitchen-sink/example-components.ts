import { AccessControlExampleComponent } from './access-control-example/access-control-example.component';
import { AddressExampleComponent } from './address-example/address-example.component';
import { AreaChartExampleComponent } from './area-chart-example/area-chart-example.component';
import { AutoCompleteExampleComponent } from './auto-complete-example/auto-complete-example.component';
import { BackExampleComponent } from './back-example/back-example.component';
import { BreadcrumbExampleComponent } from './breadcrumb-example/breadcrumb-example.component';
import { CardExampleComponent } from './card-example/card-example.component';
import { CheckboxExampleComponent } from './checkbox-example/checkbox-example.component';
import { CodeTypeExampleComponent } from './code-type-example/code-type-example.component';
import { CollapseExampleComponent } from './collapse-example/collapse-example.component';
import { CusInputExampleComponent } from './cus-input-example/cus-input-example.component';
import { CusSnackbarExampleComponent } from './cus-snackbar-example/cus-snackbar-example.component';
import { CustomFormExampleComponent } from './custom-form-example/custom-form-example.component';
import { DateInputExampleComponent } from './date-input-example/date-input-example.component';
import { DatepickerExampleComponent } from './datepicker-example/datepicker-example.component';
import { IconsExampleComponent } from './icons-example/icons-example.component';
import { IdExampleComponent } from './id-example/id-example.component';
import { KitchenSinkComponent } from './kitchen-sink.component';
import { MultiAutoExampleComponent } from './multi-auto-example/multi-auto-example.component';
import { MultiCurrencyExampleComponent } from './multi-currency-example/multi-currency-example.component';
import { MultiLinguaExampleComponent } from './multi-lingua-example/multi-lingua-example.component';
import { OrgChartExampleComponent } from './org-chart-example/org-chart-example.component';
import { PhoneNoExampleComponent } from './phone-no-example/phone-no-example.component';
import { ProgressBarExampleComponent } from './progress-bar-example/progress-bar-example.component';
import { SearchBoxExampleComponent } from './search-box-example/search-box-example.component';
import { SelectExampleComponent } from './select-example/select-example.component';
import { SidenavFormExampleComponent } from './sidenav-form-example/sidenav-form-example.component';
import { TableExampleComponent } from './table-example/table-example.component';
import { VersionExampleComponent } from './version-example/version-example.component';
import { WorkflowBarExampleComponent } from './workflow-bar-example/workflow-bar-example.component';

export const exampleComponents: Array<any> = [
  AccessControlExampleComponent,
  AddressExampleComponent,
  AreaChartExampleComponent,
  AutoCompleteExampleComponent,
  BackExampleComponent,
  BreadcrumbExampleComponent,
  CardExampleComponent,
  CheckboxExampleComponent,
  CodeTypeExampleComponent,
  CollapseExampleComponent,
  CusInputExampleComponent,
  CusSnackbarExampleComponent,
  CustomFormExampleComponent,
  DateInputExampleComponent,
  DatepickerExampleComponent,
  IconsExampleComponent,
  IdExampleComponent,
  MultiAutoExampleComponent,
  MultiCurrencyExampleComponent,
  MultiLinguaExampleComponent,
  OrgChartExampleComponent,
  PhoneNoExampleComponent,
  ProgressBarExampleComponent,
  SearchBoxExampleComponent,
  SelectExampleComponent,
  SidenavFormExampleComponent,
  TableExampleComponent,
  VersionExampleComponent,
  WorkflowBarExampleComponent,
];
