import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Countries } from '../../../lib/core/countries';
@Component({
  selector: 'app-select-example',
  templateUrl: './select-example.component.html',
  styleUrls: ['./select-example.component.scss'],
})
export class SelectExampleComponent implements OnInit {
  form: FormGroup;
  isDisabled = false;
  data = '';
  data1 = [];
  // options = Countries.map((c) => { return {code: c, dsc: c } });
  options = [{ code: 'c', dsc: 'c' }, { code: 'a', dsc: 'a' }, { code: 'b', dsc: 'b', sts: 'I' }, { code: 'd', dsc: 'd', vldIdc: 'N' },
  { code: 'e', dsc: 'e', vldIdc: 'N' }]
  constructor(private fb: FormBuilder) { }

  fn = (n) => n.dsc || '';
  ngOnInit() {
    this.form = this.fb.group({
      selectSingle: '',
      selectMulti: this.fb.control(['a', 'b'])
    });

  }
  submit() {
    console.log(this.form.valid);

  }

  single = 'd';
  multi = ['d', 'a'];

}
