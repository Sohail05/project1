import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-area-chart-example',
  templateUrl: './area-chart-example.component.html',
  styleUrls: ['./area-chart-example.component.scss'],
})
export class AreaChartExampleComponent implements OnInit {
  /* -- Area Chart -- */
  areaChartData = [
    {
      rtgDt: '1-1-17',
      rtgAgtCode: 'ABC Corp',
      gra: 'A',
      pdPct: 0.08,
      lgdPct: 0.114
    },
    {
      rtgDt: '1-2-17',
      rtgAgtCode: 'ABC Corp',
      gra: 'A',
      pdPct: 1,
      lgdPct: 3
    },
  ];

  areaChartScale = ['AAA', 'AA+', 'AA', 'AA-', 'A+', 'A', 'A-', 'BBB+', 'BBB', 'BBB-', 'BB', 'BB+', 'BB-', 'B+', 'B',
    'B-', 'CCC+', 'CCC', 'CCC-', 'CC', 'C', 'RD', 'SD', 'D'];
  areaChartConfig = [
    { x: 'rtgDt', y: 'pdPct', label: 'PD', color: '#44c4ca' },
    { x: 'rtgDt', y: 'lgdPct', label: 'LGD', color: '#2a7493' },
    { x: 'rtgDt', y: 'gra', label: 'Grade', color: '#132d3e', scale: true },
  ];

  constructor() { }

  ngOnInit() { }

}
