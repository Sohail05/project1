import { Component, OnInit } from '@angular/core';
import { entries } from './routes';

const inputControls = [
  {
    name: 'Antelope',
    child: [...entries],
  }
];

@Component({
  selector: 'app-kitchen-sink',
  templateUrl: './kitchen-sink.component.html',
  styleUrls: ['./kitchen-sink.component.scss'],
})

export class KitchenSinkComponent implements OnInit {

  inputControls: any = inputControls;

  constructor() { }

  ngOnInit() {
  }

  toReadable(title: string) {
     return title.split('-').map( (w) => w.charAt(0).toUpperCase() + w.slice(1)).join(' ');
  }


}
