import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-datepicker-example',
  templateUrl: './datepicker-example.component.html',
  styleUrls: ['./datepicker-example.component.scss'],
})
export class DatepickerExampleComponent implements OnInit {
  disabled = true;
  date: Date = new Date(2016, 9, 15);
  time: Date = new Date(1, 1, 1, 12, 10);
  datetime: Date = new Date(2016, 9, 15, 12, 10);
  minDate: Date = new Date(2016, 7, 15);
  maxDate: Date = new Date(2016, 12, 15);
  form: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      date: '',
    });
  }

  handleChange(value: any) {
    console.log('Changed data: ', value);
  }

}
