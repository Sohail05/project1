import { Component, OnInit } from '@angular/core';
import { AccessControlService } from '../../../lib/access-control/access-control.service';

@Component({
  selector: 'app-access-control-example',
  templateUrl: './access-control-example.component.html',
  styleUrls: ['./access-control-example.component.scss']
})
export class AccessControlExampleComponent implements OnInit {

  constructor(private accessControlService: AccessControlService) { }

  ngOnInit() {}

}
