import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-auto-complete-example',
  templateUrl: './auto-complete-example.component.html',
  styleUrls: ['./auto-complete-example.component.scss'],
})
export class AutoCompleteExampleComponent implements OnInit {
  form: FormGroup;
  disabled = true;
  options = [
    { code: '123', dsc: '234' },
    { code: '456', dsc: '567' },
  ];
  default = { key: '123', dsc: '234' };
  constructor(private fb: FormBuilder) { }

  fn = (n) => n ? n.dsc : '';

  ngOnInit() {
    this.form = this.fb.group({
      autoSingle: '123',
    });
  }
}
