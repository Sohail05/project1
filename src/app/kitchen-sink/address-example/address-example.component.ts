import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-address-example',
  templateUrl: './address-example.component.html',
})
export class AddressExampleComponent implements OnInit {
  form: FormGroup;
  data = {
    addr1: 'Sample Address Line 1',
    addr2: 'Sample Address Line 2',
    addr3: 'Sample Address Line 3',
    postcode: '34000',
    city: 'Bangsar South',
    cty: 'US',
    ste: 'CA',
  };

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      addrGrp: this.data,
    });
    // this.form.disable();
  }
}
