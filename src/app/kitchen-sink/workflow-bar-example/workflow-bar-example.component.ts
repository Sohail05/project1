import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../lib/core/api/api.service';
import { Headers } from '@angular/http';

@Component({
  selector: 'app-workflow-bar-example',
  templateUrl: './workflow-bar-example.component.html',
  styleUrls: ['./workflow-bar-example.component.scss']
})
export class WorkflowBarExampleComponent implements OnInit {

  form: FormGroup;
  activeWorflow = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      aplRef: ['', Validators.required],
      taskId: ['', Validators.required],
    });
  }

  submit() {
    this.activeWorflow = false;
    const loading = new Promise(
      (res, rej) => {
        this.apiService.options.setspecificHeaders(new Headers({ 'X-WFL-TAS-ID': this.form.value.taskId }), 'X-WFL-TAS-ID');
        this.apiService.options.setspecificHeaders(new Headers({ 'X-APL-REF-NO': this.form.value.aplRef }), 'X-APL-REF-NO');
        setTimeout(() => {
          res();
        }, 500);
      }
    ).then(() => {
      this.activeWorflow = true;
    });

  }

}
