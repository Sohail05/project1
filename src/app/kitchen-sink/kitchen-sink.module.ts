import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { AntelopeModule } from './../../lib/index';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KitchenSinkRoutingModule } from './kitchen-sink-routing.module';

import { SuccessOrErrorComponent } from './cus-snackbar-example/cus-snackbar-example.component';
import { MultiCurrencyExampleComponent } from './multi-currency-example/multi-currency-example.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PhoneNoExampleComponent } from './phone-no-example/phone-no-example.component';
import { AccessControlExampleComponent } from './access-control-example/access-control-example.component';
import { HomeComponent } from './home/home.component';
import { KitchenSinkComponent } from './kitchen-sink.component';

import { entries, getComponents } from './routes';
import { RouterModule } from '@angular/router';
/**
 * Extract components and concat to declaration.
 */
const kitchenSinkComponents = entries.map(getComponents)

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AntelopeModule,
    KitchenSinkRoutingModule,
    FlexLayoutModule,
    RouterModule
  ],
  exports: [],
  declarations: [
    KitchenSinkComponent,
    HomeComponent,
    SuccessOrErrorComponent,
    PhoneNoExampleComponent,
    AccessControlExampleComponent,
  ].concat(kitchenSinkComponents),
  entryComponents: [
    // SnackbarDialogComponent,
    SuccessOrErrorComponent,
  ],
  providers: [],
})
export class KitchenSinkModule { }
