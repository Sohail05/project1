import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-radio-button-example',
  templateUrl: './radio-button-example.component.html',
  styleUrls: ['./radio-button-example.component.scss']
})
export class RadioButtonExampleComponent implements OnInit {
  options = [{ code: 'c', dsc: 'c' }, { code: 'a', dsc: 'a' }, { code: 'b', dsc: 'b', sts: 'I' }, { code: 'd', dsc: 'd', vldIdc: 'N' },
  { code: 'e', dsc: 'e', vldIdc: 'N' }];
  form: FormGroup;
  radio = '';
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      radio: '',
    });
  }

}
