import { AppRoutingModule } from './app-routing';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { EntryComponents, AntelopeModule } from '../lib/index';
import { AuthModule } from '../lib/auth/index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScreenModule } from '../lib/screen/index';
import { SidenavBarModule } from '../lib/sidenav-bar/index';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AuthModule.forRoot(),
    ScreenModule.forRoot(),
    SidenavBarModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
