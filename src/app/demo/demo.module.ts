import { NgModule, ErrorHandler } from '@angular/core';
import { DirtyCheckService } from '../../lib/core/dirty-check.service';
import { TestPageModule } from './../test-page/test-page.module';
import { KitchenSinkModule } from './../kitchen-sink/kitchen-sink.module';
import { AccessControlService } from '../../lib/access-control/access-control.service';
import { MultiLinguaService } from '../../lib/multi-lingua/multi-lingua.service';
import { DemoRoutingModule } from './demo-routing.module';
import { AntelopeModule } from '../../lib/index';
import { CoreModule } from '../../lib/core/index';
import { DemoComponent } from './demo.component';
import { ValidationService } from '../../lib/core/validation.service';
import { ApiService } from '../../lib/core/api/api.service';
import { ErrorHandlerService } from '../../lib/core/api/error-handler';
import { IconsService } from '../../lib/core/icons/icons.service';
import { CodeTypeService } from '../../lib/code-type/code-type.service';
import { FormService } from '../../lib/core/form.service';
import { ErrorLoggerService } from '../../lib/core/error-logger';

@NgModule({
    imports: [
        DemoRoutingModule
    ],
    exports: [DemoComponent],
    declarations: [DemoComponent],
    providers: [
        AccessControlService,
        MultiLinguaService,
        DirtyCheckService,
        ValidationService,
        ApiService,
        ErrorHandlerService,
        IconsService,
        CodeTypeService,
        FormService,
        { provide: ErrorHandler, useClass: ErrorLoggerService }
    ],

})
export class DemoModule { }
