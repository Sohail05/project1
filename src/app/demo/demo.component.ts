import { Component, OnInit } from '@angular/core';
import { SidenavService } from '../../lib/sidenav-bar/sidenav/sidenav.service';
import { ValidationService } from '../../lib/core/validation.service';
import { MultiLinguaService } from '../../lib/multi-lingua/multi-lingua.service';
import { AccessControlService } from '../../lib/access-control/access-control.service';
import { IconsService } from '../../lib/core/icons/icons.service';

@Component({
    selector: 'app-demo',
    templateUrl: './demo.component.html',
    providers: [SidenavService]
})
export class DemoComponent implements OnInit {

    demoRoutes = [
        { name: 'Kitchen-Sink', route: '/kitchen-sink', icon: 'icn_dashboard' },
        { name: 'Test Page', route: '/test-page', icon: 'icn_dashboard' },
        { name: 'Test Menu', route: '/test-page', icon: 'icn_dashboard', access: 'SYS.PMR|VW' },
        { name: 'Test Menu 2', route: '/test-page', icon: 'icn_dashboard', access: 'NO.ACCESS|VW' },
    ]

    constructor(
        private sidenavService: SidenavService,
        private validationService: ValidationService,
        private multilingua: MultiLinguaService,
        private access: AccessControlService,
        private icn: IconsService
    ) {}

    ngOnInit() {
        this.loaded()
        this.multilingua.load();
        this.access.load();
    }

    loaded() {

        this.sidenavService.setSystemItems(this.demoRoutes);
        this.sidenavService.setAppListItems([
            {
                name: 'Application', route: '/test', icon: 'icn_application', subItems: [
                    { name: 'Details', route: '/test' },
                    { name: 'Entity', route: '/test' },
                    { name: 'Collateral', route: '/test' },
                    { name: 'Facility', route: '/test' },
                ],
            },
        ])
        this.validationService.overrideDefaultMasks({ currency: { prefix: 'MYR ' } });
    }

}
