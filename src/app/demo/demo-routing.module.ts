import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScreenComponent } from '../../lib/screen/screen.component';
import { DirtyCheckGuardService } from '../../lib/core/dirty-check-guard.service';
import { DemoComponent } from './demo.component';

const routes: Routes = [{
    path: '', component: DemoComponent, children: [
        {
            path: 'test-page',
            canDeactivate: [DirtyCheckGuardService],
            loadChildren: 'app/kitchen-sink/kitchen-sink.module#KitchenSinkModule'
        },
        { path: 'kitchen-sink', loadChildren: 'app/kitchen-sink/kitchen-sink.module#KitchenSinkModule' },
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DemoRoutingModule { }


