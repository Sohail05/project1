import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ScreenComponent } from '../lib/screen/screen.component';
import { TestPageComponent } from './test-page/test-page.component';
import { DirtyCheckGuardService } from '../lib/core/dirty-check-guard.service';
import { AuthGuard } from '../lib/auth/auth-guard';
import { LoginComponent } from '../lib/auth/login/login.component';
import { AuthComponent } from '../lib/auth/auth.component';
import { KitchenSinkModule } from './kitchen-sink/kitchen-sink.module';
import { RouteDataService } from '../lib/core/route-data.service';

export const routes: Routes = [
  { path: '', canActivate: [AuthGuard], component: ScreenComponent, loadChildren: 'app/demo/demo.module#DemoModule' },
  { path: 'login', component: LoginComponent },
  { path: 'auth', component: AuthComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
