
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AccessControlService } from '../access-control/access-control.service'
import { Router } from '@angular/router';
import { ApiService } from '../core/api/api.service';
import { environment } from 'environments/environment';
import { AccessControl } from '../access-control/access-control';

/**
 * WIP
 * Does everything related to workflow.
 * Breakdown if it's handling to much responsability.
 */

interface WorkflowObject {
    secRoleCode: string;
    tasId: string;
    viewUrl: string;
}

/**
 * todo: PostPoned fields and record access control.
 * -Sohail
 */

@Injectable()
export class WorkflowService {

    /**
     * Stored Workflow Controls
     */
    private access: Array<AccessControl> = [];

    loading: Promise<any>;

    workflowContext: WorkflowObject;

    constructor(private router: Router,
        private api: ApiService) {
    }

    /**
     * Load workflow screen.
     * @param {WorkflowObject} workflow data model returned fro API.
     * @return {Promise<any>}
     */
    load(workflow: WorkflowObject) {

        this.clear();
        this.workflowContext = workflow;
        const loadAccess = this.loadContext(environment.api.workflow.access).then((data) => {
            this.access = data;
        });

        this.loading = Promise.all([loadAccess]).then(this.navigate);
        return this.loading;
    }

    /**
     * Retrive menu access during workflow context.
     * @param code Role Code, e.g. 'SYS.PMR|VW'.
     * @return {boolean} returns `true` if permission is available.
     */
    menuAccess(code: string): boolean {
        // todo simplify & optimize
        const frag = code.split('|')
        const find = (access: AccessControl) => {
            const role = access.roleCode === frag[0];
            const opt = access.opnCodes.find((operation) => { return operation === frag[1] }) ? true : false;
            return role && opt;
        }
        return this.access.find(find) ? true : false;
    }

    /**
     * Lauch proceed process.
     */
    Proceed() {

    }

    /**
     * Clear all Workflow access controls
     */
    private clear() {
        this.access = [];
    }

    /**
     * Navigate to the workflow context page.
     */
    private navigate() {
        return this.router.navigateByUrl(this.workflowContext.viewUrl);
    }

    /**
     * Request and store Workflow Access Controls
     */
    private loadContext(url: string) {
        const params = { wflSecRole: this.workflowContext.secRoleCode }
        return this.api.get(url, params, params).toPromise().then((data) => data as Array<AccessControl>);
    }

}
