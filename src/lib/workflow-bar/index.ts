import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdSelectModule, MdButtonModule, MdToolbarModule, MdCardModule, MdIconModule } from '@angular/material';
import { WorkflowBarComponent } from './workflow-bar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CodeTypeModule } from '../code-type/index';
import { MultiAutocompleteModule } from '../multi-autocomplete/index';
import { TextareaModule } from '../textarea/index';
import { MultiLinguaModule } from '../multi-lingua/index';
import { UITableModule } from '../ui-table/index';
import { InputModule } from '../input/index';
import { AutocompleteModule } from '../autocomplete/index';
import { RadioButtonModule } from '../radio-button/index';
import { DatePickerModule } from '../date-picker/index';
import { CheckboxModule } from '../checkbox/index';
import { SelectModule } from '../select/index';

@NgModule({
    imports: [
        CommonModule,
        MdSelectModule,
        MdButtonModule,
        MdToolbarModule,
        MdIconModule,
        FlexLayoutModule,
        CodeTypeModule,
        TextareaModule,
        ReactiveFormsModule,
        MultiAutocompleteModule,
        MdCardModule,
        FormsModule,
        MultiLinguaModule,
        UITableModule,
        InputModule,
        MultiAutocompleteModule,
        AutocompleteModule,
        RadioButtonModule,
        DatePickerModule,
        CheckboxModule,
        SelectModule,
        TextareaModule,
    ],
    declarations: [WorkflowBarComponent],
    exports: [WorkflowBarComponent],
    providers: []
})
export class WorkflowBarModule {

}
