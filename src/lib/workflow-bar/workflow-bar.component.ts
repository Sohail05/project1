import {
  Component, OnInit, Output, EventEmitter, Input, animate,
  AnimationEntryMetadata,
  state,
  style,
  transition,
  trigger,
} from '@angular/core';
import { ApiService } from '../core/api/api.service';
import { environment } from 'environments/environment';
import { CodeTypeService } from '../code-type/code-type.service';
import { slideToLeftAnimation, slideToRightAnimation, slideToTopAnimation } from '../animations/animations';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy, HostListener } from '@angular/core';
import { Headers } from '@angular/http';
import { SessionService } from '../auth/session.service';
import { WorkflowService } from '../workflow/workflow.service';

@Component({
  selector: 'nt-workflow-bar',
  templateUrl: './workflow-bar.component.html',
  styleUrls: ['./workflow-bar.component.scss'],
  // todo: move out to animation.ts
  animations: [
    trigger('slideUp', [
      state('false', style({ height: '50%' })),
      state('true', style({ height: '50%' })),
      transition(':enter', [
        style({ position: 'fixed', transform: 'translateY(100%)', opacity: 0 }),
        animate('0.5s ease-in-out', style({ transform: 'translateY(0%)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ position: 'fixed', transform: 'translateY(0%)', opacity: 1 }),
        animate('0.5s ease-in-out', style({ transform: 'translateY(100%)', opacity: 0 })),
      ]),
    ])

  ]
})
export class WorkflowBarComponent implements OnInit, OnDestroy {
  // Display
  showWorkflow = true;
  showRework = false;
  showCancel = false;
  showTask = false;
  showActionInput = false;
  lock = false;
  // to show right Icon in table
  rightIcon = { delete: true };
  options: any;
  // list
  reworkableTaskList = [];
  // reworkReasonList = [];
  reworkReasonSubscriber: Subscription;
  reason = [];
  task;

  // group
  formRework: FormGroup;
  formCancel: FormGroup;
  formAction: FormGroup;
  actionInput: object = {};

  // table
  setting = [
    { key: 'actNm', label: 'wfl.lbl.wflpvsrwktas.actnm', sub: 'sub' },
    { key: 'sttDt', label: 'wfl.lbl.wflpvsrwktas.sttdt' },
    { key: 'endDt', label: 'wfl.lbl.wflpvsrwktas.enddt' },
    { key: 'tasOwner', label: 'wfl.lbl.wflpvsrwktas.tasowner' },
  ];
  // date
  today: Date = new Date();
  yesterday: Date = new Date();
  tomorrow: Date = new Date();

  @Input() actions = [];
  @Output() proceed = new EventEmitter();
  @Output() rework = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @HostListener('document:keyup.escape', ['$event'])
  onEscape(event) {
    this.closeAll();
  }


  constructor(
    private api: ApiService,
    private codeType: CodeTypeService,
    private fb: FormBuilder,
    private session: SessionService,
    private workflowService: WorkflowService
  ) { }

  ngOnInit() {
    this.api.options.appendHeader('Authorization', 'Bearer ' + this.session.token);
    // this.api.options.setspecificHeaders(new Headers({ 'X-WFL-TAS-ID': '514826' }), 'X-WFL-TAS-ID');
    // this.api.options.setspecificHeaders(new Headers({ 'X-APL-REF-NO': 'PAL/0001229' }), 'X-APL-REF-NO');
    this.getActions();
    this.generateForm();
    this.getReworkableTask();
    this.subscribeReworkReason();
    this.workflowService.load({
      secRoleCode: 'string',
      tasId: 'string',
      viewUrl: 'string'
    });
  }

  setDates() {
    this.tomorrow.setDate(this.today.getDate() + 1);
    this.yesterday.setDate(this.today.getDate() - 1);
  }

  getMinDate(vld: string, today: boolean) {
    if (vld === 'futureDateValidator') {
      if (today) {
        return this.tomorrow;
      } else {
        return this.today;
      }
    }
  }

  getMaxDate(vld: string, today: boolean) {
    if (vld === 'passDateValidator') {
      if (today) {
        return this.today;
      } else {
        return this.yesterday;
      }
    }
  }

  /**
   * toggle workflow rework panel
   */
  toggleRework() {
    this.showRework = !this.showRework;
    if (this.showRework) {
      this.showCancel = false;
      this.showActionInput = false;
    }
    this.formRework.reset();
  }

  /**
   * toggle workflow cancel panel
   */
  toggleCancel() {
    this.showCancel = !this.showCancel;
    if (this.showCancel) {
      this.showRework = false;
      this.showActionInput = false;
    }
    this.formCancel.reset();
  }

  generateForm() {
    this.formRework = this.fb.group({
      rsnCodes: [[], Validators.required],
    });
    this.formCancel = this.fb.group({
      rsnCodes: [[], Validators.required],
      rem: '',
    });
    // generate empty form on start
    this.formAction = this.fb.group({});
  }

  /**
   * close all workflow panel
   */
  closeAll() {
    this.closeCancel();
    this.closeInputAction();
    this.closeRework();
  }

  /**
   * close rework workflow panel
   */
  closeRework() {
    this.showRework = false;
  }

  /**
   * Close cancel workflow panel
   */
  closeCancel() {
    this.showCancel = false;
  }

  /**
   * Close input action panel
   */
  closeInputAction() {
    this.showActionInput = false;
  }

  /**
   * Subscribe to rework reason only show task list when user input more than one reason
   */
  subscribeReworkReason() {
    this.reworkReasonSubscriber = this.formRework.get('rsnCodes').valueChanges.subscribe((x) => {
      this.showTask = x && x.length > 0
    });
  }

  /**
   * Toggle workflow bar
   */
  toggleWorkflow() {
    if (!this.lock) {
      this.showWorkflow = !this.showWorkflow;
    }
  }

  toggleLock() {
    this.lock = !this.lock;
  }
  /**
   * when required extra field toggle proceed workflow panel
   * toggle proceed workflow
   */
  toggleProceed() {
    const input = this.actionInput[this.options];
    if (input && input.length > 0) {
      input.map((x) => {
        if (x.promptInputArgs && x.promptInputArgs.length > 0) {
          x.promptInputArgs.map((field) => {
            const validation = [];
            if (field.required) {
              validation.push(Validators.required);
            }
            this.formAction.addControl(field.nm, new FormControl({ value: field.defVal, disabled: field.readonly }, validation))
          });
        }
      });
      this.showActionInput = !this.showActionInput;
    } else if (input && input.length === 0) {
      // not required input call proceed api
      this.workflowProceed();
    }

    if (this.showActionInput) {
      this.showRework = false;
      this.showCancel = false;
    }
  }

  /**
   * Generate data and call porceed workflow API
   */
  workflowProceed() {
    // only execute when form valid
    if (this.formAction.valid) {
      this.proceed.emit(this.options);
      const data = {
        wflAction: this.options,
        wflPromptInpDatas: [],
      }
      const inputs = this.actionInput[this.options][0];
      this.actionInput[this.options].map((x) => {
        const inputArgs = x['promptInputArgs'];

        inputArgs.map((field) => {
          const inputData = {
            fctFldId: inputs['fctCode'] + '_' + field['nm'],
          };
          const inputType = field['inputT'];
          if (inputType === 'TEXT' || inputType === 'TEXT_AREA' ||
            inputType === 'SELECTONEMENU' || inputType === 'RADIOBUTTON' ||
            inputType === 'AUTOCOMPLETE' || inputType === 'PASSWORD') {
            inputData['dataT'] = 'STRING';
            inputData['strVal'] = this.formAction.controls[field['nm']].value;
          } else if (inputType === 'CALENDAR') {
            inputData['dataT'] = 'DATE';
            inputData['dtVal'] = this.formAction.controls[field['nm']].value;
          } else if (inputType === 'NUMBER') {
            inputData['dataT'] = 'DECIMAL';
            inputData['dcmVal'] = this.formAction.controls[field['nm']].value;
          } else if (inputType === 'CHECKBOX' || inputType === 'AUTOCOMPLETE_MULTIPLE') {
            inputData['dataT'] = 'STRING_ARRAY';
            // inputData['dcmVal'] = this.formAction.controls[x['nm']].value;
          }
          data.wflPromptInpDatas.push(inputData);
        });

      });
      // TODO make a call to api to
      this.api.post(environment.api.workflow.proceed, data, {}, ['X-WFL-TAS-ID', 'X-APL-REF-NO']).subscribe((x) => {
        // redirect url
        this.workflowService.load(x);
        this.closeAll();
      });

    }
  }

  /**
   * Call Rework workflow API
   */
  workflowRework(e) {
    this.rework.emit(e);
    // TODO make a call to api to
    // move to workflow service
    this.api.putWithQuery(environment.api.workflow.rework, {},
      this.formRework.value, { rwkToTaskId: e.rwkTasId }
      , ['X-WFL-TAS-ID', 'X-APL-REF-NO']).subscribe((x) => {
        // redirect to specific route
        this.workflowService.load(x);
        this.closeAll();
      });
  }

  /**
   * Call Cancel Workflow API
   */
  workflowCancel() {
    this.cancel.emit();
    // TODO make a call to api to
    // move to workflow service
    this.api.postWithQuery(environment.api.workflow.cancel, {},
      this.formCancel.value, {}, 'X-WFL-TAS-ID').subscribe((x) => {
        // redirect out
        this.workflowService.load(x);
        this.closeAll();
      });
  }

  /**
   * Get Action List
   */
  getActions() {
    // get actions by api
    this.api.get(environment.api.workflow.actionList, {}, ['X-WFL-TAS-ID', 'X-APL-REF-NO']).subscribe((list) => {
      if (list) {
        this.actionInput = list;
        Object.keys(list).map((x) => {
          this.actions.push(x);
        })
      }
    });
  }

  /**
   * Get Reworkable Task List
   */
  getReworkableTask() {
    // get reworkable task by api
    this.api.get(environment.api.workflow.reworkTasks, {}, ['X-WFL-TAS-ID', 'X-APL-REF-NO']).subscribe((task) => {
      if (Array.isArray(task)) {
        task.map((x) => {
          x.sttDt = this.formatDate(new Date(x.sttDt));
          x.endDt = this.formatDate(new Date(x.endDt));
          // process sublabel from rework task id and workflow code
          x['sub'] = '(' + x.rwkTasId + ')' + x.wflCode;
        });
        this.reworkableTaskList = task;
      }
    });
  }
  // TODO MOVE
  // Format Date
  private formatDate(date: Date) {
    const format = 'MM/DD/YYYY hh:mm';
    const formattedDate = format
      .replace('MM', this.prependZero(date.getMonth() + ''))
      .replace('DD', this.prependZero(date.getDate() + ''))
      .replace('YYYY', date.getFullYear() + '')
      .replace('hh', this.prependZero(date.getHours() + ''))
      .replace('mm', this.prependZero(date.getMinutes() + ''))
      .replace('ss', this.prependZero(date.getSeconds() + ''));
    return formattedDate;
  }

  private prependZero(value: string): string {
    return parseInt(value, 10) < 10 ? '0' + value : value;
  }

  ngOnDestroy() {
    if (this.reworkReasonSubscriber) {
      this.reworkReasonSubscriber.unsubscribe();
    }
  }

}
