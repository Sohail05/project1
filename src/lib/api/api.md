# Antelope API

Antelope ships with certain common APIs.

### Restful APIs

- Code Provider
- Security Access
- Security User
- Security Session
- System
- Workflow

### API Interfaces

Some Antelope components & services rely on the listed APIs.  
The expected API paths need to be provided as expected to function.  
The interfaces can be found in `api/interfaces.ts` which are implemented on objects at a project scope for flexibility.

### API Models

Typescript interfaces matching models/response data objects are provided in `api/model.ts` or `core/model/*` depending on the nature of the model.

