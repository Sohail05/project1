import { Pipe, PipeTransform } from '@angular/core';
import { MultiLinguaService } from '../multi-lingua/multi-lingua.service';

@Pipe({ name: 'multiLinguaTooltip' })
export class MultiLinguaTooltipPipe implements PipeTransform {

  constructor(private multiLinguaService: MultiLinguaService) {
  }

  transform(value: string): any {
    return this.multiLinguaService
      .translate(value)
      .then((locale) => { return locale.tooltip });
  }
}
