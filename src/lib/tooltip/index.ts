import { NgModule } from '@angular/core';
import { MultiLinguaTooltipPipe } from './tooltip.pipe';
import { TooltipDirective } from './tooltip.directive';

@NgModule({
  imports: [],
  exports: [MultiLinguaTooltipPipe, TooltipDirective],
  declarations: [MultiLinguaTooltipPipe, TooltipDirective],
  providers: [],
})
export class MultiLinguaTooltipModule { }
