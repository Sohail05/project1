import { Directive, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({ selector: '[ntTooltip]' })
export class TooltipDirective {
  @Output() showTooltip = new EventEmitter<boolean>();

  @HostListener('focus')
  onFocus() {
    this.showTooltip.emit(true);
  }

  @HostListener('focusout')
  onFocusOut() {
    this.showTooltip.emit(false);
  }

  constructor() { }
}
