import { Injectable, ViewContainerRef } from '@angular/core';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { DeleteDialogComponent } from './delete-dialog.component';

@Injectable()
export class DeleteDialogService {

  constructor(private dialog: MdDialog) { }
  public confirm(message: string, viewContainerRef: ViewContainerRef): Observable<boolean> {

    let dialogRef: MdDialogRef<DeleteDialogComponent>;
    const config = new MdDialogConfig();
    config.viewContainerRef = viewContainerRef;

    dialogRef = this.dialog.open(DeleteDialogComponent, config);

    dialogRef.componentInstance.message = message || 'Are you sure you wish to delete this record?';

    return dialogRef.afterClosed();
  }
}
