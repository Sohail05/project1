import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { DeleteDialogComponent } from './delete-dialog.component';
import { DeleteDialogService } from './delete-dialog.service';

@NgModule({
  imports: [ MaterialModule ],
  exports: [ DeleteDialogComponent ],
  declarations: [ DeleteDialogComponent ],
  providers: [ DeleteDialogService ],
  entryComponents: [ DeleteDialogComponent ],
})
export class DeleteDialogModule { }
