export interface SidenavItemInterface {
  name: string;
  icon: string;
  route: string;
  subItems?: SidenavSubItemInterface[];
}

export interface SidenavSubItemInterface {
  name: string;
  route: string;
  subItems?: SidenavSubItemInterface[];
}

export class SidenavItem {
  name: string;
  icon: string;
  route: string;
  parent: SidenavItem;
  subItems: SidenavItem[];

  constructor(model: any = null) {
    if (model) {
      this.name = model.name;
      this.icon = model.icon;
      this.route = model.route;
      this.parent = model.parent;
      this.subItems = this.mapSubItems(model.subItems);
    }
  }

  hasSubItems() {
    if (this.subItems) {
      return this.subItems.length > 0;
    }
    return false;
  }

  hasParent() {
    return !!this.parent;
  }

  mapSubItems(list: SidenavItem[]) {
    if (list) {
      list.forEach((item, index) => {
        list[index] = new SidenavItem(item);
      });

      return list;
    }
  }
}

export class BriefcaseItem extends SidenavItem {
  type: string;
  refNo?: string;

  constructor(model: any = null, type: string = 'Application') {
    super();
    if (model) {
      this.type = type;
      this.name = model.name;
      this.refNo = model.refNo;
      this.icon = model.icon || 'icn_application';
    }
  }
}
