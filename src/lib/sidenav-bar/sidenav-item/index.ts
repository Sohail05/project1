import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { SidenavItemComponent } from './sidenav-item.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        MaterialModule,
        FlexLayoutModule
    ],
    exports: [SidenavItemComponent],
    declarations: [SidenavItemComponent],
    providers: [],
})
export class SidenavItemModule { }
