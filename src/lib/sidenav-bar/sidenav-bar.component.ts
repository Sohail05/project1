import { Component, Inject, OnInit, ViewChild, HostListener, EventEmitter, ElementRef } from '@angular/core';
import { MediaChange } from '@angular/flex-layout';
import { ObservableMedia } from '@angular/flex-layout';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SidenavService } from './sidenav/sidenav.service';

/**
 * This class represents the navigation bar component.
 */
@Component({
  selector: 'nt-sidenav-bar',
  templateUrl: './sidenav-bar.component.html',
  styleUrls: ['./sidenav-bar.component.scss'],
})
export class SideNavBarComponent implements OnInit {

  @ViewChild('sidenav') sidenav;
  fullMenu = true;
  loading = false;

  private _mediaSubscription: Subscription;
  private _routerEventsSubscription: Subscription;
  sidenavOpen = false;
  sidenavMode = 'side';
  isMobile = false;
  iconNav = true;

  constructor( @Inject(ObservableMedia) private _media$,
    private router: Router,
    private sidenavService: SidenavService,
    private elref: ElementRef
  ) {
      this._mediaSubscription = this._media$.subscribe((change: MediaChange) => {
      const isMobile = (change.mqAlias === 'xs') || (change.mqAlias === 'sm') || (change.mqAlias === 'md');
      this.isMobile = isMobile;
      this.sidenavMode = (isMobile) ? 'over' : 'side';
      this.sidenavOpen = !isMobile;
    });
  }
  ngOnInit() {

    this._routerEventsSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd && this.isMobile) {
        this.sidenav.close();
      }
    });
  }

  menu() {
    this.fullMenu = !this.fullMenu;
  }

  onScroll(event) {
    window.dispatchEvent(new Event('resize'));
  }

  /**
   * Return the sidenav container css class based on the current active view.
   * @return {String}
   */
  currentView(): String {
    return this.sidenavService.currentView ? 'view-' + this.sidenavService.currentView : 'view-home';
  }

}
