import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { SidenavItem, BriefcaseItem, SidenavItemInterface, SidenavSubItemInterface } from '../sidenav-item/sidenav-item.model';


interface BriefcaseObject {
      fullNm;
      refNo;
      icon?
}

@Injectable()
export class SidenavService {
  private appMenuSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');
  appMenu$: Observable<string> = this.appMenuSubject.asObservable();

  private _itemsSubject: BehaviorSubject<SidenavItem[] | BriefcaseItem[]> = new BehaviorSubject<SidenavItem[] | BriefcaseItem[]>([]);
  private _mainSidenavItems: SidenavItem[] = [];
  items$: Observable<SidenavItem[] | BriefcaseItem[]> = this._itemsSubject.asObservable();

  private _currentlyOpenSubject: BehaviorSubject<SidenavItem[]> = new BehaviorSubject<SidenavItem[]>([]);
  private _currentlyOpen: SidenavItem[] = [];
  currentlyOpen$: Observable<SidenavItem[]> = this._currentlyOpenSubject.asObservable();

  private _openedApps: BriefcaseItem[] = [];
  private _countOpenedApps: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  countOpenedApps$: Observable<number> = this._countOpenedApps.asObservable();

  private _appSidenavItems: SidenavItem[] = [];
  isIconSidenav: boolean;
  currentView: string;
  constructor() { }

  /**
   *
   * @param val
   */
  setSystemItems(val: SidenavItemInterface[]) {
    this._mainSidenavItems = this._buildSidenavItem(val);
    this._itemsSubject.next(this._mainSidenavItems);
  }
  toggleAppMenu(val) {
    this.appMenuSubject.next(val);
  }

  /**
   *
   * @param val
   */
  setAppListItems(val: SidenavItemInterface[]) {
    this._appSidenavItems = this._buildSidenavItem(val);
  }

  /**
   *
   * @param item
   */
  removeItem(item: SidenavItem) {
    const index = this._mainSidenavItems.indexOf(item);
    if (index > -1) {
      this._mainSidenavItems.splice(index, 1);
    }
    this._itemsSubject.next(this._mainSidenavItems);
  }

  // todo : refactor (workflow)
  removeBriefcaseItem(item) {
    const index = this._openedApps.indexOf(item);
    if (index > -1) {
      this._openedApps.splice(index, 1);
    }
    this._countOpenedApps.next(this._openedApps.length);
    this._itemsSubject.next(this._openedApps.length ? this._openedApps : this._mainSidenavItems);
  }

  isOpen(item: SidenavItem) {
    return (this._currentlyOpen.indexOf(item) !== -1);
  }

  switchItemsStream(sideNavView: string) {
    let nextThisStream;
    this.currentView = sideNavView;
    switch (sideNavView) {
      case 'briefcase':
        nextThisStream = this._openedApps;
        break;
      case 'deal':
        nextThisStream = this._appSidenavItems;
        break;
      default:
        nextThisStream = this._mainSidenavItems;
    }
    this._itemsSubject.next(nextThisStream);
  }

  // todo: refactor (workflow)
  addItemToBriefcase(obj: BriefcaseObject) {
    const found = this._openedApps
          .filter((item) => !!item.refNo)
          .find((item) => item.refNo === obj.refNo);

    if (found) {
      return;
    }

    const newItem = new BriefcaseItem({
      name: obj.fullNm,
      refNo: obj.refNo,
      icon: obj.icon
    });
    this._openedApps.push(newItem);

    this._countOpenedApps.next(this._openedApps.length);
    this._itemsSubject.next(this._appSidenavItems);
    this.toggleCurrentlyOpen(newItem);
  }

  toggleCurrentlyOpen(item: SidenavItem) {
    let currentlyOpen = this._currentlyOpen;
    if (this.isOpen(item)) {
      if (currentlyOpen.length > 1) {
        currentlyOpen.length = this._currentlyOpen.indexOf(item);
      } else {
        currentlyOpen = [];
      }
    } else {
      currentlyOpen = this.getAllParents(item);
    }
    this._currentlyOpen = currentlyOpen;
  }

  getAllParents(item: SidenavItem, currentlyOpen: SidenavItem[] = []) {
    currentlyOpen.unshift(item);
    if (item.hasParent()) {
      return this.getAllParents(item.parent, currentlyOpen);
    } else {
      return currentlyOpen;
    }
  }

  nextCurrentlyOpen(currentlyOpen: SidenavItem[]) {
    this._currentlyOpen = currentlyOpen;
    this._currentlyOpenSubject.next(currentlyOpen);
  }

  nextCurrentlyOpenByRoute(route: string) {
    let currentlyOpen = [];
    const item = this.findByRouteRecursive(route, this._mainSidenavItems);
    if (item && item.hasParent()) {
      currentlyOpen = this.getAllParents(item);
    } else if (item) {
      currentlyOpen = [item];
    }
    this.nextCurrentlyOpen(currentlyOpen);
  }

  findByRouteRecursive(route: string, collection: SidenavItem[]) {
    let result = _.find(collection, { route });
    if (!result) {
      _.each(collection, (item) => {
        if (item.hasSubItems()) {
          const found = this.findByRouteRecursive(route, item.subItems);

          if (found) {
            result = found;
            return false;
          }
        }
      });
    }

    return result;
  }

  get currentlyOpen() {
    return this._currentlyOpen;
  }

  private _buildSidenavItem(val: SidenavItemInterface[]) {
    return val.map((v) => {
      const item = this._addItem(v.name, v.icon, v.route);
      if (v.subItems && v.subItems.length > 0) {
        this._addSubItemRecursive(v.subItems, item);
      }
      return item;
    });
  }

  private _addItem(name: string, icon: string, route: string) {
    const item = new SidenavItem({ name, icon, route, subItems: [] });

    return item;
  }

  private _addSubItemRecursive(val: SidenavSubItemInterface[], parent: SidenavItem) {
    val.forEach((v) => {
      const item = this._addSubItem(parent, v.name, v.route);
      if (v.subItems && v.subItems.length > 0) {
        this._addSubItemRecursive(v.subItems, item);
      }
    });
  }

  private _addSubItem(parent: SidenavItem, name: string, route: string) {
    const item = new SidenavItem({ name, route, parent, subItems: [] });
    parent.subItems.push(item);

    return item;
  }
}
