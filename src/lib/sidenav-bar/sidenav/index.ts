import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IconSidenavDirective } from './icon-sidenav.directive';
import { SidenavService } from './sidenav.service';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { SidenavComponent } from './sidenav.component';
import { SidenavItemModule } from '../sidenav-item/index';
import { NotifiyBadgeModule } from '../../notify-badge/index';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    SidenavItemModule,
    CommonModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    NotifiyBadgeModule,
    FormsModule,
    PerfectScrollbarModule.forChild(),
  ],
  exports: [
    SidenavComponent,
    IconSidenavDirective,
    PerfectScrollbarModule
  ],
  declarations: [SidenavComponent, IconSidenavDirective],
  providers: [],
})
export class SidenavModule { }
