import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SidenavItem, BriefcaseItem } from '../sidenav-item/sidenav-item.model';
import { SidenavService } from './sidenav.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toArray';
// import { toArray } from 'rxjs/add/operator/toArray';

@Component({
  selector: 'nt-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SidenavComponent implements OnInit, OnDestroy {
  logo = environment.logo;
  items: Observable<any[]>;
  lock = true;
  goToDeal = false;
  countBriefcaseItems = 0;
  view;
  // itemsArray = [];
  // private _itemsSubscription: Subscription;
  private briefcaseItems: Subscription;
  private _routerEventsSubscription: Subscription;

  constructor(
    private sidenavService: SidenavService,
    private router: Router,
  ) { }

  ngOnInit() {
    // Notice: use to toArray() on observable befoe iterating with NgFor
    // added itemsArray as a temp patch to avoid breaking errors.
    this.sidenavService.appMenu$.subscribe((view) => this.view = view);
    this.items = this.sidenavService.items$;
    this.briefcaseItems = this.sidenavService.countOpenedApps$
      .subscribe((count) => {
        if (count > this.countBriefcaseItems) {
          this.goToDeal = true;
          this.switchSideNavView('deal');
        }
        this.countBriefcaseItems = count;
        if (!count) {
          this.switchSideNavView('home');
        }
      });

    this._routerEventsSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.sidenavService.nextCurrentlyOpenByRoute(event.url);
        setTimeout(() => {
          window.dispatchEvent(new Event('resize'));
        }, 400);
      }
    });
  }
  switchSideNavView(val) {
    this.sidenavService.switchItemsStream(val);
  }
  toggleIconSidenav() {
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 300);

    this.sidenavService.isIconSidenav = !this.sidenavService.isIconSidenav;
  }

  isIconSidenav(): boolean {
    return this.sidenavService.isIconSidenav;
  }

  ngOnDestroy() {
    this._routerEventsSubscription.unsubscribe();
  }
}
