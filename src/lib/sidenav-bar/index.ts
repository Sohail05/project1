import { NgModule, ModuleWithProviders } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { SidenavModule } from './sidenav/index';
import { SidenavItemModule } from './sidenav-item/index';
import { SideNavBarComponent } from './sidenav-bar.component';
import { SidenavService } from './sidenav/sidenav.service';
import { TopNavBarModule } from '../top-nav-bar/index';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    SidenavModule,
    SidenavItemModule,
    TopNavBarModule,
    RouterModule,
  ],
  exports: [SideNavBarComponent],
  declarations: [SideNavBarComponent],
})
export class SidenavBarModule {
  static forRoot(): ModuleWithProviders {
        return {
            ngModule: SidenavBarModule,
            providers: [SidenavService],
        };
    }
}
