import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nt-notify-badge',
  templateUrl: './notify-badge.component.html',
  styleUrls: ['./notify-badge.component.scss']
})
export class NotifyBadgeComponent implements OnInit {
  @Input() count: number;
  @Input() icon = 'notifications';
  isAllRead = false;
  constructor() { }

  ngOnInit() {
  }

}
