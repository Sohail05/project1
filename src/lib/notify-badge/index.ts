import { NgModule } from '@angular/core';
import { NotifyBadgeComponent } from './notify-badge.component';
import { MdIconModule } from '@angular/material';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, MdIconModule],
  exports: [NotifyBadgeComponent],
  declarations: [NotifyBadgeComponent],
  providers: [],
})
export class NotifiyBadgeModule { }
