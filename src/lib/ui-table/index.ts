import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DataTableModule } from './data-table';
import { UITableComponent } from './ui-table.component';
import { MultiLinguaModule } from '../multi-lingua/index';
export * from './data-table';

@NgModule({
    imports: [DataTableModule, FormsModule, CommonModule, MaterialModule, MultiLinguaModule],
    exports: [UITableComponent],
    declarations: [UITableComponent],
    providers: [],
})
export class UITableModule { }
