export interface TableSetting {
    key: string,
    label: string,
    type?: 'currency' | 'number' | 'date' | 'array' | 'conditional' | 'function';
    currency?: string;
    currencyKey?: string;
    symbol?: boolean;
    decimal?: string;
    date?: string;
    toArray?: string;
    conditions?: object;
    appendIcon?: string;
    sub?: string;
    tag?: string;
    fn?: (x) => string;
}

