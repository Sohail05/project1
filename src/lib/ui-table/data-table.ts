import { CommonModule } from '@angular/common';
import {
  Component,
  Directive,
  DoCheck,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  IterableDiffer,
  IterableDiffers,
  ModuleWithProviders,
  NgModule,
  OnInit,
  Optional,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { FormsModule } from '@angular/forms';

export class Md2PaginationChange {
  source: PaginationComponent;
  activePage: number;
}

export interface ISortEvent {
  sortBy: string | string[];
  sortOrder: string;
}

export interface IPageEvent {
  activePage: number;
  rowsPerPage: number;
  dataLength: number;
}

export interface IDataEvent {
  length: number;
}

@Directive({
  selector: 'table[ntData]',
  exportAs: 'ntDataTable',
})
export class DataTableDirective implements DoCheck {

  private diff: IterableDiffer<string>;
  private isDataChanged = false;
  private _data: any[] = [];
  public _activePage = 1;
  private _rowsPerPage = 1000;
  private _sortBy: string | string[] = '';
  private _sortOrder = 'asc';

  data: any[];

  @Input()
  get ntData() { return this._data; }
  set ntData(value: any[]) {
    if (this._data !== value) {
      this._data = value || [];
      this.recalculatePage();
      this.isDataChanged = true;
    }
  }

  @Input()
  get activePage() { return this._activePage; }
  set activePage(value: number) {
    if (this._activePage !== value) {
      this._activePage = value;
    }
  }

  @Input()
  get rowsPerPage() { return this._rowsPerPage; }
  set rowsPerPage(value: number) {
    if (this._rowsPerPage !== value) {
      this._rowsPerPage = value;
      this.setPage(this.activePage, value);
      this.isDataChanged = true;
    }
  }

  @Input()
  get sortBy() { return this._sortBy; }
  set sortBy(value: string | string[]) {
    if (this._sortBy !== value) {
      this._sortBy = value;
      if (value) {
        this.onSortChange.next({ sortBy: this.sortBy, sortOrder: this.sortOrder });
      }
      this.isDataChanged = true;
    }
  }

  @Input()
  get sortOrder() { return this._sortOrder; }
  set sortOrder(value: string) {
    if (!(value === 'asc' || value === 'desc')) {
      console.warn('sortOrder value must be one of ["asc", "desc"], but is:', value);
      value = 'asc';
    }
    if (this._sortOrder !== value) {
      this._sortOrder = value;
      this.isDataChanged = true;
    }
  }

  @Output() activePageChange = new EventEmitter<number>();
  @Output() sortByChange = new EventEmitter<string | string[]>();
  @Output() sortOrderChange = new EventEmitter<string>();

  onSortChange = new EventEmitter<ISortEvent>();
  onPageChange = new EventEmitter<IPageEvent>();

  constructor(private differs: IterableDiffers) {
    this.diff = differs.find([]).create(null);
  }

  ngDoCheck(): any {
    const changes = this.diff.diff(this.ntData);
    if (changes) {
      this.recalculatePage();
      this.isDataChanged = true;
    }
    if (this.isDataChanged) {
      this.fillData();
      this.isDataChanged = false;
    }
  }

  getSort(): ISortEvent {
    return { sortBy: this.sortBy, sortOrder: this.sortOrder };
  }

  setSort(sortBy: string | string[], sortOrder: string) {
    if (this.sortBy !== sortBy || this.sortOrder !== sortOrder) {
      this.sortBy = sortBy;
      this.sortOrder = sortOrder;
      this.isDataChanged = true;
      this.onSortChange.next({ sortBy, sortOrder });
      this.sortByChange.emit(this.sortBy);
      this.sortOrderChange.emit(this.sortOrder);
    }
  }

  getPage(): IPageEvent {
    return {
      activePage: this.activePage,
      rowsPerPage: this.rowsPerPage,
      dataLength: this.ntData.length,
    };
  }

  setPage(activePage: number, rowsPerPage: number): void {
    if (this.rowsPerPage !== rowsPerPage || this.activePage !== activePage) {
      this.activePage = this.activePage !== activePage ?
        activePage : this.calculateNewActivePage(this.rowsPerPage, rowsPerPage);
      this.rowsPerPage = rowsPerPage;
      this.isDataChanged = true;
      this.onPageChange.emit({
        activePage: this.activePage,
        rowsPerPage: this.rowsPerPage,
        dataLength: this.ntData ? this.ntData.length : 0,
      });
      this.activePageChange.emit(this.activePage);
    }
  }

  private calculateNewActivePage(previousRowsPerPage: number, currentRowsPerPage: number): number {
    const firstRowOnPage = (this.activePage - 1) * previousRowsPerPage + 1;
    const newActivePage = Math.ceil(firstRowOnPage / currentRowsPerPage);
    return newActivePage;
  }

  private recalculatePage() {
    const lastPage = Math.ceil(this.ntData.length / this.rowsPerPage);
    if (lastPage < this.activePage) {
      this._activePage = lastPage || 1;
      setTimeout(() => {
        this.activePageChange.emit(this.activePage);
      }, 10);
    } else { }

    this.onPageChange.emit({
      activePage: this.activePage,
      rowsPerPage: this.rowsPerPage,
      dataLength: this.ntData.length,
    });
  }

  private fillData() {
    const offset = (this.activePage - 1) * this.rowsPerPage;
    let data = this.ntData;
    if (this.sortBy) {
      data = data.sort((a: any, b: any) => {
        const x = this.caseInsensitiveIteratee(a);
        const y = this.caseInsensitiveIteratee(b);
        return (x > y) ? 1 : (y > x) ? -1 : 0;
      });
    }
    if (this.sortOrder === 'desc') { data.reverse(); }
    this.data = data.slice(offset, offset + this.rowsPerPage);
  }

  private caseInsensitiveIteratee(value: any) {
    if (typeof this.sortBy === 'string' || this.sortBy instanceof String) {
      for (const sortByProperty of this.sortBy.split('.')) {
        value = value[sortByProperty];
      }
    } else {
      value = value[this.sortBy + ''];
    }
    if (value && typeof value === 'string' || value instanceof String) {
      return value.toLowerCase();
    }
    return value;
  }

}

@Component({
  selector: 'nt-sortby',
  templateUrl: './sort.html',
  styleUrls: ['./data-table.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DataTableSortByComponent implements OnInit {

  @Input() ntSortBy: string;
  @HostBinding('class.md2-sort-active') _isAsc = false;
  @HostBinding('class.md2-sort-active') _isDesc = false;

  constructor(private _md2Table: DataTableDirective) {
  }

  ngOnInit() {
    this._md2Table.onSortChange.subscribe((event: ISortEvent) => {
      this._isAsc = (event.sortBy === this.ntSortBy && event.sortOrder === 'asc');
      this._isDesc = (event.sortBy === this.ntSortBy && event.sortOrder === 'desc');
    });
  }

@HostListener('click')
  _sort() {
    if (this._isAsc) {
      this._md2Table.setSort(this.ntSortBy, 'desc');
    } else {
      this._md2Table.setSort(this.ntSortBy, 'asc');
    }
  }

}

@Component({
  selector: 'nt-pagination',
  templateUrl: './pagination.html',
  styleUrls: ['./data-table.scss'],
  exportAs: 'md2Pagination',
  encapsulation: ViewEncapsulation.None,
})
export class PaginationComponent implements DoCheck {

  public _activePage = 1;

  @Input() rowsPerPageSet: any = [];
  @Input() md2Table: DataTableDirective;

  _rowsPerPage: number;
  public _dataLength = 0;
  _lastPage: number;

  constructor( @Optional() private _dataTable: DataTableDirective) { }

  ngDoCheck() {
    this.md2Table = this.md2Table || this._dataTable;
    this.onPageChangeSubscriber(this.md2Table.getPage());
    this.md2Table.onPageChange.subscribe(this.onPageChangeSubscriber);
  }

  _setPage(pageNumber: number): void {
    this.md2Table.setPage(pageNumber, this._rowsPerPage);
  }

  _setRows(event: any): void {
    this.md2Table.setPage(this._activePage, parseInt(event.target.value, 10));
  }

  private onPageChangeSubscriber = (event: IPageEvent) => {
    this._activePage = event.activePage;
    this._rowsPerPage = event.rowsPerPage;
    this._dataLength = event.dataLength;
    this._lastPage = Math.ceil(this._dataLength / this._rowsPerPage);
  }

}

export const NT_DATA_TABLE_DIRECTIVES: any[] = [
  DataTableDirective,
  DataTableSortByComponent,
  PaginationComponent,
];

@NgModule({
  imports: [CommonModule, FormsModule],
  exports: NT_DATA_TABLE_DIRECTIVES,
  declarations: NT_DATA_TABLE_DIRECTIVES,
})
export class DataTableModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DataTableModule,
    };
  }
}
