import { Component, EventEmitter, Input, OnInit, Output, OnChanges } from '@angular/core';
import { TableSetting } from './table.model';

@Component({
  selector: 'nt-table',
  templateUrl: './ui-table.component.html',
  styleUrls: ['./ui-table.component.scss'],
})
export class UITableComponent implements OnInit, OnChanges {
  @Input() setting: Array<any> = [];
  @Input() tableData: any[] = [];
  @Input() totalData: any[] = [];
  @Input() pageSizes: number[] = [];
  @Input() defaultPageSize = 10;
  @Input() fixedSize = 0;
  @Input() action = 'none';
  @Input() requiredPagination = true;
  @Input() totalFooter = false;
  @Input() leftIcon: string;
  @Input() rightIcon: string;
  @Input() type;
  @Input() accessControl: object = {};
  @Input() splitData = true;
  @Output() showData = new EventEmitter();
  @Output() removeData = new EventEmitter();
  @Output() viewData = new EventEmitter();
  md2;
  colSize = 1;
  constructor() { }

  ngOnInit() {
    this.setTableCol();
    this.setDefault();
  }

  setTableCol() {
    this.colSize = this.setting.length;

    if (this.action === 'view') {
      this.colSize += 1;
    } else if (this.action === 'edit') {
      this.colSize += 2;
    }

    if (this.accessControl['edit'] || this.accessControl['view']) {
      this.colSize += 1;
    }
    if (this.accessControl['delete']) {
      this.colSize += 1;
    }

  }

  setDefault() {
    if (this.requiredPagination === false) {
      if (this.fixedSize > 0) {
        this.defaultPageSize = this.fixedSize;
      } else {
        this.defaultPageSize = this.tableData.length;
      }
      this.pageSizes = [];
    }
  }

  ngOnChanges() {
    if (this.tableData) {
      this.setDefault();
    }
  }

  onRecordClicked(i, e) {
    e.stopPropagation();
    e.preventDefault();
    if (this.accessControl['edit']) {
      this.showData.emit(i);
    }

    if (this.accessControl['view']) {
      this.viewData.emit(i);
    }
  }
  remove(i, e) {
    e.stopPropagation();
    e.preventDefault();
    this.removeData.emit(i);
  }

  toArray(obj) {
    let arr = [];
    if (obj) {
      if (obj instanceof Array) {
        return obj;
      } else if (Object.keys(obj).length > -1) {
        // convert object to array
        arr = Object.keys(obj).map((key) => {
          return obj[key];
        });
        // remove empty
        arr = arr.filter((d) => { return !!d; });
      }
    }
    return arr;
  }

  getData(path: string, object: any) {
    if (path) {
      return path.split('.').reduce((o, k) => (o || {})[k], object)
    }
  }

}
