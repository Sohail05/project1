export function CatchExceptions() {
    return (target, propertyKey: string, descriptor: PropertyDescriptor) => {
        // original method
        const original = descriptor.value;

        // override method to have try catch
        descriptor.value = function () {
            try {
                original.apply(this, arguments);
            } catch (exception) {
                console.error(exception);
                throw exception;
            }
        }
        return descriptor;
    }
}
