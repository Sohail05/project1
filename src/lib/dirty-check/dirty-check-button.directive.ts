import { Directive, ElementRef, Input, HostBinding, OnDestroy, OnInit, AfterContentInit, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DirtyCheckService } from '../core/dirty-check.service';
import { MultiLinguaService } from '../multi-lingua/multi-lingua.service';

@Directive({
    selector: '[ntDirtyCheckButton]'
})
export class DirtyCheckButtonDirective implements AfterContentInit, OnDestroy {
    @Input() indicator;
    @Input() dirtycheck;
    @HostBinding() disabled = true;
    private statusSubscriber: Subscription;
    private textbox;
    private textboxMsg;
    private parentEle;
    private msg;

    constructor(
        private _dirtyCheckService: DirtyCheckService,
        private _elementRef: ElementRef,
        private _renderer: Renderer2,
        private multiLinguaService: MultiLinguaService,
    ) {
    }

    ngAfterContentInit() {
        // this.createMsg;
        // this.showMsg();
        // this.subscribeChange();
        if (this.dirtycheck) {
            this.multiLinguaService.translate('cmo.lbl.no.cge').then((x) => {
                this.msg = x ? x.msg : '';
                this.createMsg();
                this.showMsg();
                this.subscribeChange();
            });
        } else {
            this.disabled = false;
        }
    }

    subscribeChange() {
        this.statusSubscriber = this._dirtyCheckService.statusChange.subscribe((x) => {
            this.disabled = !this._dirtyCheckService.getDirtyStatus(this.indicator);
            this.disabled ? this.showMsg() : this.hideMsg();
        });
    }

    ngOnDestroy() {
        if (this.statusSubscriber) {
            this.statusSubscriber.unsubscribe();
        }
    }

    createMsg() {
        this.textbox = this._renderer.createElement('span');
        this.textbox.classList.add('dirtyMsg');
        this.textboxMsg = this._renderer.createText(this.msg);
        this._renderer.appendChild(this.textbox, this.textboxMsg);
        this.parentEle = this._renderer.parentNode(this._elementRef.nativeElement);
        this._renderer.appendChild(this.parentEle, this.textbox);
    }

    showMsg() {
        this.textbox.setAttribute('style', 'visibility:visible');
    }

    hideMsg() {
        // if (this._renderer.parentNode(this.textbox) === this.parentEle) {
        //     this._renderer.removeChild(this.parentEle, this.textbox);
        // }
        this.textbox.setAttribute('style', 'visibility:hidden');
    }
}
