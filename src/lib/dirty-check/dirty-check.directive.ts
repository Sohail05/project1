import { Directive, Optional, Self, AfterContentInit, OnDestroy, Input } from '@angular/core';
import { FormGroupDirective, NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DirtyCheckService } from '../core/dirty-check.service';

@Directive({
  selector: '[ntDirtyCheck]'
})
export class DirtyCheckDirective implements AfterContentInit, OnDestroy {
  @Input() indicator;
  @Input() dirtycheck = true;
  private initForm;
  private formChangeSubscriber: Subscription;
  private formClearSubscriber: Subscription;
  private formClearAllSubscriber: Subscription;
  constructor(
    private _dirtyCheckService: DirtyCheckService,
    @Optional() private _parentFormGroup: FormGroupDirective,
    @Optional() private _parentForm: NgForm,
  ) {

  }
  ngAfterContentInit() {
    if (this.dirtycheck) {
      this.getInitData();
      this.subscribeChange();
    }
  }
  getInitData(update = false) {
    if (this._parentFormGroup) {
      this.initForm = this._parentFormGroup.control.value;

    }
    if (this._parentForm) {
      this.initForm = this._parentForm.control.value;
    }
    const formKeys = JSON.stringify(Object.keys(this.initForm));
    if (!this._dirtyCheckService.getCurrentFormValue(formKeys) || update) {
      this._dirtyCheckService.setCurrentFormValue(formKeys, Object.assign({}, this.initForm));
    }
  }
  subscribeChange() {
    if (this._parentFormGroup) {
      this.formChangeSubscriber = this._parentFormGroup.control.valueChanges.subscribe((x) => {
        this.compareForm(x);
      });
    }
    if (this._parentForm) {
      this.formChangeSubscriber = this._parentForm.control.valueChanges.subscribe((x) => {
        this.compareForm(x);
      });
    }
    this.formChangeSubscriber = this._dirtyCheckService.cleared.subscribe((x) => {
      if (this.indicator === x) {
        this.getInitData();
      }
    });
    this.formClearAllSubscriber = this._dirtyCheckService.clearedAll.subscribe((x) => {
      this.getInitData(true);
    });
  }
  compareForm(val) {
    let dirtyVal = false;
    if (this.compareRecursive(this.initForm, val)) {
      dirtyVal = true;
    }
    if (this.compareRecursive(val, this.initForm)) {
      dirtyVal = true
    }

    this._dirtyCheckService.setDirtyStatus(dirtyVal, this.indicator);
  }

  compareRecursive(var1, var2) {
    let dirtyVal = false;
    Object.keys(var1).map((x) => {
      if (typeof var1[x] !== 'object' && var2[x] !== 'object') {
        if (this.diff(var1[x], var2[x])) {
          dirtyVal = true;
        }
      } else if (var1[x] && var2[x]) {
        if (this.compareRecursive(var1[x], var2[x])) {
          dirtyVal = true;
        }
      }
    });
    return dirtyVal;
  }

  diff(var1, var2) {
    let diffVal = false;
    if (var1 && var2) {
      if (var1 !== var2) {
        diffVal = true;
      }
    } else {
      if (!var1 !== !var2) {
        diffVal = true;
      }
    }
    return diffVal;
  }

  ngOnDestroy() {
    if (this.formChangeSubscriber) {
      this.formChangeSubscriber.unsubscribe();
    }
    if (this.formClearAllSubscriber) {
      this.formClearAllSubscriber.unsubscribe();
    }
    if (this.formClearSubscriber) {
      this.formClearSubscriber.unsubscribe();
    }
  }
}
