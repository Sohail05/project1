import { NgModule } from '@angular/core';
import { DirtyCheckDirective } from './dirty-check.directive';
import { DirtyCheckButtonDirective } from './dirty-check-button.directive';

@NgModule({
    imports: [],
    declarations: [DirtyCheckDirective, DirtyCheckButtonDirective],
    exports: [DirtyCheckDirective, DirtyCheckButtonDirective],
    providers: []
})
export class DirtyCheckModule { }
export * from './dirty-check';
