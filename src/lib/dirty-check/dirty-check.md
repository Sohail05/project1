## Dirty Check

`ntDirtyCheck` directive for form  
`ntDirtyCheckButton` directive for submit button of form


## Example of Dirty Check
HTML File
```html
<form ntDirtyCheck 
   <button ntDirtyCheckButton> </button>  
</form>
```

## Example of disable navigation to other route or close screen

Component File
```javascript

 export class TestPageComponent extends DirtyCheck{
  indicator; // used to set the form indicator

  constructor(private dirtyCheckService: DirtyCheckService) {
    super(dirtyCheckService);
  }
}
```

Route File

```javascript
{ path: 'test-page', canDeactivate: [DirtyCheckGuardService], component: TestPageComponent },
```

## API References
`ntDirtyCheck`
|Name|Description|
|----|-----------|
|@Input() indicator|_**`string`**_<br>use to indicate form| 
|@Input() notCheck|_**`boolean`**_<br> true will disable dirty check| 

`ntDirtyCheckButton`
|Name|Description|
|----|-----------|
|@Input() indicator|_**`string`**_<br>use to indicate form| 


## Method
`DirtyCheckService` 
|Name|Description|
|----|-----------| 
|getDirtyStatus|indicator(_**`string`**_)<br>get form status from service|
|setDirtyStatus|status(_**`boolean`**_),indicator(_**`string`**_)<br>set form  status to service| 
