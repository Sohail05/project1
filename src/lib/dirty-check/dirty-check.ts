import { DirtyCheckService } from '../core/dirty-check.service';
import { HostListener } from '@angular/core';
export class DirtyCheck {
    indicator;
    private _dirtyCheckService: DirtyCheckService;
    @HostListener('window:beforeunload', ['$event'])
    comfirm(event) {
        if (this._dirtyCheckService.getDirtyStatus(this.indicator)) {
            return false;
        }
    }
    constructor(dirtyCheckService: DirtyCheckService) {
        this._dirtyCheckService = dirtyCheckService;
    }

    canDeactivate() {
        if (this._dirtyCheckService.getDirtyStatus(this.indicator)) {
            return this._dirtyCheckService.promptComfirm();
        } else {
            return true;
        }
    }
}
