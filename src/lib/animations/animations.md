### Animations

A set of transitions and animations are provided with the framework.

To make use of them, import and declare the animation on the `animations` property of the component meta data.

```typescript
import { fadeInSlideInY } from '../animations/animations';

@Component({
...
  animations: [fadeInSlideInY],
})
```

How to use them in templates:

```html
<div [@fadeInSlideInY] ></div>
```