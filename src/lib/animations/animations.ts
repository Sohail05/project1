import {
  animate,
  AnimationEntryMetadata,
  state,
  style,
  transition,
  trigger,
} from '@angular/core';

// Component transition animations
export const slideToLeftAnimation: AnimationEntryMetadata =
  trigger('routeAnimation', [
    state('void', style({ width: '100%' })),
    state('*', style({ width: '100%' })),
    transition(':enter', [
      style({ position: 'fixed', transform: 'translateX(100%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' })),
    ]),
    transition(':leave', [
      style({ position: 'fixed', transform: 'translateX(0%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' })),
    ]),
  ]);

export const slideToRightAnimation: AnimationEntryMetadata =
  trigger('routeAnimation', [
    state('void', style({ width: '100%' })),
    state('*', style({ width: '100%' })),
    transition(':enter', [
      style({ position: 'fixed', transform: 'translateX(-100%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' })),
    ]),
    transition(':leave', [
      style({ position: 'fixed', transform: 'translateX(0%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateX(100%)' })),
    ]),
  ]);

export const slideToBottomAnimation: AnimationEntryMetadata =
  trigger('routeAnimation', [
    state('void', style({ width: '100%', height: '100%' })),
    state('*', style({ width: '100%', height: '100%' })),
    transition(':enter', [
      style({ position: 'fixed', transform: 'translateY(-100%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' })),
    ]),
    transition(':leave', [
      style({ position: 'fixed', transform: 'translateY(0%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateY(100%)' })),
    ]),
  ]);

export const slideToTopAnimation: AnimationEntryMetadata =
  trigger('routeAnimation', [
    state('void', style({ width: '100%', height: '100%' })),
    state('*', style({ width: '100%', height: '100%' })),
    transition(':enter', [
      style({ position: 'fixed', transform: 'translateY(100%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' })),
    ]),
    transition(':leave', [
      style({ position: 'fixed', transform: 'translateY(0%)' }),
      animate('0.5s ease-in-out', style({ transform: 'translateY(-100%)' })),
    ]),
  ]);

export const fadeInSlideInY: AnimationEntryMetadata =
  trigger('fadeInSlideInY', [
    transition(':enter', [
      style({ transform: 'translateY(-100%)', opacity: 0 }),
      animate('300ms', style({ transform: 'translateY(0)', opacity: 1 })),
    ]),
    transition(':leave', [
      style({ transform: 'translateY(0)', opacity: 1 }),
      animate('200ms', style({ transform: 'translateY(-100%)', opacity: 0 })),
    ]),
  ]);
