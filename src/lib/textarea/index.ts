import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { ControlMessageModule } from '../control-message/index';
import { CommonModule } from '@angular/common';
import { TextareaComponent } from './textarea.component';
import { MultiLinguaTooltipModule } from '../tooltip/index';

@NgModule({
  imports: [
    CommonModule,
    ControlMessageModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    MultiLinguaTooltipModule,
    FormsModule
  ],
  exports: [TextareaComponent],
  declarations: [TextareaComponent],
})
export class TextareaModule { }
