import { Component, Input, OnInit, SimpleChanges, OnChanges, HostListener, ViewChild, Optional, Host } from '@angular/core';
import { ValidationService } from './../core/validation.service';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { FormGroup, Validators, FormControl, ControlValueAccessor, NgControl, NgForm, FormGroupDirective } from '@angular/forms';
import { IUIComponent, IUIComponentData } from '../ui-component/ui-component';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  providers: [
    MakeProvider(TextareaComponent),
    MakeLocaleMessageProvider(TextareaComponent)
  ],
})
export class TextareaComponent implements OnInit, IUIComponent, ControlValueAccessor {
  private _value;
  get value() {
    return this._value;
  }

  set value(val: any) {
    if (this._value !== val) {
      this._value = val;
      this.propagateChange(val);
      if (this.parent && this.key) {
        this.control.setValue(val);
      }
    }
    this.setErrors();
  }
  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(val: any): void {
    this._value = val;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }
  data = <IUIComponentData>{};
  @Input() parent: FormGroup;
  @Input() key: string;
  @Input() placeholder: string;
  @Input() isDisabled: boolean | string = false;
  @Input() required = null;
  @Input() rows: number;
  @Input() maxRows: number;
  @Input() maxLength: number;
  // @Input() validation: object;
  tooltip;
  showTooltip;
  control: FormControl;

  get _disabled() {
    return this.isDisabled || (this.parent && this.key && (this.parent.disabled || this.control.disabled)) || this.data.disabled;
  }
  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }
  @ViewChild('inputControl') inputControl: NgControl;
  constructor(
    // private formService: FormService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
    private validationService: ValidationService
  ) {
  }

  ngOnInit() {

    // this.setValidators();
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }

    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      this._value = this.control.value;

      this.control.valueChanges.subscribe((val) => {
        if (val !== this.value) {
          this._value = val;
        }
      })
    }
  }

  setErrors() {
    if (this.parent && this.key) {
      this.control.setErrors(this.inputControl.errors);
    }
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControl.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }
}
