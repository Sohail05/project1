import * as d3 from 'd3';
class BarModel {
  value: number;
  type: string;
}
export default (() => {
  // set the dimensions and margins of the graph
  const margin = { top: 20, right: 20, bottom: 30, left: 80 };
  let chartOptions;
  let data;
  const defaultOptions = {
    id: '',
    data: [],
    width: 600,
    height: 300,
    colorFill: '#90CAF9',
    endOfBarLabel: false,
  };

  function init(options) {
    chartOptions = Object.assign(defaultOptions, options);

    data = chartOptions.data;
    const width = chartOptions.width - margin.left - margin.right;
    const height = chartOptions.height - margin.top - margin.bottom;
    // set the ranges
    const y = d3.scaleBand().range([height, 0]);

    const x = d3.scaleLinear().range([0, width - margin.right * 2]);

    const svg = d3.select(chartOptions.id)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom);

    const g = svg.append('g')
      .attr('class', chartOptions.type)
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    // format the data
    data.forEach((d) => {
      d.value = +d.value;
    });

    // Scale the range of the data in the domains
    // x.domain([0, d3.max(data, (d: BarModel) => d.value)]).nice();
    y.domain(data.map((d: BarModel) => d.type)).padding(0.2);
    // add the y Axis
    const yAxis = g.append('g').call(d3.axisLeft(y));
    // append the rectangles for the bar chart
    const bars = g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
      .attr('height', y.bandwidth())
      .attr('y', (d: BarModel) => y(d.type));

    switch (chartOptions.type) {
      case 'no-axis-line':
        x.domain([0, d3.max(data, (d: BarModel) => d.value)]);
        bars.attr('class', 'bar')
          .attr('x', 0)
          .attr('width', (d: BarModel) => x(d.value))
          .style('fill', chartOptions.colorFill);
        break;
      case 'diverge-at-zero':
        x.domain(d3.extent(data, (d: BarModel) => d.value)).nice();
        // yAxis.attr('transform', 'translate(' + x(0) + ',0)');
        bars.attr('class', (d: BarModel) => 'bar bar--' + (d.value < 0 ? 'negative' : 'positive'))
          .attr('x', (d: BarModel) => x(Math.min(0, d.value)))
          .attr('width', (d: BarModel) => Math.abs(x(d.value) - x(0)));
        break;
      default:
        bars.attr('class', 'bar')
          .attr('x', 0)
          .attr('width', (d: BarModel) => x(d.value))
          .style('fill', chartOptions.colorFill);
    }

    if (chartOptions.endOfBarLabel) {
      g.selectAll('.value')
        .data(data)
        .enter().append('text')
        .attr('class', 'value')
        .attr('y', (d: BarModel) => y(d.type) + y.bandwidth() / 2)
        .attr('dy', '.35em') // vertical align middle
        .attr('x', (d: BarModel) => x(d.value))
        .attr('dx', '.35em') // margin-left
        .attr('text-anchor', 'start')
        .text((d: BarModel) => d.value);
    } else {
      // add the x Axis
      g.append('g')
        .attr('transform', 'translate(0,' + height + ')')
        .call(d3.axisBottom(x));
    }
  }

  return { init };

})();
