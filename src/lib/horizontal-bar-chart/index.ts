import { NgModule } from '@angular/core';

import { HorizontalBarChartComponent } from './horizontal-bar-chart.component';

@NgModule({
    imports: [],
    exports: [],
    declarations: [HorizontalBarChartComponent],
    providers: [],
})
export class HorizontalBarChartModule { }
