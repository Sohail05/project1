import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import horizontalBarChart from './horizontal-bar-chart';

@Component({
  selector: 'nt-horizontal-bar-chart',
  templateUrl: './horizontal-bar-chart.component.html',
  styleUrls: ['./horizontal-bar-chart.component.scss'],
})
export class HorizontalBarChartComponent implements OnInit {
  @ViewChild('chart') private chartContainer: ElementRef;
  @Input() data: Object[];
  @Input() height: number;
  @Input() container;
  @Input() offSetWidth: number;
  @Input() chartType: string;
  @Input() endOfBarLabel: boolean;
  constructor() { }

  ngOnInit() {
    const computedWidth = this.chartContainer.nativeElement.offsetWidth + this.offSetWidth;

    horizontalBarChart.init({
      type: this.chartType,
      id: this.container,
      data: this.data,
      width: computedWidth,
      height: this.height,
      colorFill: '#1976D2',
      endOfBarLabel: this.endOfBarLabel,
    });
  }

}
