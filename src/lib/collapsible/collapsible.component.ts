import { Component, Input, OnInit } from '@angular/core';
import { fadeInSlideInY } from '../animations/animations';

@Component({
  selector: 'nt-collapsible',
  templateUrl: './collapsible.component.html',
  styleUrls: ['./collapsible.component.scss'],
  animations: [fadeInSlideInY],
})
export class CollapsibleComponent implements OnInit {
  @Input() collapse = true;
  constructor() {
  }

  ngOnInit() {
  }

}
