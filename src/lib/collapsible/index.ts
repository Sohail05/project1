import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CollapsibleComponent } from './collapsible.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FlexLayoutModule
    ],
    exports: [CollapsibleComponent],
    declarations: [CollapsibleComponent],
    providers: [],
})
export class CollapsibleModule { }
