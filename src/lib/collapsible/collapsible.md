# Collapsible 

Use to Display collapsible content

## Example 
```html
    <nt-collapsible>
        <div collapsible-header>
         This is the header
        </div>
        <div collapsible-content>
         This is the collapsible content 
        </div>
    </nt-collapsible>
```


|Attribute|Description|
|------|-----|
|collapsible-header| Title displayed next to toggle |
|collapsible-content| The content view |