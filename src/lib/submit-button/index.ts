import { NgModule } from '@angular/core';
import { SubmitButtonComponent } from './submit-button.component';
import { MaterialModule } from '@angular/material';
import { MultiLinguaModule } from '../multi-lingua/index';
import { DirtyCheckModule } from '../dirty-check/index';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [CommonModule, MaterialModule, MultiLinguaModule, DirtyCheckModule],
    exports: [SubmitButtonComponent],
    declarations: [SubmitButtonComponent],
    providers: [],
})
export class SubmitButtonModule { }
