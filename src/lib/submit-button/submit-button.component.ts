import { Component, EventEmitter, Input, OnInit, Output, Optional, Host } from '@angular/core';
import { FormGroup, NgForm, FormGroupDirective } from '@angular/forms';
import { DirtyCheckService } from '../core/dirty-check.service';

@Component({
  selector: 'nt-submit-button',
  templateUrl: './submit-button.component.html',
  styleUrls: ['./submit-button.component.scss'],
})
export class SubmitButtonComponent implements OnInit {
  // will be remove
  @Input() parent: FormGroup;
  @Input() mandatory = true;
  @Input() isDisabled = false;
  @Input() dirtycheck;
  @Input() indicator;
  // @Input() resetLocaleCode = 'cmo.lbl.reset';
  @Input() resetButton = true;
  @Output() submit = new EventEmitter<any>();
  @Output() reset = new EventEmitter<any>();
  @Output() clear = new EventEmitter<any>();

  private form: NgForm | FormGroupDirective;
  constructor(
    private dirtyCheckService: DirtyCheckService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,

  ) { }

  Submit() {
    this.submit.emit();
  }

  Reset() {
    const key = JSON.stringify(Object.keys(this.form.value));
    this.form.resetForm(this.dirtyCheckService.getCurrentFormValue(key));
    this.reset.emit();
  }

  Clear() {
    this.form.resetForm();
    this.clear.emit();
  }

  ngOnInit() {
    if (this._parentForm) {
      this.form = this._parentForm;
    }
    if (this._parentFormGroup) {
      this.form = this._parentFormGroup;
    }

  }

}
