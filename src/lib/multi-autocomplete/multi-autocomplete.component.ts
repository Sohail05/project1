import { Component, forwardRef, Input, OnInit, HostListener, Optional, Host, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormGroupDirective, FormGroup, FormControl, NgControl, NgForm } from '@angular/forms';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { MdAutocompleteTrigger } from '@angular/material';
import { CODE_TYPE_CONTAINER_TOKEN, CodeTypeContainer, MakeCodeTypeProvider } from '../code-type/code-type-container';
import { CodeType } from '../code-type/code-type';
import { AutocompleteComponent } from '../autocomplete/autocomplete.component';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { Functional } from '../core/functional';
import { ValueAccessorBase } from '../value-accessor-base/value-accessor-base';
@Component({
  selector: 'nt-multi-autocomplete',
  templateUrl: './multi-autocomplete.component.html',
  styleUrls: ['./multi-autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiAutocompleteComponent),
      multi: true,
    },
    MakeCodeTypeProvider(MultiAutocompleteComponent),
    MakeLocaleMessageProvider(MultiAutocompleteComponent)
  ],
})
export class MultiAutocompleteComponent
  implements OnInit, ControlValueAccessor, IUIComponent, CodeTypeContainer, LocaleMessage {

  data = <IUIComponentData>{};
  /*
   - Get a array for "list"
   - Display the list as a dropdown
   - Autocomplete text in input field
   - check for an input event
   - if filteredList.contains(val) then push val to selected
   - clear input field
  */

  // todo
  // Support an array of objects and a closure filter
  // todo fix layout and flex
  @ViewChild(MdAutocompleteTrigger) fx: MdAutocompleteTrigger;
  @ViewChild('inputControl') inputControl: NgControl;
  // @depricated
  @Input() set fn(val) {
    this.displayFn = val;
    console.warn('Property "fn" is depricated');
  }

  showTooltip;
  tooltip;
  @Input() displayFn = Functional.codeDescription;
  storefn = Functional.codeValue;
  disabledFn = Functional.isDisabled;

  auto;
  // showRequired: boolean;
  // formSubmission: Subscription;
  control: FormControl;
  // Depricated
  @Input() set list(val) {
    this.options = val
    console.warn('Property "list" is depricated');
  }
  @Input() options: CodeType[];
  // todo: refactorings or create base class
  @Input() parent: FormGroup;
  @Input() key: string;
  @Input() value;
  @Input() placeholder = '';
  // required
  @Input() required = false;
  // disabled
  @Input() isDisabled: boolean | string = false;
  public selected = [];
  public query = '';
  public filteredList = [];

  get _disabled() {
    return (this.isDisabled || (this.parent && this.key && (this.parent.disabled || this.parent.get(this.key).disabled)));
  }

  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }

  loadCodeType(options: any) {
    this.options = options;
  };

  constructor(
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
    // private formService: FormService,
  ) {
    // super();
  }

  writeValue(val) {
    this.selected = [];
    // this.value = (typeof val === 'string') ? [val] : val;
    this.value = Array.isArray(val) ? val : [val];
    this.inject();
  }

  propagateChange = (val) => { };
  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateTouch = (val) => { };
  registerOnTouched(fn: any) {
    this.propagateTouch = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }

  ngOnInit() {

    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      this.value = this.control.value;
      this.selected = [];
      this.inject();
      this.control.valueChanges.subscribe((val) => {
        if (!Object.is(this.value, val)) {
          this.selected = [];
          this.value = Array.isArray(val) ? val : [val];
          this.inject();
        }
      });
    }

  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  // inject value to selected data
  inject() {
    if (this.value && Array.isArray(this.value)) {
      this.value.forEach((element) => {
        const val = this.options.find((opt) => this.storefn(opt) === element);
        this.query = this.displayFn(val);
        this.filter();
        this.addByInput();
      });
    }
  }

  remove(item) {
    this.selected.splice(this.selected.indexOf(item), 1);
    this.populateValue();
    this.clearQuery();
    this.setFormStatus();
  }

  // clear error status when value selected
  setFormStatus() {
    this.inputControl.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    if (this.required) {
      if (this.selected.length > 0) {
        this.inputControl.control.setErrors(null);
        if (this.parent && this.key) {
          this.control.setErrors(null);
        }
      } else {
        this.inputControl.control.setErrors({ required: true });
        if (this.parent && this.key) {
          this.control.setErrors({ required: true })
        }
      }
    }
    // reset control to untouch
    // if (!this._submitted) {
    // }
  }

  // select value
  select(item) {
    // Do not set disabled items
    if (item['vldIdc'] && item['vldIdc'] === 'N') {
      return;
    }

    this.selected.push(item);
    this.filteredList = [];
    this.populateValue();
    // re-focus when select value
    // this.setVal();
    // this.focus();
    this.clearQuery();
  }

  // populate value
  populateValue() {
    this.propagateChange(this.selected.map(this.storefn));
    if (this.parent && this.key) {
      this.control.setValue(this.selected.map(this.storefn));
    }
  }

  // clear the filter query
  clearQuery() {
    this.query = '';
  }

  // add active option to selected data
  add(event) {
    if (this.filteredList.length > 0) {
      if (this.fx.activeOption) {
        const selectedValue = this.fx.activeOption.value
        this.select(selectedValue);
      }
    }
    event.preventDefault();
    this.fx.closePanel();
  }

  // add filtered data from non user input
  addByInput() {
    if (this.filteredList.length === 1) {
      const value = this.filteredList[0];
      this.selected.push(value);
      // this.select(value);
      this.clearQuery();
    }
  }

  // filter option by query
  filter() {
    if (this.query !== '' && this.options && this.options.length > 0) {
      this.filteredList = this.options.filter(function (el) {
        return this.displayFn(el).toLowerCase().indexOf(this.query.toLowerCase()) > -1
          && !this.selected.includes(el);
      }.bind(this));
    } else {
      this.filteredList = [];
    }
  }

}
