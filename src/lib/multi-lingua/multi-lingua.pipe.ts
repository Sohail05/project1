import { Pipe, PipeTransform } from '@angular/core';
import { MultiLinguaService } from './multi-lingua.service';

@Pipe({name: 'multiLingua'})
export class MultiLinguaPipe implements PipeTransform {

    private _multiLinguaService: MultiLinguaService;

    constructor( multiLinguaService: MultiLinguaService) {
                this._multiLinguaService =  multiLinguaService;
    }

    transform(value: string): any {
        return this._multiLinguaService
        .translate(value)
        .then( (locale) => { return locale.msg });
    }



}
