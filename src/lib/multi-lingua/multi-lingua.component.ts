import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MultiLinguaService } from './multi-lingua.service';

@Component({
    selector: 'nt-multi-lingua',
    templateUrl: 'multi-lingua.component.html',
    providers: []
})
export class MultiLinguaComponent implements OnInit, OnChanges {

    /** i18n Meaning|Description */
    @Input() desc: string;
    /** Message Code from to resolve at API */
    @Input() msgCode: string;
    /** Override global language */
    @Input() langCode: string;
    text: string;

    private _multiLinguaService: MultiLinguaService;

    constructor( multiLinguaService: MultiLinguaService) {
        this._multiLinguaService =  multiLinguaService;
    }
    ngOnInit() {
        this.getTranslation();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.getTranslation();
    }

    getTranslation() {
        this._multiLinguaService
        .translate(this.msgCode, this.langCode)
        .then( locale => { this.text = locale.msg });
    }

    /** Dirty Check for multi-lingua text availability */
    set(): boolean {
        return this.text ? true : false;
    }

}
