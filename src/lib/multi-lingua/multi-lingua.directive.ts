import { Directive, OnChanges, SimpleChanges, OnInit, Input, Inject, Optional, Self, ElementRef } from '@angular/core';
import { MultiLinguaService } from './multi-lingua.service';
import { Locale } from '../core/model/locale';
import { LOCAL_MESSAGE_TOKEN, LocaleMessage } from '../core/locale-message';

@Directive({ selector: '[ntMsgCode]' })
export class MultiLinguaDirective implements OnInit, OnChanges {

    /** Message Code to retrive locale object */
    @Input() ntMsgCode: string;
    constructor( @Inject(LOCAL_MESSAGE_TOKEN) @Self() @Optional() private component: LocaleMessage,
        private multiLinguaService: MultiLinguaService,
        private el: ElementRef) {
    }

    ngOnInit() {
        this.getTranslation();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.getTranslation();
    }

    getTranslation() {
        this.multiLinguaService
            .translate(this.ntMsgCode)
            .then((locale) => this.component.loadLocaleMessage(locale))
            .catch(this.handleError.bind(this));
    }

    handleError(error) {
        // todo: send error message to backend
        console.error(error)
        console.error('Component does not implement loadLocaleMessage()', this.el.nativeElement);
    }
}
