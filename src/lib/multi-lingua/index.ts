import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultiLinguaComponent } from './multi-lingua.component';
import { MultiLinguaService } from './multi-lingua.service';
import { MultiLinguaDirective } from './multi-lingua.directive';
import { MultiLinguaPipe } from './multi-lingua.pipe';

@NgModule({
    imports: [CommonModule],
    exports: [MultiLinguaComponent, MultiLinguaDirective, MultiLinguaPipe],
    declarations: [MultiLinguaComponent, MultiLinguaDirective, MultiLinguaPipe],
    providers: [],
})
export class MultiLinguaModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: MultiLinguaModule,
            providers: [MultiLinguaService],
        };
    }
}
