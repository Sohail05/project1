import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Locale } from '../core/model/locale';
import { environment } from 'environments/environment';
import { ApiService } from '../core/api/api.service';
import { SessionService } from '../auth/session.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MultiLinguaService {

    /**
     * Promise to verify async batch loading
     */
    public loading: Promise<any>;

    /**
     * Cache Container of Locale strings
     */
    private locales: Array<Locale> = []

    constructor(
        private api: ApiService,
        private session: SessionService
    ) {}

    /**
     * Initialze batch loading
     */
    load(): Promise<any> {
        return this.loading = new Promise<any>((resolve, reject) => {
            this.requestAll().then((locales: Locale[]) => {
                if (Array.isArray(locales)) {
                    this.locales = locales;
                }
                resolve();
            })
        });
    }

    /**
     * Request locale objects associated to the application.
     * Add requests results to cache
     */
    requestAll(): Promise<Locale[]> {
        // todo: refactor into interfaces or wait on API models
        return this.api.get(
            environment.api.locales, { aplId: environment.applicationId, lang: this.session.lang })
            .toPromise()
            .then(this.setBatchLocale.bind(this))
    }

    /**
     * Request a single locale object
     * @param params
     */
    requestLocale(params): Promise<Locale> {
        return this.api.get(environment.api.localeCode, params)
            .toPromise()
            .then(this.setLocale)
    }

    /**
     * Retrive a string based on supplied locale code, uses setting language by default.
     * @param code locale code e.g. "cis.lbl.ciscuscttdtl.cttemail".
     * @param lang (optional) 2+ letter language code to override setting language.
     * @throws error when no local code is provided.
     */
    translate(code: string, lang = this.session.lang): Promise<Locale> {

        if (!code) {
            console.warn('No msg code supplied');
            return Observable.of({ msg: '', msgCode: '', tooltip: '' }).toPromise();
        }

        // Todo: refactor
        return this.loading.then(() => {
            const search = lang === this.session.lang ? this.findLocale(code) : null;
            if (search) {
                return search;
            } else {
                const params = { code, aplId: environment.applicationId, lang: lang }
                return this.requestLocale(params);
            }
        });
    }

    /**
     * Find locale object from cached batch.
     * @param code
     */
    findLocale(code) {
        return this.locales.find((locale) => { return locale.msgCode === code });
    }

    /**
     * Pass data along as Locale Object.
     * @param {null|locale} locale API response.
     */
    private setLocale(locale): Locale {
        if (locale) {
            return locale as Locale;
        } else {
            return { msg: '', msgCode: '', tooltip: '' } as Locale;
        }
    }

    /**
     * Pass data along as an array of Locale objects.
     * @param {null|Array<Local>} locales API response.
     */
    private setBatchLocale(locales): Locale[] {
        if (Array.isArray(locales)) {
            this.locales = this.locales.concat(locales);
        }
        return locales as Locale[];
    }

}
