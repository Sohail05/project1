# MultiLingua 

selector: `nt-multi-lingua`


## Example

```html
  <nt-multi-lingua 
  msgCode="abc.lbl.xyz" langCode="en" desc="this is an example text"> Example
  </nt-multi-lingua>
```
## API References

A list of available attributes that can be used on the `<nt-multi-lingua>` component.  

| Name | Description |
| ---- | ----------- |
| @input langCode | Optional: override the global language |
| @input msgCode | The short code used in the BE API |
| @input desc | Optional: i18n description text |

## Todo 

Create params input