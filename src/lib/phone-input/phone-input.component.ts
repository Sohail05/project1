import { Component, OnInit, Input, Optional, Host, ViewChild } from '@angular/core';
import { ValueAccessorBase, MakeProvider } from '../value-accessor-base/value-accessor-base';
import { FormGroup, FormBuilder, FormGroupDirective, NgControl, FormControl, ControlValueAccessor, NgForm } from '@angular/forms';
import { CodeTypeService } from '../code-type/code-type.service';
import { PhoneNo } from './phone.model';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { FormService } from '../core/form.service';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { environment } from 'environments/environment';

@Component({
  selector: 'nt-phone-input',
  templateUrl: './phone-input.component.html',
  styleUrls: ['./phone-input.component.scss'],
  providers: [
    MakeProvider(PhoneInputComponent),
    MakeLocaleMessageProvider(PhoneInputComponent)
  ]
})
export class PhoneInputComponent implements ControlValueAccessor, OnInit, IUIComponent, LocaleMessage {

  @Input() defaultCountryCode = environment.phone.no;
  @Input() placeholder = 'Phone No';
  @Input() required = false;
  @Input() disabled = false;
  @Input() maxLength = environment.phone.maxLength;
  @Input() minLength;
  @Input() parent: FormGroup;
  @Input() key;

  // control value accessor
  public phoneData: PhoneNo;
  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(obj: any): void {
    Object.assign(this.phoneData, obj);
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  options = [];
  data = <IUIComponentData>{};
  control: FormControl;
  prefix;
  tooltip;
  // showRequired: boolean;
  @Input() displayFnOnOpen: (x) => string = (x) => x.dsc;
  @Input() displayFnOnClose: (x) => string = (x) => x.code;
  @Input() displayFn = (x) => x.dsc;
  @Input() storeFn = (x) => x.atrVal1;
  disabledFn = (n: object) => { return n['vldIdc'] === 'N' ? true : false };

  get _disabled() {
    return (this.disabled || ((this.parent && this.key) && (this.parent.disabled || this.parent.get(this.key).disabled)));
  }

  get errors() {
    const error1 = this.inputControlCode ? this.inputControlCode.errors : null;
    const error2 = this.inputControlNo ? this.inputControlNo.errors : null;
    return Object.assign({}, error1, error2);
  }

  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }
  @ViewChild('inputControlCode') inputControlCode: NgControl;
  @ViewChild('inputControlNo') inputControlNo: NgControl;

  constructor(
    private fb: FormBuilder,
    private codeTypeService: CodeTypeService,
    // private formService: FormService
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
  ) {
  }

  ngOnInit() {
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    this.phoneData = new PhoneNo(this.defaultCountryCode);
    this.onClose();
    // this.setPhoneControl();
    this.getAllCountry();
    // this.subscribeChange();

    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      Object.assign(this.phoneData, this.control.value);
      // this.phoneData = this.control.value;
      this.control.valueChanges.subscribe((val: PhoneNo) => {
        // only update from dynamic value
        if (!Object.is(this.phoneData, val)) {
          // this.phoneData = val;
          Object.assign(this.phoneData, val);
          this.setPrefix();
        }
      });
    }

    this.setPrefix();
  }

  updatePhoneValue() {
    this.propagateChange(this.phoneData);
    if (this.parent && this.key) {
      this.control.setValue(this.phoneData);
    }
    this.setErrors();
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControlCode.control.markAsUntouched();
    this.inputControlNo.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  setErrors() {
    if (this.parent && this.key) {
      const err = Object.keys(this.errors).length > 0 ? this.errors : null;
      this.control.setErrors(err);
    }
  }

  onOpen() {
    if (this.displayFnOnOpen) {
      this.displayFn = this.displayFnOnOpen;
    }
  }

  onClose() {
    if (this.displayFnOnClose) {
      this.displayFn = this.displayFnOnClose;
    }
  }

  getAllCountry() {
    this.codeTypeService.getCodeTypes('CODE.CTY').subscribe((x) => {
      this.options = x;
    });
  }

  setPrefix() {
    this.prefix = this.phoneData.ctyCode;
  }
}
