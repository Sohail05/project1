import { Component, OnInit, Input, EventEmitter, Output, HostBinding } from '@angular/core';

@Component({
  selector: 'nt-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.scss']
})
export class FilterPanelComponent implements OnInit {
  @Input() selectedCheckbox: Array<string> = [];
  @Input() checkboxCodeType: string;
  @Input() checkboxPlaceholder: string;
  @Output() onCheckboxChange = new EventEmitter<String[]>();
  @Output() filterToggleBtnClick = new EventEmitter<boolean>();

  // @HostBinding('style.position') positionType = 'relative';

  filterTrigger: boolean;

  constructor() { }
  checkboxChange(val) {
    this.onCheckboxChange.emit(val);
  }
  onToggleBtnClick() {
    this.filterTrigger = !this.filterTrigger;
    // this.positionType = this.positionType === 'relative' ? 'absolute' : 'relative';
    this.filterToggleBtnClick.emit(this.filterTrigger);
  }
  ngOnInit() { }

}
