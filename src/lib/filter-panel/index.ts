import { NgModule } from '@angular/core';
import { FilterPanelComponent } from './filter-panel.component';
import { CommonModule } from '@angular/common';
import { CheckboxModule } from '../checkbox/index';
import { FormsModule } from '@angular/forms';
import { CodeTypeModule } from '../code-type/index';
import { MdChipsModule, MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MultiLinguaModule } from '../multi-lingua/index';

@NgModule({
  imports: [
    CommonModule,
    CheckboxModule,
    FormsModule,
    CodeTypeModule,
    MdChipsModule,
    FlexLayoutModule,
    MaterialModule,
    MultiLinguaModule
  ],
  exports: [FilterPanelComponent],
  declarations: [FilterPanelComponent],
  providers: [],
})
export class FilterPanelModule { }
