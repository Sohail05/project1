import { NgModule } from '@angular/core';

import { AreaChartComponent } from './area-chart.component';

@NgModule({
    imports: [],
    exports: [AreaChartComponent],
    declarations: [AreaChartComponent],
    providers: [],
})
export class AreaChartModule { }
