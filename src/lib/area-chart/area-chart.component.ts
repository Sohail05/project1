import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import * as d3 from 'd3';
export class ChartConfigModel {
  label = '';
  color = '';
  x = '';
  y = '';
  scale = false;
}
@Component({
  selector: 'nt-area-chart',
  templateUrl: './area-chart.component.html',
  styleUrls: ['./area-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class AreaChartComponent implements OnInit, OnChanges {
  @ViewChild('chart') private chartContainer: ElementRef;
  @Input() private data: any[] = [];
  @Input() private chartConfig: ChartConfigModel[] = [];
  @Input() height = 300;
  @Input() scale: any[] = [];
  private margin: any = { top: 20, bottom: 20, left: 30, right: 30 };
  private chart: any;
  private width = 300;
  /** legend */
  private legend: any;
  /** scale */
  private xScale: any;
  private yLeftScale: any;
  private yRightScale: any;
  /** axis */
  private xAxis: any;
  private yLeftAxis: any;
  private yRightAxis: any;
  /** graph */
  private area1: any;
  private area2: any;
  private line1: any;
  private line2: any;

  /** chart  */
  private lineChart1: any;
  private lineChart2: any;
  private areaChart1: any;
  private areaChart2: any;
  private circleChart: any;
  /** data */
  private dataset: any[];
  /** Svg */
  private svg: any;

  constructor() { }

  ngOnInit() {

    this.createChart();
    if (this.data.length > 0) {
      this.updateChart();
    }
  }
  ngOnViewInit() {

  }
  ngOnChanges() {
    if (this.chart) {

      this.updateChart();
    }
  }

  createChart() {
    const element = this.chartContainer.nativeElement;
    this.width = element.offsetWidth - this.margin.left - this.margin.right;
    //  area
    this.area1 = d3.area()
      .x((d) => { return this.xScale(d['x']); })
      .y0(this.height)
      .y1((d) => { return this.yLeftScale(d['y']); });
    this.area2 = d3.area()
      .x((d) => { return this.xScale(d['x']); })
      .y0(this.height)
      .y1((d) => { return this.yRightScale(d['y']); });

    // line
    this.line1 = d3.line()
      .x((d) => { return this.xScale(d['x']); })
      .y((d) => { return this.yLeftScale(d['y']); });
    this.line2 = d3.line()
      .x((d) => { return this.xScale(d['x']); })
      .y((d) => { return this.yRightScale(d['y']); });

    // define svg
    this.svg = d3.select(element).append('div')
    // container class to make it responsive
    .classed('svg-container', true)

    .append('svg')


    // responsive SVG needs these 2 attributes and no width and height attr
    .attr('preserveAspectRatio', 'xMinYMin meet')
    .attr('viewBox', '0 0 500 500')
    // class to make it responsive
    .classed('svg-content-responsive', true);

    this.createChartbase();
    // legend
    this.legend = this.svg.append('g')
      .attr('class', 'legend')
      .attr('x', 0)
      .attr('y', 0)
      .attr('height', 20)
      .attr('width', this.width)
      .attr('transform', 'translate(20,20)');
    // scale
    this.xScale = d3.scaleTime().range([0, this.width - this.margin.left - this.margin.right]);
    this.yLeftScale = d3.scaleLinear().range([this.height, 0]).domain([0, 9]);
    this.yRightScale = d3.scalePoint().range([0, this.height]).domain(['start', 'end']);

    // axis
    this.xAxis = this.svg.append('g')
      .attr('class', 'axis axis-x')
      .attr('transform', 'translate(' + this.margin.left + ',' + (this.height + this.margin.top) + ')');
    this.yLeftAxis = this.svg.append('g')
      .attr('class', 'axis axis-y')
      .attr('transform', 'translate(' + this.margin.left + ',' + (this.margin.top) + ')');
    this.yRightAxis = this.svg.append('g')
      .attr('class', 'axis axis-y')
      .attr('transform', 'translate(' + (this.width - this.margin.right) + ',' + (this.margin.top) + ')');
  }
  createChartbase() {
    // define chart area
    this.chart = this.svg.append('g')
      .attr('class', 'chartDiv')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }
  removeChart() {
    this.chart.remove();
  }
  updateChart() {
    this.removeChart();
    this.createChartbase();
    // make sure dataset empty when chart update
    this.dataset = [];
    this.dataset = this.chartConfig.map((config) => {
      return {
        label: config.label,
        color: config.color,
        scale: config.scale,
        data: this.data.map((d) => {
          return { x: new Date(d[config.x]), y: d[config.y] };
        }),
      };
    });

    // sort all x data
    this.dataset.map((dt) => {
      dt.data.sort((a, b) => {
        return a['x'] - b['x'];
      });
    });

    // get minX and maxX
    const minX = this.dataset[0].data[0].x;
    const maxX = this.dataset[0].data[this.dataset[0].data.length - 1].x;
    const minY = d3.min(this.dataset, (kv, i) => {
      if (!this.chartConfig[i].scale) {
        return d3.min(kv.data, (d, a) => { return d['y']; });
      }
    });
    const maxY = d3.max(this.dataset, (kv, i) => {
      if (!this.chartConfig[i].scale) {
        return d3.max(kv.data, (d, a) => { return d['y']; });
      }
    });
    // update scale
    this.xScale.domain([minX, maxX]);
    this.yLeftScale.domain([minY, maxY]);
    this.yRightScale.domain(this.scale);

    // update axis
    this.xAxis.transition().call(d3.axisBottom(this.xScale).ticks(5).tickFormat(d3.timeFormat('%b %Y')));
    this.yLeftAxis.transition().call(d3.axisLeft(this.yLeftScale));
    this.yRightAxis.transition().call(d3.axisRight(this.yRightScale));

    this.dataset.map((data, i) => {

      if (data.scale) {
        this.drawLine2(data, i);
        this.drawArea2(data, i);
      } else {
        this.drawLine(data, i);
        this.drawArea(data, i);
      }
      this.drawCircle(data, i);
    });
    this.drawLegend();

  }

  drawLine(d, i) {
    this.lineChart1 = this.chart
      .selectAll('.line-no-scale' + i)
      .data([d.data]);

    this.lineChart1
      .enter()
      .append('path')
      .attr('class', 'line-no-scale' + i)
      .style('fill', 'none')
      .transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .style('stroke', d.color)
      .style('stroke-width', '2px')
      .attr('d', this.line1);

    this.lineChart1
      .exit()
      .remove();

  }
  drawLine2(d, i) {
    this.lineChart2 = this.chart
      .selectAll('.line-scale' + i)
      .data([d.data]);

    this.lineChart2
      .enter()
      .append('path')
      .attr('class', 'line-scale' + i)
      .style('fill', 'none')
      .transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .style('stroke', d.color)
      .style('stroke-width', '2px')
      .attr('d', this.line2);

    this.lineChart2
      .exit()
      .remove();

  }
  drawArea(d, i) {
    this.areaChart1 = this.chart
      .selectAll('.area-no-scale' + i)
      .data([d.data]);

    this.areaChart1
      .enter()
      .append('path')
      .attr('class', 'area-no-scale' + i)
      .style('opacity', '0.4')
      .transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .style('fill', d.color)
      .attr('d', this.area1);

    this.areaChart1
      .exit()
      .remove();

  }
  drawArea2(d, i) {
    this.areaChart2 = this.chart
      .selectAll('.area-scale' + i)
      .data([d.data]);

    this.areaChart2
      .enter()
      .append('path')
      .attr('class', 'area-scale' + i)
      .style('opacity', '0.4')
      .transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .style('fill', d.color)
      .attr('d', this.area2);

    this.areaChart2
      .exit()
      .remove();

  }
  drawCircle(d, i) {
    this.circleChart = this.chart
      .selectAll('.dot' + i)
      .data(d.data);

    this.circleChart
      .enter()
      .append('circle')
      .attr('class', 'dot' + i)
      .transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .style('fill', d.color)
      .attr('r', 3)
      .attr('cx', (c) => {
        return this.xScale(c.x);
      })
      .attr('cy', (c) => {
        return d.scale ? this.yRightScale(c.y) : this.yLeftScale(c.y);
      });

    this.circleChart
      .exit()
      .remove();

  }
  drawLegend() {
    this.legend.selectAll('rect')
      .data(this.chartConfig)
      .enter()
      .append('rect')
      .attr('x', (d, i) => { return (this.width / 4) + (i * 50); })
      .attr('y', 0)
      .attr('width', 10)
      .attr('height', 10)
      .style('fill', (d, i) => { return this.chartConfig[i].color; });
    this.legend.selectAll('text')
      .data(this.chartConfig)
      .enter()
      .append('text')
      .attr('x', (d, i) => { return (this.width / 4) + (i * 50 + 15); })
      .attr('y', 10)
      .text((d, i) => { return this.chartConfig[i].label; });

  }
}
