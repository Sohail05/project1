import * as d3 from 'd3';

export interface DataType {
    x: any;
    y: any;
}
export default (() => {
    let svg;
    let width: number;
    let height: number;
    let chartOptions;
    const margin = { top: 45, right: 50, bottom: 30, left: 50 };
    const defaultOptions = {
        id: '',
        data: [],   // { data: []} format of array
        config: [], // { x: '', y: '', label: '', color: '' } format of array
        width: 180,
        height: 150,
    };

    function init(options) {
        chartOptions = Object.assign(defaultOptions, options);
        // set size
        width = chartOptions.width;
        height = chartOptions.height;
        svg = d3.select(chartOptions.id)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform',
            'translate(' + margin.left + ',' + margin.top + ')');

        update(chartOptions.data, chartOptions.config);
    }
    function update(data: any[], config) {
        const dataset = [];
        // processing data
        data.forEach((d, i) => {
            const temp = [];
            d.data.forEach((de) => {
                temp.push({ x: new Date(de[config[i].x]), y: de[config[i].y] });
            });
            dataset.push({ data: temp });
        });
        dataset.sort((a, b) => {
            return a['x'] - b['x'];
        });
        const bisectDate = d3.bisector((d) => d['x']).left;

        const x = d3.scaleTime().range([0, width]);
        const y = d3.scaleLinear().range([height, 0]);

        const y1 = d3.scalePoint().range([0, height]);
        // define the area
        const area1 = d3.area()
            .x((d) => { return x(d['x']); })
            .y0(height)
            .y1((d) => { return y(d['y']); });
        const area2 = d3.area()
            .x((d) => { return x(d['x']); })
            .y0(height)
            .y1((d) => { return y1(d['y']); });
        // define the line
        const valueline1 = d3.line()
            .x((d) => { return x(d['x']); })
            .y((d) => { return y(d['y']); });
        const valueline2 = d3.line()
            .x((d) => { return x(d['x']); })
            .y((d) => { return y1(d['y']); });

        // scale the range of the data
        const minX = d3.min(dataset, (kv) => { return d3.min(kv.data, (d, i) => { return d['x']; }); });
        const maxX = d3.max(dataset, (kv) => { return d3.max(kv.data, (d, i) => { return d['x']; }); });
        const minY = d3.min(dataset, (kv) => { return d3.min(kv.data, (d, i) => { return d['y']; }); });
        const maxY = d3.max(dataset, (kv) => { return d3.max(kv.data, (d, i) => { return d['y']; }); });
        x.domain([new Date(minX), new Date(maxX)]);
        // temporary hardcord min and max value
        y.domain([0, 10]);

        // add legend
        const legend = svg.append('g')
            .attr('class', 'legend')
            .attr('x', 0)
            .attr('y', 0)
            .attr('height', 20)
            .attr('width', width)
            .attr('transform', 'translate(0,-20)');
        dataset.forEach( (d, i) => {
            if (config[i].scale !== undefined) {
                // set the domain for y1 axis
                y1.domain(config[i].scale);
                // add the area2

                svg.selectAll('.area')
                    .data([d.data])
                    .enter()
                    .append('path')
                    .transition()
                    .duration(2000)
                    .ease(d3.easeBounce)
                    .style('opacity', '0.4')
                    .style('fill', config[i].color)
                    .attr('d', area2);

                // add the valueline2 path.
                svg.selectAll('.line')
                    .data([d.data])
                    .enter()
                    .append('path')
                    .transition()
                    .duration(2000)
                    .ease(d3.easeBounce)
                    .style('fill', 'none')
                    .style('stroke', config[i].color)
                    .style('stroke-width', '2px')
                    .attr('d', valueline2);

                svg.append('g')
                    .attr('class', 'dot')
                    .selectAll('circle')
                    .data(d.data)
                    .enter()
                    .append('circle')
                    .transition()
                    .duration(2000)
                    .ease(d3.easeBounce)
                    .style('fill', config[i].color)
                    .attr('r', 3)
                    .attr('cx', (dx, j) => { return x(dx['x']); })
                    .attr('cy', (dy, j) => { return y1(dy['y']); });
            } else {
                // add the area
                svg.selectAll('.area')
                    .data([d.data])
                    .enter()
                    .append('path')
                    .transition()
                    .duration(2000)
                    .ease(d3.easeBounce)
                    .style('opacity', '0.4')
                    .style('fill', config[i].color)
                    .attr('d', area1);

                // add the valueline path.
                svg.selectAll('.line')
                    .data([d.data])
                    .enter()
                    .append('path')
                    .transition()
                    .duration(2000)
                    .ease(d3.easeBounce)
                    .style('fill', 'none')
                    .style('stroke', config[i].color)
                    .style('stroke-width', '2px')
                    .attr('d', valueline1);

                svg.append('g')
                    .attr('class', 'dot')
                    .selectAll('circle')
                    .data(d.data)
                    .enter()
                    .append('circle')
                    .transition()
                    .duration(2000)
                    .ease(d3.easeBounce)
                    .style('fill', config[i].color)
                    .attr('r', 3)
                    .attr('cx', (dx, j) => { return x(dx['x']); })
                    .attr('cy', (dy, j) => { return y(dy['y']); });
            }

            // legend
            legend.selectAll('rect')
                .data(config)
                .enter()
                .append('rect')
                .attr('x', (dx, ix) => { return (width / 4) + (ix * 50); })
                .attr('y', 0)
                .attr('width', 10)
                .attr('height', 10)
                .style('fill', (dx, ix) => { return config[ix].color; });
            legend.selectAll('text')
                .data(dataset)
                .enter()
                .append('text')
                .attr('x', (dx, ix) => { return (width / 4) + (ix * 50 + 15); })
                .attr('y', 10)
                .text((dx, ix) => { return config[ix].label; });

        });

        // add the X Axis
        svg.append('g')
            .attr('transform', 'translate(0,' + height + ')')
            .call(d3.axisBottom(x).ticks(5).tickFormat(d3.timeFormat('%Y')));

        // add the Y Axis
        svg.append('g')
            .call(d3.axisLeft(y));
        // add the Y Axis
        svg.append('g')
            .attr('transform', 'translate( ' + width + ', 0 )')
            .call(d3.axisRight(y1));
    }

    return { init };

})();
