import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DateService {
  private clear: BehaviorSubject<any> = new BehaviorSubject<any>(false);
  clear$: Observable<string> = this.clear.asObservable();
  constructor() { }
  clearValue(val) {
    this.clear.next(val);
  }
}
