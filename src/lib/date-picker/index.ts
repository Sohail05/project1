import { DateUtil } from './dateUtil';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { DatePickerComponent } from './date-picker.component';
import { ControlMessageModule } from '../control-message/index';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DateService } from './date.service';

@NgModule({
    imports: [CommonModule, FlexLayoutModule, ControlMessageModule],
    exports: [DatePickerComponent],
    declarations: [DatePickerComponent],
    providers: [],
})
export class DatePickerModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: DatePickerModule,
            providers: [
                DateUtil,
                DateService,
            ]
        };
    }
}
