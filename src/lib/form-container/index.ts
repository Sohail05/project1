import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormContainerComponent } from './form-container.component';
import { TopNavBarModule } from '../top-nav-bar/index';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
  ],
  exports: [FormContainerComponent],
  declarations: [FormContainerComponent],
})
export class FormContainerModule { }
