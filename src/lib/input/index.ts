import { NgModule, forwardRef } from '@angular/core';
import { InputComponent } from './input.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { ControlMessageModule } from '../control-message/index';
import { TextMaskModule } from 'angular2-text-mask';
import { CommonModule } from '@angular/common';
import { MultiLinguaTooltipModule } from '../tooltip/index';
import { ValidatorModule } from '../validator/index';

@NgModule({
  imports: [
    CommonModule,
    ControlMessageModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    TextMaskModule,
    FormsModule,
    MultiLinguaTooltipModule,
    ValidatorModule
  ],
  exports: [InputComponent],
  declarations: [InputComponent],
})
export class InputModule { }
export { InputComponent } from './input.component';
