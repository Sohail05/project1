import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, Optional, Host, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormControl, NgControl, ControlValueAccessor, FormGroupDirective, NgForm } from '@angular/forms';
import { ValidationService } from '../core/validation.service';
import { IUIComponent, IUIComponentData } from '../ui-component/ui-component';
import { MakeProvider, ValueAccessorBase } from '../value-accessor-base/value-accessor-base';
import { Subscription } from 'rxjs/Subscription';
// import { FormService } from '../core/form.service';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';


@Component({
  selector: 'nt-input',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    MakeProvider(InputComponent),
    MakeLocaleMessageProvider(InputComponent)
  ],
})
export class InputComponent implements OnInit, OnChanges, IUIComponent, LocaleMessage, ControlValueAccessor {
  private _value;
  get value() {
    return this._value;
  }

  set value(val: any) {
    if (this._value !== val) {
      this._value = val;
      this.propagateChange(val);
      if (this.parent && this.key) {
        this.control.setValue(val);
      }
    }
    this.setErrors();
  }

  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(val: any): void {
    this._value = val;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }
  tooltip;
  data = <IUIComponentData>{};
  @Input() parent: FormGroup;
  @Input() type = 'text';
  @Input() key;
  @Input() placeholder: string;
  @Input() isDisabled = false;
  @Input() required = null;
  @Input() maxLength: number;
  @Input() minLength: number;
  @Input() min: number;
  @Input() max: number;
  @Input() masking = 'text';
  @Input() maskingOpts: object;
  @Input() pattern: string;
  @Input() email: boolean;
  @Input() validation: object;
  @Input() conditional: { [key: string]: string };
  @Input() prefix = '';
  @Input() suffix = '';

  inputMask;
  // showRequired: boolean;
  showTooltip: boolean;
  // formSubmission: Subscription;
  control: FormControl;
  @ViewChild('inputControl') inputControl: NgControl;
  get _disabled() {
    return this.isDisabled || (this.parent && this.key && (this.parent.disabled || this.control.disabled)) || this.data.disabled;
  }
  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }
  constructor(
    private validationService: ValidationService,
    // private formService: FormService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
  ) { }

  setErrors() {
    if (this.parent && this.key) {
      this.control.setErrors(this.inputControl.errors);
    }
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControl.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }
  ngOnInit() {
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    this.getInputMask();

    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      this._value = this.control.value;
      this.control.valueChanges.subscribe((val) => {
        if (this.value !== val) {
          this._value = val;
        }
      });
    }
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  getInputMask(val?) {
    this.inputMask = this.validationService.getInputMask(this.data.masking || this.masking || val, this.maskingOpts);
  }

  ngOnChanges(changes: SimpleChanges) {

    // if (changes['validation'] || changes['required'] || changes['isDisabled']) {
    //   this.setValidators();
    // }
    if (changes['masking'] || changes['maskingOpts']) {
      this.getInputMask(changes['masking'].currentValue);
    }
  }
}
