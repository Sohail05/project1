import { FormService } from '../core/form.service';
import { Component, OnInit, Input, Optional, Host, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, ControlValueAccessor, NgForm, FormGroupDirective, NgControl } from '@angular/forms';
import { ValidationService } from '../core/validation.service';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { MakeCodeTypeProvider, CodeTypeContainer } from '../code-type/code-type-container';
import { Functional } from '../core/functional';
import { IdModel } from './input-id.model';
import { MakeProvider } from '../value-accessor-base/value-accessor-base';
import { LocaleMessage } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-input-id',
  templateUrl: './input-id.component.html',
  styleUrls: ['./input-id.component.scss'],
  providers: [
    MakeProvider(InputIdComponent),
    MakeCodeTypeProvider(InputIdComponent)]
})
export class InputIdComponent implements OnInit, IUIComponent, CodeTypeContainer, LocaleMessage, ControlValueAccessor {


  public id: IdModel;
  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(obj: any): void {
    Object.assign(this.id, obj);
    this.setExistingMask();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }

  updateIdValue() {
    this.propagateChange(this.id);
    if (this.parent && this.key) {
      this.control.setValue(this.id);
    }
    this.setErrors();
  }

  showTooltip: boolean;
  data = <IUIComponentData>{};
  inputMask;
  options = [];
  control: FormControl;
  // controlGroup: FormGroup;
  // showRequired: boolean;
  // childKeys: Array<string> = [];
  @Input() maxlength: number;
  @Input() parent: FormGroup;
  @Input() type = 'SSN';
  @Input() key: string;
  @Input() masking: string;
  @Input() maskingOpts: object = {
    allowDecimal: false,
    allowNegative: false,
  };
  @Input() required = false;
  @Input() isDisabled = false;
  disabledFn = Functional.isDisabled;
  @Input() displayFn = Functional.codeDescription;
  @Input() storeFn = Functional.codeValue;
  @Input() displayFnOnOpen = Functional.codeDescription;
  @Input() displayFnOnClose = Functional.codeValue;
  @Input() placeholder;

  get _disabled() {
    return this.isDisabled || (this.parent && this.key && (this.parent.disabled || this.parent.get(this.key).disabled));
  }

  get errors() {
    const error1 = this.inputControlT ? this.inputControlT.errors : null;
    const error2 = this.inputControlNbr ? this.inputControlNbr.errors : null;
    return Object.assign({}, error1, error2);
  }

  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }

  @ViewChild('inputControlT') inputControlT: NgControl;
  @ViewChild('inputControlNbr') inputControlNbr: NgControl;
  constructor(
    private validationService: ValidationService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
    // private formService: FormService
  ) { }

  loadCodeType(options: any) {
    this.options = options;
    this.setExistingMask();
  };

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
  };

  ngOnInit() {
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    this.onClose();
    this.id = new IdModel(this.type);

    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      Object.assign(this.id, this.control.value);
      // this.id = this.control.value;
      this.control.valueChanges.subscribe((val) => {
        if (!Object.is(this.id, val)) {
          Object.assign(this.id, val);
          this.setExistingMask();
        }
      });
    }

    this.setExistingMask();
  }

  setErrors() {
    if (this.parent && this.key) {
      const err = Object.keys(this.errors).length > 0 ? this.errors : null;
      this.control.setErrors(err);
    }
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControlNbr.control.markAsUntouched();
    this.inputControlT.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }

  onOpen() {
    if (this.displayFnOnOpen) {
      this.displayFn = this.displayFnOnOpen;
    }
  }

  onClose() {
    if (this.displayFnOnClose) {
      this.displayFn = this.displayFnOnClose;
    }
  }

  setExistingMask() {
    const idType = this.id.idT;
    let mask;
    if (idType) {
      mask = this.options.find((opt) => this.storeFn(opt) === idType);
    }
    this.getInputMask(mask ? (mask.atrVal1 || 'number') : 'number');
  }

  getInputMask(val?) {
    this.inputMask = this.validationService.getInputMask(this.data.masking || this.masking || val, this.maskingOpts);
    // this.controlGroup.get(this.childKeys[1]).setValidators(Validators.pattern(val));
  }

}
