import { Component, Input, OnInit, Output, OnDestroy, Optional, Host, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, ControlValueAccessor, FormGroupDirective, NgForm, NgControl } from '@angular/forms';
import { Countries } from '../core/countries';
import { States } from '../core/states';
import { ValidationService } from './../core/validation.service';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { AddressModel } from './address.model';
import { CodeTypeService } from '../code-type/code-type.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { FormService } from '../core/form.service';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  providers: [
    MakeProvider(AddressComponent),
    MakeLocaleMessageProvider(AddressComponent)
  ],
})
export class AddressComponent implements OnInit, OnDestroy, IUIComponent, LocaleMessage, ControlValueAccessor {

  addr: AddressModel;

  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(obj: any): void {
    Object.assign(this.addr, obj);
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
  updateAddressValue() {
    this.propagateChange(this.addr);
    if (this.parent && this.key) {
      this.control.setValue(this.addr);
    }
    this.setErrors();
  }

  data = <IUIComponentData>{};
  @Input() parent: FormGroup;
  @Input() key;
  @Input() placeholder = 'Address';
  @Input() msgCode: string;
  @Input() required = false;
  @Input() disabled = false;
  // TODO set by global variable or enviroment
  @Input() defaultCountry: string;
  // showRequired: boolean;
  // formSubmission: Subscription;
  control: FormGroup;
  friendlyName = {
    addr1: 'Address Line 1',
    addr2: 'Address Line 2',
    addr3: 'Address Line 3',
    postcode: 'Postal Code',
    city: 'City',
    ste: 'State',
    cty: 'Country'
  }

  maxLength = {
    addr1: 100,
    addr2: 100,
    addr3: 100,
    postcode: 10,
    city: 100
  }
  tooltip;
  showTooltip

  // private form: FormGroup;
  // Listing of Countries & States
  countries;
  // Todo: Display states based on Country
  private allStates = [];
  // states based on countries selected;
  states = [];

  private countryObv: Subscription;
  private allStateObv: Subscription;
  private stateObv: Subscription;

  disabledFn = (n: object) => { return n['vldIdc'] === 'N' ? true : false };

  get _disabled() {
    return (this.disabled || (this.parent && this.key) && (this.parent.disabled || this.control.disabled));
  }

  get errors() {
    const errAddr = this.inputControlAddr1 ? this.inputControlAddr1.errors : null;
    const errPostcode = this.inputControlPostcode ? this.inputControlPostcode.errors : null;
    const errCity = this.inputControlCity ? this.inputControlCity.errors : null;
    const errSte = this.inputControlSte ? this.inputControlSte.errors : null;
    const errCty = this.inputControlCty ? this.inputControlCty.errors : null;
    return Object.assign({}, errAddr, errCity, errCty, errPostcode, errSte);
  }

  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }

  @ViewChild('inputControlAddr1') inputControlAddr1: NgControl;
  @ViewChild('inputControlPostcode') inputControlPostcode: NgControl;
  @ViewChild('inputControlCty') inputControlCty: NgControl;
  @ViewChild('inputControlSte') inputControlSte: NgControl;
  @ViewChild('inputControlCity') inputControlCity: NgControl;
  constructor(
    private validate: ValidationService,
    private codeType: CodeTypeService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
  ) {
  }

  ngOnInit() {
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    this.addr = new AddressModel();
    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormGroup;
      Object.assign(this.addr, this.control.value);
      this.filterStates();
      this.control.valueChanges.subscribe((val) => {
        if (!Object.is(this.addr, val)) {
          Object.assign(this.addr, val);
          this.filterStates();
        }
      });
    }

    this.countryObv = this.codeType.getCodeTypes('CODE.CTY').subscribe((c) => {
      this.countries = c;
    });
    this.allStateObv = this.codeType.getCodeTypes('CODE.CTY_STE').subscribe((s) => {
      this.allStates = s;
      // filter after get all states
      this.filterStates();
    });
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
  }

  setErrors() {
    if (this.parent && this.key) {
      const err = (Object.keys(this.errors).length > 0) ? this.errors : null;
      this.control.setErrors(err);
    }
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControlAddr1.control.markAsUntouched();
    this.inputControlCity.control.markAsUntouched();
    this.inputControlCty.control.markAsUntouched();
    this.inputControlSte.control.markAsUntouched();
    this.inputControlPostcode.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }
  ngOnDestroy() {
    // unsubcribe observables
    if (this.countryObv) {
      this.countryObv.unsubscribe();
    }
    if (this.allStateObv) {
      this.allStateObv.unsubscribe();
    }
    if (this.stateObv) {
      this.stateObv.unsubscribe();
    }
  }

  filterStates() {
    if (this.addr && this.addr.cty) {
      this.states = this.allStates.filter((s) => {
        return s.grpCodes.indexOf(this.addr.cty) > -1;
      });
    } else {
      this.states = [];
    }
  }


}
