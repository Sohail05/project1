import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { AddressComponent } from './address.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ControlMessageModule } from '../control-message/index';
import { CommonModule } from '@angular/common';
import { MultiLinguaModule } from '../multi-lingua/index';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ControlMessageModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MultiLinguaModule,
    FormsModule
  ],
  exports: [AddressComponent],
  declarations: [AddressComponent],
})
export class AddressModule { }
export * from './address.model'
