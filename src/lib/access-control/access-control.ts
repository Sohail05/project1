import { fldAcsMde } from '../core/type/api';
export interface AccessControl {
    mainRoleCode: string;
    roleCode: string;
    opnCodes: string[];
    fldId: string;
    fldAcsMde: fldAcsMde;
    recCodes: string;
}

