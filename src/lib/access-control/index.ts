import { NgModule, ModuleWithProviders } from '@angular/core';
import { AccessControlService } from './access-control.service';
import { AccessControlDirective } from './access-control.directive';
import { AccesssControlPipe } from './access-control.pipe';
import { ApiService } from '../core/api/api.service';

@NgModule({
    imports: [],
    exports: [AccessControlDirective, AccesssControlPipe],
    declarations: [AccessControlDirective, AccesssControlPipe],
    providers: [],
    bootstrap: []
})
export class AccessControlModule {
        static forRoot(): ModuleWithProviders {
        return {
            ngModule: AccessControlModule,
            providers: [AccessControlService],
        };
    }
}
