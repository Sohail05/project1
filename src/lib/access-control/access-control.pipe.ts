import { Pipe, PipeTransform } from '@angular/core';
import { AccessControlService } from './access-control.service';

@Pipe({name: 'accessControl'})
export class AccesssControlPipe implements PipeTransform {

    constructor(private accessControlService: AccessControlService) {
    }

    /**
     * Resolve Access Control Code to boolean
     * @param code the access control code. e.g. SYS.PMR|VW
     * @param type may be one of the following: menu, field, record
     */
    transform(code: string, type: string) {
        if (type === 'menu') {
            return this.accessControlService.menuAccess(code);
        }else if (type === 'field') {
            return this.accessControlService.fieldAccess(code);
        }else if (type === 'record') {
            return this.accessControlService.recordAccess(code);
        }

        throw new Error('accessControl must specify access type e.g. accessControl:"menu"');
    }

}
