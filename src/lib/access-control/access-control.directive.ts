import { Directive, OnInit } from '@angular/core';
import { AccessControlService } from './access-control.service';

@Directive({ selector: '[ntAccessMenu],[ntAccessField],[ntAccessRecord]' })
export class AccessControlDirective implements OnInit {
    constructor(private accessControlService: AccessControlService) {}
    // todo : determine use case
    ngOnInit() {}

}

