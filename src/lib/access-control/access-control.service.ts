
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ApiService } from '../core/api/api.service';
import { environment } from 'environments/environment';
import { AccessControl } from './access-control';
import { fldAcsMde } from '../core/type/api';

/**
 * Todo: PostPoned fields and record access control.
 */

@Injectable()
export class AccessControlService {

    /*
     * Stored Access Controls Permissions.
     */
    private menuPermissions: AccessControl[] = [];
    private fieldPermissions: AccessControl[] = [];
    private recordPermissions: AccessControl[] = [];

    /**
     * Promise to verify loading progress of permissions.
     */
    loading: Promise<any>;

    constructor(private api: ApiService) {}

    /**
     * Initialize loading of access control permissions.
     * @return {Promise<any>} resolves when all permissions have been loaded.
     */
    load() {

        const menuPromise = this.loadMenuAccess()
            .then((data) => {
                this.menuPermissions = data;
            });

        /*
        const fieldPromise = new Promise((resolve, reject) => {
            this.loadFieldAccess().then((data) => {
                this.fieldPermissions = data;
                resolve();
            })
        });

        const recordPromise = new Promise((resolve, reject) => {
            this.loadMenuAccess().then((data) => {
                this.recordPermissions = data;
                resolve();
            })
        });*/

        return this.loading = Promise.all([menuPromise /*, fieldPromise, recordPromise*/]);
    }

    /**
     * Execute api requests.
     * @param {string} url
     */
    loadContext(url: string) {
        return this.api.get(url)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    /**
     * Extract, process and cast Object to an array of AccessControl.
     * @param {object} data retrived from api.
     * @return {Array<AccessControl>}
     */
    extractData(data): AccessControl[] {
        if (Array.isArray(data)) {
            return data as AccessControl[];
        }else {
            return [] as AccessControl[];
        }
    }

    /**
     * Additonal Error handling.
     */
    handleError(error) {
        return [] as AccessControl[];
    }

    /**
     * Load menu permissions.
     */
    private loadMenuAccess() {
        return this.loadContext(environment.api.accessMenu);
    }

    /**
     * Load field permissions.
     */
    private loadFieldAccess() {
        return this.loadContext(environment.api.accessField);
    }

    /**
     * Load record permissions.
     */
    private loadRecordAccess() {
        return this.loadContext(environment.api.accessRecord);
    }

    /**
     * Menu/Pages/Screens access control.
     * @param {string} roleCode e.g.'SYS.PMR|VW'
     * @return {boolean} returns `true` with the permission is available.
     */
    menuAccess(roleCode: string): boolean {
        return this.getAccess(this.menuPermissions, roleCode) ? true : false;
    }

    /**
     * Fields access control.
     * @param roleCode e.g.'SYS.PMR|VW'
     * @return {fldAcsMde}
     */
    fieldAccess(roleCode: string): fldAcsMde {
        return this.getAccess(this.fieldPermissions, roleCode).fldAcsMde;
    }

    /**
     * Record access control.
     * @param roleCode e.g.'SYS.PMR|VW'
     * @return {string}
     */
    recordAccess(roleCode: string): string {
        return this.getAccess(this.recordPermissions, roleCode).recCodes;
    }

    /**
     * Retrive the AccessControl based on the role code.
     * @param {Array<AccessControl>} container the array associated with the type of access control.
     * @param {string} code role code and op.
     */
    private getAccess(container: AccessControl[], code: string): AccessControl {

        const frag = code.split('|')
        if ( frag.length < 2) {
            return null;
        }

        const find = (access: AccessControl) => {
            const role = access.roleCode === frag[0];
            const opt = access.opnCodes.find((operation) => { return operation === frag[1] }) ? true : false;
            return role && opt;
        }

        return container.find(find);
    }

}
