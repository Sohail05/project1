import { Component, Input, OnInit, ViewChild, OnDestroy, Optional, Host } from '@angular/core';
import {
  NgModel, FormGroup, FormBuilder, FormControl,
  Validators, ControlValueAccessor, NgForm, FormGroupDirective, NgControl
} from '@angular/forms';
import 'rxjs/add/observable/combineLatest';
import { Observable } from 'rxjs/Observable';
import { fadeInSlideInY } from '../animations/animations';
import { ICodeType } from './../core/model/common';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { ValidationService } from '../core/validation.service';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { CodeTypeService } from '../code-type/code-type.service';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { MakeLocaleMessageProvider, LocaleMessage } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { FxAmt } from '../core/model/cmo/FxAmt';
import { MultiCurrencyModel } from './multi-currency.model';

@Component({
  selector: 'nt-multi-currency',
  templateUrl: './multi-currency.component.html',
  styleUrls: ['./multi-currency.component.scss'],
  animations: [fadeInSlideInY],
  providers: [
    MakeProvider(MultiCurrencyComponent),
    MakeLocaleMessageProvider(MultiCurrencyComponent)
  ],
})

export class MultiCurrencyComponent implements OnInit, OnDestroy, ControlValueAccessor, IUIComponent, LocaleMessage {

  public value: MultiCurrencyModel;

  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(val: any): void {
    Object.assign(this.value, val);
    this.setAmt();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }

  data = <IUIComponentData>{};
  @Input() min: number;
  @Input() max: number;
  @Input() changeFx = false;
  @Input() default: string;
  // TODO SET WITH GLOBAL VARIABLE OR ...
  @Input() baseCurrency = 'USD';
  @Input() parent: FormGroup;
  @Input() key;
  @Input() maskingOpts: object = {};
  @Input() required = false;
  @Input() isDisabled: boolean | string = false;
  @Input() conditional: { [key: string]: string };
  @Input() validation: object;
  control: FormControl;
  showRequired: boolean;
  showTooltip: boolean;
  @Input() placeholder;
  @Input() tooltip;
  currentCurrencyType;
  private currentAmount;
  private currentRate;
  convertedAmount;
  inputMask;
  private changesRate = false;

  // TODO GET FROM SERVER
  currencyList = [];

  // Observable
  private currencyList$: Subscription;


  // TODO GET FROM SERVER
  currencyRateTable = {
    USD: 1,
    SGD: 0.7,
    GBP: 1.25,
    lastUpdated: new Date(),
  };

  // amount in string
  altAmt;

  // indicate form submit status
  get _disabled() {
    return this.isDisabled || (this.parent && this.key && (this.parent.disabled || this.parent.get(this.key).disabled));
  }

  get errors() {
    const errCode = this.inputControlCode ? this.inputControlCode.errors : null;
    const errAmt = this.inputControlAmt ? this.inputControlAmt.errors : null;
    const errRate = this.inputControlRate ? this.inputControlRate.errors : null;
    const errDate = this.inputControlDate ? this.inputControlDate.errors : null;
    return Object.assign({}, errCode, errAmt, errDate, errRate);
  }

  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }

  disabledFn = (n: object) => { return n['vldIdc'] === 'N' ? true : false };

  @ViewChild('inputControlCode') inputControlCode: NgControl;
  @ViewChild('inputControlAmt') inputControlAmt: NgControl;
  @ViewChild('inputControlRate') inputControlRate: NgControl;
  @ViewChild('inputControlDate') inputControlDate: NgControl;

  constructor(
    private fb: FormBuilder,
    private validationService: ValidationService,
    private codeTypeService: CodeTypeService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
  ) {
  }


  ngOnInit() {
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    // set default
    this.value = new MultiCurrencyModel(this.baseCurrency);

    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      Object.assign(this.value, this.control.value);
      this.setAmt();
      this.control.valueChanges.subscribe((val) => {
        if (!Object.is(this.value, val)) {
          Object.assign(this.value, val);
          this.setAmt();
        }
      });
    }
    this.getAllCurrency();
    this.getInputMask();
    this.calculateAmount();
  }

  updateValue() {
    this.propagateChange(this.value);
    if (this.parent && this.key) {
      this.control.setValue(this.value);
    }
    this.setErrors();
  }

  setAmt() {
    this.altAmt = this.value.atlAmt ? this.value.atlAmt.toString() : '';
    // this.calculateAmount();
  }
  updateAmt() {
    this.value.atlAmt = parseFloat(this.altAmt.replace(/,/g, '')) || 0;
    this.calculateAmount();
    this.updateValue();
  }

  setErrors() {
    if (this.parent && this.key) {
      const err = (Object.keys(this.errors).length > 0) ? this.errors : null;
      this.control.setErrors(err);
    }
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    if (this.inputControlCode) {
      this.inputControlCode.control.markAsUntouched();
    }
    if (this.inputControlDate) {
      this.inputControlDate.control.markAsUntouched();
    }
    if (this.inputControlRate) {
      this.inputControlRate.control.markAsUntouched();
    }
    if (this.inputControlAmt) {
      this.inputControlAmt.control.markAsUntouched();
    }
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  getAllCurrency() {
    this.currencyList$ = this.codeTypeService.getCodeTypes('CODE.CUR').subscribe((c) => {
      this.currencyList = c;
      this.setMasking();
      this.getInputMask();
      this.autoFormat();
    });
  }
  getInputMask(val?) {
    this.inputMask = this.validationService.getInputMask('decimal', this.maskingOpts);
  }

  setMasking() {
    if (this.value.atlCode) {
      const curT = this.value.atlCode;
      const decimal = this.currencyList.find((cur) => cur.code === curT).atrVal2;
      this.maskingOpts['decimalLimit'] = parseInt(decimal, 10);
      // this.maskingOpts['requireDecimal'] = true;
    }
  }


  formatBlank() {
    if (!this.value.atlCode) {
      this.altAmt = '';
      this.updateAmt();
    }
  }

  currencyChange() {
    this.formatBlank();
    if (this.value.atlCode) {
      this.setMasking();
    }
    this.getInputMask();
    this.autoFormat();
    this.value.fxRtDt = this.currencyRateTable['lastUpdated'];
    this.value.fxRt = this.currencyRateTable[this.value.atlCode];
    this.calculateAmount();
    this.updateValue();
  }

  rateChanges() {
    if (this.changeFx) {
      this.calculateAmount();
      this.updateValue();
    }
  }

  autoFormat() {
    if (this.altAmt) {
      const curCode = this.currencyList.find((cur) => cur.code === this.value.atlCode);
      const decimal = curCode ? curCode.atrVal2 : 0;
      if (decimal !== '0') {

        // if user removed . replace .
        if (this.altAmt.indexOf('.') < 0) {
          this.altAmt += '.';
        }

        // set zeros
        let zeros = '';
        for (let i = 0; i < decimal; i++) {
          zeros = zeros + '0';
        }
        this.altAmt += zeros;
        const curlength = this.altAmt.split('.')[1].length;
        // remove extra zeros
        if (curlength < decimal) {
          this.altAmt += zeros;
        } else if (curlength > decimal) {
          // set reduced decimals
          const sliceDecimals = (curlength - decimal) * -1;
          this.altAmt = this.altAmt.slice('.', sliceDecimals);
          // this.getControl('amount').setValue(inputVal.slice('.', sliceDecimals));

          // when currency has no decimal
        } else if (decimal === '0') {
          this.altAmt = this.altAmt.split('.')[0];
        }
        this.updateAmt()
      }
    }
  }

  calculateAmount() {
    this.value.lccAmt = (this.value.fxRt * (this.value.atlAmt || 0)) || 0;
    // this.updateValue();
  }

  ngOnDestroy() {
    if (this.currencyList$) {
      this.currencyList$.unsubscribe();
    }
  }
}
