import { FxAmt } from '../core/model/cmo/FxAmt';
export class MultiCurrencyModel extends FxAmt {
    constructor(code: string) {
        super();
        // default actual code and local code will be same
        this.atlCode = code;
        this.lccCode = code;
    }
}
