import { NgModule } from '@angular/core';
import { DateComponent } from './date.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { ControlMessageModule } from '../control-message/index';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [CommonModule, MaterialModule, ControlMessageModule, FormsModule],
    exports: [DateComponent],
    declarations: [DateComponent],
    providers: [],
})
export class DateModule { }
