import { Component, Input, OnInit } from '@angular/core';
import { ValidationService } from './../core/validation.service';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { FormGroup } from '@angular/forms';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
  providers: [
    MakeProvider(DateComponent),
    MakeLocaleMessageProvider(DateComponent)
  ],
})
export class DateComponent extends ValueAccessorBase<string> implements OnInit, LocaleMessage {

  // todo: FormGroup Support
  // todo: refactor NgModel

  @Input() value = '';
  @Input() type = '';
  @Input() placeholder: string;
  @Input() required = false;
  @Input() disabled = false;

  constructor(private validate: ValidationService) {
    super();
  }

  ngOnInit() {
  }

  loadLocaleMessage (locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  checkType() {

    let val = '';

    switch (this.type) {
      case 'dates':
        val = 'date';
      break;
      case 'times':
        val = 'time';
      break;
      case 'datetimes':
        val = 'datetime-local';
      break;
      default:
        val = 'text';
      break;

    }
    return val;
}


}
