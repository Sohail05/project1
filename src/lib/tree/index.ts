import { NgModule } from '@angular/core';
import { TreeComponent } from './tree.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TreeModule as TreeNodesModule } from 'angular-tree-component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MdCardModule } from '@angular/material';
import { MdIconModule } from '@angular/material';
import { MultiLinguaModule } from '../multi-lingua/index';

@NgModule({
    imports: [CommonModule, FlexLayoutModule, MdCardModule, TreeNodesModule, MdIconModule, MultiLinguaModule],
    exports: [TreeComponent],
    declarations: [TreeComponent],
    providers: [],
})
export class TreeModule { }
