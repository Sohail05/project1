import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'nt-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
})
export class TreeComponent implements OnInit {
  @Input() config;
  @Input() nodes;
  @Input() overrideTreeOpts: object;
  @Input() styleRows;
  // Emit clicked node to be consumed outside tree component
  @Output() nodeObject = new EventEmitter<object>();
  nodesKeys;
  headers;
  treeOpts = {
    levelPadding: 20,
    animateExpand: true,
    animateSpeed: 200,
    animateAcceleration: 1.5,
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          this.nodeObject.emit(node);
        }
      }
    },
  }

  constructor() { }
  ngOnInit() {
    this.treeOpts = Object.assign(this.treeOpts, this.overrideTreeOpts);
  }
}
