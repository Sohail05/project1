# Select
Select allow user to do selection on a set of options.<br>Here are available 3 types of select shows as following: single select, multiple select and autocomplete.
## **\<nt-select\>**

## Example of Single Select

```html
<nt-select 
    [(ngModel)]="data" 
    name="sample" 
    placeholder="Select sample single" 
    [options]="options" 
    [displayFn]="fn" 
    required>
</nt-select>
```

## Example of Multiple Select

```html
 <nt-select 
    [(ngModel)]="data1" 
    multiple 
    name="sample1" 
    placeholder="Select sample multiple"
    [options]="options"
    [displayFn]="fn" 
    required>
</nt-select>
```

## API References

|Name|Description|
|----|-----------|
|@Input() parent|_**`formGroup`**_<br>parent form| 
|@Input() key|_**`string`**_<br>Form control name| 
|@Input() codeType|_**`string`**_<br>Code Type Api Code|
|@Input() options|_**`string`**_<br>Array that storing the options values.|
|@Input() placeholder|_**`string`**_<br>Set of custom placeholder name.|
|@Input() required|_**`boolean`**_<br>|
|@Input() disabled|_**`boolean`**_<br>|
|@Input() multiple|_**`boolean`**_<br>| 
|@Input() displayFn|_**`(value: any) => string`**_<br>	Function that maps an option's control value to its display value.| 
|@Input() storeFn|_**`(value: any) => string`**_<br>	Function that maps an option's control value to its store value.| 
|@Input() validation|_**`Object`**_<br>Extra Validation: email,minlength,maxlength| 
