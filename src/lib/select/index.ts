import { ControlMessageModule } from '../control-message/index';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SelectComponent } from './select.component';

@NgModule({
    imports: [
        CommonModule,
        ControlMessageModule,
        FlexLayoutModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [SelectComponent],
    declarations: [SelectComponent],
    providers: [],
})
export class SelectModule { }
