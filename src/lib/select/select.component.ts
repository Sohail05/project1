import { Component, Input, OnInit, HostListener, Output, EventEmitter, Optional, Host, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormControl, ControlValueAccessor, NgForm, FormGroupDirective, NgControl } from '@angular/forms';
import { IUIComponent, IUIComponentData } from './../ui-component/ui-component';
import { MakeProvider } from '../value-accessor-base/value-accessor-base';
import { Subscription } from 'rxjs/Subscription';
import { CodeTypeContainer, MakeCodeTypeProvider } from '../code-type/code-type-container';
import { MakeLocaleMessageProvider, LocaleMessage } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { Functional } from '../core/functional';

@Component({
  selector: 'nt-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    MakeProvider(SelectComponent),
    MakeCodeTypeProvider(SelectComponent),
    MakeLocaleMessageProvider(SelectComponent)
  ],
})
export class SelectComponent implements OnInit, IUIComponent, CodeTypeContainer, ControlValueAccessor, LocaleMessage {

  // todo remove all commentted code after confirm remove parent and key
  // multi dropdown not supportted for parent and key
  // todo remove setErrors when not support parent and key
  private _value;
  get value() {
    return this._value;
  }

  set value(val: any) {
    if (this._value !== val) {
      this._value = val;
      this.propagateChange(val);
      if (this.parent && this.key) {
        this.control.setValue(val);
      }
    }
    this.setErrors();
  }
  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(val: any): void {
    this._value = val;
    this.initialValue = val;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }

  tooltip;

  data = <IUIComponentData>{};
  // Group Code
  private grpCode;
  // showRequired: boolean;
  // formSubmission: Subscription;
  control: FormControl;
  // Emitter for selected code type's code
  // @Output() onCodeTypeSelectedEvent = new EventEmitter();
  // Parent Form
  @Input() parent: FormGroup;
  // Form Control Name
  @Input() key;
  // Code Type
  @Input() codeType: string;
  // array of data
  @Input() options = [];
  // placeholder
  @Input() placeholder: string;
  // required
  @Input() required = false;
  // disabled
  @Input() isDisabled = false;
  // Multiple
  @Input() multiple = false;
  @Input() empty = true;
  // Display function
  @Input() displayFn = Functional.codeDescription;
  // Store function
  @Input() storeFn = Functional.codeValue;
  disabledFn = Functional.isDisabled;
  initialValue;
  // todo remove
  // Default
  @Input() default: string;
  // validation
  @Input() validation: object;

  get _disabled() {
    return this.isDisabled || (this.parent && this.key && (this.parent.disabled || this.control.disabled)) || this.data.disabled;
  }
  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }
  @ViewChild('inputControl') inputControl: NgControl;

  constructor(
    // private formService: FormService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
  ) {
  }

  ngOnInit() {
    // will be remove parent and key
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }

    // get init value form parent and key
    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      this._value = this.control.value;
      this.initialValue = this.value;

      // subscribe to dynamic value change
      // will escape value change from user
      this.control.valueChanges.subscribe((val) => {
        if (val !== this.value) {
          this.initialValue = val;
          this._value = val;
        }
      });
    }
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControl.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }

  // can remove when not supported parent and key anymore
  setErrors() {
    if (this.parent && this.key) {
      this.control.setErrors(this.inputControl.errors)
    }
  }

  isInitialValue(n) {
    if (this.initialValue && n) {
      return this.initialValue.includes(n['code'] || n)
    } else {
      return false;
    }
  }



  loadCodeType(options: any) {
    this.options = options;
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  setAttributes() {
    if (this.data.options) {
      this.options = this.data.options;
    }
    if (this.data.displayFn) {
      this.displayFn = this.data.displayFn;
    }
    if (this.data.storeFn) {
      this.storeFn = this.data.storeFn;
    }
    if (this.data.codeType) {
      this.codeType = this.data.codeType;
    }
  }

  /**
   * Filter inactive and no accessible Code Type
   */
  // filterCodeTypes(value) {
  //   return value.filter((x) => {
  //     return this.value === x.code ||
  //       (x.vldIdc === 'Y' && x.grpCodes ? ((x.grpCodes.length > 0) ? x.grpCodes.indexOf(this.grpCode) > -1 : false) : true);
  //   });
  // }

}
