import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nt-recursive-table',
  templateUrl: './recursive-table.component.html',
  styleUrls: ['./recursive-table.component.scss'],
})
export class RecursiveTableComponent implements OnInit {
  @Input() TableData: any[];
  constructor() { }

  ngOnInit() {
  }
  checkChild(child) {
    if (child.children !== undefined) {
      if (child.children.length > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

}
