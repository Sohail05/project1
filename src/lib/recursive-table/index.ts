import { NgModule } from '@angular/core';

import { RecursiveTableComponent } from './recursive-table.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [FormsModule, CommonModule],
    exports: [],
    declarations: [RecursiveTableComponent],
    providers: [],
})
export class RecursiveTableModule { }
