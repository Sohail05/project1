# Version Build

Use to display the ``Build No`` and ``Version No`` of application.

## Example

```html
    <nt-version-build></nt-version-build>
```

``Note : Please ensure the enviroment file have the 'version' and 'build' variables .``