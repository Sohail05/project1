import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';

@Component({
  selector: 'nt-version-build',
  templateUrl: './version-build.component.html',
  styleUrls: ['./version-build.component.scss'],
})
export class VersionBuildComponent implements OnInit {

  versionNo = environment.version;
  buildNo = environment.build;
  appName = environment.appName;
  env = environment.env;
  constructor() { }

  ngOnInit() {
  }

}
