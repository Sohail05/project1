import { NgModule } from '@angular/core';

import { VersionBuildComponent } from './version-build.component';

@NgModule({
    imports: [],
    exports: [VersionBuildComponent],
    declarations: [VersionBuildComponent],
    providers: [],
})
export class VersionBuildModule { }
