import { Component, OnInit, Input, OnDestroy, ViewChild, Host, Optional, OnChanges, SimpleChanges } from '@angular/core';
import { ValueAccessorBase, MakeProvider } from '../value-accessor-base/value-accessor-base';
import { FormGroup, FormControl, ControlValueAccessor, NgControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DateAdapter } from '@angular/material';
import { MakeLocaleMessageProvider, LocaleMessage } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { CustomDateAdapter } from './datepickerAdapter';

@Component({
  selector: 'nt-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    MakeProvider(DatepickerComponent),
    MakeLocaleMessageProvider(DatepickerComponent),
    { provide: DateAdapter, useClass: CustomDateAdapter },
  ]
})
export class DatepickerComponent implements OnInit, OnChanges, OnDestroy, LocaleMessage, ControlValueAccessor {

  private _value;
  get value() {
    return this._value;
  }

  set value(val: any) {
    if (this._value !== val) {
      this._value = val;
      this.propagateChange(new Date(val).getTime());
      if (this.parent && this.key) {
        this.control.setValue(new Date(val).getTime());
      }
    }
    this.setErrors();
  }

  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(val: any): void {
    this._value = new Date(val);
    this.setErrors()
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
  @Input() parent: FormGroup;
  @Input() key;
  @Input() disabled: boolean;
  @Input() required: boolean;

  /**
   * Min Date
   */
  private _min;
  @Input()
  get min() {
    return this._min;
  }
  set min(val) {
    this._min = new Date(val);
  }

  /**
   * Max Date
   */
  private _max;
  @Input()
  get max() {
    return this._max;
  }
  set max(val) {
    this._max = new Date(val);
  }

  @Input() touchUi;
  @Input() placeholder: string;
  @Input() tooltip: string;
  @Input() format = 'MM/DD/YYYY';
  @Input() monthLabel = 'MMM YYYY';
  // private innerValue: any;
  // private valueSubscriber: Subscription;
  // private valueChangeSubscriber: Subscription;
  control: FormControl;
  picker;
  showTooltip
  get _disabled() {
    return this.disabled || (this.parent && this.key && (this.parent.disabled || this.parent.get(this.key).disabled));
  }

  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }
  @ViewChild('inputControl') inputControl: NgControl;
  constructor(
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
    private dateAdapter: DateAdapter<Date>,
  ) {
    // super();
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  ngOnInit() {
    this.dateAdapter['formatDisplay'] = this.format;
    this.dateAdapter['monthLabelDisplay'] = this.monthLabel;
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    // will be remove after completly remove parent and key
    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      this.value = this.control.value;
      this.control.valueChanges.subscribe((val) => {
        if (new Date(this.value).getTime() !== val) {
          this._value = new Date(val);
        }
      })
    }
  }

  setErrors() {
    if (this.parent && this.key) {
      this.control.setErrors(this.inputControl.errors);
    }
  }

  blur() {
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControl.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }

  markTouch() {
    this.inputControl.control.markAsDirty();
  }

  ngOnDestroy() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['format']) {
      this.dateAdapter['formatDisplay'] = this.format;
    }

    if (changes['monthLabel']) {
      this.dateAdapter['monthLabelDisplay'] = this.monthLabel;
    }
  }

}
