import { NativeDateAdapter } from '@angular/material';
import { NT_DATE_FORMATS } from './date-format';
import * as moment from 'moment';

export class CustomDateAdapter extends NativeDateAdapter {
    public formatDisplay;
    public monthLabelDisplay;
    format(date: Date, displayFormat: Object): string {
        const display = JSON.stringify(displayFormat);
        const monthLabel = JSON.stringify(NT_DATE_FORMATS.display.monthYearLabel);
        // month label display
        if (display === monthLabel) {
            return moment(date).format(this.monthLabelDisplay);
        }
        return moment(date).format(this.formatDisplay);
    };

    parse(value: any): Date | null {
        if (!moment(value, this.formatDisplay, true).isValid()) {
            return null;
        } else {
            return new Date(moment(value, this.formatDisplay, true).toString());
        }
    }
}
