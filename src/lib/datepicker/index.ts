import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdDatepickerModule, MdInputModule, DateAdapter, MdNativeDateModule, MD_DATE_FORMATS } from '@angular/material';
import { DatepickerComponent } from './datepicker.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NT_DATE_FORMATS } from './date-format';
import { CustomDateAdapter } from './datepickerAdapter';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ControlMessageModule } from '../control-message/index';
import { MultiLinguaTooltipModule } from '../tooltip/index';

@NgModule({
    imports: [
        CommonModule,
        MdDatepickerModule,
        MdInputModule,
        FormsModule,
        ReactiveFormsModule,
        // MdNativeDateModule,
        FlexLayoutModule,
        MultiLinguaTooltipModule,
        ControlMessageModule
    ],
    declarations: [DatepickerComponent],
    exports: [DatepickerComponent],
    providers: [
        { provide: MD_DATE_FORMATS, useValue: NT_DATE_FORMATS },

    ]
})
export class DatepickerModule { }
