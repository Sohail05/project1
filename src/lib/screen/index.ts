import { NgModule, ModuleWithProviders } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { ScreenComponent } from './screen.component';
import { TopNavBarModule } from '../top-nav-bar/index';
import { RouterModule } from '@angular/router';
import { SidenavBarModule } from '../sidenav-bar/index';
import { FormsModule } from '@angular/forms';
import { InputModule } from '../input/index';
import { SidenavService } from '../sidenav-bar/sidenav/sidenav.service';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    SidenavBarModule,
    FormsModule,
    InputModule
  ],
  exports: [ScreenComponent],
  declarations: [ScreenComponent],
})
export class ScreenModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ScreenModule
    };
  }


 }
