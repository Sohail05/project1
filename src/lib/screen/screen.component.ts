import { Component, OnInit } from '@angular/core';
import { MultiLinguaService } from '../multi-lingua/multi-lingua.service';
import { AccessControlService } from '../access-control/access-control.service';

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.scss'],
})
export class ScreenComponent {

  constructor() {}

}
