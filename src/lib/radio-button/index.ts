import { ControlMessageModule } from '../control-message/index';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RadioButtonComponent } from './radio-button.component';
import { MdRadioModule } from '@angular/material';



@NgModule({
    imports: [
        CommonModule,
        ControlMessageModule,
        FlexLayoutModule,
        MdRadioModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [RadioButtonComponent],
    declarations: [RadioButtonComponent],
    providers: [],
})
export class RadioButtonModule { }
