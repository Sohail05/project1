import { Component, OnInit, Input, AfterViewInit, ViewChild, Optional, Host } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, ControlValueAccessor, NgModel, NgForm, FormGroupDirective } from '@angular/forms';
import { ValueAccessorBase, MakeProvider } from '../value-accessor-base/value-accessor-base';
import { Subscription } from 'rxjs/Subscription';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { FormService } from '../core/form.service';
import { CodeTypeContainer, MakeCodeTypeProvider } from '../code-type/code-type-container';
import { Locale } from '../core/model/locale';
import { CodeType } from '../code-type/code-type';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Functional } from '../core/functional';
@Component({
  selector: 'nt-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss'],
  providers: [
    MakeProvider(RadioButtonComponent),
    MakeCodeTypeProvider(RadioButtonComponent),
    MakeLocaleMessageProvider(RadioButtonComponent)
  ]
})
export class RadioButtonComponent implements
  OnInit, IUIComponent, CodeTypeContainer, LocaleMessage, ControlValueAccessor {
  private _value;
  get value() {
    return this._value;
  }

  set value(val) {
    if (this._value !== val) {
      this._value = val;

      this.propagateChange(val);
      if (this.parent && this.key) {
        this.control.setValue(val);
      }
    }
    this.setErrors();
  }
  propagateChange = (val) => { };
  propagateTouch = (val) => { };
  writeValue(val: any): void {
    this._value = val;
    this.initialValue = val;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }
  @ViewChild('inputControl') inputControl: NgModel;

  disabledFn = Functional.isDisabled;
  displayFn = Functional.codeDescription;
  displayValFn = Functional.codeValue;

  data = <IUIComponentData>{};
  showRequired: boolean;
  formSubmission;
  control: FormControl;
  @Input() options = [];
  @Input() required = false;
  @Input() isDisabled = false;
  @Input() parent: FormGroup;
  @Input() key;
  @Input() vertical = false;
  initialValue;
  @Input() placeholder;
  tooltip;


  get _disabled() {
    return this.isDisabled || (this.parent && this.key && (this.parent.disabled || this.parent.get(this.key).disabled));
  }
  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }
  // todo clean up template
  // implement displayFn
  constructor(
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
  ) {
  }

  loadCodeType(options: CodeType[]) {
    this.options = options;
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  // set errors for parent and key
  setErrors() {
    if (this.parent && this.key) {
      this.control.setErrors(this.inputControl.errors);
    }
  }
  isInitialValue(n) {
    if (this.initialValue && n) {
      return this.initialValue.includes(n['code'] || n)
    } else {
      return false;
    }
  }

  ngOnInit() {

    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      this.value = this.control.value;
      this.initialValue = this.value;
      this.control.valueChanges.subscribe((val) => {
        if (val !== this.value) {
          this.initialValue = val;
          this.value = val;
        }
      });
    }

  }


}
