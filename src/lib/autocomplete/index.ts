import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { MultiAutocompleteModule } from '../multi-autocomplete/index';
import { ControlMessageModule } from '../control-message/index';
import { AutocompleteComponent } from './autocomplete.component';
import { MultiLinguaTooltipModule } from '../tooltip/index';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ControlMessageModule,
    MultiAutocompleteModule,
    FlexLayoutModule,
    MaterialModule,
    MultiLinguaTooltipModule

  ],
  exports: [AutocompleteComponent],
  declarations: [AutocompleteComponent],
  providers: [],
})
export class AutocompleteModule { }
