# Autocomplete
Here are available 2 types of autocomplete shows as following: 
- autocomplete (single)
- multiple autocomplete
## **\<nt-autocomplete\>**

## Example of Single Autocomplete

```html
<nt-autocomplete 
    [(ngModel)]="data" 
    name="sample" 
    placeholder="autocomplete single" 
    type="autocomplete" 
    [options]="options" 
    [fn]="fn" 
    required>
</nt-autocomplete>
```

## Example of Multiple Autocomplete

```html
 <nt-autocomplete 
    [(ngModel)]="data1" 
    multiple="true" 
    name="sample1" 
    placeholder="autocomplete multiple"
    [options]="options"
    [fn]="fn" 
    required>
</nt-autocomplete>
```

## API References

|Name|Description|
|----|-----------|
|@Input() parent|_**`formGroup`**_<br>parent form| 
|@Input() key|_**`string`**_<br>Form control name| 
|@Input() codeType|_**`string`**_<br>Code Type Api Code|
|@Input() default|_**`any`**_<br>Default Value|
|@Input() options|_**`string`**_<br>Array that storing the options values.|
|@Input() placeholder|_**`string`**_<br>Set of custom placeholder name.|
|@Input() required|_**`boolean`**_<br>|
|@Input() disabled|_**`boolean`**_<br>|
|@Input() multiple|_**`boolean`**_<br>| 
|@Input() validation|_**`Object`**_<br>Validation| 
|@Input() fn|_**`(value: any) => string;`**_<br> Function to map display value| 
