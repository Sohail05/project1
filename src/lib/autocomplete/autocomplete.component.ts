import { Component, forwardRef, Input, OnInit, HostListener, ViewChild, OnDestroy, Optional, Host } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR, FormControl, FormGroupDirective, NgForm, NgControl } from '@angular/forms';
import { IUIComponent, IUIComponentData } from '../ui-component/ui-component';
import { ValidationService } from './../core/validation.service';
import { CodeTypeService } from '../code-type/code-type.service';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { MdAutocompleteTrigger } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { MakeProvider, ValueAccessorBase } from '../value-accessor-base/value-accessor-base';
import { CodeTypeContainer, MakeCodeTypeProvider } from '../code-type/code-type-container';
import { MakeLocaleMessageProvider, LocaleMessage } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { CodeType } from '../code-type/code-type';
import { Functional } from '../core/functional';


@Component({
  selector: 'nt-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AutocompleteComponent),
      multi: true,
    },
    MakeCodeTypeProvider(AutocompleteComponent),
    MakeLocaleMessageProvider(AutocompleteComponent)
  ]
})
export class AutocompleteComponent
  implements OnInit, OnDestroy, ControlValueAccessor, IUIComponent, CodeTypeContainer, LocaleMessage {

  /**
   * TODO
   * -add code type service
   * -store method
   * - extend the ValueAccessorBase class
   */
  data = <IUIComponentData>{};
  private codeTypeSubscriber: Subscription;
  private valueSubscriber: Subscription;
  private innerValue: any;

  showTooltip
  @Input() tooltip = '';

  // Function to map display value
  displayFn = Functional.codeDescription;
  disabledFn = Functional.isDisabled;
  @Input() fn = Functional.codeDescription;
  @Input() storeFn = Functional.codeValue;

  // filtered list of auto complete
  filteredList: any = [];
  // Temp should get from service
  // Group Code
  private grpCode;
  private selected;
  query;
  auto;
  @ViewChild(MdAutocompleteTrigger) fx: MdAutocompleteTrigger;
  // Parent Form
  @Input() parent: FormGroup;
  // Form Control Name
  @Input() key;
  // Code Type
  @Input() codeType: string;
  // array of data
  @Input() options: any[] = [];
  // placeholder
  @Input() placeholder: string;
  // required
  @Input() required = false;
  // disabled
  @Input() disabled: boolean | string = false;
  // validation
  @Input() validation: object = {};
  // showRequired: boolean;
  // formSubmission: Subscription;
  control: FormControl;
  propagateChange = (_: any) => { };


  loadCodeType(options: CodeType[]) {
    this.options = options
    this.setSelectedValFromStored();
    this.setDisplayVal();
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }


  get _disabled() {
    return this.disabled || (this.parent && this.key && (this.parent.disabled || this.control.disabled || this.data.disabled));
  }

  get value(): any {
    return this.innerValue;
  }
  set value(val: any) {
    if (this.innerValue !== val) {
      this.innerValue = val;
      this.propagateChange(val);
      if (this.parent && this.key) {
        this.control.setValue(val);
      }
    }
  }
  get _submitted() {
    return (this._parentForm && this._parentForm.submitted) || (this._parentFormGroup && this._parentFormGroup.submitted)
  }
  @ViewChild('inputControl') inputControl: NgControl;
  constructor(
    private validationService: ValidationService,
    private codeTypeService: CodeTypeService,
    // private formService: FormService,
    @Host() @Optional() private _parentFormGroup: FormGroupDirective,
    @Host() @Optional() private _parentForm: NgForm,
  ) {
  }

  select(item) {

    // Do not set disabled items
    if (Functional.isDisabled(item)) {
      return;
    }

    this.filteredList = [];
    this.selected = item;
    this.setDisplayVal();
    this.setErrors();
  }

  add(event) {
    if (this.filteredList.length > 0) {
      if (this.fx.activeOption) {
        const selectedValue = this.fx.activeOption.value
        this.select(selectedValue);
      }
    }
    event.preventDefault();
    this.fx.closePanel();
  }

  blur() {
    if (this.query !== this.fn(this.selected)) {
      this.selected = {};
    }
    this.setDisplayVal();
    // reset control to untouch
    // if (!this._submitted) {
    this.inputControl.control.markAsUntouched();
    if (this.parent && this.key) {
      this.control.markAsUntouched();
    }
    // }
    this.setErrors();
  }

  setDisplayVal() {
    if (this.options && this.options.length > 0) {
      this.query = this.fn(this.selected);
      this.value = this.storeFn(this.options.find((x) => x === this.selected) || '');
    }
  }

  setSelectedValFromStored() {
    if (this.options && this.options.length > 0) {
      this.selected = this.options.find((x) => this.storeFn(x) === this.value) || {};
    }
  }

  writeValue(value: any) {
    if (value) {
      this.innerValue = value;
      this.setSelectedValFromStored();
      this.setDisplayVal();
    }
  }

  registerOnChange(fn: (value: any) => void) {
    this.propagateChange = fn;
  }

  touch() { }

  registerOnTouched(fn: () => void) { }
  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
  ngOnInit() {
    if (this._parentFormGroup) {
      this._parentFormGroup.ngSubmit.subscribe((val) => {
        this.setErrors();
      });
    }
    if (this.codeType) {
      this.codeTypeSubscriber = this.codeTypeService.getCodeTypes(this.codeType)
        .subscribe((success) => {
          this.options = this.filterCodeTypes(success);

          this.setSelectedValFromStored();
          this.setDisplayVal();
        });
    }
    if (this.parent && this.key) {
      this.control = this.parent.get(this.key) as FormControl;
      const keyVal = this.control.value;
      this.innerValue = keyVal;
      this.valueSubscriber = this.control.valueChanges.subscribe((x) => {
        if (this.value !== x) {
          this.innerValue = x;
          this.setSelectedValFromStored();
          this.setDisplayVal();
        }
      })
    }

  }
  setAttributes() {
    if (this.data.options) {
      this.options = this.data.options;
    }
    if (this.data.displayFn) {
      this.fn = this.data.displayFn;
    }
    if (this.data.codeType) {
      this.codeType = this.data.codeType;
    }
  }

  setErrors() {
    if (this.parent && this.key) {
      this.control.setErrors(this.inputControl.errors);
    }
  }

  /**
   * Filter inactive and no accessible Code Type
   */
  filterCodeTypes(value) {
    return value.filter((x) => {
      return this.value === x.code ||
        (x.sts !== 'I' && x.grpCodes ? ((x.grpCodes.length > 0) ? x.grpCodes.indexOf(this.grpCode) > -1 : false) : true);
    });
  }

  /**
   * Filter based on user input
   */
  filter() {
    if (this.options) {
      this.filteredList = this.query ? this.options.filter(
        (s) => {
          let f = this.fn(s);
          let qry = this.query;
          if (typeof qry !== 'string') {
            qry = qry + '';
          }
          if (typeof f !== 'string') {
            f = f + '';
          }
          return f.toLowerCase().indexOf(qry.toLowerCase()) > -1;
        }
      )
        : this.options;
    }
  }
  ngOnDestroy() {
    if (this.valueSubscriber) {
      this.valueSubscriber.unsubscribe();
    }
    if (this.codeTypeSubscriber) {
      this.codeTypeSubscriber.unsubscribe();
    }
  }

}
