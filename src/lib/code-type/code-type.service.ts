import { Injectable } from '@angular/core';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMap';
import { Observable } from 'rxjs/Observable';
import { CodeType } from './code-type';
import { ApiService } from '../core/api/api.service';
import { environment } from 'environments/environment';

@Injectable()
export class CodeTypeService {

  /**
   * Cached codeTypes.
   */
  codeTypes: Map<string, Array<CodeType>> = new Map();

  constructor(private api: ApiService) {}

  /**
   * Get the codeTypes.
   * @param {string} code
   * @return {Observable}
   */
  getCodeTypes(code: string): Observable<any> {

    if (this.codeTypes.has(code)) {
      return Observable.of(this.codeTypes.get(code));
    }

    return this.requestCodeTypes(code);
  }

  /**
   * Request codeTypes from API.
   * @param code
   */
  requestCodeTypes(code: string): Observable<any> {
    const obv = this.api.get(environment.api.codeType, { code });
    obv.subscribe((codeTypes) => { this.codeTypes.set(code, codeTypes) })
    return obv;
  }

  resolveCodeType(code, value) {

    const find = (ct: CodeType) => ct.code === value;

    if (this.codeTypes.has(code)) {
      return Observable.of(this.codeTypes.get(code).find(find));
    }
    return this.requestCodeTypes(code)
  }

}
