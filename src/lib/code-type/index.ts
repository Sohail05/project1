import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CodeTypeDirective } from './code-type.directive';
import { CodeTypeService } from './code-type.service';
import { CodeTypeConfig, CodeTypeConfigInterface } from './code-type-config';
export * from './code-type-config'

@NgModule({
    imports: [],
    exports: [CodeTypeDirective],
    declarations: [CodeTypeDirective],
    providers: [],
})
export class CodeTypeModule {
     static forRoot(/*config: CodeTypeConfigInterface*/): ModuleWithProviders {
        return {
            ngModule: CodeTypeModule,
            providers: [ CodeTypeService,
                 /*{ provide: CodeTypeConfig, useValue: config }*/
            ]
        };
    }
}
