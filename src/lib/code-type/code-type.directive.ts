import { Directive, Input, OnChanges, SimpleChanges, Inject, Self, Optional, ElementRef } from '@angular/core';
import { CodeTypeService } from './code-type.service';
import { CodeTypeContainer, CODE_TYPE_CONTAINER_TOKEN } from './code-type-container';
import { MdSelect } from '@angular/material';

@Directive({
  selector: '[ntCodeType]',
})
export class CodeTypeDirective implements OnChanges {

  @Input() ntCodeType: string;

  constructor( @Inject(CODE_TYPE_CONTAINER_TOKEN) @Self() @Optional() private component: CodeTypeContainer,
    private codeTypeService: CodeTypeService,
    private el: ElementRef) {
  }

  ngOnChanges(change: SimpleChanges) {
    if (change['ntCodeType']) {
      this.getCodeType();
    }
  }

  getCodeType() {
    this.codeTypeService.getCodeTypes(
      this.ntCodeType).subscribe((codeTypes) => {
        if (this.component) {
          this.component.loadCodeType(codeTypes);
        } else {
          this.handleError('');
        }
      },
      this.handleError.bind(this))
  }

  handleError(error) {
    // todo: send error message to backend
    console.error(error)
    console.error('Component does not implement loadCodeType()', this.el.nativeElement);
  }

}
