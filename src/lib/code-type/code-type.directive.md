# Directive: Code Type

This is a guide to show how to apply this directive to obtain any particular code type.

## Usage Example

````html
<md-select
    ntCodeType="CODE-CUR"
    name="type"
    placeholder="Currency">
    <md-option *ngFor="let c of currencyList"
      [value]="c.code">{{c.dsc}}
    </md-option>
</md-select>
````

One things to note here:
1. `ntCodeType="YOUR_CODE_TYPE_NAME_HERE"`  

   In this case, I would like to retrieve all the currencies that are associated with the code type key `CODE-CUR`, therefore `ntCodeType="CODE-CUR"`.