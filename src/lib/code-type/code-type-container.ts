import { CodeType } from './code-type';
import { InjectionToken, forwardRef } from '@angular/core';
export const CODE_TYPE_CONTAINER_TOKEN = new InjectionToken<CodeTypeContainer>('CodeTypeToken');

export abstract class CodeTypeContainer {
    options: Array<CodeType>;
    displayFn: (n: any) => string;
    loadCodeType: (options) => any;
}

export function MakeCodeTypeProvider(component) {

    return {
        provide: CODE_TYPE_CONTAINER_TOKEN,
        useExisting: forwardRef(() => component)
    };
}
