export class CodeType {
    code: string;
    dsc: string;
    vldIdc?: string;
}
