export interface CodeTypeConfigInterface {
    url: string;
    lang?: string;
}

export class CodeTypeConfig implements CodeTypeConfigInterface {

    public static config: CodeTypeConfigInterface;
    public url: string;
    public lang?: string;

   public static Configure(config: CodeTypeConfigInterface) {
        return new CodeTypeConfig(config);
    }

    constructor(config: CodeTypeConfigInterface) {
        config.lang = config.lang || 'en';
        this.assign(config);
    }

    public assign(config: CodeTypeConfigInterface) {
        for (const key in config) {
            if (config.hasOwnProperty(key)) {
                this[key] = config[key];
            }
        }

    }
}
