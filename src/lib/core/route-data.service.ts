import { Injectable } from '@angular/core';

@Injectable()
export class RouteDataService {

  private val: any = {};

  constructor() {
  }

  /**
   * Get data from variable
   * @param key
   */
  getVal(key: string) {
    return this.val[key];
  }

  /**
   * Get data from variable and delete the data
   */
  getValOnce(key: string) {
    const data = this.val[key];
    this.val[key] = undefined;
    return data;
  }

  /**
   * remove data from variable
   * @param key
   */
  removeVal(key: string) {
    delete this.val[key];
  }

  /**
   * Set data to variable
   * @param key
   * @param val
   */
  setVal(key: string, val: any) {
    this.val[key] = val;
  }

  /**
   * Get data from local storage
   * @param key
   */
  getLocal(key: string) {
    return localStorage.getItem(key) || null;
  }

  /**
   * Get data from local storage and delete the data
   * @param key
   */
  getLocalOnce(key: string) {
    const data = localStorage.getItem(key);
    localStorage.removeItem(key);
    return data;
  }

  /**
   * remove data from local storage
   * @param key
   */
  removeLocal(key: string) {
    localStorage.removeItem(key);
  }

  /**
   * Set data to local storage
   * @param key
   * @param val
   */
  setLocal(key: string, val: any) {
    localStorage.setItem(key, val);
  }

  /**
   * Get data from session storage
   * @param key
   */
  getSession(key: string) {
    return sessionStorage.getItem(key) || null;
  }

  /**
   * Get data from session storage and delete the data
   * @param key
   */
  getSessionOnce(key: string) {
    const data = sessionStorage.getItem(key);
    sessionStorage.removeItem(key);
    return data;
  }

  /**
   * remove data from session storage
   * @param key
   */
  removeSession(key: string) {
    sessionStorage.removeItem(key);
  }

  /**
   * Set data to session storage
   * @param key
   * @param val
   */
  setSession(key: string, val: any) {
    sessionStorage.setItem(key, val);
  }
}
