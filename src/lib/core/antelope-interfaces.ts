export interface AntelopeEnvironment {
    production: boolean;
    version: string,
    build: string,
    applicationName: string,
    applicationId: string,
    clientId: string,
    env: string,
    logo: string,
    apiUrl: string,
    authUrl: string,
    iconUrl: string,
    loginRoute: string,
    api: AntelopeApi,
    phone: Phone,
    oauth: OAuthApi,
    [propName: string]: any;
}

export interface AntelopeApi {
    workflow: WorkflowApi;
    system: SystemApi;
    /** Todo: move to system API */
    langs: string;
    /** Todo: move to system API */
    locales: string;
    /** Todo: move to system API */
    localeCode: string;
    codeType: string;
    /** Todo: move to access API */
    accessMenu: string;
    accessField: string;
    accessRecord: string;
    userInfo: string;
    [propName: string]: any;
}

export interface WorkflowApi {
    accessField: string;
    accessRecord: string;
    access: string;
    pickup: string;
    cancel: string;
    proceed: string;
    rework: string;
    returnTask: string;
    reworkTasks: string;
    view: string;
    actionList: string;
    [propName: string]: any;
}

export interface SystemApi {
    appProperties: string;
    /** locales */
    /** locale code */
    log: string;
    /** langs */
    [propName: string]: any;
}

export interface AccessApi {
    accessMenu: string;
    accessField: string;
    accessRecord: string;
    [propName: string]: any;
}

export interface Phone {
    no: string;
    maxLength: number;
}
export interface OAuthApi {
    authorize: string;
    token: string;
    logout: string;
    lastUserAccess: string;
    [propName: string]: any;
}
