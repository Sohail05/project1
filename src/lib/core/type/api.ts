
/**
 * Field Access Mode found in Security Access APIs.
 * 0 (Hidden), 1 (Readonly), 2 (Editable)
 */
export type fldAcsMde = 0 | 1 | 2;
