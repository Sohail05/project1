import { CodeType } from '../code-type/code-type';

/**
 * tmp class to gauge necessity or eventually create methods on codeType Object.
 */
export class Functional {

    /**
     * Check if CodeType is valid.
     * @param codeType CodeTypeObject
     */
    static isDisabled(codeType: CodeType) {
        if (codeType) {
            return codeType.vldIdc === 'N' ? true : false;
        } else {
            return true
        }
    }

    static codeDescription(codeType: CodeType) {
        return codeType.dsc;
    }

    static codeValue(codeType: CodeType) {
        return codeType.code;
    }

}
