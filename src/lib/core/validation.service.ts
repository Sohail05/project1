import { AbstractControl, FormControl, ValidatorFn, Validators, FormGroup } from '@angular/forms';
import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

export interface MaskConfig {
  mask: boolean | RegExpMatchArray;
  guide: boolean;
  keepCharPositions?: boolean;
  pipe?: Function;
  placeholderChar?: string;
};
const defaultMaskConfig: MaskConfig = {
  mask: false,
  guide: false,
};
const allMaskConfig = {
  date: {
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
    guide: true,
    keepCharPositions: true,
    pipe: Function,
    placeholderChar: '\u2000',
  }
};
const masksTable = {
  decimal: (options) => createNumberMask(options),
  currency: (options) => createNumberMask(options),
  number: (options) => createNumberMask(options),
  percent: (options) => createNumberMask(options),
  date: (options) => createAutoCorrectedDatePipe(options),
};
const maskRegEx = {
  d: /\d/
};
const validationRegEx = {
  // tslint:disable-next-line:max-line-length
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
}

// todo: add a function for error message customization, missing min value and max value (interger) checking
const humaneErrorMsg = {
  required: (name, validateVal?) => `${name} is required`,
  minlength: (name, validateVal) => `Minimum length ${validateVal.requiredLength}`,
  email: () => 'Email is invalid',
  pattern: (name) => `Invalid ${name} input for the selected type`,
  range: (name, validateVal) => `Value must be between ${validateVal}`
};

export class ValidationService {

  private _defaultMasksOpts = {
    decimal: {
      prefix: '',
      allowDecimal: true,
    },
    currency: {
      prefix: '$ ',
      allowDecimal: true,
    },
    percent: {
      prefix: '',
      suffix: ' %',
      allowDecimal: true,
      includeThousandsSeparator: false,
    },
    number: {
      prefix: '',
      includeThousandsSeparator: false,
      allowDecimal: true,
      allowNegative: true,
    },
    date: {
      format: 'mm/dd/yyyy',
    },
  };

  validatorMap = {
    required: Validators.required,
    minlength: Validators.minLength,
    email: this.getRegexValidator('email'),
    pattern: Validators.pattern,
    range: this.getRangeValidator,
    consolidateVal(validationObj) {
      const validatorArray = [];
      for (const rule in validationObj) {
        if (typeof validationObj[rule] !== 'boolean' &&
          typeof this[rule] === 'function') {
          validatorArray.push(this[rule](validationObj[rule]));
        } else {
          validatorArray.push(this[rule]);
        }
      }
      return validatorArray;
    },
  };

  overrideDefaultMasks(obj: { [key: string]: any }) {
    this._defaultMasksOpts = Object.assign(this._defaultMasksOpts, obj);
  }
  getRangeValidator(range): ValidatorFn {
    return (control: AbstractControl): { [key: string]: string } => {
      let isValid = true;
      if (control.value > range.max || control.value < range.min) {
        isValid = false;
      };
      return isValid ? null : { range: `${range.min} - ${range.max}` };
    }
  }
  getRegexValidator(type): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } => {
      const isValid = validationRegEx[type].test(control.value);
      return isValid ? null : { [type]: true };
    };
  }
  getConditionalValidator(conditionObj): ValidatorFn {
    const keys = Object.keys(conditionObj);
    return (control: AbstractControl): { [key: string]: boolean } => {
      const formGroup = control.root;
      if (!(formGroup instanceof FormGroup)) {
        return;
      }

      const isValid = control[keys[1]] === conditionObj[keys[1]];
      const targetControl = formGroup.controls[conditionObj[keys[0]]];

      if (control.dirty && isValid) {
        targetControl.enable();
      }
      if (!isValid) {
        targetControl.disable();
      }
      return null;
    };
  }
  getValidatorErrorMessage(validatorName: string, name?: string, validatorValue?: any) {
    return humaneErrorMsg[validatorName] ? humaneErrorMsg[validatorName](name, validatorValue) : validatorName;
  }

  getInputMask(type: string, options: object) {
    let inputMaskOptions = this._defaultMasksOpts[type];
    const maskConfig = Object.assign({}, defaultMaskConfig);
    if (typeof type === 'undefined' || type === 'text') {
      return maskConfig;
    }
    if (type === 'date') {
      const mask = allMaskConfig[type];
      mask.pipe = masksTable[type](inputMaskOptions.format);
      return mask;
    }
    if (!masksTable[type]) {
      const maskingType = this.processMask(type);
      maskConfig.mask = maskingType;
      return maskConfig;
    }
    inputMaskOptions = Object.assign(this._defaultMasksOpts[type], options);
    maskConfig.mask = masksTable[type](inputMaskOptions);
    return maskConfig;
  }

  getAllValidator(obj) {
    return this.validatorMap.consolidateVal(obj);
  }
  processMask(mask): Array<any> {
    return mask
      .replace(/\\/g, '')
      .split('')
      .map((char) => maskRegEx[char] ? maskRegEx[char] : char)
  }
  getMaskingValidator(val) {
  }
}
