import { Injectable, ErrorHandler } from '@angular/core';
import { ApiService } from './api/api.service';
import { environment } from 'environments/environment';
/**
 * To use the ErrorLoggerService
 * Add the following code into the provider of app.module.ts
 *     { provide: ErrorHandler, useClass: ErrorLoggerService }
 *
 * Or By import the Core Module with forRoot()
 *
 * It will override the ErrorHandler of Angular
 *
 * Note: ApiService need to provide before the ErrorLoggerService order is important
 */
@Injectable()
export class ErrorLoggerService implements ErrorHandler {

    constructor(private api: ApiService) { }

    handleError(error) {
        if (!environment.production) {
            console.error(error);
        }
        const errorString = JSON.stringify(error, ['message', 'arguments', 'type', 'name']);
        console.log(typeof errorString);

        this.api
            .putWithQuery(environment.api.system.log, {}, { errStack: errorString })
            .toPromise()
            .then((x) => {
                console.warn('Error has been logged.')
            });
    }

}
