import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { ValidationService } from './validation.service';
import { IconsService } from './icons/icons.service';
import { SidenavFormService } from '../sidenav-form/sidenav-form.service';
import { SidenavService } from '../sidenav-bar/sidenav/sidenav.service';
import { ApiService } from './api/api.service';
import { FormService } from './form.service';
import { DirtyCheckService } from './dirty-check.service';
import { DirtyCheckGuardService } from './dirty-check-guard.service';
import { ErrorHandlerService } from './api/error-handler';
import { DeleteDialogModule } from '../delete-dialog/index';
import { UserService } from './user.service';
import { RouteDataService } from './route-data.service';
import { WorkflowService } from '../workflow/workflow.service';
import { ErrorLoggerService } from './error-logger';

// NOTE:
// This core folder is a place for:
// 1) Single - use components (e.g., spinners, message toasts, and modal
//    dialogs) that appear only in the AppComponent template
// 2) Singleton services which each must be registered EXACTLY ONCE when the app starts.

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    DeleteDialogModule
  ],
  declarations: [],
  providers: [],
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        ValidationService,
        IconsService,
        SidenavFormService,
        SidenavService,
        ApiService,
        ErrorHandlerService,
        FormService,
        DirtyCheckService,
        DirtyCheckGuardService,
        UserService,
        RouteDataService,
        WorkflowService,
        { provide: ErrorHandler, useClass: ErrorLoggerService }
      ]
    };
  }
}
