import { Injectable, EventEmitter, HostListener } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class DirtyCheckService {
  private dirty = {};
  statusChange = new EventEmitter();
  cleared = new EventEmitter();
  clearedAll = new EventEmitter();
  private formValueStore: object = {};

  constructor() { }
  /**
   * Get dirty status of form
   * @param indicator:string
   * Optional default to form
   */
  getDirtyStatus(indicator: string = 'form') {
    return this.dirty[indicator];
  }

  /**
   * Set dirty status of form
   * @param status :boolean
   * @param indicator :string
   * Optional default to form
   */
  setDirtyStatus(stat: boolean, indicator: string = 'form') {
    this.dirty[indicator] = stat;
    this.statusChange.emit();
  }

  setCurrentFormValue(key, value: object) {
    this.formValueStore[key] = value;
  }
  getCurrentFormValue(key: string) {
    return this.formValueStore[key];
  }

  /**
   * Clear all form dirty stat in current component
   */
  clearAll() {
    this.dirty = {};
    this.statusChange.emit();
    this.clearedAll.emit();
  }

  /**
   * Clear specific form in current component
   * @param val : string indicator of form
   */
  clear(val: string = 'form') {
    this.dirty[val] = false;
    this.statusChange.emit();
    this.cleared.emit(val);
  }

  /**
   * Prompt
   */
  promptComfirm() {
    const confirm = window.confirm('Are you sure you want to leave the page and discard the changes?');
    if (confirm) {
      this.dirty = {};
    }
    return confirm;
  }
}
