import { Injectable } from '@angular/core';
import { ApiService } from './api/api.service';
import { User } from './model/user';
import { environment } from 'environments/environment';
import { RouteDataService } from './route-data.service';

@Injectable()
export class UserService {

  get user(): User {
    return JSON.parse(this.routeDataService.getSession('loggedInUser'));
  }
  set user(user: User) {
    this.routeDataService.setSession('loggedInUser', JSON.stringify(user));
  }

  constructor(
    private api: ApiService,
    private routeDataService: RouteDataService
  ) {
    this.getUser();
  }
  getUser(): Promise<any> {
    if (this.user) {
      return Promise.resolve(this.user);
    } else {
      return this.loadUser();
    }
  }
  loadUser() {
    return this.api.get(environment.api.userInfo)
      .toPromise()
      .then(this.format.bind(this));
  }

  format(user): User {
    this.user = user;
    return user as User;
  }
}
