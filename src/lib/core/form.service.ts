import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class FormService {
  private formSubmittedSource = new Subject<boolean>();
  formSubmitted$ = this.formSubmittedSource.asObservable();

  constructor() { }
  triggerValidation(val: boolean) {
    this.formSubmittedSource.next(val);
  }
}
