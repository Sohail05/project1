import { InjectionToken, forwardRef } from '@angular/core';
import { Locale } from './model/locale';
export const LOCAL_MESSAGE_TOKEN = new InjectionToken<LocaleMessage>('LocalePlaceToken');
export abstract class LocaleMessage {
  loadLocaleMessage: (locale: Locale) => any;
}

export function MakeLocaleMessageProvider(component) {

    return {
        provide: LOCAL_MESSAGE_TOKEN,
        useExisting: forwardRef(() => component)
    };
}
