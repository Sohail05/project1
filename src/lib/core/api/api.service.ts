import { Injectable } from '@angular/core';
import { Http, Headers, ResponseContentType, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { environment } from 'environments/environment';
import { ErrorHandlerService } from './error-handler';
import { ApiOptions } from './api-options';
import { SessionService } from '../../auth/session.service';

@Injectable()
export class ApiService {
    private domain = environment.apiUrl;
    public options: ApiOptions;
    constructor(
        private http: Http,
        private errorHandler: ErrorHandlerService,
        private session: SessionService
    ) {
        this.domain = environment.apiUrl;
        this.options = new ApiOptions();
    }

    /**
     * GET REQUEST
     * @param url : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    get(url: string, params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        const path = this.updateUrl(url, params);
        return this.process(this.http.get(path, opt));
    }

    /**
     * GET REQUEST WITH QUERY
     * @param url : string
     * @param query : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    getWithQuery(url: string, query: object,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.params = this.processQuery(query);
        const path = this.updateUrl(url, params);
        return this.process(this.http.get(path, opt))
    }


    /**
     * GET REQUEST File
     * @param url : string
     * @param fileType
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    getFile(url: string, fileType: string,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.headers.append('Accept', fileType);
        opt.responseType = ResponseContentType.Blob;
        const path = this.updateUrl(url, params);
        return this.http.get(path, opt).map((res) => {
            return new Blob([res['_body']], { type: fileType })
        })
            .catch(this.errorHandler.processApiError.bind(this.errorHandler));
    }

    /**
     * POST REQUEST
     * @param url : string
     * @param body
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    post(url: string, body: any,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        const path = this.updateUrl(url, params);
        return this.process(this.http.post(path, JSON.stringify(body), opt));
    }

    /**
     * POST REQUEST WITH QUERY
     * @param url : string
     * @param body
     * @param query : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    postWithQuery(url: string, body: any, query: object,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.params = this.processQuery(query);
        const path = this.updateUrl(url, params);
        return this.process(this.http.post(path, JSON.stringify(body), opt));
    }

    /**
     * POST REQUEST
     * @param url : string
     * @param file : Files[]
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    postFile(url: string, fileTag: string, file: File[],
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.headers.delete('Content-Type');
        const path = this.updateUrl(url, params);
        const formData: FormData = this.processFormData(file, fileTag);
        return this.process(this.http.post(path, formData, opt));
    }

    /**
     * POST REQUEST WITH QUERY
     * @param url : string
     * @param file:Files[]
     * @param query : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    postWithQueryFile(url: string, fileTag: string, file: File[],
        query: object, params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.params = this.processQuery(query);
        opt.headers.delete('Content-Type');
        const path = this.updateUrl(url, params);
        const formData: FormData = this.processFormData(file, fileTag);
        return this.process(this.http.post(path, formData, opt));
    }

    /**
     * POST REQUEST FORMDATA
     * @param url : string
     * @param formData:FormData
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    postFormData(url: string, formData: FormData,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.headers.delete('Content-Type');
        const path = this.updateUrl(url, params);
        return this.process(this.http.post(path, formData, opt));
    }

    /**
     * POST REQUEST WITH QUERY AND FORMDATA
     * @param url : string
     * @param formData:FormData[]
     * @param query : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    postFormDataWithQuery(url: string, formData: FormData,
        query: object, params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.params = this.processQuery(query);
        opt.headers.delete('Content-Type');
        const path = this.updateUrl(url, params);
        return this.process(this.http.post(path, formData, opt));
    }

    /**
     * PATCH REQUEST
     * @param url : string
     * @param body
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    patch(url: string, body: any,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        const path = this.updateUrl(url, params);
        return this.process(this.http.patch(path, JSON.stringify(body), opt));
    }

    /**
     * PATCH REQUEST WITH QUERY
     * @param url : string
     * @param body
     * @param query : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    patchWithQuery(url: string, body: any, query: object,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.params = this.processQuery(query);
        const path = this.updateUrl(url, params);
        return this.process(this.http.patch(path, JSON.stringify(body), opt));
    }

    /**
     * PUT REQUEST
     * @param url : string
     * @param body
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    put(url: string, body: any,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        const path = this.updateUrl(url, params);
        return this.process(this.http.put(path, JSON.stringify(body), opt))
    }

    /**
     * PUT REQUEST WITH QUERY
     * @param url : string
     * @param body
     * @param query : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    putWithQuery(url: string, body: any, query: object,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.params = this.processQuery(query);
        const path = this.updateUrl(url, params);
        return this.process(this.http.put(path, JSON.stringify(body), opt))
    }

    /**
     * DELETE REQUEST
     * @param url : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    delete(url: string, params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        const path = this.updateUrl(url, params);
        return this.process(this.http.delete(path, opt));
    }

    /**
     * DELETE REQUEST WITH QUERY
     * @param url : string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    deletewWithQuery(url: string, query: object,
        params?: object, specificHeadersKey?: string | object): Observable<any> {
        const opt = this.processHeaders(specificHeadersKey);
        opt.params = this.processQuery(query);
        const path = this.updateUrl(url, params);
        return this.process(this.http.delete(path, opt));
    }

    /**
     * set params for url path
     * @param req :string
     * @param params : Object , optional
     * @param specificHeadersKey : string , optional
     */
    private updateUrl(req: string, params?: object) {
        if (params) {
            const keys = Object.keys(params);
            if (keys.length > 0) {
                keys.map((key) => {
                    if (key) {
                        req = req.replace(`{${key}}`, `${params[key]}`)
                    }
                });
            }
        }

        return this.domain + req;
    }

    /**
     * Post-Process on Observable object
     * @param obv Observable
     */
    private process(obv: Observable<any>): Observable<any> {
        return obv
            .map((res) => {

                const data = res.json();
                return data ? data.data : data || {};
            })
            .catch(this.errorHandler.processApiError.bind(this.errorHandler))
            .share();
    }

    // todo: refactor
    /**
     * CLONE NEW OPTIONS WITH EXISTING OPTIONS AND HEADERS
     */
    private newOptionsHeaders() {
        const options = new RequestOptions(this.options.options);
        const headers = new Headers(this.options.headers);
        options.headers = headers;
        return options;
    }
    // todo: refactor
    /**
     * PROCESS OPTIONS WITH SPECIFIC HEADERS BY KEY
     * @param val :string
     */
    private processHeaders(val: string | object) {
        const opt = this.newOptionsHeaders();

        if (this.session.isValid()) {
            opt.headers.set('Authorization', 'bearer ' + this.session.token)
        }
        if (this.session.lang) {
            opt.headers.set('xLanguage', this.session.lang)
        }

        if (typeof val === 'string') {
            const specOpt = this.options.getspecificHeaders(val);
            if (val && specOpt) {
                specOpt.keys().map((x) => {
                    opt.headers.set(x, specOpt.get(x));
                });
            }
        } else if (typeof val === 'object') {
            const keys = Object.keys(val);
            if (keys.length > 0) {
                keys.map((key) => {
                    const value = val[key];
                    if (value) {
                        const specOpt = this.options.getspecificHeaders(value);
                        if (value && specOpt) {
                            specOpt.keys().map((x) => {
                                opt.headers.set(x, specOpt.get(x));
                            });
                        }
                    }
                });
            }
        }
        return opt;
    }

    /**
     * PROCESS FILE TO FORMDATA
     * @param file :File[]
     * @param fileTag :string
     */
    processFormData(file: File[], fileTag: string) {
        const formData: FormData = new FormData();
        file.map((f) => {
            formData.append(fileTag, f, f.name);
        });
        return formData;
    }

    private processQuery(query: object): URLSearchParams {
        const params: URLSearchParams = new URLSearchParams();
        if (query) {
            Object.keys(query).map((x) => {
                if (typeof query[x] === 'object') {
                    Object.keys(query[x]).map((q) => {
                        if (query[x][q]) {
                            params.append(x, query[x][q]);
                        }
                    });
                } else {
                    if (query[x]) {
                        params.append(x, query[x]);
                    }
                }
            });
        }
        return params;
    }
}
