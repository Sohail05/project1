import { Headers, RequestOptions } from '@angular/http';

export class ApiOptions {

    private _headers: Headers = new Headers();
    private _options: RequestOptions = new RequestOptions({ headers: this._headers });
    private _specificHeaders = {};

    /**
     * GET COMMON HEADERS
     */
    get headers() {
        return this._headers;
    }

    /**
     * SET COMMON HEADERS
     */
    set headers(val: Headers) {
        this._headers = val;
    }

    /**
     * GET COMMON OPTIONS
     */
    get options() {
        return this._options;
    }

    /**
     * SET COMMON OPTIONS
     */
    set options(val: RequestOptions) {
        this._options = val;
    }

    /**
     * GET SPECIFIC HEADERS
     * @param tag string
     */
    getspecificHeaders(tag: string) {
        return this._specificHeaders[tag];
    }

    /**
     * SET SEPCIFIC HEADERS
     * @param headers :Headers
     * @param tag :string
     */
    setspecificHeaders(headers: Headers, tag: string) {
        this._specificHeaders[tag] = headers;
    }

    /**
     * APPEND TO COMMON HEADERS
     * @param name :string
     * @param val :any
     */
    appendHeader(name: string, val: any) {
        this._headers.append(name, val);
        this.updateOptionsHeader();
    }

    /**
     * REMOVE FROM COMMON HEADERS
     * @param name :string
     */
    removeHeader(name: string) {
        this._headers.delete(name);
        this.updateOptionsHeader();
    }

    /**
     * SET TO COMMON HEADERS
     */
    setHeader(name: string, val: any) {
        this._headers.set(name, val);
        this.updateOptionsHeader();
    }

    /**
     * UPDATE OPTIONS HEADERS
     */
    updateOptionsHeader() {
        this._options.headers = this.headers;
    }
}
