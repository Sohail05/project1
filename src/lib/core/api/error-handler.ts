import { Injectable, EventEmitter, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { Router } from '@angular/router';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { MdSnackBar } from '@angular/material';
import { environment } from 'environments/environment';
import { Subscription } from 'rxjs/Subscription';

/**
 * Rename to API Error Handler.
 */

@Injectable()
export class ErrorHandlerService {

    constructor(
        private router: Router,
        private http: Http,
        private snackbar: MdSnackBar,
    ) {}

    processApiError(response: Response) {

        if (response.status === 500) {

            this.promptError('API error')
            return Observable.throw(response.statusText);

        } else if (response.status >= 400 && response.status < 500) {

            response.status === 401 ? this.promptError(response.json().error_detail) : (() => {})() ;
            this.redirectUser();
            return Observable.throw(response.json().error_detail);

        } else {

            this.promptError('Unknown error');
            return Observable.throw('Unknown error');

        }

    }

    /**
     * Text to display on screen.
     * @param {string} err
     */
    private promptError(err: string) {
        this.snackbar.open(err, 'x');
    }

    /**
     * redirect user to environennt login route.
     */
    private redirectUser() {
        this.router.navigate([environment.loginRoute]);
    }

}
