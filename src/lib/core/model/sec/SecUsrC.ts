import { SecUsrBase } from './SecUsrBase';

/**
 *
 */
export class SecUsrC extends SecUsrBase {

    /**
     * Description : User
     * Label Code : sec.lbl.secusrc.usrid
     */
    usrId: string;
}
