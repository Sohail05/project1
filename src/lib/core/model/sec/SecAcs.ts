/**
 *
 */

export class SecAcs {
  /**
   *
   */
  fldId: string;

  /**
   * Description : Field Level Access Mode
   * Label Code : sec.lbl.secacs.fldacsmde
   */
  fldAcsMde: number;

  /**
   * Description : Record Level Access Control Codes
   * Label Code : sec.lbl.secacs.reccodes

   */
  recCodes: string[];

  /**
   * Code Type : SEC.ROLE
   */
  mainRoleCode: string;

  /**
   * Description : Operation Codes
   * Label Code : sec.lbl.secacs.opncodes
   */
  opnCodes: string[];

  /**
   * Code Type : SEC.ROLE
   */
  roleCode: string;
}
