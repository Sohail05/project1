import { PhoneNoExn } from '../cmo/PhoneNoExn';
import { PhoneNo } from '../cmo/PhoneNo';
import { ApvBase } from '../cmo/ApvBase';

/**
 *
 */

export class SecUsrBase extends ApvBase {
    /**
     * Description : Code
     * Label Code : sec.lbl.secusrbase.refcode
     */
    refCode: string;

    /**
     * Description : E-mail
     * Label Code : sec.lbl.secusrbase.email
     */
    email: string;

    /**
     * Description : User Upline
     * Label Code : sec.lbl.secusrbase.uplineusrid
     * Code Type : SEC.USR
     */
    uplineUsrId: string;

    /**
     * Description : User Status Update By
     * Label Code : sec.lbl.secusrbase.usrstsupdby
     */
    usrStsUpdBy: string;

    /**
     * Description : User Status Update Date
     * Label Code : sec.lbl.secusrbase.usrstsupddt
     */
    usrStsUpdDt: Date;

    /**
     * Description : Expiry Date
     * Label Code : sec.lbl.secusrbase.expdt
     */
    expDt: Date;

    /**
     * Description : Login Name
     * Label Code : sec.lbl.secusrbase.loginnm
     */
    loginNm: string;

    /**
     * Description : User Status
     * Label Code : sec.lbl.secusrbase.usrsts
     * Code Type : SEC.USR_STS
     */
    usrSts: string;

    /**
     * Description : User Time Zone
     * Label Code : sec.lbl.secusrbase.tmz
     * Code Type : SYS.TMZ
     */
    tmz: string;

    /**
     * Description : Office Number
     * Label Code : sec.lbl.secusrbase.offtel
     */
    offTel: PhoneNoExn;

    /**
     * Description : User Type
     * Label Code : sec.lbl.secusrbase.usrt
     * Code Type : SEC.USR_T
     */
    usrT: string;

    /**
     * Description : Channel/Division Group
     * Label Code : sec.lbl.secusrbase.divcode
     * Code Type : SEC.USR_DIV
     */
    divCode: string;

    /**
     * Description : Effective Date
     * Label Code : sec.lbl.secusrbase.efcdt
     */
    efcDt: Date;

    /**
     * Description : Mobile Number
     * Label Code : sec.lbl.secusrbase.mbitel
     */
    mbiTel: PhoneNo;

    /**
     * Description : Attending Branch
     * Label Code : sec.lbl.secusrbase.entcode
     * Code Type : CODE.BRN
     */
    entCode: string;

    /**
     * Description : Full Name
     * Label Code : sec.lbl.secusrbase.fullnm
     */
    fullNm: string;

    /**
     * Description : Code Status
     * Label Code : sec.lbl.secusrbase.codests
     */
    codeSts: string;
}
