import { FmtT } from './FmtT';

import { InputT } from './InputT';


/**
 *
 */
export class FieldProxy {
  /**
   *
   */
  required: boolean;

  /**
   *
   */
  showAllGrp: boolean;

  /**
   *
   */
  readonly: boolean;

  /**
   *
   */
  longMsg: string;

  /**
   *
   */
  grpBy: string;

  /**
   *
   */
  vldId: string;

  /**
   *
   */
  useGrp: boolean;


  /**
   *
   */
  defMsg: string;

  /**
   *
   */
  includeToday: boolean;

  /**
   *
   */
  defVal: string;

  /**
   *
   */
  nm: string;

  /**
   *
   */
  fmtT: FmtT;

  /**
   *
   */
  inputT: InputT;

  /**
   *
   */
  codeT: string;
}
