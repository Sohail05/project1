/**
 *
 */
export class CodeDtl {
  /**
   *
   */
  code: string;

  /**
   *
   */
  dsc: string;


  /**
   *
   */
  atrVal1: string;

  /**
   *
   */
  atrVal2: string;

  /**
   *
   */

  atrVal3: string;

  /**
   *
   */
  vldIdc: string;

  /**
   *
   */
  grpCodes: string[];

  /**
   *
   */
  fctGrpCodes: string[];
}
