/**
 *
 */
export class FxAmt {
  /**
   * Description : Currency
   * Label Code : cmo.lbl.fxamt.atlcode
   * Code Type : CODE.CUR
   */
  atlCode: string;

  /**
   * Description : Exchange Rate
   * Label Code : cmo.lbl.fxamt.fxrt
   */
  fxRt: number;

  /**
   * Description : Rate Date
   * Label Code : cmo.lbl.fxamt.fxrtdt
   */
  fxRtDt: Date;

  /**
   * Description : Amount in Local Currency
   * Label Code : cmo.lbl.fxamt.lccamt
   */
  lccAmt: number;

  /**
   * Description : Amount
   * Label Code : cmo.lbl.fxamt.atlamt
   */
  atlAmt: number;

  /**
   * Description : Local Currency Unit
   * Label Code : cmo.lbl.fxamt.lccunt
   */
  lccUnt: number;

  /**
   * Description : Local Currency
   * Label Code : cmo.lbl.fxamt.lcccode
   * Code Type : CODE.CUR
   */
  lccCode: string;

  /**
   * Description : Currency Unit
   * Label Code : cmo.lbl.fxamt.atlunt
   */
  atlUnt: number;
}
