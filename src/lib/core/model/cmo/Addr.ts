/**
 *
 */
export class Addr {
  /**
   * Description : Address Line 1
   * Long Description : Address Line 1
   * Label Code : cmo.lbl.addr.addr1
   */
  addr1: string;

  /**
   * Description : Address Line 2
   * Long Description : Address Line 2
   * Label Code : cmo.lbl.addr.addr2
   */
  addr2: string;

  /**
   * Description : Address Line 3
   * Long Description : Address Line 3
   * Label Code : cmo.lbl.addr.addr3
   */
  addr3: string;

  /**
   * Description : City
   * Long Description : City
   * Label Code : cmo.lbl.addr.city
   */
  city: string;

  /**
   * Description : Address Line 4
   * Long Description : Address Line 4
   * Label Code : cmo.lbl.addr.addr4
   */
  addr4: string;

  /**
   * Description : State
   * Long Description : State
   * Label Code : cmo.lbl.addr.ste
   * Code Type : CODE.CTY_STE
   */
  ste: string;

  /**
   * Description : Country
   * Long Description : Country
   * Label Code : cmo.lbl.addr.cty
   * Code Type : CODE.CTY
   */
  cty: string;

  /**
   * Description : Postcode
   * Long Description : Postcode
   * Label Code : cmo.lbl.addr.postcode
   */
  postcode: string;
}
