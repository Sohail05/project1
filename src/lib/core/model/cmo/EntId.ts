/**
 *
 */
export class EntId {

    /**
     * Description : ID Type
     * Long Description : ID Type
     * Label Code : cmo.lbl.entid.idt
     * Code Type : CODE.ID_T
     */
    idT: string;

    /**
     * Description : ID No.
     * Long Description : ID Number
     * Label Code : cmo.lbl.entid.idnbr
     */
    idNbr: string;

}
