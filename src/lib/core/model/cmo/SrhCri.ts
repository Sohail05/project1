import { OrderBy } from './OrderBy';

/**
 *
 */
export class SrhCri {
  /**
   *
   */
  filters: any;

  /**
   *
   */
  orderBys: OrderBy[];

  /**
   *
   */
  mapCris: any;

  /**
   *
   */
  maxRec: number;

  /**
   *
   */
  sttAtIdx: number;

  /**
   *
   */
  usrLocale: any;

  /**
   *
   */
  mapFilterModes: any;

  /**
   *
   */
  mapCrisDec: any;

  /**
   *
   */
  endAtIdx: number;

  /**
   *
   */
  mapCrisDt: any;

}
