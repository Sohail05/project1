import { MultiLingua } from './MultiLingua';

/**
 *
 */
export class RefCode {
  /**
   *
   */
  seq: number;

  /**
   *
   */
  dsc: MultiLingua;

  /**
   *
   */
  code: string;

  /**
   *
   */
  codeSts: string;

  /**
   *
   */
  atrMap: any;

  /**
   *
   */
  grpCodes: string[];

  /**
   *
   */
  fctGrpCodes: string[];


  /**
   *
   */
  codeExpDt: Date;

  /**
   *
   */

  codeEfcDt: Date;

  /**
   *
   */
  defDsc: string;
}
