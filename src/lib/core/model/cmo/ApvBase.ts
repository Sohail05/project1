import { CrtInf } from './CrtInf';

/**
 *
 */
export class ApvBase extends CrtInf {

    /**
     * Description : Reviewed By
     * Long Description : Reviewed By
     * Label Code : cmo.lbl.apvbase.apvby
     */
    apvBy: string;

    /**
     * Description : Reviewed Date
     * Long Description : Reviewed date
     * Label Code : cmo.lbl.apvbase.apvdt
     */
    apvDt: Date;
}
