/**
 *
 */
export class CurAmt {
  /**
   * Description : Currency
   * Label Code : cmo.lbl.curamt.atlcode
   * Code Type : CODE.CUR
   */
  atlCode: string;

  /**
   * Description : Amount in Local Currency
   * Label Code : cmo.lbl.curamt.lccamt
   */
  lccAmt: number;

  /**
   * Description : Amount
   * Label Code : cmo.lbl.curamt.atlamt
   */
  atlAmt: number;
}
