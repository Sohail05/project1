/**
 *
 */
export class PhoneNo {
  /**
   * Description : Phone No.
   * Long Description : Phone Number
   * Label Code : cmo.lbl.phoneno.no
   */
  no: string;

  /**
   * Description : Country Code
   * Long Description : Country Code
   * Label Code : cmo.lbl.phoneno.ctycode
   */
  ctyCode: string;
}
