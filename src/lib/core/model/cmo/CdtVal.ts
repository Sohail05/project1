/**
 *
 */
export class CdtVal {
  /**
   * Description : Condition Value in Date From
   * Label Code : cmo.lbl.cdtval.cdtvaldtfr
   */
  cdtValDtFr: Date;

  /**
   * Description : Condition Value in Date To
   * Label Code : cmo.lbl.cdtval.cdtvaldtto
   */
  cdtValDtTo: Date;

  /**
   * Description : Condition Value in Number From
   * Label Code : cmo.lbl.cdtval.cdtvalnumfr
   */
  cdtValNumFr: number;

  /**
   * Description : Condition Value in Number To
   * Label Code : cmo.lbl.cdtval.cdtvalnumto
   */

  cdtValNumTo: number;

  /**
   * Description : Condition Value in String
   * Label Code : cmo.lbl.cdtval.cdtvalstr
   */
  cdtValStr: string;

  /**
   * Description : Condition Sequence
   * Label Code : cmo.lbl.cdtval.cdtseq
   */
  cdtSeq: number;
}
