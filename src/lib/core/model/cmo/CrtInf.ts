/**
 *
 */
export class CrtInf {
  /**
   * Description : Updated By
   * Long Description : Updated By
   * Label Code : cmo.lbl.crtinf.updby
   */
  updBy: string;

  /**
   * Description : System Row Version
   * Label Code : cmo.lbl.crtinf.rowvrs
   */
  rowVrs: number;

  /**
   * Description : Created By
   * Long Description : Created By
   * Label Code : cmo.lbl.crtinf.crtby
   */
  crtBy: string;

  /**
   * Description : Updated Date
   * Long Description : Updated Date
   * Label Code : cmo.lbl.crtinf.upddt
   */
  updDt: Date;

  /**
   * Description : Created Date
   * Long Description : Created Date
   * Label Code : cmo.lbl.crtinf.crtdt
   */
  crtDt: Date;
}
