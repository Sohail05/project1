import { PhoneNo } from './PhoneNo';

/**
 *
 */
export class PhoneNoExn extends PhoneNo {
  /**
   * Description : Extension
   * Long Description : Extension
   * Label Code : cmo.lbl.phonenoexn.exn
   */
  exn: string;
}
