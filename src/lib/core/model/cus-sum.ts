import { IEntId } from './cus';
export interface ICisCusSum {
        id: number;
        fullNm: string;
        cifNo: string;
        // identity TIN/SSN
        id1: IEntId;
        aumCcy: String;
        aumAmt: number;
        relSince: Date;
        // controlling entity
        ctlEnt: string;
        //
        cpt: string;
        // parent "group
        grp: string;
        // temp, need to remove
        type: string;
        ownType: string;
        // RM name
        crmUsrId: string;
        // update date
        updDt: Date;
}
export interface ICisCusGrpSum {
        id: number;
        fullNm: string;
        grpCifNo: string;
        aumCcy: String;
        aumAmt: number;
        relSncDt: Date;
        // controlling entity
        ctlEnt: string;
        //
        cpt: string;
        // parent "group
        grp: string;
        // temp, need to remove
        type: string;
        ownType: string;
        // RM name
        grmUsrId: string;
        // update date
        updDt: Date;
}
