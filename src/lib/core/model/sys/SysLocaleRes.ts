/**
 *
 */
export class SysLocaleRes {
  /**
   *
   */
  msg: string;

  /**
   *
   */
  tooltip: string;

  /**
   *
   */
  msgCode: string;
}
