import { IAddr, ICrtInf, IPhoneNo } from './common';
//
export interface ICisCus {
    cusId: number;
    cifNo: string;
    fullNm: string;
    cusTCode: string;
    // identity TIN/SSN
    id1: IEntId;
    id2: IEntId;
    id3: IEntId;
    idIssCtyCode: string;
    crmUsrId: string;
    shrInfIdc: string;
    bthDt: Date;
    gdrCode: string;
    salCode: string;
    cpyCxCode: string;
    ctyResOpnCode: string;
    ctyRskCode: string;
    eduLvlCode: string;
    mrlStsCode: string;
    nalCode: string;
    ntuBusCode: string;
    ntuBusGrpCode: string;
    oriCtyCode: string;
    pbyLtdIdc: string;
    resTCode: string;
    cusCatCode: string;
    busSze: string;
    lstRvwDt: Date;
    nxtRvwDt: Date;
    stsCode: string;
    aumAmt: number;
    yrOfOcy: number;
    mthOfOcy: number;
    relSince: Date;
    updBy: string;
    updDt: Date;
    crtBy: string;
    crtDt: Date;
}

/** counter party address */
export interface ICisCusAddr extends ICrtInf {
    addrId: number;
    cusId: number;
    addrTCode: string;
    resStsCode: string;
    yrOfOcy: number;
    mthOfOcy: number;
    addr: IAddr;
}

/** activity log  */
export interface ICisCusAtyLog extends ICrtInf {
    atyLogHId: number;
    cusId: number;
    log: string;
}

/** counterparty contact */
export interface ICisCusCttDtl extends ICrtInf {
    cttId: number;
    cusId: number;
    cttTCode: string;
    cttDes: string;
    cttNm: string;
    cttTel: IPhoneNo;
    cttEmail: string;
}

/** Financial Stmt */
export interface ICisCusFinStm {
    finStmId: number;
    cusId: number;
    stmDt: Date;
    stmTCode: string;
    qlfIdc: string;
    curCode: string;
    crtBy: string;
    crtDt: Date;
}

/** counter party group */
export interface ICisCusGrp extends ICrtInf {
    grpId: number;
    grpCifNo: string;
    fullNm: string;
    ctyRskCode: string;
    grmUsrId: string;
    relSncDt: Date;
    ntuBusGrpCode: string;
    ntuBusCode: string;
    aumAmt: number;
    grpExuAmt: number;
    stsCode: string;
    crtBy: string;
    crtDt: Date;
    apvDcs: string;
    apvBy: string;
    apvDt: Date;
    rowVrs: number;
    atlRowVrs: number;
    pngActT: string;
    lstRvwDt: Date;
    nxtRvwDt: Date;
    updDt: Date;
    updBy: string;
    hid: number;
}

/** counterparty group member */
export interface ICisCusGrpMbr {
    /** composite key */
    grpId: number;
    /** composite key */
    cusId: number;
    pntEntId: number;
    pntHId: number;
    relToPntCode: string;
    shnPct: number;
    apvBy: string;
    pngActT: string;
    apvDt: Date;
    apvDcs: string;
    crtBy: string;
    crtDt: Date;
    rowVrs: number;
    atlRowVrs: number;
    hid: number;
}

/** counter party group rating */
export interface ICisCusGrpRtg {
    rtgId: number;
    grpId: number;
    rtgModCode: string;
    rtgDt: Date;
    stsCode: string;
    gra: string;
    pdPct: number;
    eadAmt: number;
    lgdPct: number;
    rtgTCode: string;
    rtgAgtCode: string;
}

/** Counterparty relationship */
export interface ICisCusRelDtl {
    CisCusRelId: number;
    cusId: number;
    relCusId: number;
    relCode: string;
    relToCtpCode: string;
    shnPct: number;
    desCode: string;
}
/** counterparty rating */
export interface ICisCusRtg {
    inlRtgId: number;
    cusId: number;
    rtgModCode: string;
    rtgDt: Date;
    stsCode: string;
    gra: string;
    pdPct: number;
    eadAmt: number;
    lgdPct: number;
    rtgTCode: string;
    rtgAgtCode: string;
}

/** identity for the Counterparty */
export interface IEntId {
    idT: string;
    idNbr: string;
}
