export interface Locale {
    msg: string;
    msgCode: string;
    tooltip: string;
}
