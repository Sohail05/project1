/**
 *
 */
export class WflPromptFct {

  /**
   *
   */
  fctDsc: string;

  /**
   *
   */
  fctCode: string;

  /**
   *
   */
  mdlCode: string;

  /**
   *
   */
  promptInputArgs: any;
}
