/**
 *
 */
export class WflTasVw {

  /**
   * Code Type : SEC.ROLE
   */
  secRoleCode: string;

  /**
   *
   */
  refId: string;

  /**
   *
   */
  tasId: string;

  /**
   *
   */
  viewUrl: string;
}
