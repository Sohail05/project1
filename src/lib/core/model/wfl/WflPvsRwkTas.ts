/**
 *
 */
export class WflPvsRwkTas {

  /**
   * Description : End Date
   * Label Code : wfl.lbl.wflpvsrwktas.enddt
   */
  endDt: Date;

  /**
   * Description : Start Date
   * Label Code : wfl.lbl.wflpvsrwktas.sttdt
   */
  sttDt: Date;

  /**
   *
   */
  oriActinstId: string;

  /**
   *
   */
  rwkActinstId: string;

  /**
   *
   */
  catCode: string;

  /**
   * Description : Task Owner
   * Label Code : wfl.lbl.wflpvsrwktas.tasowner
   */
  tasOwner: string;

  /**
   * Description : Task ID No
   * Label Code : wfl.lbl.wflpvsrwktas.rwktasid
   */
  rwkTasId: string;

  /**
   * Description : Workflow Code
   * Label Code : wfl.lbl.wflpvsrwktas.wflcode
   */
  wflCode: string;

  /**
   * Description : Task Element ID
   * Label Code : wfl.lbl.wflpvsrwktas.actid
   */
  actId: string;

  /**
   * Description : Task Element Name
   * Label Code : wfl.lbl.wflpvsrwktas.actnm
   */
  actNm: string;
}
