export class User {
  'crtBy': string;
  'crtDt': number;
  'updBy': string;
  'updDt': number;
  'rowVrs': number;
  'saveCrtInfo': boolean;
  'saveUpdInfo': boolean;
  'dirty': boolean;
  'apvBy': string;
  'apvDt': number;
  'fullNm': string;
  'email': string;
  'efcDt': number;
  'expDt': number;
  'usrSts': string;
  'codeSts': string;
  'loginNm': string;
  'tmz': string;
  'offTel': {
    'ctyCode': any,
    'no': any,
    'exn': any
  }
}
