/** Server response */
export interface IServerResponse {
  id: number;
  status: string;
}
/** phone number */
export interface IPhoneNo  {
    ctyCode: string;
    no: string;
}

/** date time or user information related to a record */
export interface ICrtInf {
    rowVrs: number;
    updBy: string;
    updDt: Date;
    crtBy: string;
    crtDt: Date;
}

/** address */
export interface IAddr  {
    addr1: string;
    addr2: string;
    addr3: string;
    addr4: string;
    city: string;
    postcode: string;
    cty: string;
    ste: string;
}

export interface IRefCode {
    grpCode: string;
    code: string;
    dsc: string;
}

export class Post {
  name = '';
  date: Date;
  id = '';
  title = '';
  desc = '';
  file = '';
  replies: ReplyPost[] = [];
}

export class ReplyPost {
  name = '';
  id = '';
  desc = '';
}
// Common Code Type Interface
export interface ICodeType {
  code: string;
  dsc: string;
}
