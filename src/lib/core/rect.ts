export class Rect {

    left: Number;
    top: Number;
    width: Number;
    heigth: Number;

    constructor(left: Number, top: Number, width: Number, height: Number) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.heigth = height;
    }

    toParamString() {
        return `left = ${this.left}, top = ${this.top}, width = ${this.width}, heigth = ${this.heigth}`;
    }

}
