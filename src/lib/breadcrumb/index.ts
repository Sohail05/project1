import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { BreadcrumbComponent } from './breadcrumb.component';
import { BreadcrumbService } from './breadcrumb.service';

@NgModule({
    imports: [CommonModule],
    declarations: [BreadcrumbComponent],
    exports: [BreadcrumbComponent],
})
export class BreadcrumbModule {}
