import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BreadcrumbComponent } from './breadcrumb.component';
import { Router, RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { ScreenComponent } from '../screen/screen.component';
import { ScreenModule } from '../screen/index';
import { BreadcrumbService } from './breadcrumb.service';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([
          {path: 'test', component: ScreenComponent},
          {path: 'other-test', component: ScreenComponent}
      ]),
        ScreenModule
    ],
      declarations: [],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/' },
        BreadcrumbService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });


  it('should return capitalized words', () => {
    expect(component.friendlyName('say-hello')).toBe('Say Hello');
  });
});
