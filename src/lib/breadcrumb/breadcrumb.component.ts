import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NavigationEnd, Router, UrlTree } from '@angular/router';
import { BreadcrumbService } from './breadcrumb.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'nt-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: ['./breadcrumb.component.scss'],
    providers: [BreadcrumbService]
})
export class BreadcrumbComponent implements OnInit {

    segments: string[] = [];
    routerSubscription: Subscription;

    constructor(
        private router: Router,
        public breadcrumbService: BreadcrumbService,
    ) { }

    ngOnInit(): void {
        this.routerSubcribe();
    }

    OnDestroy(): void {
        this.routerSubscription.unsubscribe();
    }

    /**
     * Subribe to router events for updating the displayed breadcrumb text.
     */
    routerSubcribe() {
        this.routerSubscription = this.router.events.subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    this.segments = [];
                    this.generateBreadcrumbTrail(event.urlAfterRedirects ? event.urlAfterRedirects : event.url);
                }
            });
    }

    /**
     *
     * @param url
     */
    generateBreadcrumbTrail(url: string): void {

        // Add url to beginning of array (since the url is being recursively broken down from full url to its parent)
        if (!this.breadcrumbService.isRouteHidden(url)) {
            this.segments.unshift(url);
        }

        // Find last '/' and add everything before it as a parent route
        if (url && url.lastIndexOf('/') > 0) {
            this.generateBreadcrumbTrail(url.substr(0, url.lastIndexOf('/')));
        }
    }

    /**
     * Navigate to the url segment.
     * @param {string} url
     */
    navigateTo(url: string): void {
        this.router.navigateByUrl(url);
    }

    /**
     * Url string to translate to humain readable format.
     * @param {string} url
     */
    friendlyName(url: string): string {
        const friendly = url ? this.breadcrumbService.getFriendlyNameForRoute(url) : '';
        return friendly.split('-').map(
            (word) => { return word.charAt(0).toLocaleUpperCase() + word.substr(1)}
        ).join(' ');
    }

}
