import { Injectable } from '@angular/core';
import { UrlTree } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';


/**
 * Todo: #1: create a single method addRoute method to handle all cases.
 * #2 Create a method to add routes in bulk (Array).
 */

@Injectable()
export class BreadcrumbService {
  private prependTextSubject: BehaviorSubject<any> = new BehaviorSubject<any>('');
  prependText$: Observable<string> = this.prependTextSubject.asObservable();
  /**
   * Todo: create an Array of objects which takes in different properties.
   * That will avoid creating multiple Maps and Arrays.
   */
  private routesFriendlyNames: Map<string, string> = new Map<string, string>();
  private routesFriendlyNamesRegex: Map<string, string> = new Map<string, string>();

  /**
   * todo: Design, Like the idea of a callback but unsure of the use case.
   * Can also be achived through a subcription on router navigation.
   * warning: Not currently implemented anywhere.
   */
  private routesWithCallback: Map<string, (string) => string> = new Map<string, (string) => string>();
  private routesWithCallbackRegex: Map<string, (string) => string> = new Map<string, (string) => string>();

  /**
   * todo: merge with the remaining routes.
   */
  private hideRoutes: any = new Array<string>();
  private hideRoutesRegex: any = new Array<string>();

  prependText(val) {
    this.prependTextSubject.next(val);
  }
  /**
   * Specify a friendly name for the corresponding route.
   *
   * @param route
   * @param name
   */
  addFriendlyNameForRoute(route: string, name: string): void {
    this.routesFriendlyNames.set(route, name);
  }

  /**
   * Specify a friendly name for the corresponding route matching a regular expression.
   *
   * @param route
   * @param name
   */
  addFriendlyNameForRouteRegex(routeRegex: string, name: string): void {
    this.routesFriendlyNamesRegex.set(routeRegex, name);
  }

  /**
   * Specify a callback for the corresponding route.
   * When a mathing url is navigatedd to, the callback function is invoked to get the name to be displayed in the breadcrumb.
   */
  addCallbackForRoute(route: string, callback: (id: string) => string): void {
    this.routesWithCallback.set(route, callback);
  }

  /**
   * Specify a callback for the corresponding route matching a regular expression.
   * When a mathing url is navigatedd to, the callback function is invoked to get the name to be displayed in the breadcrumb.
   */
  addCallbackForRouteRegex(routeRegex: string, callback: (id: string) => string): void {
    this.routesWithCallbackRegex.set(routeRegex, callback);
  }

  /**
   * Show the friendly name for a given route (url). If no match is found the url (without the leading '/') is shown.
   * @param route
   * @returns {*}
   */
  getFriendlyNameForRoute(route: string): string {
    let name;
    const routeEnd = route.substr(route.lastIndexOf('/') + 1, route.length);

    this.routesFriendlyNames.forEach((value, key, map) => {
      if (key === route) {
        name = value;
      }
    });

    this.routesFriendlyNamesRegex.forEach((value, key, map) => {
      if (new RegExp(key).exec(route)) {
        name = value;
        const cb = this.routesWithCallbackRegex.get(key);
        if (cb) {
          this.prependText(cb(null));
        }
      }
    });

    this.routesWithCallback.forEach((value, key, map) => {
      if (key === route) {
        name = value(routeEnd);
      }
    });

    // this.routesWithCallbackRegex.forEach((value, key, map) => {
    //   if (new RegExp(key).exec(route)) {
    //     name = value(routeEnd);
    //   }
    // });

    return name ? name : routeEnd;
  }

  /**
   * Specify a route (url) that should not be shown in the breadcrumb.
   */
  hideRoute(route: string): void {
    if (!this.hideRoutes.includes(route)) {
      this.hideRoutes.push(route);
    }
  }

  /**
   * Specify a route (url) regular expression that should not be shown in the breadcrumb.
   */
  hideRouteRegex(routeRegex: string): void {
    if (!this.hideRoutesRegex.includes(routeRegex)) {
      this.hideRoutesRegex.push(routeRegex);
    }
  }

  /**
   * Returns true if a route should be hidden.
   */
  isRouteHidden(route: string): boolean {
    let hide = this.hideRoutes.includes(route);

    this.hideRoutesRegex.forEach((value) => {
      if (new RegExp(value).exec(route)) {
        hide = true;
      }
    });

    return hide;
  }
}
