## Breadcrumb

### Breadcrumb component
```html
<nt-breadcrumb></nt-breadcrumb>
```

- Automatically updates whenever a navigation action ends.

### Breadcrumb Service

Mapping a route to a fixed string is possible with `addFriendlyNameForRoute` and `addFriendlyNameForRouteRegex` methods.

`route`: the original route .
`name`: The new string to display on the component.

```typescript
addFriendlyNameForRoute(route: string, name: string)
```

`routeRegex`: Regular Expression to match .
`name`: The new string to display on the component.

```typescript
addFriendlyNameForRouteRegex(routeRegex: string, name: string)
```

### Style

Style variables can be found in the projects `_var.scss` file.

- `$multi-2`: Affects the base text color.
- `$multi-3`: Affects the active segment text color.