import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationBarComponent } from './application-bar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MdButtonModule, MdIconModule } from '@angular/material';


@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        MdButtonModule,
        MdIconModule,
    ],
    declarations: [ApplicationBarComponent],
    exports: [ApplicationBarComponent],
    providers: []
})
export class ApplicationBarModule { }
