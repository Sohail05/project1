import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RichTextAreaComponent } from './rich-text-area.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [RichTextAreaComponent],
  declarations: [RichTextAreaComponent],
})
export class RichTextAreaModule { }
