import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
declare var tinymce: any;
// The import previously worked but something happened
// import 'tinymce/themes/modern/theme'

@Component({
  selector: 'nt-rich-text-area',
  templateUrl: './rich-text-area.component.html',
  styleUrls: ['./rich-text-area.component.scss'],
})
export class RichTextAreaComponent implements OnInit, AfterViewInit, OnDestroy {
  static editors = [];
  static init = false;

  @Input() elementId: String;
  @Output() onEditorKeyup = new EventEmitter<any>();

  id;

  constructor() {
  }

  ngOnInit() { }
  ngAfterViewInit() {

    if (RichTextAreaComponent.init === false) {
      tinymce.init({
        selector: '.rich-text-area',
        menubar: 'edit insert view format table tools',
        plugins: ['link', 'paste', 'table'],
        skin_url: '/assets/skins/lightgray',
        setup: (editor) => {
          RichTextAreaComponent.editors.push(editor);
          this.id = RichTextAreaComponent.editors.length - 1;
          editor.on('keyup', () => {
            const content = editor.getContent();
            this.onEditorKeyup.emit(content);
          });
        },
      });
      RichTextAreaComponent.init = true;
    }

  }

  ngOnDestroy() {
    tinymce.remove(RichTextAreaComponent.editors[this.id]);
  }
}
