import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SearchComponent } from './search.component';

@NgModule({
    imports: [CommonModule, MaterialModule, FormsModule, FlexLayoutModule],
    exports: [SearchComponent],
    declarations: [SearchComponent],
    providers: [],
})
export class SearchModule { }
