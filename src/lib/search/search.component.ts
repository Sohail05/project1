import { Component, ElementRef, Input, OnInit, Renderer, ViewChild } from '@angular/core';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [
    MakeProvider(SearchComponent),
    MakeLocaleMessageProvider(SearchComponent)
  ],
})

export class SearchComponent extends ValueAccessorBase<string> implements OnInit, LocaleMessage {
  value;
  auto;
  @Input() placeholder;
  isOpen: boolean;

  @ViewChild('input')
  input: ElementRef;

  constructor(public renderer: Renderer) {
    super();
  }

  ngOnInit() {
  }

  loadLocaleMessage (locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  open() {
    this.isOpen = true;

    setTimeout(() => {
      this.renderer.invokeElementMethod(
        this.input.nativeElement, 'focus', []);
    }, 100);

  }

  close() {
    this.isOpen = false;
  }

}
