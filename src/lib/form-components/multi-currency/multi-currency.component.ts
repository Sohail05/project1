import { Component, Input, OnInit, ViewChild, OnDestroy, HostListener } from '@angular/core';
import { NgModel, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import 'rxjs/add/observable/combineLatest';
import { Observable } from 'rxjs/Observable';
import { fadeInSlideInY } from '../animations/animations';
import { ICodeType } from './../core/model/common';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { ValidationService } from '../core/validation.service';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { CodeTypeService } from '../code-type/code-type.service';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { MakeLocaleMessageProvider, LocaleMessage } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-multi-currency',
  templateUrl: './multi-currency.component.html',
  styleUrls: ['./multi-currency.component.scss'],
  animations: [fadeInSlideInY],
  providers: [
    MakeProvider(MultiCurrencyComponent),
    MakeLocaleMessageProvider(MultiCurrencyComponent)
  ],
})

export class MultiCurrencyComponent extends ValueAccessorBase<any> implements OnInit, OnDestroy, IUIComponent, LocaleMessage {
  data = <IUIComponentData>{};
  @Input() min: number;
  @Input() max: number;
  @Input() changeFx = false;
  @Input() default: string;
  // TODO SET WITH GLOBAL VARIABLE OR ...
  @Input() baseCurrency = 'USD';
  @Input() parent: FormGroup;
  @Input() key;
  @Input() maskingOpts: object = {};
  @Input() required = false;
  @Input() isDisabled: boolean | string = false;
  @Input() conditional: { [key: string]: string };
  @Input() validation: object;
  control: FormControl;
  showRequired: boolean;
  showTooltip: boolean;
  placeholder;
  currentCurrencyType;
  private currentAmount;
  private currentRate;
  convertedAmount;
  inputMask;
  private changesRate = false;

  // TODO GET FROM SERVER
  currencyList = [];

  // Observable
  private currencyList$: Subscription;


  // TODO GET FROM SERVER
  currencyRateTable = {
    USD: 1,
    SGD: 0.7,
    GBP: 1.25,
    lastUpdated: new Date(),
  };

  disabledFn = (n: object) => { return n['vldIdc'] === 'N' ? true : false };

  constructor(
    private fb: FormBuilder,
    private validationService: ValidationService,
    private codeTypeService: CodeTypeService,
    private formService: FormService
  ) {
    super();
  }


  ngOnInit() {
    this.control = this.getControl('amount');
    this.setCurrencyControl();
    this.getAllCurrency();
    this.showConvertedAmountOnInput();
    // this.subscribeChange();
    this.getInputMask();
    this.setValidators();
    this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && !this.control.value && val;
    });
  }


  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  setValidators() {
    if (Object.keys(this.data).length || !this.parent) {
      return;
    }

    if (this.isDisabled === 'true') {
      this.control.disable();
      return;
    }
    const conditionalValidatorArr = [];
    if (this.conditional) {
      conditionalValidatorArr.push(this.validationService.getConditionalValidator(this.conditional));
    }
    let validatorArr = [];
    validatorArr = this.validationService.getAllValidator(this.validation);
    this.control.setValidators(Validators.compose([...validatorArr, ...conditionalValidatorArr]));
    this.control.updateValueAndValidity();
  }

  getAllCurrency() {
    this.currencyList$ = this.codeTypeService.getCodeTypes('CODE.CUR').subscribe((c) => {
      this.currencyList = c;
      this.setMasking();
      this.getInputMask();
    });
  }
  getInputMask(val?) {
    this.inputMask = this.validationService.getInputMask('decimal', this.maskingOpts);
  }

  setMasking() {
    if (this.getControl('cur').value) {
          const curT = this.getControl('cur').value;
          const decimal = this.currencyList.find((cur) => cur.code === curT).atrVal2;
          this.maskingOpts['decimalLimit'] = parseInt(decimal, 10);
          // this.maskingOpts['requireDecimal'] = true;
    }
  }

  setCurrencyControl() {

    const val = this.parent.get(this.key).value;
    if (!val) {
      return;
    }

    const curAmt = {
      cur: '',
      amount: null,
      date: null,
      rate: null,
    }

    if (!val.amount) {
      val.amount = this.default;
    }
    if (!val.cur) {
      val.cur = this.baseCurrency;
    }

    Object.assign(curAmt, val);
    if (this._disabled) {
      this.parent.setControl(this.key, this.fb.group(curAmt));
      this.parent.disable();
    } else {
      this.parent.setControl(this.key, this.fb.group(curAmt));
    }
  }

  showConvertedAmountOnInput() {
    if (this.baseCurrency !== this.getControl('cur').value) {
      this.currentAmount = this.getControl('amount').value;
      this.currentRate = this.getControl('rate').value;
      this.calculateAmount();
    }
  }

  formatBlank() {
    if (!this.getControl('cur').value) {
      this.getControl('amount').setValue('');
    }
  }

  valuesChanges() {
    // set amount to '' when user deselect currency
    this.formatBlank();
    const type = this.getControl('cur').value;
    let amt = this.getControl('amount').value;
    if (this.currentCurrencyType !== type) {
      this.changesRate = false;
      this.currentCurrencyType = type;
      if (this.getControl('cur').value) {
        this.setMasking();
      }
      this.getInputMask();
    }
    // set value
    if (typeof amt !== 'string') {
      amt = amt + '';
    }

    this.currentAmount = parseFloat(amt.replace(/,/g, ''));
    let date = this.currencyRateTable['lastUpdated'];
    let rate = this.currencyRateTable[this.currentCurrencyType];

    if (this.currentCurrencyType === this.baseCurrency) {
      this.convertedAmount = undefined;
      date = null;
      rate = null;
      this.setDateRate(date, rate);
      return;
    }

    // check changeFx if true only set date rate once
    if (this.changeFx) {
      if (!this.changesRate) {
        this.setDateRate(date, rate);
        this.changesRate = true;
      }
    } else {
      this.setDateRate(date, rate);
    }
    this.calculateAmount();
  }

  rateChanges() {
    if (this.changeFx) {
      const rate = this.getControl('rate').value;
      this.currentRate = rate;
      this.calculateAmount();
    }
  }

  // subscribeChange() {
  //   let setDefaultRate = false;
  //   this.currencyType$ = this.getControl('cur').valueChanges;
  //   this.currencyAmount$ = this.getControl('amount').valueChanges;
  //   this.manualInputRate$ = this.getControl('rate').valueChanges;

  //   Observable.combineLatest(
  //     this.currencyType$,
  //     this.currencyAmount$,
  //   ).subscribe((values) => {
  //     // reset rate when change currency type
  //     if (this.currentCurrencyType !== values[0]) {
  //       setDefaultRate = false;
  //       this.currentCurrencyType = values[0];
  //       this.setMasking();
  //       this.getInputMask();
  //       console.log(1);

  //     }

  //     // set value
  //     this.currentAmount = values[1];
  //     const date = this.currencyRateTable['lastUpdated'];
  //     const rate = this.currencyRateTable[this.currentCurrencyType];
  //     console.log(date);

  //     if (this.currentCurrencyType === this.baseCurrency) {
  //       this.convertedAmount = undefined;
  //       return;
  //     }

  //     // check changeFx if true only set date rate once
  //     if (this.changeFx) {
  //       if (!setDefaultRate) {
  //         this.setDateRate(date, rate);
  //         setDefaultRate = true;
  //       }
  //     } else {
  //       this.setDateRate(date, rate);
  //     }
  //     this.calculateAmount();
  //   });

  //   if (this.changeFx) {
  //     this.manualInputRate$.subscribe((value) => {
  //       this.currentRate = value;
  //       this.calculateAmount();
  //     });
  //   }
  // }

  autoFormat() {
    // set amount to '' when user deselect currency
    this.formatBlank();
    // set input value
    let inputVal = this.getControl('amount').value;
    // only format when text input and dropdown is not empty
    if (inputVal !== '' &&
        inputVal !== null &&
        this.getControl('cur').value) {
        // get currency decimals
        const decimal = this.currencyList.find((cur) => cur.code === this.getControl('cur').value).atrVal2;
        // when currency has decimals
        if (decimal !== '0') {
          // if user removed . replace .
          if (inputVal.indexOf('.') < 0) {
            this.getControl('amount').setValue(inputVal + '.');
          }
          // if user removed 0 before . set 0.
          if (inputVal.split('.')[0] === '') {
          this.getControl('amount').setValue
          ( inputVal.split('.')[0] = '0.' +
            inputVal.split('.')[1] );
          }
          // set to latest value
          inputVal = this.getControl('amount').value;
          // set inputted decimals
          const inputDec = inputVal.split('.')[1].length;
          // set zeros
          let zeros = '';
          for (let i = inputDec; i < decimal; i++) {
            zeros = zeros + '0';
          }
          // if user changes from low to high decimal, add zeros
          // else if user changes from high to low decimal, slice reduced decimals
          if (inputDec < decimal) {
            this.getControl('amount').setValue(inputVal + zeros);
          } else if (inputDec > decimal ) {
            // set reduced decimals
            const sliceDecimals = (inputDec - decimal) * -1;
            this.getControl('amount').setValue(inputVal.slice('.', sliceDecimals));
          }
        // when currency has no decimal
        } else if (decimal === '0') {
          this.getControl('amount').setValue(inputVal.split('.')[0]);
        }
    }
  }

  setDateRate(date: Date, rate: number) {
    this.getControl('date').setValue(date);
    this.getControl('rate').setValue(rate);
    this.currentRate = rate;
  }

  calculateAmount() {
    this.convertedAmount = (this.currentRate * (this.currentAmount ? this.currentAmount : 0)) || 0;
  }

  getControl(controlName: string): FormControl {
    return this.parent.get(this.key).get(controlName) as FormControl;
  }

  ngOnDestroy() {
    if (this.currencyList$) {
      this.currencyList$.unsubscribe();
    }
  }
}
