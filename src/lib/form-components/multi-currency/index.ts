import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MultiCurrencyComponent } from './multi-currency.component';
import { DatePickerModule } from '../date-picker/index';
import { TextMaskModule } from 'angular2-text-mask';
import { ControlMessageModule } from '../control-message/index';
import { MultiLinguaTooltipModule } from '../tooltip/index';
import { CodeTypeModule } from './../code-type/index';
import { MultiLinguaModule } from '../multi-lingua/index';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    DatePickerModule,
    TextMaskModule,
    ControlMessageModule,
    MultiLinguaTooltipModule,
    CodeTypeModule,
    MultiLinguaModule
  ],
  exports: [MultiCurrencyComponent],
  declarations: [MultiCurrencyComponent],
  providers: [],
})
export class MultiCurrencyModule { }
