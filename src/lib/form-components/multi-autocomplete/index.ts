import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ControlMessageModule } from '../control-message/index';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { MultiAutocompleteComponent } from './multi-autocomplete.component';
import { MultiLinguaTooltipModule } from '../tooltip/index';


@NgModule({
  imports: [
    ControlMessageModule,
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MaterialModule,
    MultiLinguaTooltipModule
  ],
  exports: [MultiAutocompleteComponent],
  declarations: [MultiAutocompleteComponent],
  providers: [],
})
export class MultiAutocompleteModule { }
