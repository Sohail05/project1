import { Component, forwardRef, Input, OnInit, HostListener, Optional, Host, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormGroupDirective, FormGroup, FormControl } from '@angular/forms';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { MdAutocompleteTrigger } from '@angular/material';
import { CODE_TYPE_CONTAINER_TOKEN, CodeTypeContainer, MakeCodeTypeProvider } from '../code-type/code-type-container';
import { CodeType } from '../code-type/code-type';
import { AutocompleteComponent } from '../autocomplete/autocomplete.component';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { Functional } from '../core/functional';
import { ValueAccessorBase } from '../value-accessor-base/value-accessor-base';
@Component({
  selector: 'nt-multi-autocomplete',
  templateUrl: './multi-autocomplete.component.html',
  styleUrls: ['./multi-autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiAutocompleteComponent),
      multi: true,
    },
    MakeCodeTypeProvider(MultiAutocompleteComponent),
    MakeLocaleMessageProvider(MultiAutocompleteComponent)
  ],
})
export class MultiAutocompleteComponent extends ValueAccessorBase<Array<string>>
  implements OnInit, ControlValueAccessor, IUIComponent, CodeTypeContainer, LocaleMessage {

    data = <IUIComponentData>{};
  /*
   - Get a array for "list"
   - Display the list as a dropdown
   - Autocomplete text in input field
   - check for an input event
   - if filteredList.contains(val) then push val to selected
   - clear input field
  */

  // todo
  // Support an array of objects and a closure filter
  // todo fix layout and flex
  @ViewChild(MdAutocompleteTrigger) fx: MdAutocompleteTrigger;

  // @depricated
  @Input() set fn(val) {
    this.displayFn = val;
    console.warn('Property "fn" is depricated');
  }

  showTooltip;
  tooltip;
  @Input() displayFn = Functional.codeDescription;
  storefn = Functional.codeValue;
  disabledFn = Functional.isDisabled;

  auto;
  showRequired: boolean;
  formSubmission: Subscription;
  control: FormControl;
  // Depricated
  @Input() set list(val) {
    this.options = val
    console.warn('Property "list" is depricated');
  }
  @Input() options: CodeType[];
  // todo: refactorings or create base class
  @Input() parent: FormGroup;
  @Input() key: string;
  @Input() value;
  @Input() placeholder = '';
  // required
  @Input() required = false;
  // disabled
  @Input() isDisabled: boolean | string = false;
  public selected = [];
  public query = '';
  public filteredList = [];
  // private propagateChange = (_: any) => { };

  loadCodeType(options: any) {
    this.options = options;
  };

  get _disabled() {
    return (!!this.isDisabled || this._formGroupDirective.control ? !!this._formGroupDirective.control.disabled : false);
  }
  constructor(
    private formService: FormService,
    @Optional() @Host() private _formGroupDirective: FormGroupDirective
  ) {
    super();
   }

  writeValue(val) {
    this.selected = [];
    // this.value = (typeof val === 'string') ? [val] : val;
    this.value = Array.isArray(val) ? val : [val];

    this.inject();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any) { }

  ngOnInit() {
    this.control = this.parent.get(this.key) as FormControl;
    this.formSubmission = this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && !this.control.value && val;
    });
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }


  inject() {
    if (this.value && Array.isArray(this.value)) {
      this.value.forEach((element) => {
        this.query = this.displayFn(element);
        this.filter();
        this.addByInput();
      });
    }
  }
  setVal() {
    this.query = this.selected.map((x) => x = this.displayFn(x)).join(', ');
  }
  remove(item) {
    this.selected.splice(this.selected.indexOf(item), 1);
    if (this.parent && this.key) {
      this.control.setValue(this.selected.map(this.storefn));
      this.control.markAsDirty();
    } else {
      this.propagateChange(this.selected.map(this.storefn));
    }
    this.setVal();
  }

  select(item) {

    // Do not set disabled items
    if (item['vldIdc'] && item['vldIdc'] === 'N') {
      return;
    }

    this.selected.push(item);
    this.filteredList = [];
    if (this.parent && this.key) {
      this.control.setValue(this.selected.map(this.storefn));
      this.control.markAsDirty();
    } else {
      this.propagateChange(this.selected.map(this.storefn));
    }
    // re-focus when select value
    this.setVal();
    // this.focus();
  }

  add(event) {
    if (this.filteredList.length > 0) {
      if (this.fx.activeOption) {
        const selectedValue = this.fx.activeOption.value
        this.select(selectedValue);
      }
    }
    event.preventDefault();
    this.fx.closePanel();
  }
  addByInput() {
    if (this.filteredList.length === 1) {
      const value = this.filteredList[0];
      this.select(this.value);
    }
  }
  focus() {
    this.query = '';
  }
  blur() {
    this.setVal();
  }
  filter() {
    if (this.query !== '' && this.options && this.options.length > 0) {
      this.filteredList = this.options.filter(function (el) {
        return this.displayFn(el).toLowerCase().indexOf(this.query.toLowerCase()) > -1
          && !this.selected.includes(el);
      }.bind(this));
    } else {
      this.filteredList = [];
    }
  }

}
