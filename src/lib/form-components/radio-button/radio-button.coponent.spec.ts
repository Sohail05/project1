import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioButtonComponent } from './radio-button.component';
import { CommonModule } from '@angular/common';
import { ControlMessageModule } from '../control-message/index';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MdRadioModule } from '@angular/material';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { CoreModule } from '../core/index';
import { FormService } from '../core/form.service';
import { ValidationService } from '../core/validation.service';
import { CodeType } from '../code-type/code-type';

describe('RadioButtonComponent', () => {
  let component: RadioButtonComponent;
  let fixture: ComponentFixture<RadioButtonComponent>;
  let fb: FormBuilder;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        ControlMessageModule,
        FlexLayoutModule,
        MdRadioModule,
        ReactiveFormsModule,
      ],
      declarations: [RadioButtonComponent],
      providers: [FormService, ValidationService, { provide: FormBuilder, useValue: fb }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioButtonComponent);
    component = fixture.componentInstance;
    fb = new FormBuilder;
    component.key = 'key';
    component.parent = fb.group({ key: 'empty' })
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('`disabledFn` method should return true for invalid codetype', () => {
    const invalidCodeObject = { code: 'NVL', dsc: 'Invalid', vldIdc: 'N' } as CodeType
    expect(component.disabledFn(invalidCodeObject)).toBeTruthy();
  });

  it('`disabledFn` method should return false for valid codetype', () => {
    const validCodeObject = { code: 'VLD', dsc: 'Valid', vldIdc: 'Y' } as CodeType
    expect(component.disabledFn(validCodeObject)).toBeFalsy()
  });

  it('Should update placeholder when changed', () => {
    component.placeholder = 'MyPlaceholder';
    fixture.detectChanges();
    expect(document.querySelector('h6').innerText).toContain('MyPlaceholder');
  });

  it('Should only display valid options', () => {
    const validCodeObject = { code: 'VLD', dsc: 'Valid', vldIdc: 'Y' } as CodeType;
    const invalidCodeObject = { code: 'NVL', dsc: 'Invalid', vldIdc: 'N' } as CodeType
    component.options = [validCodeObject, invalidCodeObject];
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelectorAll('md-radio-button').length).toBeLessThanOrEqual(1)
    expect(fixture.nativeElement.querySelector('md-radio-button').innerHTML).toContain('Valid')
  });


});
