import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ValueAccessorBase } from '../value-accessor-base/value-accessor-base';
import { Subscription } from 'rxjs/Subscription';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { FormService } from '../core/form.service';
import { CodeTypeContainer, MakeCodeTypeProvider } from '../code-type/code-type-container';
import { Locale } from '../core/model/locale';
import { CodeType } from '../code-type/code-type';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Functional } from '../core/functional';
@Component({
  selector: 'nt-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss'],
  providers: [
    MakeCodeTypeProvider(RadioButtonComponent),
    MakeLocaleMessageProvider(RadioButtonComponent)
  ]
})
export class RadioButtonComponent extends ValueAccessorBase<any> implements OnInit, IUIComponent, CodeTypeContainer, LocaleMessage {

  disabledFn = Functional.isDisabled;
  displayFn = Functional.codeDescription;
  displayValFn = Functional.codeValue;

  data = <IUIComponentData>{};
  showRequired: boolean;
  formSubmission: Subscription;
  control: FormControl;
  @Input() options = [];
  @Input() required = false;
  @Input() isDisabled = false;
  @Input() parent: FormGroup;
  @Input() key;
  @Input() vertical = false;
  initialValue;
  placeholder;

  // todo clean up template
  // implement displayFn
  constructor(private formService: FormService) {
    super();
  }

  loadCodeType(options: CodeType[]) {
    this.options = options;
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  get val(): Array<any> {
    if (this.parent && this.key) {
      return this.parent.get(this.key).value;
    } else {
      return this.value;
    }
  }

  isInitialValue(n) {
    if (this.initialValue && n) {
      return this.initialValue.includes(n['code'] || n)
    } else {
      return false;
    }
  }

  ngOnInit() {

    this.control = this.parent.get(this.key) as FormControl;
    if (this.isDisabled) {
      this.control.disable();
    }
    this.initialValue = this.val;
    this.formSubmission = this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && !this.control.value && val;
    });
  }


}
