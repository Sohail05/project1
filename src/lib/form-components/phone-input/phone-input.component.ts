import { Component, OnInit, Input, Optional, Host } from '@angular/core';
import { ValueAccessorBase, MakeProvider } from '../value-accessor-base/value-accessor-base';
import { FormGroup, FormBuilder, FormGroupDirective, NgControl, FormControl } from '@angular/forms';
import { CodeTypeService } from '../code-type/code-type.service';
import { PhoneNo } from './phone.model';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { FormService } from '../core/form.service';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';
import { environment } from 'environments/environment';

@Component({
  selector: 'nt-phone-input',
  templateUrl: './phone-input.component.html',
  styleUrls: ['./phone-input.component.scss'],
  providers: [
    MakeProvider(PhoneInputComponent),
    MakeLocaleMessageProvider(PhoneInputComponent)
  ]
})
export class PhoneInputComponent extends ValueAccessorBase<any> implements OnInit, IUIComponent, LocaleMessage {

  options = [];
  data = <IUIComponentData>{};
  @Input() placeholder = 'Phone No';
  @Input() required = false;
  @Input() disabled = false;
  @Input() maxLength = environment.phone.maxLength;
  @Input() minLength;
  @Input() defaultCountryCode = environment.phone.no;
  @Input() parent: FormGroup;
  @Input() key;
  control: FormControl;
  prefix;
  showRequired: boolean;
  @Input() displayFnOnOpen: (x) => string = (x) => x.dsc;
  @Input() displayFnOnClose: (x) => string = (x) => x.code;
  @Input() displayFn = (x) => x.dsc;
  @Input() storeFn = (x) => x.atrVal1;
  disabledFn = (n: object) => { return n['vldIdc'] === 'N' ? true : false };

  get _disabled() {
    return (this.disabled || this.parent.disabled || this.parent.get(this.key).disabled);
  }
  constructor(
    private fb: FormBuilder,
    private codeTypeService: CodeTypeService,
    private formService: FormService
    // @Optional() @Host() private _formGroupDirective: FormGroupDirective,
  ) {
    super();
  }

  ngOnInit() {
    this.onClose();
    this.setPhoneControl();
    this.getAllCountry();
    this.subscribeChange();
    this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && !this.control.value && val;
    });
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  onOpen() {
    if (this.displayFnOnOpen) {
      this.displayFn = this.displayFnOnOpen;
    }
  }

  onClose() {
    if (this.displayFnOnClose) {
      this.displayFn = this.displayFnOnClose;
    }
  }

  setPhoneControl() {
    this.control = this.parent.get(this.key) as FormControl;
    const phone = new PhoneNo();
    const val = this.control.value;
    // set default country
    if (!val.ctyCode) {
      val.ctyCode = this.defaultCountryCode;
    }
    this.prefix = val.ctyCode;
    Object.assign(phone, val);

    if (this._disabled) {
      this.parent.setControl(this.key, this.fb.group(phone));
      this.parent.disable();
    } else {
      this.parent.setControl(this.key, this.fb.group(phone));
    }
  }
  getAllCountry() {
    this.codeTypeService.getCodeTypes('CODE.CTY').subscribe((x) => {
      this.options = x;
    });
  }
  subscribeChange() {
    this.parent.get(this.key).get('ctyCode').valueChanges.subscribe((c) => {
      this.setPrefix(c);
    });
  }

  setPrefix(val) {
    this.prefix = val;
  }
}
