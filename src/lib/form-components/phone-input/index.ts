import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdInputModule, MdSelectModule } from '@angular/material';
import { PhoneInputComponent } from './phone-input.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import { ControlMessageModule } from '../control-message/index';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MdInputModule,
    MdSelectModule,
    FlexLayoutModule,
    ControlMessageModule
  ],
  declarations: [PhoneInputComponent],
  exports: [PhoneInputComponent],
  providers: []
})
export class PhoneInputModule { }
