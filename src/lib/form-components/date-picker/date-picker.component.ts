import { CommonModule } from '@angular/common';
import {
  AfterContentInit, Component, ElementRef, EventEmitter, HostBinding, HostListener, Input, ModuleWithProviders,
  NgModule, Optional, Output, ViewChild, ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NgControl, FormGroup, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { DateUtil } from './dateUtil';
import { coerceBooleanProperty } from './service/coercion/boolean-property';
import {
  DOWN_ARROW, END, ENTER, ESCAPE, HOME, LEFT_ARROW, PAGE_DOWN, PAGE_UP, RIGHT_ARROW, SPACE, TAB, UP_ARROW
} from './service/keyboard/keycodes';
import { IDate, IDay, IWeek } from './date-picker.model';
import { ValueAccessorBase } from '../value-accessor-base/value-accessor-base';
import { OnInit, forwardRef } from '@angular/core';
import { FormService } from '../core/form.service';
import { Subscription } from 'rxjs/Subscription';
import { IUIComponent, IUIComponentData } from '../ui-component/ui-component';
import { Locale } from '../core/model/locale';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { DateService } from './date.service';

@Component({
  moduleId: module.id,
  selector: 'nt-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MakeLocaleMessageProvider(DatePickerComponent)]
})
export class DatePickerComponent implements AfterContentInit, OnInit, IUIComponent, LocaleMessage {

  /**
  * Increment for datepicker elements ID
  */
  static trackingID = 0;
  showRequired: boolean;
  formSubmission: Subscription;
  control: FormControl;
  data = <IUIComponentData>{};
  @Input() parent: FormGroup;
  @Input() key: string;

  @ViewChild('datePicker') datePicker;
  @ViewChild('dpPanel') dpPanel;

  @Output() change: EventEmitter<any> = new EventEmitter<any>();
  @Input() floatPlaceholder = false;
  @Input() type: 'date' | 'time' | 'datetime' = 'date';
  @Input() name = '';
  // modified default format to month / day / years
  @Input() format: string = this.type === 'date' ?
    'MM/DD/YYYY' : this.type === 'time' ? 'HH:mm' : this.type === 'datetime' ?
      'DD/MM/YYYY HH:mm' : 'DD/MM/YYYY';
  @Input() tabindex = 0;

  @HostBinding('attr.role') datepicker = 'datepicker';
  @HostBinding('id')
  @Input() id: string = 'md2-datepicker-' + (++DatePickerComponent.trackingID);
  @HostBinding('class.md2-datepicker-disabled')
  @HostBinding('attr.aria-disabled')
  @Input()
  get disabled(): boolean { return this._disabled; }
  set disabled(value) { this._disabled = coerceBooleanProperty(value); };
  @HostBinding('attr.aria-label')
  @Input() placeholder: string;
  @HostBinding('attr.aria-required')
  @Input()
  get required(): boolean { return this._required; }
  set required(value) { this._required = coerceBooleanProperty(value); }

  private _value: Date = null;
  private _readonly = false;
  private _required = false;
  private _disabled = false;
  private _isInitialized = false;

  _isDatepickerVisible: boolean;
  _isYearsVisible: boolean;
  _isCalendarVisible: boolean;
  _isHoursVisible = true;

  private months: string[] = ['January', 'February', 'March', 'April',
    'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  _days: string[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
    'Friday', 'Saturday'];
  _hours: Object[] = [];
  _minutes: Object[] = [];

  _prevMonth = 1;
  _currMonth = 2;
  _nextMonth = 3;

  _years: number[] = [];
  _dates: Object[] = [];
  private today: Date = new Date();
  private _displayDate: Date = null;
  _selectedDate: Date = null;
  _viewDay: IDay = { year: 0, month: '', date: '', day: '', hour: '', minute: '' };
  _viewValue = '';

  _clock: any = {
    dialRadius: 120,
    outerRadius: 99,
    innerRadius: 66,
    tickRadius: 17,
    hand: { x: 0, y: 0 },
    x: 0, y: 0,
    dx: 0, dy: 0,
    moved: false,
  };

  private _min: Date = null;
  private _max: Date = null;

  _onChange = (value: any) => { };
  _onTouched = () => { };

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
  }

  constructor(
    public _dateUtil: DateUtil,
    private element: ElementRef,
    private formService: FormService,
    private dateService: DateService,
    @Optional() public _control: NgControl
  ) {
    this.getYears();
    this.generateClock();
    if (this._control) {
      this._control.valueAccessor = this;
    }
  }
  clearValue() {
    this._value = null;
    this._viewValue = null;
  }
  ngOnInit() {
    this.dateService.clear$.subscribe((val) => this.clearValue());
    this.control = this.parent.get(this.key) as FormControl;

    if (this.parent.disabled || this.control.disabled) {
      this.disabled = true;
    }
    if (this.disabled === true) {
      this.control.disable();
    }
    this.formSubmission = this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && !this.control.value && val;
    });
  }

  ngAfterContentInit() {
    this._isInitialized = true;
    this._isCalendarVisible = this.type !== 'time' ? true : false;
  }

  @Input()
  get readonly(): boolean { return this._readonly; }
  set readonly(value) { this._readonly = coerceBooleanProperty(value); }

  @Input() set min(value: Date) {
    if (value && this._dateUtil.isValidDate(value)) {
      this._min = new Date(value);
      this._min.setHours(0, 0, 0, 0);
      this.getYears();
    } else { this._min = null; }
  }
  @Input() set max(value: Date) {
    if (value && this._dateUtil.isValidDate(value)) {
      this._max = new Date(value);
      this._max.setHours(0, 0, 0, 0);
      this.getYears();
    } else { this._max = null; }
  }

  @Input()
  get value(): any { return this._value; }
  set value(value: any) {
    if (value && value !== this._value) {
      if (this._dateUtil.isValidDate(value)) {
        this._value = value;
      } else {
        if (this.type === 'time') {
          this._value = new Date('1-1-1 ' + value);
        } else {
          this._value = new Date(value);
        }
      }
      this._viewValue = this._formatDate(this._value);
      this.control.setValue(this._viewValue);
      this.control.markAsDirty();


      let date = '';
      if (this.type !== 'time') {
        // set by pks to change output value
        date = this._formatDate(this._value);
        // original date output value
        // date += this._value.getFullYear() + '-' + (this._value.getMonth() + 1) +
        //   '-' + this._value.getDate();
      }
      if (this.type === 'datetime') {
        date += ' ';
      }
      if (this.type !== 'date') {
        date += this._value.getHours() + ':' + this._value.getMinutes();
      }
      if (this._isInitialized) {
        if (this._control) {
          this._onChange(date);
        }
        this.change.emit(date);
      }
    }
  }

  get displayDate(): Date {
    if (this._displayDate && this._dateUtil.isValidDate(this._displayDate)) {
      return this._displayDate;
    } else {
      return this.today;
    }
  }
  set displayDate(date: Date) {
    if (date && this._dateUtil.isValidDate(date)) {
      if (this._min && this._min > date) {
        date = this._min;
      }
      if (this._max && this._max < date) {
        date = this._max;
      }
      this._displayDate = date;
      this._viewDay = {
        year: date.getFullYear(),
        month: this.months[date.getMonth()],
        date: this._prependZero(date.getDate() + ''),
        day: this._days[date.getDay()],
        hour: this._prependZero(date.getHours() + ''),
        minute: this._prependZero(date.getMinutes() + ''),
      };
    }
  }
  //  added to move up datepicker
  setDatePickerPanel() {
    const parentHeight = this.datePicker.nativeElement.offsetParent.offsetParent.clientHeight;
    const dpCrHeight = this.datePicker.nativeElement.offsetParent.offsetTop;
    const range = parentHeight - dpCrHeight;
    const windowWidth = window.innerWidth;
    let threshold = 350;
    if (windowWidth < 600) {
      threshold = 430;
    } else {
      threshold = 350;
    }
    if (range < threshold) {
      const pushTop = threshold - Math.abs(range);
      this.dpPanel.nativeElement.style.top = '-' + pushTop + 'px';
    }
  }
  @HostListener('click', ['$event'])
  _handleClick(event: MouseEvent) {
    if (this.disabled) {
      event.stopPropagation();
      event.preventDefault();
      return;
    }
  }

  @HostListener('keydown', ['$event'])
  _handleKeydown(event: KeyboardEvent) {
    if (this.disabled) { return; }

    if (this._isDatepickerVisible) {
      event.preventDefault();
      event.stopPropagation();

      switch (event.keyCode) {
        case TAB:
        case ESCAPE: this._onBlur(); break;
        default:
      }
      const displayDate = this.displayDate;
      if (this._isYearsVisible) {
        switch (event.keyCode) {
          case ENTER:
          case SPACE: this._onClickOk(); break;

          case DOWN_ARROW:
            if (this.displayDate.getFullYear() < (this.today.getFullYear() + 100)) {
              this.displayDate = this._dateUtil.incrementYears(displayDate, 1);
              this._scrollToSelectedYear();
            }
            break;
          case UP_ARROW:
            if (this.displayDate.getFullYear() > 1900) {
              this.displayDate = this._dateUtil.incrementYears(displayDate, -1);
              this._scrollToSelectedYear();
            }
            break;
          default:
        }

      } else if (this._isCalendarVisible) {
        switch (event.keyCode) {
          case ENTER:
          case SPACE: this.setDate(this.displayDate); break;

          case RIGHT_ARROW:
            this.displayDate = this._dateUtil.incrementDays(displayDate, 1);
            break;
          case LEFT_ARROW:
            this.displayDate = this._dateUtil.incrementDays(displayDate, -1);
            break;

          case PAGE_DOWN:
            if (event.shiftKey) {
              this.displayDate = this._dateUtil.incrementYears(displayDate, 1);
            } else {
              this.displayDate = this._dateUtil.incrementMonths(displayDate, 1);
            }
            break;
          case PAGE_UP:
            if (event.shiftKey) {
              this.displayDate = this._dateUtil.incrementYears(displayDate, -1);
            } else {
              this.displayDate = this._dateUtil.incrementMonths(displayDate, -1);
            }
            break;

          case DOWN_ARROW:
            this.displayDate = this._dateUtil.incrementDays(displayDate, 7);
            break;
          case UP_ARROW:
            this.displayDate = this._dateUtil.incrementDays(displayDate, -7);
            break;

          case HOME:
            this.displayDate = this._dateUtil.getFirstDateOfMonth(displayDate);
            break;
          case END:
            this.displayDate = this._dateUtil.getLastDateOfMonth(displayDate);
            break;
          default:
        }
        if (!this._dateUtil.isSameMonthAndYear(displayDate, this.displayDate)) {
          this.generateCalendar();
        }
      } else if (this._isHoursVisible) {
        switch (event.keyCode) {
          case ENTER:
          case SPACE: this.setHour(this.displayDate.getHours()); break;

          case UP_ARROW:
            this.displayDate = this._dateUtil.incrementHours(displayDate, 1); this._resetClock();
            break;
          case DOWN_ARROW:
            this.displayDate = this._dateUtil.incrementHours(displayDate, -1); this._resetClock();
            break;
          default:
        }
      } else {
        switch (event.keyCode) {
          case ENTER:
          case SPACE:
            this.setMinute(this.displayDate.getMinutes());
            break;

          case UP_ARROW:
            this.displayDate = this._dateUtil.incrementMinutes(displayDate, 1); this._resetClock();
            break;
          case DOWN_ARROW:
            this.displayDate = this._dateUtil.incrementMinutes(displayDate, -1); this._resetClock();
            break;
          default:
        }
      }
    } else {
      switch (event.keyCode) {
        case ENTER:
        case SPACE:
          event.preventDefault();
          event.stopPropagation();
          this._showDatepicker();
          break;
        default:
      }
    }
  }

  @HostListener('blur')
  _onBlur() {
    this._isDatepickerVisible = false;
    this._isYearsVisible = false;
    this._isCalendarVisible = this.type !== 'time' ? true : false;
    this._isHoursVisible = true;
    this._onTouched();
  }
  /**
   * Display Years
   */
  _showYear() {
    this._isYearsVisible = true;
    this._isCalendarVisible = true;
    this._scrollToSelectedYear();
  }

  private getYears() {
    const startYear = this._min ? this._min.getFullYear() : 1900;
    const endYear = this._max ? this._max.getFullYear() : this.today.getFullYear() + 100;
    this._years = [];
    for (let i = startYear; i <= endYear; i++) {
      this._years.push(i);
    }
  }

  private _scrollToSelectedYear() {
    setTimeout(() => {
      const yearContainer = this.element.nativeElement.querySelector('.md2-calendar-years');
      const selectedYear = this.element.nativeElement.querySelector('.md2-calendar-year.selected');
      yearContainer.scrollTop = (selectedYear.offsetTop + 20) - yearContainer.clientHeight / 2;
    }, 0);
  }

  /**
   * select year
   * @param year
   */
  _setYear(year: number) {
    const date = this.displayDate;
    this.displayDate = new Date(year, date.getMonth(), date.getDate(),
      date.getHours(), date.getMinutes());
    this.generateCalendar();
    this._isYearsVisible = false;
  }

  /**
   * Display Datepicker
   */
  _showDatepicker() {
    if (this.disabled || this.readonly) { return; }
    this._isDatepickerVisible = true;
    this._selectedDate = this.value || new Date(1, 0, 1);
    this.displayDate = this.value || this.today;
    this.generateCalendar();
    this._resetClock();
    this.setDatePickerPanel();
    this.element.nativeElement.focus();
  }

  /**
   * Display Calendar
   */
  _showCalendar() {
    this._isYearsVisible = false;
    this._isCalendarVisible = true;
  }

  /**
   * Toggle Hour visiblity
   */
  _toggleHours(value: boolean) {
    this._isYearsVisible = false;
    this._isCalendarVisible = false;
    this._isYearsVisible = false;
    this._isHoursVisible = value;
    this._resetClock();
  }

  /**
   * Ok Button Event
   */
  _onClickOk() {
    if (this._isYearsVisible) {
      this.generateCalendar();
      this._isYearsVisible = false;
      this._isCalendarVisible = true;
    } else if (this._isCalendarVisible) {
      this.setDate(this.displayDate);
    } else if (this._isHoursVisible) {
      this._isHoursVisible = false;
      this._resetClock();
    } else {
      this.value = this.displayDate;
      this._onBlur();
    }
  }

  /**
   * Date Selection Event
   * @param event Event Object
   * @param date Date Object
   */
  _onClickDate(event: Event, date: any) {
    event.preventDefault();
    event.stopPropagation();
    if (date.disabled) { return; }
    if (date.calMonth === this._prevMonth) {
      this._updateMonth(-1);
    } else if (date.calMonth === this._currMonth) {
      this.setDate(new Date(date.dateObj.year, date.dateObj.month,
        date.dateObj.day, this.displayDate.getHours(), this.displayDate.getMinutes()));
    } else if (date.calMonth === this._nextMonth) {
      this._updateMonth(1);
    }
  }

  /**
   * Set Date
   * @param date Date Object
   */
  private setDate(date: Date) {
    if (this.type === 'date') {
      this.value = date;
      this._onBlur();
    } else {
      this._selectedDate = date;
      this.displayDate = date;
      this._isCalendarVisible = false;
      this._isHoursVisible = true;
      this._resetClock();
    }
  }

  /**
   * Update Month
   * @param noOfMonths increment number of months
   */
  _updateMonth(noOfMonths: number) {
    this.displayDate = this._dateUtil.incrementMonths(this.displayDate, noOfMonths);
    this.generateCalendar();
  }

  /**
   * Check is Before month enabled or not
   * @return boolean
   */
  _isBeforeMonth() {
    return !this._min ? true :
      this._min && this._dateUtil.getMonthDistance(this.displayDate, this._min) < 0;
  }

  /**
   * Check is After month enabled or not
   * @return boolean
   */
  _isAfterMonth() {
    return !this._max ? true :
      this._max && this._dateUtil.getMonthDistance(this.displayDate, this._max) > 0;
  }

  /**
   * Check the date is enabled or not
   * @param date Date Object
   * @return boolean
   */
  private _isDisabledDate(date: Date): boolean {
    if (this._min && this._max) {
      return (this._min > date) || (this._max < date);
    } else if (this._min) {
      return (this._min > date);
    } else if (this._max) {
      return (this._max < date);
    } else {
      return false;
    }

    // if (this.disableWeekends) {
    //   let dayNbr = this.getDayNumber(date);
    //   if (dayNbr === 0 || dayNbr === 6) {
    //     return true;
    //   }
    // }
    // return false;
  }

  /**
   * Generate Month Calendar
   */
  private generateCalendar(): void {
    const year = this.displayDate.getFullYear();
    const month = this.displayDate.getMonth();

    this._dates.length = 0;

    const firstDayOfMonth = this._dateUtil.getFirstDateOfMonth(this.displayDate);
    const numberOfDaysInMonth = this._dateUtil.getNumberOfDaysInMonth(this.displayDate);
    const numberOfDaysInPrevMonth = this._dateUtil.getNumberOfDaysInMonth(
      this._dateUtil.incrementMonths(this.displayDate, -1));

    let dayNbr = 1;
    let calMonth = this._prevMonth;
    for (let i = 1; i < 7; i++) {
      const week: IWeek[] = [];
      if (i === 1) {
        const prevMonth = numberOfDaysInPrevMonth - firstDayOfMonth.getDay() + 1;
        for (let j = prevMonth; j <= numberOfDaysInPrevMonth; j++) {
          const iDate: IDate = { year, month: month - 1, day: j, hour: 0, minute: 0 };
          const date: Date = new Date(year, month - 1, j);
          week.push({
            date,
            dateObj: iDate,
            calMonth,
            today: this._dateUtil.isSameDay(this.today, date),
            disabled: this._isDisabledDate(date),
          });
        }

        calMonth = this._currMonth;
        const daysLeft = 7 - week.length;
        for (let j = 0; j < daysLeft; j++) {
          const iDate: IDate = { year, month, day: dayNbr, hour: 0, minute: 0 };
          const date: Date = new Date(year, month, dayNbr);
          week.push({
            date,
            dateObj: iDate,
            calMonth,
            today: this._dateUtil.isSameDay(this.today, date),
            disabled: this._isDisabledDate(date),
          });
          dayNbr++;
        }
      } else {
        for (let j = 1; j < 8; j++) {
          if (dayNbr > numberOfDaysInMonth) {
            dayNbr = 1;
            calMonth = this._nextMonth;
          }
          const iDate: IDate = {
            year,
            month: calMonth === this._currMonth ? month : month + 1,
            day: dayNbr, hour: 0, minute: 0,
          };
          const date: Date = new Date(year, iDate.month, dayNbr);
          week.push({
            date,
            dateObj: iDate,
            calMonth,
            today: this._dateUtil.isSameDay(this.today, date),
            disabled: this._isDisabledDate(date),
          });
          dayNbr++;
        }
      }
      this._dates.push(week);
    }
  }

  /**
   * Select Hour
   * @param event Event Object
   * @param hour number of hours
   */
  _onClickHour(event: Event, hour: number) {
    event.preventDefault();
    event.stopPropagation();
    this.setHour(hour);
  }

  /**
   * Select Minute
   * @param event Event Object
   * @param minute number of minutes
   */
  _onClickMinute(event: Event, minute: number) {
    event.preventDefault();
    event.stopPropagation();
    this.setMinute(minute);
  }

  /**
   * Set hours
   * @param hour number of hours
   */
  private setHour(hour: number) {
    const date = this.displayDate;
    this._isHoursVisible = false;
    this.displayDate = new Date(date.getFullYear(), date.getMonth(),
      date.getDate(), hour, date.getMinutes());
    this._resetClock();
  }

  /**
   * Set minutes
   * @param minute number of minutes
   */
  private setMinute(minute: number) {
    const date = this.displayDate;
    this.displayDate = new Date(date.getFullYear(), date.getMonth(),
      date.getDate(), date.getHours(), minute);
    this._selectedDate = this.displayDate;
    this.value = this._selectedDate;
    this._onBlur();
  }

  /**
   * reser clock hands
   */
  private _resetClock() {
    const hour = this.displayDate.getHours();
    const minute = this.displayDate.getMinutes();

    const value = this._isHoursVisible ? hour : minute;
    const unit = Math.PI / (this._isHoursVisible ? 6 : 30);
    const radian = value * unit;
    const radius = this._isHoursVisible && value > 0 && value < 13 ?
      this._clock.innerRadius : this._clock.outerRadius;
    const x = Math.sin(radian) * radius;
    const y = - Math.cos(radian) * radius;
    this._setClockHand(x, y);
  }

  /**
   * set clock hand
   * @param x number of x position
   * @param y number of y position
   */
  private _setClockHand(x: number, y: number) {
    let radian = Math.atan2(x, y);
    const unit = Math.PI / (this._isHoursVisible ? 6 : 30);
    const z = Math.sqrt(x * x + y * y);
    const inner = this._isHoursVisible && z < (this._clock.outerRadius + this._clock.innerRadius) / 2;
    const radius = inner ? this._clock.innerRadius : this._clock.outerRadius;
    let value = 0;

    if (radian < 0) { radian = Math.PI * 2 + radian; }
    value = Math.round(radian / unit);
    radian = value * unit;
    if (this._isHoursVisible) {
      if (value === 12) { value = 0; }
      value = inner ? (value === 0 ? 12 : value) : value === 0 ? 0 : value + 12;
    } else {
      if (value === 60) { value = 0; }
    }

    this._clock.hand = {
      x: Math.sin(radian) * radius,
      y: Math.cos(radian) * radius,
    };
  }

  /**
   * render Click
   */
  private generateClock() {
    this._hours.length = 0;

    for (let i = 0; i < 24; i++) {
      const radian = i / 6 * Math.PI;
      const inner = i > 0 && i < 13;
      const radius = inner ? this._clock.innerRadius : this._clock.outerRadius;
      this._hours.push({
        hour: i === 0 ? '00' : i,
        top: this._clock.dialRadius - Math.cos(radian) * radius - this._clock.tickRadius,
        left: this._clock.dialRadius + Math.sin(radian) * radius - this._clock.tickRadius,
      });
    }

    for (let i = 0; i < 60; i += 5) {
      const radian = i / 30 * Math.PI;
      this._minutes.push({
        minute: i === 0 ? '00' : i,
        top: this._clock.dialRadius - Math.cos(radian) * this._clock.outerRadius -
        this._clock.tickRadius,
        left: this._clock.dialRadius + Math.sin(radian) * this._clock.outerRadius -
        this._clock.tickRadius,
      });
    }
  }

  /**
   * format date
   * @param date Date Object
   * @return string with formatted date
   */
  private _formatDate(date: Date): string {
    return this.format
      .replace('YYYY', date.getFullYear() + '')
      .replace('MM', this._prependZero((date.getMonth() + 1) + ''))
      .replace('DD', this._prependZero(date.getDate() + ''))
      .replace('HH', this._prependZero(date.getHours() + ''))
      .replace('mm', this._prependZero(date.getMinutes() + ''))
      .replace('ss', this._prependZero(date.getSeconds() + ''));
  }

  /**
   * Prepend Zero
   * @param value String value
   * @return string with prepend Zero
   */
  private _prependZero(value: string): string {
    return parseInt(value, 10) < 10 ? '0' + value : value;
  }

  /**
   * Get Offset
   * @param element HtmlElement
   * @return top, left offset from page
   */
  private _offset(element: any) {
    let top = 0;
    let left = 0;
    do {
      top += element.offsetTop || 0;
      left += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);

    return {
      top,
      left,
    };
  }

  writeValue(value: any): void {
    if (value && value !== this._value) {
      if (this._dateUtil.isValidDate(value)) {
        this._value = value;
      } else {
        if (this.type === 'time') {
          this._value = new Date('1-1-1 ' + value);
        } else {
          this._value = new Date(value);
        }
      }
      this._viewValue = this._formatDate(this._value);
      let date = '';
      if (this.type !== 'time') {
        date += this._value.getFullYear() + '-' + (this._value.getMonth() + 1) +
          '-' + this._value.getDate();
      }
      if (this.type === 'datetime') {
        date += ' ';
      }
      if (this.type !== 'date') {
        date += this._value.getHours() + ':' + this._value.getMinutes();
      }
    } else {
      this._value = null;
      this._viewValue = null;
    }
  }

  registerOnChange(fn: (value: any) => void): void { this._onChange = fn; }

  registerOnTouched(fn: () => {}): void { this._onTouched = fn; }

}
