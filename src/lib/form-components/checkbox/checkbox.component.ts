import { Component, OnInit, Input, OnDestroy, HostListener, forwardRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValueAccessorBase, MakeProvider } from '../value-accessor-base/value-accessor-base';
import { CODE_TYPE_CONTAINER_TOKEN, CodeTypeContainer, MakeCodeTypeProvider } from '../code-type/code-type-container';
import { Locale } from '../core/model/locale';
import { CodeType } from '../code-type/code-type';
import { MakeLocaleMessageProvider, LocaleMessage } from '../core/locale-message';
import { Functional } from '../core/functional';

@Component({
  selector: 'nt-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    MakeProvider(CheckboxComponent),
    MakeCodeTypeProvider(CheckboxComponent),
    MakeLocaleMessageProvider(CheckboxComponent)
  ]
})
export class CheckboxComponent extends ValueAccessorBase<any> implements OnInit, OnDestroy, CodeTypeContainer, LocaleMessage {

  private parentObservable;
  private valObservable

  disabledFn = Functional.isDisabled;
  @Input() displayFn = Functional.codeDescription;
  @Input() storeFn = Functional.codeValue;

  selectedCheckbox: boolean[] = [];
  @Input() parent: FormGroup;
  @Input() key: string;
  @Input() options = [];
  @Input() rowOrColumn = 'column';
  @Input() align: string;
  @Input() group: string;
  @Input() required = false;
  @Input() disabled = false;
  @Input() placeholder;
  initialValue;

  get val(): Array<any> {
    if (this.parent && this.key) {
    return  this.parent.get(this.key).value;
    } else {
      return this.value;
    }
  }

  isSelected(n) {
    if (this.val && n) {
      return this.val.includes(n['code'] || n)
    }else {
      return false;
    }
  }

  isInitialValue(n) {
    if (this.initialValue && n) {
      return this.initialValue.includes(n['code'] || n)
    }else {
      return false;
    }
  }

  get _disabled() {
    return (this.disabled || (this.parent && this.parent.disabled) || (this.parent && this.key && this.parent.get(this.key).disabled));
  }

  constructor() {
    super();
  }

  ngOnInit() {
    this.subscribeChange();
    this.initialValue = this.val
  }

  loadCodeType(options: CodeType[]) {
    this.options = options;
    this.getSelectedCheckbox();
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  subscribeChange() {
    if (this.parent && this.key) {
      this.parentObservable = this.parent.get(this.key).valueChanges.subscribe((x) => {
        this.getSelectedCheckbox();
      });
    } else {
      this.valObservable = this.valueObservable.subscribe((x) => {
        this.getSelectedCheckbox();
      });
    }

  }

  unsubscribeChange() {
    if (this.parent && this.key) {
      if (this.parentObservable) {
      this.parentObservable.unsubscribe();
      }
    } else {
      if (this.valObservable) {
        this.valObservable.unsubscribe();
      }
    }
  }

  getSelectedCheckbox() {
    let selected = [];
    this.selectedCheckbox = [];
    if (this.parent && this.key) {
      selected = this.parent.get(this.key).value || [];
    } else {
      selected = this.value || [];
    }
    this.options.map((x, i) => {
      if (selected.indexOf(this.storeFn(x)) > -1) {
        this.selectedCheckbox[i] = !this.selectedCheckbox[i];
      }
    });
  }

  setSelectedCheckbox(index: number) {
    const selected = [];
    this.selectedCheckbox[index] = !this.selectedCheckbox[index];
    this.selectedCheckbox.map((x, i) => {
      if (x) {
        selected.push(this.storeFn(this.options[i]));
      }
    });
    if (this.parent && this.key) {
      this.parent.get(this.key).setValue(selected);
    } else {
      this.value = selected;
    }
  }

  ngOnDestroy() {
    this.unsubscribeChange();
  }

}
