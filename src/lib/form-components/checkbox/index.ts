import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdCheckbox, MdCheckboxModule, MdIconModule } from '@angular/material';
import { CheckboxComponent } from './checkbox.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MultiLinguaModule } from '../multi-lingua/index';


@NgModule({
  imports: [CommonModule, MdCheckboxModule, FlexLayoutModule, MdIconModule, MultiLinguaModule],
  exports: [CheckboxComponent],
  declarations: [CheckboxComponent],
  providers: [],
})
export class CheckboxModule { }
