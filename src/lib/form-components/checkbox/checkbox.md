# Checkbox

## **\<nt-checkbox\>** 

## API References

A list of attributes that can be used by the `<nt-address>` component.<br>

|Name|Description|
|----|-----------|
|@Input()<br>parent|_**`Form Group`**_<br> Form Group|
|@Input()<br>key|_**`string`**_<br>Field name |
|@Input()<br>disabled|_**`boolean`**_<br>`True` as disabled/readOnly field.|
|@Input()<br>placeholder|_**`string`**_<br>`True` as disabled/readOnly field.|
|@Input()<br>displayFn|_**`function`**_<br>Function to set the display value|
|@Input()<br>storeFn|_**`function`**_<br>Function to set the store value|

