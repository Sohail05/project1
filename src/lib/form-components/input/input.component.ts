import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../core/validation.service';
import { IUIComponent, IUIComponentData } from '../ui-component/ui-component';
import { MakeProvider, ValueAccessorBase } from '../value-accessor-base/value-accessor-base';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';


@Component({
  selector: 'nt-input',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    MakeProvider(InputComponent),
    MakeLocaleMessageProvider(InputComponent)
    ],
})
export class InputComponent extends ValueAccessorBase<string> implements OnInit, OnChanges, IUIComponent, LocaleMessage {

  tooltip;
  data = <IUIComponentData>{};
  @Input() parent: FormGroup;
  @Input() type = 'text';
  @Input() key;
  @Input() placeholder: string;
  @Input() isDisabled = false;
  @Input() required = null;
  @Input() maxLength: number;
  @Input() masking = 'text';
  @Input() maskingOpts: object;
  @Input() validation: object;
  @Input() conditional: { [key: string]: string };
  @Input() prefix = '';
  @Input() suffix = '';

  inputMask;
  showRequired: boolean;
  showTooltip: boolean;
  formSubmission: Subscription;
  control: FormControl;

  constructor(
    private validationService: ValidationService,
    private formService: FormService
  ) {
    super(); // value accessor base
  }


  ngOnInit() {
    this.getInputMask();
    this.setValidators();
    this.formSubmission = this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && val;
    });
  }

  loadLocaleMessage (locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  setValidators() {
    if (Object.keys(this.data).length || !this.parent) {
      return;
    }
    this.control = this.parent.get(this.key) as FormControl;

    if (this.isDisabled) {
      this.control.disable();
      return;
    }
    const conditionalValidatorArr = [];
    if (this.conditional) {
      conditionalValidatorArr.push(this.validationService.getConditionalValidator(this.conditional));
    }
    let validatorArr = [];
    validatorArr = this.validationService.getAllValidator(this.validation);
    this.control.setValidators(Validators.compose([...validatorArr, ...conditionalValidatorArr]));
    this.control.updateValueAndValidity();
  }

  getInputMask(val?) {
    this.inputMask = this.validationService.getInputMask(this.data.masking || this.masking || val, this.maskingOpts);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes['validation'] || changes['required'] || changes['isDisabled']) {
      this.setValidators();
    }
    if (changes['masking']) {
      this.getInputMask(changes['masking'].currentValue);
    }
  }
}
