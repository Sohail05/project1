# Input 

## **\<nt-input\>**  

## Example

```html
  <nt-input
      key="modelName"
      [parent]="form"
      [placeholder]="selectType"
      [validation]="{ minlength: 10, required: true, email: true }"
      [masking]="currency">
  </nt-input>
```
## API References

A list of available attributes that can be used on the `<nt-input>` component.  

Please note that the square bracket syntax i.e. `[]` is not required for an attribute name if its value is not an expression but a primitive value e.g. `boolean`, `string`, as stated [here](https://angular.io/docs/ts/latest/guide/template-syntax.html#!#one-time-string-initialization).

| Name                      | Description                                         |
| ------------------------- | --------------------------------------------------- |
| @Input() <br> parent | _**`string: FormGroup`**_ _(required)_ <br><br> Pass `FormGroup` of parent form.
| @Input() <br> placeholder | _**`string`**_ _(required)_ <br> <br> Set a custom placeholder name.        |
| @Input() <br> masking        | _**`string`**_ _(optional)_ <br><br> Apply input masking capability. <br><br>`text` : Accepts any characters (default) <br> `number`: Only integers and without decimal points <br> `currency`: Formatted currency value. Accepts decimals. <br> `decimal`: Accepts numbers with decimal points(defaults to 2) <br> `percent`: Numbers with '%' suffix. Accepts 2 decimals by default. <br> `date`: Restricts value to a date value. Defaults to `mm/dd/yyy` format.
| @Input() <br> maskingOpts | _**`object`**_ _(optional)_ <br><br> Accepts an object of properties to override the defaults of each `type`. <br> All available properties, except for `date` type, can be found [here](https://github.com/text-mask/text-mask/tree/master/addons/#createnumbermask).  <br> To override the `date`'s default format to, say `dd/mm/yyyy`, do this `[maskingOpts]={ format: 'dd/mm/yyyy' }`       
| @Input() <br> isDisabled    | _**`boolean`**_ \| _**`string`**_ _(optional)_ <br><br> `true` to disable this input. `false` by default.                   |
| @Input() <br> validation | _**`object literal`**_ _(optional)_ <br><br> Accepts a Javascript object literal that contains various validation rules with each as a property name that define the rules. <br> _e.g._ `[validation]="{ required: true, minlength: 11, email: true }"`, in which case, validates the input value to ensure that it's `required`, `minlength` is 11, and is in the format of `email`.
| @Input() <br> prefix | add prefix to the input, eg: [ $ ___________ ]
| @Input() <br> suffix | add suffix to the input, eg: [ ___________ $ ]
|