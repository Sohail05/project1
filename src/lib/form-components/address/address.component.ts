import { Component, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Countries } from '../core/countries';
import { States } from '../core/states';
import { ValidationService } from './../core/validation.service';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { Address } from './address.model';
import { CodeTypeService } from '../code-type/code-type.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { FormService } from '../core/form.service';
import { LocaleMessage, MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  providers: [
    MakeProvider(AddressComponent),
    MakeLocaleMessageProvider(AddressComponent)
  ],
})
export class AddressComponent extends ValueAccessorBase<Address> implements OnInit, OnDestroy, IUIComponent, LocaleMessage {
  data = <IUIComponentData>{};
  @Input() parent: FormGroup;
  @Input() key;
  @Input() placeholder = 'Address';
  @Input() msgCode: string;
  @Input() required = false;
  @Input() disabled = false;
  // TODO set by global variable or enviroment
  @Input() defaultCountry: string;
  showRequired: boolean;
  formSubmission: Subscription;
  control: FormGroup;
  friendlyName = {
    addr1: 'Address Line 1',
    addr2: 'Address Line 2',
    addr3: 'Address Line 3',
    postcode: 'Postal Code',
    city: 'City',
    ste: 'State',
    cty: 'Country'
  }
  errorMessage = null;

  private form: FormGroup;
  // Listing of Countries & States
  countries;
  // Todo: Display states based on Country
  private allStates = [];
  // states based on countries selected;
  states = [];

  private countryObv: Subscription;
  private allStateObv: Subscription;
  private stateObv: Subscription;

  disabledFn = (n: object) => { return n['vldIdc'] === 'N' ? true : false };

  get _disabled() {
    return (this.disabled || this.parent.disabled || this.control.disabled);
  }

  constructor(
    private validate: ValidationService,
    private codeType: CodeTypeService,
    private fb: FormBuilder,
    private formService: FormService
  ) {
    super();
  }

  ngOnInit() {
    // set addres component controls
    // this.setAddressControl();
    this.control = this.parent.get(this.key) as FormGroup;

    this.countryObv = this.codeType.getCodeTypes('CODE.CTY').subscribe((c) => {
      this.countries = c;
    });
    this.allStateObv = this.codeType.getCodeTypes('CODE.CTY_STE').subscribe((s) => {
      this.allStates = s;
      // subscribe after get all states
      this.subscribeStates();
    });
    if (this.msgCode) {
      this.getPlaceholder();
    }
    this.formSubmission = this.formService.formSubmitted$.subscribe((val) => {
      for (const control of Object.keys(this.control.value)) {
        if (!this.control.value[control]) {
          if (control === 'ste' && !this.states.length) {
            break;
          }
          this.errorMessage = `${this.friendlyName[control]} is required`;
          break;
        }
        this.errorMessage = null;
      }
    });
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
  }

  setAddressControl() {
    const address = new Address();
    const val = this.control.value;
    // set default country
    if (!val.cty) {
      val.cty = this.defaultCountry;
    }
    Object.assign(address, val);

    if (this._disabled) {
      this.parent.setControl(this.key, this.fb.group(address));
      this.parent.disable();
    } else {
      this.parent.setControl(this.key, this.fb.group(address));
    }
  }

  ngOnDestroy() {
    // todo: unsubcribe observables
    if (this.countryObv) {
      this.countryObv.unsubscribe();
    }
    if (this.allStateObv) {
      this.allStateObv.unsubscribe();
    }
    if (this.stateObv) {
      this.stateObv.unsubscribe();
    }
  }

  getPlaceholder() {
    // get label by msgCode
  }

  subscribeStates() {
    // set states if country has val
    const countryVal = this.control.get('cty').value;
    if (countryVal) {
      this.filterStates(countryVal);
    }
    // filter states with country code
    this.stateObv = this.control.get('cty').valueChanges.subscribe((x) => {
      if (this.allStates.length > 0) {
        this.filterStates(x);
      }
    });
  }

  filterStates(val) {
    this.states = this.allStates.filter((s) => {
      return s.grpCodes.indexOf(val) > -1;
    });
  }


}
