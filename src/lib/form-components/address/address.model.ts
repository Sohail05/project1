export interface IAddress {
  addr1: string;
  addr2: string;
  addr3: string;
  postalCode: string;
  city: string;
  state: string;
  country: string;
};

export class Address implements IAddress {
  addr1 = '';
  addr2 = '';
  addr3 = '';
  postalCode = '';
  city = '';
  state = '';
  country = '';
}
