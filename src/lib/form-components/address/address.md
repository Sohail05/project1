# Address

## **\<nt-address\>** 
Address allow user to input a group of address related data.
## Usage

#### Using `<nt-address>` _outside_ of `<form>`
```javascript
// sample address data object
address = {
    addr1: 'Sample Address Line 1',
    addr2: 'Sample Address Line 2',
    addr3: 'Sample Address Line 3',
    postalCode: '34000',
    city: 'Bangsar South',
    country: 'Somalia',
    state: 'CA',
};
```

```html
<nt-address [data]="address"></nt-address>
```

#### Using `<nt-address>` _inside_ of `<form>`
```html
<form [formGroup]="form">
  <nt-address [parent]="form.get('address')"></nt-address>
</form>
```

## API References

A list of attributes that can be used by the `<nt-address>` component.<br>

|Name|Description|
|----|-----------|
|@Input()<br>fieldLabel|_**`String`**_<br>Set a custom field label for the address group.|
|@Input()<br>dataModel|_**`IAddress interface`**_<br>Accept the address group data.<br>It included addr1, addr2, addr3, postcode, city, state and country field.|
|@Input()<br>required|_**`boolean`**_<br>`True` as required/mandatory field.<br>As per requirement this attribute only affects `Line 1`, `PostalCode`, `City`, `State` and `Country` fields.|
|@Input()<br>disabled|_**`boolean`**_<br>`True` as disabled/readOnly field.|

### IAddress interface properties
|Name|Types|
|----|-----------|
|addr1|_**`string`**_|
|addr2|_**`string`**_|
|addr3|_**`string`**_|
|postcode|_**`number`**_|
|city|_**`string`**_|
|state|_**`string`**_|
|country|_**`string`**_|