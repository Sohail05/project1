import { Component, Input, OnInit, SimpleChanges, OnChanges, HostListener } from '@angular/core';
import { ValidationService } from './../core/validation.service';
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { IUIComponent, IUIComponentData } from '../ui-component/ui-component';
import { Subscription } from 'rxjs/Subscription';
import { FormService } from '../core/form.service';
import { MakeLocaleMessageProvider } from '../core/locale-message';
import { Locale } from '../core/model/locale';

@Component({
  selector: 'nt-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  providers: [
    MakeProvider(TextareaComponent),
    MakeLocaleMessageProvider(TextareaComponent)
  ],
})
export class TextareaComponent extends ValueAccessorBase<string> implements OnInit, OnChanges, IUIComponent {
  data = <IUIComponentData>{};
  @Input() parent: FormGroup;
  @Input() key: string;
  @Input() placeholder: string;
  @Input() isDisabled: boolean | string = false;
  @Input() required = null;
  @Input() rows: number;
  @Input() maxRows: number;
  @Input() maxLength: number;
  @Input() validation: object;
  tooltip;
  showTooltip;
  showRequired: boolean;
  formSubmission: Subscription;
  control: FormControl;

  constructor(
    private formService: FormService,
    private validationService: ValidationService
  ) {
    super();
  }

  ngOnInit() {
    this.setValidators();
    this.formSubmission = this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && !this.control.value && val;
    });
  }

  loadLocaleMessage(locale: Locale) {
    this.placeholder = locale.msg;
    this.tooltip = locale.tooltip;
  }

  setValidators() {
    if (!this.parent) {
      return;
    }
    this.control = this.parent.get(this.key) as FormControl;
    if (Object.keys(this.data).length) {
      return;
    }
    if (this.isDisabled === 'true') {
      this.control.disable();
      return;
    }

    let validatorArray = [];
    validatorArray = this.validationService.getAllValidator(this.validation);
    this.control.setValidators(Validators.compose([...validatorArray]));
    this.control.updateValueAndValidity();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['validation'] || changes['required'] || changes['isDisabled']) {
      this.setValidators();
    }
  }

}
