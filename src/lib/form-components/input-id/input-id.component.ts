import { FormService } from '../core/form.service';
import { Component, OnInit, Input, HostListener, AfterContentInit, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidationService } from '../core/validation.service';
import { IUIComponentData, IUIComponent } from '../ui-component/ui-component';
import { MakeCodeTypeProvider, CodeTypeContainer } from '../code-type/code-type-container';
import { Functional } from '../core/functional';

@Component({
  selector: 'nt-input-id',
  templateUrl: './input-id.component.html',
  styleUrls: ['./input-id.component.scss'],
  providers: [MakeCodeTypeProvider(InputIdComponent)]
})
export class InputIdComponent implements AfterContentInit, OnChanges, IUIComponent, CodeTypeContainer {
  showTooltip: boolean;
  data = <IUIComponentData>{};
  inputMask;
  options = [];
  control: FormControl;
  controlGroup: FormGroup;
  showRequired: boolean;
  childKeys: Array<string> = [];
  @Input() maxlength: number;
  @Input() parent: FormGroup;
  @Input() type: string;
  @Input() key: string;
  @Input() masking: string;
  @Input() maskingOpts: object = {};
  @Input() required = false;
  @Input() isDisabled = false;

  disabledFn = Functional.isDisabled;
  @Input() displayFn = Functional.codeDescription;
  @Input() storeFn = Functional.codeValue;
  @Input() displayFnOnOpen = Functional.codeDescription;
  @Input() displayFnOnClose = Functional.codeValue;

  get _isDisabled() {
    return (this.isDisabled || this.parent.disabled || this.controlGroup.disabled);
  }
  constructor(
    private validationService: ValidationService,
    private formService: FormService
  ) { }

  loadCodeType(options: any) {
    this.options = options;
    this.setExistingMask();
  }

  ngAfterContentInit() {
    this.controlGroup = this.parent.get(this.key) as FormGroup;
    this.childKeys = Object.keys(this.controlGroup.controls);
    this.control = this.controlGroup.get(this.childKeys[1]) as FormControl;
    this.onClose();
    this.getInputMask();
    if (this._isDisabled) {
      (<FormControl>this.controlGroup.get(this.childKeys[0])).disable();
    }
    (<FormControl>this.controlGroup.get(this.childKeys[1])).disable();
    (<FormControl>this.controlGroup.get(this.childKeys[0])).valueChanges.subscribe((val) => {
      if (val) {
        (<FormControl>this.controlGroup.get(this.childKeys[1])).enable();
      } else {
        (<FormControl>this.controlGroup.get(this.childKeys[1])).disable();
      }
    });
    this.controlGroup.get(this.childKeys[0]).valueChanges.subscribe((value) => {
      if (!value) {
        return;
      }
      const opt = this.options.find((option) => this.storeFn(option) === value);
      if (!opt) {
        return;
      }
      if (opt.atrVal1) {
        this.getInputMask(opt.atrVal1);
      }
    });
    this.formService.formSubmitted$.subscribe((val) => {
      if (this.control && !this.control.value && !this.required) {
        this.control.markAsUntouched();
        return;
      }
      this.showRequired = this.required && !this.control.value && val;
    });
  }

  onOpen() {
    if (this.displayFnOnOpen) {
      this.displayFn = this.displayFnOnOpen;
    }
  }

  onClose() {
    if (this.displayFnOnClose) {
      this.displayFn = this.displayFnOnClose;
    }
    // set value to '' when user deselect idType
    this.formatBlank();
  }

  formatBlank() {
    // set value to '' when user deselect idType
    // todo: replace array [i] with property or better.
    if (!this.controlGroup.get(this.childKeys[0]).value) {
      this.controlGroup.get(this.childKeys[1]).setValue('');
    }
  }

  setExistingMask() {
    if (!this.childKeys.length) {
      return;
    }
    const idType = this.controlGroup.get(this.childKeys[0]).value;
    if (idType) {
      this.getInputMask(this.options.find((opt) => this.storeFn(opt) === idType).atrVal1);
    }
  }
  getInputMask(val?) {
    this.inputMask = this.validationService.getInputMask(this.data.masking || this.masking || val, this.maskingOpts);
    this.controlGroup.get(this.childKeys[1]).setValidators(Validators.pattern(val));
  }

  ngOnChanges(change: SimpleChanges) {
    if (this.controlGroup) {
      if (this._isDisabled) {
        (<FormControl>this.controlGroup.get(this.childKeys[0])).disable();
        (<FormControl>this.controlGroup.get(this.childKeys[1])).disable();
      }

    }
  }

}
