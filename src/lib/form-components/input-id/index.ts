import { NgModule } from '@angular/core';
import { InputIdComponent } from './input-id.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CodeTypeModule } from '../code-type/index';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MdSelectModule, MdInputModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { TextMaskModule } from 'angular2-text-mask';
import { MultiLinguaModule } from '../multi-lingua/index';
import { ControlMessageModule } from '../control-message/index';
import { MultiLinguaTooltipModule } from '../tooltip/index';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MdSelectModule,
    MdInputModule,
    CodeTypeModule,
    TextMaskModule,
    MultiLinguaModule,
    ControlMessageModule,
    MultiLinguaTooltipModule
  ],
  exports: [InputIdComponent],
  declarations: [InputIdComponent],
  providers: [],
})
export class InputIdModule { }
