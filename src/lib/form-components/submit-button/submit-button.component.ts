import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DirtyCheckService } from '../core/dirty-check.service';

@Component({
  selector: 'nt-submit-button',
  templateUrl: './submit-button.component.html',
  styleUrls: ['./submit-button.component.scss'],
})
export class SubmitButtonComponent implements OnInit {
  @Input() parent: FormGroup;
  @Input() mandatory = true;
  @Input() isDisabled = false;
  @Input() dirtycheck;
  @Input() resetLocaleCode = 'cmo.lbl.reset';
  @Output() submit = new EventEmitter<any>();
  @Output() reset = new EventEmitter<any>();

  constructor(private dirtyCheckService: DirtyCheckService) {
   }

  Submit() {
    this.submit.emit();
  }

  Reset() {
    if (this.parent) {
      const key = JSON.stringify(Object.keys(this.parent.value));
      this.parent.reset(this.dirtyCheckService.getCurrentFormValue(key));
    }
    this.reset.emit();
  }

  ngOnInit() {
  }

}
