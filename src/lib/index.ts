/** Angular Modules */
import { NgModule } from '@angular/core';

/** Antelope Modules */
import { AddressModule } from './address/index';
import { AreaChartModule } from './area-chart/index';
import { AutocompleteModule } from './autocomplete/index';
import { BackButtonModule } from './back-button/index';
import { BreadcrumbModule } from './breadcrumb/index';
import { CollapsibleModule } from './collapsible/index';
import { DatePickerModule } from './date-picker/index';
import { UITableModule } from './ui-table/index';
import { SelectModule } from './select/index';
import { MultiCurrencyModule } from './multi-currency/index';
import { SearchModule } from './search/index';
import { VersionBuildModule } from './version-build/index';
import { InputModule, InputComponent } from './input/index';
import { SubmitButtonModule } from './submit-button/index';
import { SidenavFormModule } from './sidenav-form/index';
import { TextareaModule } from './textarea/index';
import { RichTextAreaModule } from './rich-text-area/index';
import { DateModule } from './date/index';
import { CodeTypeModule } from './code-type/index';
import { DeleteDialogModule } from './delete-dialog/index';
import { HorizontalBarChartModule } from './horizontal-bar-chart/index';
import { TreeModule } from './tree/index';
import { StackedBarChartModule } from './stacked-bar-chart/index';
import { RecursiveTableModule } from './recursive-table/index';
import { ProgressBarModule } from './progress-bar/index';
import { TopNavBarModule } from './top-nav-bar/index';
import { SidenavBarModule } from './sidenav-bar/index';
import { ScreenModule } from './screen/index';
import { OrgChartModule } from './org-chart/index';
import { MultiAutocompleteModule } from './multi-autocomplete/index';
import { MultiLinguaModule } from './multi-lingua/index';
import { CoreModule } from './core/index';
import { WorkflowBarModule } from './workflow-bar/index';
import { NotifiyBadgeModule } from './notify-badge/index';
import { InputIdModule } from './input-id/index';
import { RadioButtonModule } from './radio-button/index';
import { AccessControlModule } from './access-control/index';
import { CheckboxModule } from './checkbox/index';
import { PhoneInputModule } from './phone-input/index';
import { FilterPanelModule } from './filter-panel/index';
import { PatchModule } from './patch/index';
import { DirtyCheckModule } from './dirty-check/index';
import { FormContainerModule } from './form-container/index';
import { MultiLinguaTooltipModule } from './tooltip/index';
import { ApplicationBarModule } from './application-bar/index';
import { LogoModule } from './logo/index';
import { AuthModule } from './auth/index';
import { DatepickerModule } from './datepicker/index';
import { CardModule } from './card/index';

const Modules = [
  AuthModule,
  LogoModule,
  PatchModule,
  AccessControlModule,
  CodeTypeModule,
  MultiAutocompleteModule,
  HorizontalBarChartModule,
  MultiLinguaModule,
  TreeModule,
  StackedBarChartModule,
  RecursiveTableModule,
  ProgressBarModule,
  SubmitButtonModule,
  DeleteDialogModule,
  DateModule,
  BackButtonModule,
  BreadcrumbModule,
  AutocompleteModule,
  AreaChartModule,
  VersionBuildModule,
  InputModule,
  CollapsibleModule,
  SearchModule,
  AddressModule,
  SelectModule,
  MultiCurrencyModule,
  UITableModule,
  DatePickerModule,
  SidenavFormModule,
  TextareaModule,
  RichTextAreaModule,
  TopNavBarModule,
  SidenavBarModule,
  ScreenModule,
  OrgChartModule,
  WorkflowBarModule,
  NotifiyBadgeModule,
  InputIdModule,
  RadioButtonModule,
  CheckboxModule,
  PhoneInputModule,
  FilterPanelModule,
  DirtyCheckModule,
  FormContainerModule,
  MultiLinguaTooltipModule,
  ApplicationBarModule,
  DatepickerModule,
  CardModule
];

export const Components = {
  InputComponent
};
export const EntryComponents = [
  InputComponent
];

@NgModule({
  imports: Modules,
  exports: Modules,
})
export class AntelopeModule {
  static forRoot() {
    return [
      CoreModule.forRoot(),
      DatePickerModule.forRoot(),
      SidenavBarModule.forRoot(),
      CodeTypeModule.forRoot(),
      MultiLinguaModule.forRoot(),
      AccessControlModule.forRoot(),
      AuthModule.forRoot(),
    ];
  }
}
