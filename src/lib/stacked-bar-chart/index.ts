import { NgModule } from '@angular/core';

import { StackedBarChartComponent } from './stacked-bar-chart.component';

@NgModule({
    imports: [],
    exports: [],
    declarations: [StackedBarChartComponent],
    providers: [],
})
export class StackedBarChartModule { }
