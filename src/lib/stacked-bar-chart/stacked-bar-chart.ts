import * as d3 from 'd3';

export default (() => {
  const margin = { top: 20, right: 50, bottom: 30, left: 120 };
  const barAnimateDelay = 40;
  const stackedAnimateDelay = 200;
  let data;
  let chartOptions;
  let x;
  let y;
  const defaultOptions = {
    id: '',
    data: [],
    width: 300,
    height: 150,
    stackKey: [],
    duration: 650,
    offSetBars: false,
    colorRange: ['#1976D2', '#90CAF9'],
  };
  function init(options) {
    chartOptions = Object.assign(defaultOptions, options);

    data = chartOptions.data;
    data.map((d) => {
      d['total'] = 0;
      chartOptions.stackKey.map((key) => {
        d['total'] += d[key];
      });
      return d;
    });

    const width = chartOptions.width - margin.left - margin.right;
    const height = chartOptions.height - margin.top - margin.bottom;

    const svg = d3.select(chartOptions.id)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom);

    const g = svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    x = d3.scaleBand()
      .rangeRound([0, width])
      .padding(0.2)
      .align(0.2);

    y = d3.scaleLinear()
      .rangeRound([height, 40]);

    const color: any = d3.scaleOrdinal()
      .range(chartOptions.colorRange);

    const stack = d3.stack()
      .keys(chartOptions.stackKey)
      .offset(chartOptions.offSetBars ? d3.stackOffsetExpand : d3.stackOffsetNone);

    data.sort((a, b) => b.total - a.total);

    x.domain(data.map((d) => d.type));

    if (!chartOptions.offSetBars) {
      y.domain( [0, d3.max(data, (d) => d['total']) ]).nice();
    }

    const layers = stack(data);

    g.selectAll('.layer')
      .data(layers)
      .enter().append('g')
      .attr('class', 'layer')
      .attr('fill', (d: any, i) => color(i))
      .selectAll('rect')
      .data((d: any) => d)
      .enter().append('rect')
      .attr('x', (d: any) => x(d.data.type))
      .attr('y', height)
      .attr('height', 0)
      .attr('width', x.bandwidth())
      .call(transitionStackedBars);

    g.append('g')
      .attr('class', 'd3-axis--noline')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x));

    g.selectAll('.text-total')
      .data(data)
      .enter().append('text')
      .attr('class', 'text-total')
      .attr('x', (d: any) => x(d.type))
      .attr('y', 16)
      .attr('dy', '-.35em')
      .attr('text-anchor', 'start')
      .style('font-size', '0.8rem')
      .text((d: any) => d.total);

    const concatLayers = [];

    for (const keyLayers of layers) {
      for (const keyLayer of keyLayers) {
        keyLayer['key'] = keyLayers.key;
        concatLayers.push(keyLayer);
      }
    }

    const moreMap = {};

    g.selectAll('.text')
      .data(concatLayers)
      .enter().append('text')
      .attr('class', 'text')
      .attr('x', (d: any) => x(d.data.type))
      .attr('y', (d) => {
        moreMap[d.key] = y(d[1]);
        return y(d[1]);
      })
      .attr('dy', '-.35em')
      .attr('fill', (d: any, i) => color(d.key))
      .attr('text-anchor', 'start')
      .style('font-size', '0.8rem')
      .text((d: any) => d.data[d.key]);
    // TODO: Need refactor
    chartOptions.stackKey.push('total');
    moreMap['total'] = 10;
    moreMap['pre-approved(\'000)'] = height;

    g.selectAll('.text--label')
      .data(chartOptions.stackKey)
      .enter().append('text')
      .attr('class', 'text--label')
      .attr('x', 0)
      .attr('y', (d: any) => moreMap[d])
      .attr('dy', (d: any, i) => d === 'total' ? '0' : '-.50em')
      .attr('fill', (d: any, i) => d === 'total' ? '#000' : color(d))
      .attr('text-anchor', 'end')
      .style('text-transform', 'uppercase')
      .style('font-size', '0.6rem')
      .text((d: any) => d);
  }

  function barDelay (d, i) { return i * barAnimateDelay; };

  function transitionStackedBars(selection) {
    setTimeout(() => {
      selection.transition()
        .duration(chartOptions.duration)
        .delay(barDelay)
        .attr('y', (d) => y(d[1]))
        .attr('height', (d) => y(d[0]) - y(d[1]));
    }, stackedAnimateDelay);
  }

  return { init };

})();
