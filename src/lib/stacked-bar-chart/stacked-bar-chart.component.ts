import { Component, Input, OnInit } from '@angular/core';
import stackedBarChart from './stacked-bar-chart';

@Component({
  selector: 'nt-stacked-bar-chart',
  templateUrl: './stacked-bar-chart.component.html',
  styleUrls: ['./stacked-bar-chart.component.scss'],
})
export class StackedBarChartComponent implements OnInit {
  @Input() data;
  @Input() stackKey;
  constructor() { }

  ngOnInit() {
    stackedBarChart.init({
      id: '#stacked-bar-chart',
      data: this.data,
      width: 500,
      height: 300,
      stackKey: this.stackKey,
      duration: 400,
      offSetBars: true,
      colorRange: ['#1976D2', '#90CAF9'],
    });
  }

}
