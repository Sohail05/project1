import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { TopNavBarComponent } from './top-nav-bar.component';
import { BreadcrumbModule } from '../breadcrumb/index';
import { NotifiyBadgeModule } from '../notify-badge/index';
import { SessionDropdownComponent } from './session-dropdown.component';
import { VersionBuildModule } from '../version-build/index';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    BreadcrumbModule,
    NotifiyBadgeModule,
    VersionBuildModule,
  ],
  exports: [TopNavBarComponent, SessionDropdownComponent],
  declarations: [TopNavBarComponent, SessionDropdownComponent],
})
export class TopNavBarModule { }
