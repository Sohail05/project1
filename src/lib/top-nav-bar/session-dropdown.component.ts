import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '../auth/session.service';
import { JWT } from '../auth/auth';
import { environment } from 'environments/environment';

@Component({
  selector: 'nt-session-dropdown',
  templateUrl: './session-dropdown.component.html',
  styleUrls: ['./session-dropdown.component.scss'],
})
export class SessionDropdownComponent {

    get username() {
        return new JWT(this.session.token).payload().user_name;
    }

    constructor(
        private router: Router,
        private session: SessionService
    ) { }

    /**
     * Clear session and redirect to login page.
     */
    logout() {
        this.session.clear();
        this.router.navigateByUrl(environment.loginRoute);
    }
}
