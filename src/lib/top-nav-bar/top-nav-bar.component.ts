import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import screenfull from 'screenfull';

/**
 * This class represents the navigation bar component.
 */
@Component({
    selector: 'nt-topnav-bar',
    templateUrl: './top-nav-bar.component.html',
    styleUrls: ['./top-nav-bar.component.scss'],
    providers: [],
})
export class TopNavBarComponent {
    @Input() showMenu = false;
    @Output() menu = new EventEmitter();
    notificationBlock = false;
    isFullscreen = false;
    private data;
    currentDate = new Date();
    yesterdayDate;
    @HostListener('document:webkitfullscreenchange', [])
    fullscreen() {
        if (screenfull.enabled) {
            this.isFullscreen = screenfull.isFullscreen;
        }
    }
    constructor() {
    }
    menuTrigger() {
        this.menu.emit();
    }
    toggleFullscreen() {
        if (screenfull.enabled) {
            screenfull.toggle();
        }
    }

    logout() {
    }
}
