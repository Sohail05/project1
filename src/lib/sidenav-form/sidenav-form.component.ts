import { Component, ComponentFactoryResolver, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ViewContainerDirective } from './view-container.directive';
import { SidenavFormService } from './sidenav-form.service';

@Component({
  selector: 'nt-sidenav-form',
  templateUrl: './sidenav-form.component.html',
  styleUrls: ['./sidenav-form.component.scss'],
})
export class SidenavFormComponent implements OnInit {
  @ViewChild(ViewContainerDirective) dynamicHost: ViewContainerDirective;
  @ViewChild('sidenav') sidenav;
  @Input() questions = [];
  form: FormGroup;
  controlsToSanitize: { [key: string]: any } = {};

  constructor(private sidenavFormService: SidenavFormService) { }

  setQuestions(questions) {
    if (questions === this.questions) {
      this.sidenav.open();
      return;
    }
    this.sidenav.open();
    this.questions = questions;
    this.loadComponent();
  }
  loadComponent() {
    this.dynamicHost.viewContainerRef.clear();
    this.form = this.sidenavFormService.toFormGroup(this.questions);
    this.sidenavFormService.insertComponents(this.dynamicHost, this.questions, this.form);
    this.controlsToSanitize = this.sidenavFormService.getControlsToSanitize(this.questions);
    this.subscribeToSanitizeForm();
  }
  subscribeToSanitizeForm() {
    this.form.valueChanges.subscribe((res) => {
      for (const prop  of Object.keys(this.controlsToSanitize)) {
        res[prop] = res[prop].replace(/[$,%]/g, '').trim();
      }
    });
  }
  ngOnInit() {
    this.form = this.sidenavFormService.toFormGroup(this.questions);
  }
  onSubmit() {
    // this.payLoad.next(JSON.stringify(this.form.value));
  }

}
