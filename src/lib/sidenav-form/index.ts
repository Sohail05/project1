import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { SidenavFormComponent } from './sidenav-form.component';
import { SubmitButtonModule } from '../submit-button/index';
import { SidenavFormService } from './sidenav-form.service';
import { ViewContainerDirective } from './view-container.directive';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    SubmitButtonModule
  ],
  exports: [SidenavFormComponent],
  declarations: [SidenavFormComponent, ViewContainerDirective],
  providers: [SidenavFormService]
})
export class SidenavFormModule { }
