import { Directive, ViewContainerRef  } from '@angular/core';

@Directive({ selector: '[ntDynamicCmpHost]' })
export class ViewContainerDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }

  get parentInjector() {
    return this.viewContainerRef.parentInjector;
  }
}
