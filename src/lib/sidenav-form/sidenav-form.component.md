# \<nt-sidenav-form\>

This component is only used once in the entire app in the `<app-side-bar-nav>`. It will be used by any controllers that need to invoke a side bar form that slides in and out from the right side of the screen.

The `<nt-sidenav-form>` is initially empty until its populated with components. To populate the form from whatever controller you are in, an array is passed. The array contains elements of Javascript objects that each specifies a specific UI component's metadata and validation rules:

```javascript
// sample.component.ts
constructor(private sidenavFormService: SidenavFormService) { }

questions = [
    {
      component: 'InputComponent', // class's name.
      data: {
        value: 'You are awesome',
        key: 'grpNm',
        placeholder: 'Group Name',
        validation: {
          required: true,
        },
      },
    },
    {
      component: 'SelectComponent', // another different component type
      data: {
        key: 'grpId',
        placeholder: 'Group ID',
        // masking: 'currency',
        validation: {
          required: true,
          email: true,
        },
      },
    },
  ];
  // emit the questions to the subscriber in <nt-sidenav-bar-bar>
  generateQuestions() {
    this.sidenavFormService.setQuestions(this.questions);
  }
  ngOnInit() {
    this.questions['type'] = 'Update'; // Set the button's text in the <nt-sidenav-form>
    this.questions['title'] = 'Custom Title'; // Set the title of the form
  }

```

This line,

```javascript
this.sidenavFormService.setQuestions(this.questions);
```
emits the questions as an observable to a subscriber in `<nt-sidenav-bar-bar>`:

```javascript

  @ViewChild(SidenavFormComponent) sidenavForm: SidenavFormComponent;

 setSidenavForm() {
    this.sidenavFormService.questions$.subscribe((questions) => {
      this.sidenavForm.setQuestions(questions);
    });
  }
```

This line,
```javascript
  this.sidenavForm.setQuestions(questions);
```
calls the `setQuestions` method in `<nt-sidenav-form>` that will call the `loadComponent` method which will actually generate the components:

```javascript
// sidenav-form.component.ts

@ViewChild(ViewContainerDirective) dynamicHost: ViewContainerDirective;
@ViewChild('sidenav') sidenav;

constructor(private sidenavFormService: SidenavFormService) { }

setQuestions(questions) {
    // just opens the side nav form if they are the same object
    if (questions === this.questions) {
      this.sidenav.open();
      return;
    }
    this.sidenav.open();
    this.questions = questions;
    this.loadComponent();
}
loadComponent() {
    // clear the side nav form of any previously inserted components
    this.dynamicHost.viewContainerRef.clear();
    // transform the questions to a FormGroup
    this.form = this.sidenavFormService.toFormGroup(this.questions);
    // generates and insert the questions as a component
    this.sidenavFormService.insertComponents(this.dynamicHost, this.questions, this.form);
    // to remove values of their masking characters
    this.controlsToSanitize = this.sidenavFormService.getControlsToSanitize(this.questions);
    // subscribe to FormGroup's valueChanges to do the sanitization
    this.subscribeToSanitizeForm();
  }
```
This line,
```javascript
@ViewChild(ViewContainerDirective) dynamicHost: ViewContainerDirective;
```
returns the `ntDynamicCmpHost` directive of type `ViewContainerDirective` from the view,
```html
// sidenav-form.component.html
<ng-template ntDynamicCmpHost></ng-template>
```

All dynamic component generations are handled in the `sidenav-form.service.ts`.


