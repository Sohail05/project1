import { ComponentFactoryResolver, ComponentRef, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { ValidationService } from './../core/validation.service';
import { ViewContainerDirective } from './view-container.directive';
import { IUIComponent } from './../ui-component/ui-component';
import { InputComponent } from '../input/input.component';

@Injectable()
export class SidenavFormService {
  private questions = new Subject<any>();
  questions$ = this.questions.asObservable();

  constructor(
    private validationService: ValidationService,
    private _componentFactoryResolver: ComponentFactoryResolver,
  ) { }
  setQuestions(questions) {
    this.questions.next(questions);
  }
  getComponent(name: string) {
    return InputComponent;
  }
  getComponentRef(vcr: ViewContainerDirective, componentData: any): ComponentRef<any> {
    const viewContainerRef = vcr.viewContainerRef;
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(this.getComponent(componentData.component));
    const componentRef = viewContainerRef.createComponent(componentFactory);
    return componentRef;
  }
  inputDataToComponentRef(componentRef, componentData, formGroup: FormGroup) {
    (<IUIComponent>componentRef.instance).parent = formGroup;
    (<IUIComponent>componentRef.instance).data = componentData.data;
  }
  insertComponents(host, questions, formGroup) {
    for (const cmp of questions) {
      const componentRef = this.getComponentRef(host, cmp);
      if (cmp.data) {
        this.inputDataToComponentRef(componentRef, cmp, formGroup);
      }
    }
  }
  getControlsToSanitize(questions) {
    const obj = {};
    for (const cmp of questions) {
      if (cmp.data.masking) {
        obj[cmp.data.key] = cmp.data.key;
      }
    }
    return obj;
  }
  toFormGroup(questions: any) {
    const group: any = {};
    questions.forEach((question) => {
      let validatorArray = [];
      if (question.data.validation && !question.data.disabled) {
        validatorArray = this.validationService.getAllValidator(question.data.validation);
      }
      group[question.data.key] = question.data.disabled ?
        new FormControl({ value: question.data.value || '', disabled: true }) :
        new FormControl(question.data.value || '', [...validatorArray]);
    });
    return new FormGroup(group);
  }
}
