import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BackButtonComponent } from './back-button.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule
    ],
    exports: [BackButtonComponent],
    declarations: [BackButtonComponent],
    providers: [],
})
export class BackButtonModule { }
