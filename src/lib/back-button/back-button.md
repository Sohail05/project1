# Placeholder

## **\<nt-placeholder\>** 

## Example

```html
<nt-back-button></nt-back-button>
```

Back button allow the user to navigate back to the previous "page".

``Note : Need to be use within minimal 1 router outlet``