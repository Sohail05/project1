# Card Listinng

## **\<nt-card\>** 

## API References

A list of attributes that can be used by the `<nt-card>` component.<br>

|Name|Description|
|----|-----------|
|@Input()<br>setting|_**`Array<CardLabel>`**_<br> Array of card label and data|
|@Input()<br>data|_**`Array<any>`**_<br>Array of Data |
|@Input()<br>labelStyle|_**`'left-right'|'top-down'|'no-label'`**_<br> Use to set the label display style. Default to 'left-right|
|@Input()<br>pageSizes|_**`Array<number>`**_<br>Array of page size options|
|@Input()<br>defaultPageSize|_**`number`**_<br> Default page size |
|@Input()<br>length|_**`number`**_<br>Set data size manually default to data size|
|@Input()<br>custom|_**`boolean`**_<br>Set to true when required custom designm for card|
|@Input()<br>cardSize|_**`string`**_<br>Width of card|
|@Input()<br>showResultSize|_**`boolean`**_<br>Display result size|
|@Input()<br>showSortOption|_**`boolean`**_<br>Display sort option|
|@Input()<br>pagination|_**`boolean`**_<br>Display pagination|
|@Input()<br>sortOptions|_**`Array<SortOption>`**_<br>Option used in sorting drop down|
|@Input()<br>sortFn|_**`function`**_<br>Function to set the sort funtion|
|@Input()<br>headerSetting|_**`CardHeaderSetting`**_<br>Setting for card header|
|@Input()<br>cardActions|_**`CardActions`**_<br>Setting for card body action|
|@Input()<br>clickable|_**`boolean`**_<br>Enabled click to select card|
|@Ouput()<br>iconEvent|_**`IconEvent`**_<br>Emit card header icon clicked|
|@Ouput()<br>selected|_**`object`**_<br>Emit data selected|
|@Ouput()<br>actionEvent|_**`IconEvent`**_<br>Emit card body icon clickded|

