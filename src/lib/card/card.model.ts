
export class CardHeaderSetting {
    sts?: string;
    stsIcon?: string;
    icons?: Array<string>;
    stsColor?: object;
    title?: string;
}

export class CardLabel {
    label: string;
    key: string;
    type?: 'currency' | 'number' | 'date' | 'array' | 'conditional' | 'function';
    currency?: string;
    currencyKey?: string;
    symbol?: boolean;
    decimal?: string;
    date?: string;
    toArray?: string;
    conditions?: object;
    appendIcon?: string;
    fn?: (x) => string;
    classes?: string;
}


export class SortOption {
    key: string;
    label: string;
    order?: string;
}

export class IconEvent {
    iconName: string;
    data: any;
    iconIndex: number;
}

export class CardActions {
    align: 'start' | 'end';
    icons: Array<string>;
}
