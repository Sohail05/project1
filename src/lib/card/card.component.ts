import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, IterableDiffer, IterableDiffers
} from '@angular/core';
import { PageEvent } from '@angular/material';
import { CardLabel, CardHeaderSetting, IconEvent, SortOption, CardActions } from './card.model';
@Component({
  selector: 'nt-card',
  exportAs: 'nt-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {
  @Input() data = [];
  @Input() setting: Array<CardLabel> = [];
  @Input() labelStyle: 'left-right' | 'top-down' | 'no-label' = 'left-right';
  @Input() showHeader = false;
  @Input() pageSizes: Array<number> = [];
  @Input() defaultPageSize = 10;
  @Input() length: number;
  @Input() custom = false;
  @Input() cardSize: string;
  @Input() showResultSize = true;
  @Input() showSortOption = true;
  @Input() pagination = true;
  @Input() sortOptions: Array<SortOption> = [];
  @Input() headerSetting: CardHeaderSetting = {
    sts: '',
    stsIcon: '',
    stsColor: {},
    icons: [],
    title: '',
  };
  @Input() cardActions: CardActions;
  @Input() clickable = false;
  @Input() sortFn = (x, y) => this.getData(this.sortBy.key, x) > this.getData(this.sortBy.key, y);
  @Output() iconEvent = new EventEmitter();
  @Output() selected = new EventEmitter();
  @Output() actionEvent = new EventEmitter();
  paginatedData = [];
  sortBy: SortOption;
  page: PageEvent;
  selectedData = { page: null, index: null };
  componentInit = false;
  private diff: IterableDiffer<string>;

  constructor(
    private differs: IterableDiffers
  ) {
    this.diff = differs.find([]).create(null);
  }

  ngOnInit() {
    this.page = new PageEvent();
    this.page.length = this.data.length;
    this.page.pageIndex = 0;
    this.page.pageSize = this.defaultPageSize;
    this.paginateData();
    this.componentInit = true;
  }

  ngOnChanges() {
    const changes = this.diff.diff(this.data);
    if (this.componentInit && changes) {
      this.paginateData();
    }
  }

  /**
   * Trigger pagination and update PageEvent
   * @param page PageEvent
   */
  pageChange(page: PageEvent) {
    this.page = page;
    this.paginateData();
  }

  /**
   * Process paginated Data
   */
  paginateData() {
    if (this.pagination) {
      const lastIndex = (this.page.pageIndex + 1) * this.page.pageSize;
      const firstIndex = this.page.pageIndex * this.page.pageSize;
      this.paginatedData = this.data.slice(firstIndex, lastIndex);
    } else {
      this.paginatedData = this.data;
    }
  }

  getData(path: string, object: any) {
    if (path) {
      return path.split('.').reduce((o, k) => (o || {})[k], object)
    }
  }

  /**
   * Emit event when card header icon click
   * @param data
   * @param iconIndex
   * @param iconName
   */
  iconClick(data, iconIndex, iconName) {
    const event: IconEvent = {
      data: data,
      iconIndex: iconIndex,
      iconName: iconName,
    }
    this.iconEvent.emit(event);
  }

  /**
   * Emit event when card body icon click
   * @param data
   * @param iconIndex
   * @param iconName
   */
  bodyIconClick(data, iconIndex, iconName) {
    const event: IconEvent = {
      data: data,
      iconIndex: iconIndex,
      iconName: iconName,
    }
    this.actionEvent.emit(event);
  }

  /**
   * Trigger sort data and paginate data
   */
  sortData() {
    if (this.sortBy) {
      this.data.sort(this.sortFn.bind(this));
      if (this.sortBy.order === 'dsc') {
        this.data.reverse();
      }
      this.paginateData();
    }
  }

  toArray(obj) {
    let arr = [];
    if (obj) {
      if (obj instanceof Array) {
        return obj;
      } else if (Object.keys(obj).length > -1) {
        // convert object to array
        arr = Object.keys(obj).map((key) => {
          return obj[key];
        });
        // remove empty
        arr = arr.filter((d) => { return !!d; });
      }
    }
    return arr;
  }

  /**
   * Set selected Card
   * @param i
   */
  clickToSelect(i) {
    if (this.clickable) {
      const index = this.paginatedData.indexOf(i);
      if (index > -1) {
        if (this.selectedData.page === this.page.pageIndex && this.selectedData.index === index) {
          this.selectedData.index = null;
          this.selectedData.page = null;
        } else {
          this.selectedData.page = this.page.pageIndex;
          this.selectedData.index = index;
          this.selected.emit(i);
        }
      }
    }
  }

  /**
   * Get selected Card
   * @param index
   */
  getSelected(index: number) {
    if (this.clickable) {
      return this.selectedData && this.page ? (this.selectedData.index === index && this.selectedData.page === this.page.pageIndex) : false;
    } else {
      return false;
    }
  }
}
