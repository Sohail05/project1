import { NgModule } from '@angular/core';
import { MdCardModule, MdIconModule, MdPaginatorModule, MdTooltipModule, MdSelectModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CardComponent } from './card.component';
import { MultiLinguaModule } from '../multi-lingua/index';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        MdCardModule,
        CommonModule,
        FlexLayoutModule,
        MdIconModule,
        MdPaginatorModule,
        MdTooltipModule,
        MultiLinguaModule,
        MdSelectModule,
        FormsModule,
    ],
    declarations: [
        CardComponent
    ],
    exports: [
        CardComponent
    ],
    providers: []
})
export class CardModule { }
