import { FormGroup } from '@angular/forms';
export interface IUIComponentData {
  key: string;
  value: any;
  masking: string;
  required?: boolean;
  disabled?: boolean;
  minlength?: number;
  placeholder?: string;
  validation?: { [key: string]: any };
  options?: any;
  multiple?: boolean;
  displayFn?: (value: any) => string;
  storeFn?: (value: any) => string;
  codeType?: string;
}
export interface IUIComponent {
  data: IUIComponentData;
  parent?: FormGroup;
}
