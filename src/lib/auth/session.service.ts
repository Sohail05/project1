import { Injectable } from '@angular/core';
import { JWT } from './auth';
import { Http } from '@angular/http';
import { environment } from 'environments/environment';

@Injectable()
export class SessionService {

  get lang() {
    return localStorage.getItem('lang');
  }
  set lang(val: string) {
    localStorage.setItem('lang', val);
  }

  get token() {
    return sessionStorage.getItem('token');
  }

  set token(val: string) {
    sessionStorage.setItem('token', val)
  }

  constructor(private http: Http) {}

  /**
   * Clear OAuth Session.
   */
  clear() {

    this.http.post(
      environment.authUrl +
      environment.oauth.logout.replace('{access_token}', this.token),
      {}
    ).subscribe()

    sessionStorage.clear();
  }

  /**
   * Set a session variable.
   * @param key
   * @param val
   */
  set(key: string, val: string) {
    sessionStorage.setItem(key, val);
  }

  /**
   * Get a session variable
   * @param key
   * @return {string}
   */
  get(key: string): string {
    return sessionStorage.getItem(key);
  }

  /**
   * Check token for session validity.
   * @return {boolean}
   */
  isValid(): boolean {

    if (!this.token) {
      return false;
    }

    try {
      return !(new JWT(this.token).isExpired())
    } catch (e) {
      console.warn(e);
      return false;
    }

  }

}
