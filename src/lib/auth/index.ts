import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth-guard';
import { MaterialModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SessionService } from './session.service';
import { LogoModule } from '../logo/index';
import { VersionBuildModule } from '../version-build/index';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LogoModule,
    HttpModule,
    VersionBuildModule
  ],
  exports: [AuthComponent, LoginComponent],
  declarations: [AuthComponent, LoginComponent],
})
export class AuthModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AuthModule,
            providers: [AuthGuard, SessionService],
        };
    }
}
