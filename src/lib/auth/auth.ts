import { URLSearchParams } from '@angular/http';

/**
 * Properties from the JWT payload
 */
export interface Payload {

    authorities: Array<string>;
    client_id: string;
    exp: Number;
    jti: string;
    scope: Array<string>;
    user_name: string;
}

/**
 * JWT token Object
 */
export class JWT {

    // JWT Token in plain string.
    token: string;

    constructor(token: string) {
        this.token = token;
    }

    /**
     * Extract header from token.
     */
    header() {
        return JSON.parse(atob(this.token.split('.')[0]));
    }

    /**
     * Check for expired token.
     * @return {boolean}
     */
    isExpired(): boolean {
        return this.payload().exp < Date.now() / 1000;
    }

    /**
     * Extract payload from token.
     */
    payload(): Payload {
        return JSON.parse(atob(this.token.split('.')[1])) as Payload;
    }

}

/**
 * OAuth reponse with implicit flow.
 */
export class OAuthResponse {

    access_token: string;
    token_type: string;
    expires_in: string;
    scope: string;
    authorities: string;
    jti: string;

    constructor(query: string) {
        const params = new URLSearchParams(query);
        this.access_token = params.get('access_token')
        this.token_type = params.get('token_type')
        this.expires_in = params.get('expires_in')
        this.scope = params.get('scope')
        this.authorities = params.get('authorities')
        this.jti = params.get('jti')
    }

}
