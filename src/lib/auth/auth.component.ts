import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Location } from '@angular/common';
import { OAuthResponse } from './auth';
import { SessionService } from './session.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  constructor(
    private route: ActivatedRoute,
    private session: SessionService,
    private router: Router
  ) {
    this.route.fragment.subscribe((hash) => {
      this.process(hash)
    });
  }


  /**
   * Set access token and navigate to requested page.
   */
  process(hash) {
    this.session.token = new OAuthResponse(hash).access_token;
    const returnUrl = this.session.get('returnUrl') || '/';
    console.log(returnUrl);

    this.router.navigateByUrl(returnUrl);
  }





}
