import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { SessionService } from '../session.service';
import { OAuthResponse } from '../auth';
import { Rect } from '../../core/rect';
import { ApiService } from '../../core/api/api.service';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  langSelect = 'EN';
  langs = [];

  constructor(
    private route: ActivatedRoute,
    private session: SessionService,
    private http: Http
  ) {}

  ngOnInit() {
    this.session.set('returnUrl', this.route.snapshot.queryParams['returnUrl'] || '/');
    this.loadLangs();
  }

  /**
   * Set selected lang and navigate to OAuth.
   */
  login() {
    this.session.lang = this.langSelect;

    window.location.href =
    environment.authUrl +
    environment.oauth.authorize
    .replace('{response_type}', 'token')
    .replace('{client_id}', environment.clientId)
    .replace('{redirect_uri}', window.location.href.replace('login', 'auth') );
  }

  /**
   * Load available languages from API.
   */
  private loadLangs() {
    this.http.get( environment.apiUrl + environment.api.langs).subscribe((response) => {
      this.langs = response.json()['data'];
    });
  }

}
