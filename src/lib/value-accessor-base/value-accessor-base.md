# Control Value Accessor  

This is a guide on how to utilize the `value-accessor-base.ts` class in order to progagate changes from a custom component's templates back to its `ngModel`.

This base class contains all the definitions of the Angular2's `ControlValueAccessor` interface.  

A custom component would simply extend this base class and provide a provider inside the component's class.

## Usage

Here we are using a custom component `<nt-multi-currency>` like so:

```html

<nt-multi-currency 
  name="multiCurrency" 
  [ngModel]="currencyDataModel">
</nt-multi-currency>

```
The `currencyDataModel` variable in this case contains `{ type: 'USD', amount: 12345 }`.  

Inside its component class `multi-currency.component.ts`:

```javascript
/*
* 'ValueAccessorBase' is the class that implements the ng2's 
*  ControlValueAccessor interface.
*  
* 'MakeProvider' is a factory function that simply returns the provider's 
*  recipe:
*/
import { MakeProvider, ValueAccessorBase } from './../value-accessor-base/value-accessor-base';

// Extends the ValueAccessorBase class and provides a generic type <T> 
export class MultiCurrencyComponent extends ValueAccessorBase<{ type: string, amount: number }> {
  @Component({
    selector: 'nt-multi-currency',
    templateUrl: './multi-currency.component.html',
    styleUrls: ['./multi-currency.component.scss'],
    providers: [MakeProvider(MultiCurrencyComponent)], // creates a provider
  })
  constructor() { 
    super(); // call super() to call the constructor of ValueAccessorBase
  }
}

```

Inside the template `multi-currency-component.html`:
```html

    <md-select
      [ngModel]="value?.type"
      (ngModelChange)="value.type = $event"
      name="type">
      <md-option></md-option>
    </md-select>
    <md-input-container>
      <input
        [ngModel]="value?.amount"
        (ngModelChange)="value.amount = $event"
        name="amount"
        mdInput>
    </md-input-container>

```
* The `value` variable name is **required** as the value of the `ngModel`. It is defined in the `ValueAccessorBase` and was created as a component variable during the `super()`.

* `[ngModel]=value.type` and `[ngModel]=value.amount` are first populated from the initial value of `currencyDataModel` variable above.  

* The corresponding `(ngModelChange)` is used to assign new input values and propagate them to the `[ngModel]` of the multi-currency `component`.  
