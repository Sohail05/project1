import { forwardRef, HostListener, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs/Subject';

export class ValueAccessorBase<T> implements ControlValueAccessor {

  /** Placeholder Label */
  @Input() placeholder = 'Placeholder';
  /** Tooltip Label */
  @Input() tooltip = '';
  /**
   * stored Type
   */
  private _innerValue: T;

  /** Component disabled state based on ng form API */
  _disabled;
  public valueObservable = new Subject<T>();

  propagateChange = (_: T) => { };

  /**
   * Get/Set to simulate form element
   */
  get value(): T {
    return this._innerValue;
  }

  set value(value: T) {
    if (this._innerValue !== value) {
      this._innerValue = value;
      this.propagateChange(value);
    }
  }

  /**
   * Methods inherited from ControlValueAccessor.
   */
  writeValue(value: T) {
    if (value) {
      this._innerValue = value;
      this.valueObservable.next(value);
    }
  }

  registerOnChange(fn: (value: T) => void) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => void) { }


  setDisabledState(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

}

/**
 * Required to produce an template type.
 * e.g. Provider[MakeProvider(component)]
 */
export function MakeProvider(type: any) {
  return {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => type),
    multi: true,
  };
}
