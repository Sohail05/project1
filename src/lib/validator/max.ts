import { Directive, Input, forwardRef } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[ntMax][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => MaxValidatorDirective), multi: true }
    ]
})

export class MaxValidatorDirective implements Validator {
    @Input() ntMax: string | number
    validate(c: AbstractControl): { [key: string]: any; } {
        const max = typeof this.ntMax === 'number' ? this.ntMax : parseFloat(this.ntMax);
        const value = typeof c.value === 'number' ? c.value : parseFloat(c.value);
        return max ? value <= max ? null : { max: max } : null;
    }
}
