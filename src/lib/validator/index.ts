import { NgModule } from '@angular/core';
import { MinValidatorDirective } from './min';
import { MaxValidatorDirective } from './max';
import { RangeValidatorDirective } from './range';

@NgModule({
    imports: [],
    declarations: [MinValidatorDirective, MaxValidatorDirective, RangeValidatorDirective],
    exports: [MinValidatorDirective, MaxValidatorDirective, RangeValidatorDirective],
    providers: []
})
export class ValidatorModule { }
