import { Directive, Input, forwardRef } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[range][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => RangeValidatorDirective), multi: true }
    ]
})

export class RangeValidatorDirective implements Validator {
    @Input() min: string | number
    @Input() max: string | number
    validate(c: AbstractControl): { [key: string]: any; } {
        const max = typeof this.max === 'number' ? this.max : parseFloat(this.max);
        const min = typeof this.min === 'number' ? this.min : parseFloat(this.min);
        const value = typeof c.value === 'number' ? c.value : parseFloat(c.value);
        return (value <= max || value >= min) ? null : { range: { max: max, min: min } };
    }
}
