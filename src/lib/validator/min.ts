import { Directive, Input, forwardRef } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[ntMin][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => MinValidatorDirective), multi: true }
    ]
})

export class MinValidatorDirective implements Validator {
    @Input() ntMin: string | number;
    validate(c: AbstractControl): { [key: string]: any; } {
        const min = typeof this.ntMin === 'number' ? this.ntMin : parseFloat(this.ntMin);
        const value = typeof c.value === 'number' ? c.value : parseFloat(c.value);
        return min ? value >= min ? null : { min: min } : null;
    }
}
