import { NgModule } from '@angular/core';

import { ProgressBarComponent } from './progress-bar.component';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [MaterialModule, FormsModule],
    exports: [ProgressBarComponent],
    declarations: [ProgressBarComponent],
    providers: [],
})
export class ProgressBarModule { }
