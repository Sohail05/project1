# Progress Bar
Progress bar allow user for indicating the process and activities.
## **\<nt-progress-bar\>**

## Example

```html
<nt-progress-bar 
    [mode]="'determinate'" 
    [value]="0">
</nt-progress-bar>
```
## API References

|Name|Description|
|----|-----------|
|@Input()<br>mode|_**`string`**_<br>Mode of the progress bar.<br>Here are the available values: determinate, indeterminate, buffer, query and defaults.|
|@Input()<br>value|_**`array`**_<br>Value of the progress bar.<br>The value started from 0.|
|completed|_**`boolean`**_<br>`True` when the progress value completed.|
