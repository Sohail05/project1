import { Component, Input, OnInit } from '@angular/core';
import { MdProgressBarModule } from '@angular/material';

@Component({
  selector: 'nt-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss'],
})
export class ProgressBarComponent implements OnInit {
  @Input() mode = '';
  @Input() value: any[] = [];
  completed = 'false';
  constructor() { }

  ngOnInit() {
  }

  checkComplete(e) {
    if (e === 100) {
      this.completed = 'true';
    } else {
      this.completed = 'false';
    }
  }

}
