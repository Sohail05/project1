import * as d3 from 'd3';

export default (() => {
  let counter = 0;
  let treeMap;
  let root;
  let svg;
  let callerNode;
  let width: number;
  let height: number;
  let rectWidth: number;
  let rectHeight: number;
  let chartOptions;
  let nodeContentModel;
  let xLevels = [];
  let yLevels = 0;
  const margin = { top: 45, right: 50, bottom: 30, left: 50 };
  const defaultOptions = {
    id: '',
    data: {},
    nodeContentModel: {},
    rectWidth: 180,
    rectHeight: 150,
    duration: 650,
    childPerRow: 5,
    fixedDepth: 200,
    auth: false,
  };
  const css = {
    maskOpacity: 0.1,
  };

  function convertFlatToNestedTree(data) {

    if (!Array.isArray(data)) {
      return [];
    }

    const dataMap = data.reduce((map, node) => {
      map[node[chartOptions.parentKeyIdentifier]] = node;
      return map;
    }, {});

    // create the tree array
    const treeData = [];

    data.forEach((node) => {
      // add to parent
      const parent = dataMap[node[chartOptions.parentKeyIdentifierInNode]];
      if (parent) {
        // create child array if it doesn't exist
        (parent.children || (parent.children = []))
          // add node to child array
          .push(node);
      } else {
        // parent is null or missing
        treeData.push(node);
      }
    });

    return treeData;
  }
  function computeLevels(level, n) {
    if (n.children && n.children.length > 0) {
      yLevels += 1;
      if (xLevels.length <= level + 1) { xLevels.push(0); }
      xLevels[level + 1] += n.children.length;
      n.children.forEach((d) => {
        computeLevels(level + 1, d);
      });
    }
  }
  function init(options) {

    chartOptions = Object.assign(defaultOptions, options);
    nodeContentModel = chartOptions.nodeContentModel;

    rectWidth = chartOptions.rectWidth;
    rectHeight = chartOptions.rectHeight;

    chartOptions.data = convertFlatToNestedTree(chartOptions.data);

    // todo add proper "No data" handler
    if (!chartOptions.data.length) {
      return;
    }

    root = d3.hierarchy(chartOptions.data[0], (d) => d.children);

    xLevels = [0];
    yLevels = 1;
    computeLevels(0, root);

    width = d3.max(xLevels) * (rectWidth * 1.7);
    height = yLevels * (rectHeight * 1.7);

    svg = d3.select(chartOptions.id)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      // .call(d3.zoom().on('zoom', () => {
      //   svg.attr('transform', d3.event.transform);
      // }))
      .append('g');

    svg.attr('transform', `translate(-${margin.right}, ${margin.top})`);
    height = height - margin.top - margin.bottom - rectHeight;
    treeMap = d3.tree().size([width, height]);
    // assigns the data to a hierarchy using parent-child relationships
    root.x0 = height / 2;
    root.y0 = 0;
    // Collapse after the second level
    // root.children.forEach(collapse);
    update(root);
  }
  // Collapse the node and all it's children
  function collapse(d) {
    if (d.children) {
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;
    }
  }
  // Toggle children on click.
  function nodeClick(node) {
    if (node.id === 1 || (!node.descendantIds || !node.descendantIds.length)) { return; }
    node.inFocus = !node.inFocus;
    if (node.children) {
      node._children = node.children;
      node.children = null;
      callerNode = node;
    } else {
      node.children = node._children;
      node._children = null;
      callerNode = node;
    }
    if (node.inFocus) {
      svg.selectAll('.node').filter((d) => {
        if (node.id === d.id || node.parent.id === d.id || node.descendantIds.indexOf(d.id) !== -1) {
          return false;
        } else {
          return true;
        }
      }).transition()
        .style('opacity', css.maskOpacity);

      svg.selectAll('.d3-line').filter((d) => {
        if (node.id === d.target.id || node.descendantIds.indexOf(d.target.id) !== -1) {
          return false;
        } else {
          return true;
        }
      }).transition()
        .style('opacity', css.maskOpacity);
    } else {
      svg.selectAll('.node')
        .transition()
        .style('opacity', 1);

      svg.selectAll('.d3-line')
        .transition()
        .style('opacity', 1);
    }
    // update(d);
  }
  const lineFunction = d3.line()
    .x((d: any) => d.x)
    .y((d: any) => d.y);

  function formatNodeContent(d) {
    const dataObj = d.data;
    const text = d3.select(this);

    for (const key of Object.keys(nodeContentModel)) {
      if (!dataObj[key] || nodeContentModel.delimited.indexOf(key) !== -1) {
        continue;
      }

      const textSpanConfig = nodeContentModel[key];

      let delimitedValues;
      if (textSpanConfig.delimiter) {
        delimitedValues = [];
        for (const delimited of textSpanConfig.delimiter) {
          delimitedValues.push(dataObj[delimited]);
        }
      }

      const tSpan = text.append('tspan')
        .attr('x', textSpanConfig.x)
        .attr('y', textSpanConfig.y)
        .attr('class', key)
        .attr('text-anchor', textSpanConfig.anchor)
        .style('text-transform', textSpanConfig.textTransform)
        .style('font-size', textSpanConfig.fontSize)
        .style('font-weight', textSpanConfig.fontWeight)
        .text(textSpanConfig.delimiter ? [dataObj[key], ...delimitedValues].join(' \u00B7 ') : dataObj[key])
        .each(wrap(rectWidth - 50, 10, textSpanConfig.linesBreak));

      if (textSpanConfig.label) {
        const label = textSpanConfig.label;
        text.append('tspan')
          .attr('x', label.x)
          .attr('y', label.y)
          .attr('text-anchor', 'end')
          .style('font-size', '10')
          .style('font-weight', '700')
          .text(label.name);
      }
      if (textSpanConfig.rectBg) {
        const tSpanDimension = (<any>tSpan.node()).getBBox();

        d3.select(text.node().parentNode).insert('rect', '.node-text')
          .attr('x', tSpanDimension.x - 3 / 2)
          .attr('y', tSpanDimension.y - 2 / 2)
          .attr('rx', 3)
          .attr('ry', 3)
          .attr('width', tSpanDimension.width + 3)
          .attr('height', tSpanDimension.height + 2)
          .style('fill', textSpanConfig.rectBgFill);
      }
    }
  }

  function wrap(xwidth, padding, linesBreak) {
    return  () => {
      const self = d3.select(this);
      const textLenPixel = self.node().getComputedTextLength();

      if (textLenPixel <= xwidth) {
        return;
      }

      const charBreakArr = [];
      const fullText = self.text();
      const textSplit = fullText.split(' ');
      let strConcat = '';
      let subStrLen;

      for (let i = 0; i < textSplit.length; i++) {
        strConcat += textSplit[i] + ' ';
        self.text(strConcat);
        subStrLen = self.node().getComputedTextLength();
        if (subStrLen > xwidth - 2 * padding || i === textSplit.length - 1) {
          charBreakArr.push(strConcat);
          strConcat = '';
        }
      }

      self.text(charBreakArr[0]);

      for (let i = 1; i < linesBreak; i++) {
        if (!charBreakArr[i]) {
          continue;
        }
        self.append('tspan')
          .attr('x', rectWidth / 2)
          .attr('dy', '1.2em')
          .attr('text-anchor', 'middle')
          .text(i === linesBreak - 1 && charBreakArr[linesBreak] ? charBreakArr[i] + '...' : charBreakArr[i]);
      }
      if (charBreakArr[linesBreak]) {
        self.append('title')
          .text(fullText);
      }
    };
  }
  function breakChildren(nodes, nodesWithRoot) {
    nodes.forEach((d) => {
      d.x1 = d.x;
      d.id = d.id || ++counter;
    });
    let breakDepth = 0;
    // Normalize for fixed-depth.
    nodes.forEach((d, index) => {
      const lineNumber = index % chartOptions.childPerRow;
      const ids = [];
      if (lineNumber === 0) {
        breakDepth++;
      }
      d.descendantIds = getDescendants(d, ids);
      d.x = nodes[lineNumber].x1 + (lineNumber * rectWidth / 1.5);
      d.y = (Math.min(1.3, d.depth) + breakDepth - 0.5) * chartOptions.fixedDepth;
      d.x0 = d.x;
      d.y0 = d.y;
    });

    nodesWithRoot[0].x = nodes[chartOptions.childPerRow - 1].x - ((nodes[chartOptions.childPerRow - 1].x - nodes[0].x) / 2);
    nodes.unshift(nodesWithRoot[0]);
  }

  function getDescendants(node, ids) {
    if (!node.children) {
      return null;
    }
    node.children.forEach((d) => {
      getDescendants(d, ids);
      ids.push(d.id);
    });
    return ids;
  }

  function update(source) {
    const parentTargetLinks = root.links();
    // maps the node data to the tree layout
    const treeData = treeMap(root);
    const nodes = treeData.descendants();
    // let nodes = nodesWithRoot.slice(1);
    nodes.forEach((d) => {
      // d.x1 = d.x;
      d.x0 = d.x;
      d.y0 = d.y;
      d.id = d.id || ++counter;
    });
    nodes.forEach((d) => {
      const ids = [];
      d.descendantIds = getDescendants(d, ids);
    });
    // breakChildren(nodes, nodesWithRoot);
    // adds each node as a group
    const node = svg.selectAll('g.node')
      .data(nodes);

    const nodeEnter = node.enter()
      .append('g')
      .attr('class', 'node')
      .attr('transform', (d) => `translate(${source.x0}, ${source.y0} )`)
      .on('click', nodeClick)
      .on('mouseover', (d) => {
        d3.select(this).select('.editBtn').style('visibility', 'visible');
        d3.select(this).select('.delBtn').style('visibility', 'visible');
      })
      .on('mouseout', (d) => {
        d3.select(this).select('.editBtn').style('visibility', 'hidden');
        d3.select(this).select('.delBtn').style('visibility', 'hidden');
      });

    nodeEnter.append('rect')
      .attr('width', rectWidth)
      .attr('height', rectHeight)
      .attr('id', (d) => d.id)
      // .attr('fill', function (d)
      // { return (d.children || d._children || d.hasChild) ? 'url(#gradientchilds)' : 'url(#gradientnochilds)'; })
      .style('cursor', (d) => (d.children || d._children) ? 'pointer' : 'default')
      .attr('class', 'd3-box');

    if (nodeContentModel.line) {
      nodeEnter.insert('path', 'text.node-text')
        .attr('d', `${nodeContentModel.line.startPoint} ${nodeContentModel.line.drawTill}`)
        .attr('stroke', 'black');
    }

    if (chartOptions.auth) {
      nodeEnter.append('text')
        .attr('x', rectWidth - 20)
        .attr('y', 0)
        .attr('dy', '1.2em')
        .attr('font-family', 'Material Icons')
        .attr('font-size', '0.8em')
        .attr('text-anchor', 'end')
        .attr('class', 'editBtn')
        .style('visibility', 'hidden')
        .style('cursor', 'pointer')
        .text('edit');

      nodeEnter.append('text')
        .attr('x', rectWidth - 2)
        .attr('y', 0)
        .attr('dy', '1.2em')
        .attr('font-family', 'Material Icons')
        .attr('font-size', '0.8em')
        .attr('text-anchor', 'end')
        .attr('class', 'delBtn')
        .style('visibility', 'hidden')
        .style('cursor', 'pointer')
        .text('delete');
    }

    const nodeText = nodeEnter.append('text')
      .attr('x', rectWidth / 2)
      .attr('y', 0)
      .attr('text-anchor', 'middle')
      .attr('class', 'node-text')
      // .style('cursor', (d) => (d.children || d._children) ? 'pointer' : 'default')
      .each(formatNodeContent);

    // arrow-head to line connection
    svg.append('defs').append('marker')
      .attr('id', 'arrow-head')
      .attr('viewBox', '0 0 10 10')
      .attr('refX', 9)
      .attr('refY', 5)
      .attr('markerWidth', 4)
      .attr('markerHeight', 4)
      .attr('orient', 'auto')
      .append('path')
      .attr('d', 'M 0 0 L 10 5 L 0 10 z')
      .attr('fill', '#757575');

    const nodeUpdate = nodeEnter.merge(node);
    // Transition nodes to their new position.
    nodeUpdate.transition()
      .duration(chartOptions.duration)
      .attr('transform', (d) => `translate(${d.x}, ${d.y})`);
    // Remove any exiting nodes
    const nodeExit = node.exit()
      .transition()
      .duration(chartOptions.duration)
      .attr('transform', (d) => `translate(${source.x}, ${source.y})`)
      .remove();
    // On exit reduce the opacity of text labels
    nodeExit.select('text')
      .style('fill-opacity', 1e-6);
    // Update the links
    const link = svg.selectAll('path.d3-line')
      .data(parentTargetLinks, (d) => d.target.id);
    // Enter any new links at the parent's previous position
    const linkEnter = link.enter()
      .insert('path', 'g')
      .attr('class', 'd3-line')
      .attr('d', (d) => {
        const u_line: any = ((data) => {
          const u_linedata = [
            {
              x: data.source.x0 + Math.floor(rectWidth / 2),
              y: data.source.y0 + rectHeight + 2,
            },
            {
              x: data.source.x0 + Math.floor(rectWidth / 2),
              y: data.source.y0 + rectHeight + 2,
            },
            {
              x: data.source.x0 + Math.floor(rectWidth / 2),
              y: data.source.y0 + rectHeight + 2,
            },
            {
              x: data.source.x0 + Math.floor(rectWidth / 2),
              y: data.source.y0 + rectHeight + 2,
            },
          ];
          return u_linedata;
        })(d);
        return lineFunction(u_line);
      })
      .attr('marker-end', 'url(#arrow-head)');
    // Update
    const linkUpdate = linkEnter.merge(link);
    // Transition back to the parent element position
    linkUpdate.transition()
      .duration(chartOptions.duration)
      .attr('d', (d) => {
        const u_line: any = ((data) => {
          const u_linedata = [
            {
              x: data.source.x + Math.floor(rectWidth / 2),
              y: data.source.y + rectHeight,
            },
            {
              x: data.source.x + Math.floor(rectWidth / 2),
              y: data.target.y - margin.top / 2,
            },
            {
              x: data.target.x + Math.floor(rectWidth / 2),
              y: data.target.y - margin.top / 2,
            },
            {
              x: data.target.x + Math.floor(rectWidth / 2),
              y: data.target.y,
            },
          ];
          return u_linedata;
        })(d);
        return lineFunction(u_line);
      });
    // Remove any exiting links
    const linkExit = link.exit()
      .transition()
      .duration(chartOptions.duration)
      .attr('d', (d) => {
        /* This is needed to draw the lines right back to the caller */
        const u_line: any = ((data) => {
          const u_linedata = [
            {
              x: callerNode.x + Math.floor(rectWidth / 2),
              y: callerNode.y + rectHeight + 2,
            },
            {
              x: callerNode.x + Math.floor(rectWidth / 2),
              y: callerNode.y + rectHeight + 2,
            },
            {
              x: callerNode.x + Math.floor(rectWidth / 2),
              y: callerNode.y + rectHeight + 2,
            },
            {
              x: callerNode.x + Math.floor(rectWidth / 2),
              y: callerNode.y + rectHeight + 2,
            },
          ];
          return u_linedata;
        })(d);
        return lineFunction(u_line);
      })
      .remove();

  }

  return { init };

})();
