import { Component, OnInit, Input } from '@angular/core';
import orgChart from './organization-chart';

@Component({
  selector: 'nt-org-chart',
  templateUrl: './org-chart.component.html',
  styleUrls: ['./org-chart.component.scss']
})
export class OrgChartComponent implements OnInit {
  @Input() config;
  @Input() data = [];
  @Input() nodeConfig;
  private cConfig = {
    id: '#d3-orgchart-container',
    rectWidth: 200,
    rectHeight: 110,
    childPerRow: 4,
    auth: false,
    parentKeyIdentifier: 'fullNm',
    parentKeyIdentifierInNode: 'parent'
  };
  constructor() { }

  ngOnInit() {
    if (this.config) {
      Object.assign(this.cConfig, this.config);
    }
    if (this.data) {
      // deep clone data ;
      this.cConfig['data'] = JSON.parse(JSON.stringify(this.data));
    }

    if (this.nodeConfig) {
      this.cConfig['nodeContentModel'] = this.nodeConfig;
    }
    orgChart.init(this.cConfig);
  }

}
