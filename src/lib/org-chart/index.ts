import { NgModule } from '@angular/core';
import { OrgChartComponent } from './org-chart.component';

@NgModule({
  imports: [],
  exports: [OrgChartComponent],
  declarations: [ OrgChartComponent ],
  providers: [],
})
export class OrgChartModule { }
