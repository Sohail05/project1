import { Component, Input, OnInit, Host, Optional } from '@angular/core';
import { FormControl, NgModel, FormGroup, FormGroupDirective, NgForm } from '@angular/forms';
import { ValidationService } from './../core/validation.service';

@Component({
  selector: 'nt-control-message',
  templateUrl: './control-message.component.html',
  styleUrls: ['./control-message.component.scss'],
})
export class ControlMessageComponent {
  // @Input() parent: FormGroup;
  // to be remove
  @Input() control: FormControl;
  @Input() tooltip: string;
  @Input() fieldName: string;
  @Input() showRequired: boolean;
  @Input() showTooltip: boolean;
  @Input() type; // todo remove this
  @Input() submitted: boolean;
  @Input() errors: object;

  constructor(private validate: ValidationService) { }

  get errorMessage() {
    if (!this.errors) {
      return null;
    }

    if (this.submitted) {
      const firstErrorName = Object.keys(this.errors)[0];
      return this.validate.getValidatorErrorMessage(firstErrorName, this.fieldName, this.errors[firstErrorName]);
    }
  }

}
