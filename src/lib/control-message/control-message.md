# Input Validation

## \<nt-control-message>
<br>
The component `nt-control-message` is used to validate a particular input element's value, and shows custom error message if the value is invalid. 

This guide will show you how to use the validation component, as well as creating a custom validator if desired.
***
## Usage

````html
<div>
  <md-input-container>
    <input
      mdInput
      required="true"
      minlength="20"
      type="text"
      #control="ngModel">
  </md-input-container>
  <nt-control-message 
    [type]="type" 
    [control]="control">
  </nt-control-message>
<div>
````

The template variable `#control` has the value `"ngModel"` (always `ngModel`) which gives you a reference to the Angular `NgModel` directive associated with this control that you can use in the template to check for control states such as `valid` and `dirty`.

The `type` is a component's variable that stores the value of a custom input's type e.g `currency`, `email`.

The `#control` and `type` variables are then passed to the corresponding `@Input` in the `<nt-control-message>` component.
***
## Create a Custom Validator (Edit 6/14/2017: DEPRECATED)

Custom validation is normally created as a `directive` which is then used on an input element itself.

We will learn how by looking at the email validator directive:

```javascript
import { Directive, forwardRef } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, Validators } from '@angular/forms';
import { ValidationService } from './../core/validation.service';

@Directive({
  // use this name as an attribute in an input you wish to validate
  selector: '[validateEmail]', 
  // extends ng2's NG_VALIDATORS service with this class to perform the validation 
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => EmailValidatorDirective),
    multi: true,
  }],
})
export class EmailValidatorDirective implements Validator {

  private valFn = Validators.nullValidator;
  
  constructor(private validateService: ValidationService) {
    // Get the validator function that will perform validation on the host input in the validate() func below.
    this.valFn = validateService.getCustomValidator('email');
  }
  validate(control: AbstractControl): { [key: string]: any } {
    // 'control' will be the input control that this directive is attached to
    return this.valFn(control);
  }
}
```
By default, Angular will call the `validate` method to perform the validation.
If the value is invalid, calling the `valFn` function will return an error object, otherwise `null` is returned by convention. Hopefully all of this becomes clearer if we look at the body of `getCustomValidator(type)` method in the `validationService`:

```javascript 
// validation.service.ts

getCustomValidator(type): ValidatorFn {
    const re = new RegExp(validationRegEx[type], 'i');
    // returns this validator function to be called in the `validate` method later
    return (control: AbstractControl): { [key: string]: any } => {
      const no = re.test(control.value);  // validity check with a email's regex 
      return no ? null : { email: { type } };  // if valid, returns null
    };
  }
```

Now simply apply the email validator's directive name `validateEmail` as one of the attributes in a control element:

```html
<div>
  <md-input-container>
    <input
      mdInput        
      validateEmail 
      type="text"
      #control="ngModel">
  </md-input-container>
  <nt-control-message 
    [type]="type" 
    [control]="control">
  </nt-control-message>
<div>
```
Angular2 will store any default HTML5 validation errors(e.g. `required`, `minlength`) in the `errors` property of a `FormControl` instance, which in this case is our `#control`(_i.e._ `control.errors`). This applies to custom validation errors as well - If the entered email is invalid, the object `{ email: { type } }` will be merged to the `control.errors`. And if you were to now loop the errors object, you would find the `email` property containing an object `{ type }`.   







