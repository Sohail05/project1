import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ControlMessageComponent } from './control-message.component';

@NgModule({
    imports: [CommonModule],
    exports: [ControlMessageComponent],
    declarations: [ControlMessageComponent],
    providers: [],
})
export class ControlMessageModule { }
