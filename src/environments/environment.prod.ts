import { AntelopeEnvironment } from '../lib/core/antelope-interfaces';
import { API } from '../api/api';
import { OAuth } from '../api/oauth';
export const environment: AntelopeEnvironment = {
  production: true,
  version: '',
  build: '',
  env: 'prod',
  applicationName: 'Antelope',
  applicationId: 'ANTELOPE',
  clientId: 'aquila-imp',
  logo: 'assets/generic-logo-text.png',
  apiUrl: '',
  authUrl: '',
  iconUrl: 'assets/icn_set.svg',
  loginRoute: '/login',
  api: API,
  phone: {
    no: '+60',
    maxLength: 8
  },
  oauth: OAuth
};
