import { AntelopeEnvironment } from '../lib/core/antelope-interfaces';
import { API } from '../api/api';
import { OAuth } from '../api/oauth';
export const environment: AntelopeEnvironment = {
  production: false,
  version: '',
  build: '',
  env: 'Dev',
  applicationName: 'Antelope',
  applicationId: 'ANTELOPE',
  clientId: 'aquila-imp',
  logo: 'assets/generic-logo-text.png',
  apiUrl: 'http://poseidon:8796/rest/api/private',
  authUrl: 'http://poseidon:8896/sec',
  iconUrl: 'assets/icn_set.svg',
  loginRoute: '/login',
  api: API,
  phone: {
    no: '+60',
    maxLength: 8
  },
  oauth: OAuth
};
