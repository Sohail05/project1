import { API } from '../api/api';
import { OAuth } from '../api/oauth';
import { AntelopeEnvironment } from '../lib/core/antelope-interfaces';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment: AntelopeEnvironment = {
  production: false,
  version: '',
  build: '',
  env: 'dev',
  applicationName: 'Antelope',
  applicationId: 'ANTELOPE',
  clientId: 'aquila-imp',
  logo: 'assets/generic-logo-text.png',
  apiUrl: 'http://poseidon:8796/rest/api',
  authUrl: 'http://poseidon:8896/sec',
  iconUrl: 'assets/icn_set.svg',
  loginRoute: '/login',
  api: API,
  phone: {
    no: '+60',
    maxLength: 8
  },
  oauth: OAuth
};
