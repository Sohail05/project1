import { AntelopePage } from './app.po';

describe('Antelope App', () => {
  let page: AntelopePage;

  beforeEach(() => {
    page = new AntelopePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
