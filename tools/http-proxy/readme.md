#### Start Test Server:


1. Edit app.js, change the target for oauthTokenProxy and restApiProxy:
```
var oauthTokenProxy = proxy('/sec/oauth/**', {target: 'http://localhost:8187'});
var restApiProxy = proxy('/rest/api/**', {target: 'http://localhost:8188'});
```
2. Edit /views/test.html, change the value for variable oath2AuthorisationServer:
```
var oath2AuthorisationServer = 'http://localhost:8187';
```
3. open command line, enter:
```
npm start
```
4. Open browser, go to http://localhost:3000/test



