var env = [
    {path:'/sec/oauth/**', config:{target: 'http://poseidon:8896'}},
    {path:'/rest/api/**', config:{target: 'http://poseidon:8796'}},
    {path:'/app/**', config:{target: 'http://localhost:4200'}},
];

exports.env = env;