var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path    = require("path");
var proxy = require('http-proxy-middleware');
var env = require('./env'); 

//var index = require('./routes/index');
//var users = require('./routes/users');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Get path and configurations from env.js
 */
env.env.forEach((e) => {
    app.use(proxy(e.path, e.config));
});

app.get('/test',function(req,res){
   res.sendFile(path.join(__dirname+'/views/test.html'));

});

app.get('/oauthCallback',function(req,res){
   res.sendFile(path.join(__dirname+'/views/oauthCallback.html'));

});

module.exports = app;
