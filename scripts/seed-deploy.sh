#!/usr/bin/env sh
# Run this script to deploy the project skeleton to Antelope seed branch.

# Exit if any subcommand fails.
set -e

echo "Started deploying"

# Checkout seed branch.
if [ `git branch | grep seed` ]
then
  git branch -D seed
fi
git checkout -b seed

# Delete and move files.
rm -rf src/lib
git submodule init
git submodule update --recursive 
git submodule foreach 'git fetch origin; git checkout $(git describe --tags `git rev-list --tags --max-count=1`);'

# Push to seed.
git add -fA
git commit --allow-empty -m "$(git log -1 --pretty=%B) [ci skip]"
git push -f -q origin seed

# Move back to previous branch.
git checkout -

echo "Deployed Successfully!"

exit 0