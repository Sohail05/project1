#!/bin/bash

set -e

echo "Started deploy"

# Get Version
version=$1

# Get Enviroment
env=$2

# todo: Remove hardcoded path
deployPath=/opt/AQUILA/www/antelope
repoPath=/opt/AQUILA/repo/antelope

echo 'Version:' + $version + ' Environment: ' $env

cd $repoPath
git reset --hard
git pull
npm install
node scripts/version-update.js $version $env
npm run build:$env
cp -R dist/* $deployPath

echo "Deployed Successfully!"

exit 0