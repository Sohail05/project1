#!/usr/bin/env sh
# Run this script to deploy the lib directory to Antelope lib branch.

# Exit if any subcommand fails.
set -e

echo "Started deploying"

# Checkout lib branch.
if [ `git branch | grep lib` ]
then
  git branch -D lib
fi
git checkout -b lib

# Compile Typescript to javascript.
tsc -p src/tsconfig.json

# Include latest tag as version
npm version $(git describe --tags $(git rev-list --tags --max-count=1))

# Delete and move files.
find . -maxdepth 1 ! -name 'src' ! -name '.git' ! -name '.gitignore' -exec rm -rf {} \;
mv src/lib/* .
rm -R src/

# Commit & push to remote origin.
git add -fA
git commit --allow-empty -m "$(git log -1 --pretty=%B) [ci skip]"
git push -f -q origin lib

# Move back to previous branch.
git checkout -

echo "Deployed Successfully!"

exit 0