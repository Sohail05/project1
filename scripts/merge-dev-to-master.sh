#!/usr/bin/env sh
# Run this script to merge the dev branch to master.

# Exit if any subcommand fails.
set -e

echo "Started Merging dev to master"

# Switch to dev to avoid conflicts and maintain master clean
git checkout dev
# Merge master to dev
git merge master

# Switch to master
git checkout master
# Merge dev up tp master
git merge dev

echo "Merged Successfully!"

exit 0