var replace = require('replace-in-file');
var package = require("../package.json");
var revision = require('child_process')
  .execSync('git rev-parse HEAD')
  .toString().trim();

var version = process.argv[2] || package.version;
var env = process.argv[3] || 'stage';

const options = {    
  files: 'src/environments/environment.' + env.toLowerCase() + '.ts',
  from: [/version: '(.*)'/g, /build: '(.*)'/g],
  to: ["version: '" + version + "'", "build: '" + revision + "'"],
  allowEmptyPaths: false,
}; 

try {    
  let changedFiles = replace.sync(options);     
  console.log('Version No: ' + version);
  console.log('Build No :' + revision);
} catch (error) {    
  console.error('Error occurred:', error);    
  throw error
}
